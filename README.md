# Captive Interactive App

This is the repo to make UI and redirect API

## Setup

Clone this source.

Go to the source folder

_Install_

```
npm install
```

<br/>

_Build_

```
npm run build
```

<br/>

_Start_

Fisrt to edit the .env to link API and Sentry DSN

Then start

```
npm start
```
