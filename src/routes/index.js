/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

import HomePage from "views/pages/Home";
import ProductsPage from "views/pages/Products";
import CreateProduct from "views/pages/CreateProduct";
import DetailProduct from "views/pages/DetailProduct";
import Marketplace from "views/pages/Marketplace";
import CreatePasswordPage from "views/pages/CreatePassword";
import SignInPage from "views/pages/SignIn";
import ResetPasswordPage from "views/pages/ResetPassword";
import ResetPasswordSuccessPage from "views/pages/ResetPasswordSuccess";
import SignUpPage from "views/pages/SignUp";
import HostPage from "views/pages/Host";
import ImportHostList from "views/pages/ImportHost";
import SignUpHost from "views/pages/SignUpHost";
import NotFoundPage from "views/pages/NotFound";
import Thanks from "views/pages/Thanks";
import FlowSignUp from "views/pages/FlowSignUp";
import VerifyEmail from "views/pages/VerifyEmail";
import HostProfile from "views/pages/HostProfile";
import Company from "views/pages/Company";
import ResetPasswordHost from "views/pages/ResetPasswordHost";
import CompanyOnboarding from "views/pages/CompanyOnboarding";
import SignUpCompany from "views/pages/SignUpCompany";
import RecommenderSeller from "views/pages/RecommenderSeller";
import SelectRole from "views/pages/SelectRole";
import MyJobs from "views/pages/MyJobs";
import DetailCampaign from "views/pages/DetailProduct/components/DetailCampaign";
import CampaignList from "views/pages/CampaignList";
import FeaturedProducts from "views/pages/FeaturedProducts";
import CampaignDetail from "views/pages/CampaignDetail";
import JobOrder from "views/pages/JobOrder/JobOrder";

import { constants } from "helpers";
import AllIcons from "views/pages/AllIcons";
import Payment from "views/pages/Payment/index";

const { router } = constants;

const routerArray = [
  { path: router.home, component: HomePage }, // TO DO - Need to show component SignIn
  { path: router.products, component: ProductsPage },
  { path: router.createProduct, component: CreateProduct },
  { path: router.detailProduct, component: DetailProduct },
  { path: router.editProduct, component: CreateProduct },
  { path: router.viewCampaign, component: DetailCampaign },
  { path: router.marketPlace, component: Marketplace },
  { path: router.createPassword, component: CreatePasswordPage },
  { path: router.resetPassword, component: ResetPasswordPage },
  { path: router.resetPasswordSuccess, component: ResetPasswordSuccessPage },
  { path: router.signUp, component: SignUpPage },
  { path: router.signUpCompany, component: SignUpCompany },
  { path: router.signIn, component: SignInPage },
  { path: router.selectRole, component: SelectRole },
  { path: router.hostList, component: HostPage },
  { path: router.importHost, component: ImportHostList },
  { path: router.signUpHost, component: SignUpHost },
  { path: router.thanks, component: Thanks },
  { path: router.flowSignUp, component: FlowSignUp },
  { path: router.verifyEmail, component: VerifyEmail },
  { path: router.hostProfile, component: HostProfile },
  { path: router.companyProfile, component: Company },
  { path: router.resetPasswordHost, component: ResetPasswordHost },
  { path: router.companyOnboarding, component: CompanyOnboarding },
  { path: router.recommenderSeller, component: RecommenderSeller },
  { path: router.myJobs, component: MyJobs },
  { path: router.campaigns, component: CampaignList },
  { path: router.featureProducts, component: FeaturedProducts },
  { path: router.campaignDetail, component: CampaignDetail },
  { path: router.payment, component: Payment },
  { path: router.jobOrder, component: JobOrder },
  { path: router.allIcons, component: AllIcons },
  { path: router.notFound, component: NotFoundPage }, // should be put at the last of array
  { path: router.orders, component: DetailCampaign },
];

export default routerArray;
