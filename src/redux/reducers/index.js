/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import login from "views/pages/SignIn/reducer";
import signUp from "views/pages/SignUpHost/reducer";
import createPassword from "views/pages/CreatePassword/reducer";
import hostList from "views/pages/Host/reducer";
import hostProfile from "views/pages/HostProfile/reducer";
import company from "views/pages/Company/reducer";
import products from "views/pages/Products/reducer";
import createProduct from "views/pages/CreateProduct/reducer";
import detailProduct from "views/pages/DetailProduct/reducer";
import recommenderSeller from "views/pages/RecommenderSeller/reducer";
import jobReducer from "views/pages/MyJobs/reducer";
import paymentReducer from "views/pages/Payment/reducer";
import campaign from "views/pages/DetailProduct/components/reducer";
import marketplace from "views/pages/Marketplace/reducer";
import conversation from "views/components/Conversation/reducer";
import jobOrder from "views/pages/JobOrder/reducer";
import { commonReducer } from "redux/common";
import testReducer from "../testStore/testSlice";
// const routes = (history) =>
//   combineReducers({
//     router: connectRouter(history),
//     common: commonReducer,
//     login,
//     signUp,
//     createPassword,
//     hostList,
//     hostProfile,
//     products,
//     company,
//     createProduct,
//     detailProduct,
//     recommenderSeller,
//     jobReducer,
//     campaign,
//   });

const routes = (history) => ({
  router: connectRouter(history),
  common: commonReducer,
  login,
  signUp,
  createPassword,
  hostList,
  hostProfile,
  products,
  company,
  createProduct,
  detailProduct,
  recommenderSeller,
  jobReducer,
  paymentReducer,
  campaign,
  marketplace,
  test: testReducer,
  conversation,
  jobOrder,
});
export default routes;
