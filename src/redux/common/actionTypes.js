/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

const REDUCER = 'common';

export const SET_ON_INIT_AUTH = `@@${REDUCER}/SET_ON_INIT_AUTH`;
export const SET_USER = `@@${REDUCER}/SET_USER`;

