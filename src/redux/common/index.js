/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

import * as commonTypes from './actionTypes';
import { commonReducer } from './reducer';

export * from './actions';
export { commonTypes, commonReducer };
