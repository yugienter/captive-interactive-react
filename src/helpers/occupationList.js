export const OCCUPATION_LIST = [
    {
        id: "1",
        name: "Accountant",
        item: [
            {
                id: "1.1",
                name: "Accountant (General)"
            },
            {
                id: "1.2",
                name: "Management Accountant"
            },
            {
                id: "1.3",
                name: "External Auditor"
            },
            {
                id: "1.4",
                name: "Tax Accountant"
            },
        ]
    },
    {
        id: "2",
        name: "Architect",
        item: [
            {
                id: "2.1",
                name: "Landscape Architect"
            },
            {
                id: "2.2",
                name: "Qualified Architect"
            },
            {
                id: "2.3",
                name: "Naval Architect"
            },
        ]
    },
    {
        id: "3",
        name: "Engineer",
        item: [
            {
                id: "3.1",
                name: "Aeronautical Engineer"
            },
            {
                id: "3.2",
                name: "Agricultural Engineer"
            },
            {
                id: "3.3",
                name: "Biomedical Engineer"
            },
            {
                id: "3.4",
                name: "Chemical Engineer"
            },
            {
                id: "3.5",
                name: "Civil Engineer"
            },
            {
                id: "3.6",
                name: "Civil Engineering Draftsperson"
            },
            {
                id: "3.7",
                name: "Civil Engineering Technician"
            },
            {
                id: "3.8",
                name: "Electrical Engineer"
            },
            {
                id: "3.9",
                name: "Electrical Engineering Draftsperson"
            },
            {
                id: "3.10",
                name: "Electrical Engineering Technician"
            },
            {
                id: "3.11",
                name: "Engineering Manager"
            },
            {
                id: "3.12",
                name: "Engineering Professionals"
            },
            {
                id: "3.13",
                name: "Environmental Engineer"
            },
            {
                id: "3.14",
                name: "Geotechnical Engineer"
            },
            {
                id: "3.15",
                name: "Industrial Engineer"
            },
            {
                id: "3.16",
                name: "Materials Engineer"
            },
            {
                id: "3.17",
                name: "Mechanical Engineer"
            },
            {
                id: "3.18",
                name: "Mining Engineer (excluding Petroleum)"
            },
            {
                id: "3.19",
                name: "Petroleum Engineer"
            },
            {
                id: "3.20",
                name: "Physicist"
            },
            {
                id: "3.21",
                name: "Production or Plant Engineer"
            },
            {
                id: "3.22",
                name: "Structural Engineer"
            },
            {
                id: "3.23",
                name: "Telecommunications Engineer"
            },
            {
                id: "3.24",
                name: "Telecommunications Field Engineer"
            },
            {
                id: "3.25",
                name: "Transport Engineer"
            },
        ]
    },
    {
        id: "4",
        name: "Medical Practitioner",
        item: [
            {
                id: "4.1",
                name: "Biochemis"
            },
            {
                id: "4.2",
                name: "Biomedical Engineer"
            },
            {
                id: "4.3",
                name: "Cardiologist"
            },
            {
                id: "4.4",
                name: "Cardiothoracic Surgeon"
            },
            {
                id: "4.5",
                name: "Chemist"
            },
            {
                id: "4.6",
                name: "Chiropractor"
            },
            {
                id: "4.7",
                name: "Clinical Haematologist"
            },
            {
                id: "4.8",
                name: "Clinical Psychologist"
            },
            {
                id: "4.9",
                name: "Dermatologist"
            },
            {
                id: "4.10",
                name: "Diagnostic and Interventional Radiologist"
            },
            {
                id: "4.11",
                name: "Emergency Medicine Specialist"
            },
            {
                id: "4.12",
                name: "Endocrinologist"
            },
            {
                id: "4.13",
                name: "Gastroenterologist"
            },
            {
                id: "4.14",
                name: "General Practitioner"
            },
            {
                id: "4.15",
                name: "Intensive Care Specialist"
            },
            {
                id: "4.16",
                name: "Medical Diagnostic Radiographer"
            },
            {
                id: "4.17",
                name: "Medical Laboratory Scientist"
            },
            {
                id: "4.18",
                name: "Medical Oncologist"
            },
            {
                id: "4.19",
                name: "Medical Practitioners nec"
            },
            {
                id: "4.20",
                name: "Medical Radiation Therapist"
            },
            {
                id: "4.21",
                name: "Neurologist"
            },
            {
                id: "4.22",
                name: "Neurosurgeon"
            },
            {
                id: "4.23",
                name: "Obstetrician and Gynaecologist"
            },
            {
                id: "4.24",
                name: "Occupational Therapist"
            },
            {
                id: "4.25",
                name: "Ophthalmologist"
            },
            {
                id: "4.26",
                name: "Optometrist"
            },
            {
                id: "4.27",
                name: "Organisational Psychologist"
            },
            {
                id: "4.28",
                name: "Orthopaedic Surgeon"
            },
            {
                id: "4.29",
                name: "Orthotist or Prosthetist"
            },
            {
                id: "4.30",
                name: "Osteopath"
            },
            {
                id: "4.31",
                name: "Otorhinolaryngologist"
            },
            {
                id: "4.32",
                name: "Paediatric Surgeon"
            },
            {
                id: "4.33",
                name: "Paediatrician"
            },
            {
                id: "4.34",
                name: "Pathologist"
            },
            {
                id: "4.35",
                name: "Physiotherapist"
            },
            {
                id: "4.36",
                name: "Plastic and Reconstructive Surgeon"
            },
            {
                id: "4.37",
                name: "Podiatrist"
            },
            {
                id: "4.38",
                name: "Psychiatrist"
            },
            {
                id: "4.39",
                name: "Psychologists"
            },
            {
                id: "4.40",
                name: "Radiation Oncologist"
            },
            {
                id: "4.41",
                name: "Renal Medicine Specialist"
            },
            {
                id: "4.42",
                name: "Rheumatologist"
            },
            {
                id: "4.43",
                name: "Sonographer"
            },
            {
                id: "4.44",
                name: "Specialist Physician (General Medicine)"
            },
            {
                id: "4.45",
                name: "Specialist Physicians nec"
            },
            {
                id: "4.46",
                name: "Speech Pathologist"
            },
            {
                id: "4.47",
                name: "Surgeon (General)"
            },
            {
                id: "4.48",
                name: "Thoracic Medicine Specialist"
            },
            {
                id: "4.49",
                name: "Urologist"
            },
            {
                id: "4.50",
                name: "Vascular Surgeon"
            },


        ]
    },
    {
        id: "5",
        name: "Nurse",
        item: [
            {
                id: "5.1",
                name: "Nurse Practitioner"
            },
            {
                id: "5.2",
                name: "Registered Nurse (Aged Care)"
            },
            {
                id: "5.3",
                name: "Registered Nurse (Child and Family Health)"
            },
            {
                id: "5.4",
                name: "Registered Nurse (Community Health)"
            },
            {
                id: "5.5",
                name: "Registered Nurse (Critical Care and Emergency)"
            },
            {
                id: "5.6",
                name: "Registered Nurse (Developmental Disability)"
            },
            {
                id: "5.7",
                name: "Registered Nurse (Disability and Rehabilitation)"
            },
            {
                id: "5.8",
                name: "Registered Nurse (Medical Practice)"
            },
            {
                id: "5.9",
                name: "Registered Nurse (Medical)"
            },
            {
                id: "5.10",
                name: "Registered Nurse (Mental Health)"
            },
            {
                id: "5.11",
                name: "Registered Nurse (Paediatrics)"
            },
            {
                id: "5.12",
                name: "Registered Nurse (Perioperative)"
            },
            {
                id: "5.13",
                name: "Registered Nurse (Surgical)"
            },
            {
                id: "5.14",
                name: "Registered Nurses"
            },
        ]
    },
    {
        id: "6",
        name: "Veterinarian",
        item: [
            {
                id: "6.1",
                name: "Veterinarian"
            },
        ]
    },
    {
        id: "7",
        name: "Surveyor",
        item: [
            {
                id: "7.1",
                name: "Surveyor"
            },
        ]
    },
    {
        id: "8",
        name: "Cartographer",
        item: [
            {
                id: "8.1",
                name: "Cartographer"
            },
        ]
    },
    {
        id: "9",
        name: "Social Worker",
        item: [
            {
                id: "9.1",
                name: "Social Worker"
            },
        ]
    },
    {
        id: "10",
        name: "Chef",
        item: [
            {
                id: "10.1",
                name: "Chef"
            },
        ]
    },
    {
        id: "11",
        name: "Pharmacist",
        item: [
            {
                id: "11.1",
                name: "Hospital Pharmacist"
            },
            {
                id: "11.2",
                name: "Retail Pharmacist"
            },
            {
                id: "11.3",
                name: "Industrial Pharmacist"
            },
        ]
    },
]