/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

const apiPath = {
  // authen
  login: "/auth/sign-in",
  signUp: "/auth/sign-up", // TO-DO Wait server phase 1
  createPassword: "/auth/create-password",
  refreshToken: "/auth/refresh-token",
  verify: (token) => `/auth/verify/${token}`,
  logout: (token) => `/auth/logout/${token}`,
  logoutAll: "/auth/logout-all",
  myInfo: "/auth/token",
  feedback: "/auth/feedback",

  //user
  getUserById: (id) => `/users/${id}`,
  getUsers: "/users",

  //Tags
  createTag: "/tags",
  getTags: "/tags",

  // hosts
  createHost: "/hosts",
  getHosts: "/hosts",
  getHostProfile: (email) => `/hosts/${email}`,
  updateHostProfile: "/hosts/update",
  importHosts: "/hosts/import",
  getRateRange: "/hosts/rate-range", // get max min rates of host
  selectHost: "/hosts/select-host",
  rateHost: ({ hostCode, jobCode, dealCode, score }) =>
    `/deals/${dealCode}/job/${jobCode}/seller/${hostCode}/rate?point=${score}`,

  // media
  uploadMedia: "/media/upload",
  getLocalMedia: (url) => `/media/${url}`,

  // not found
  notFound: "*",

  // prduct
  listProduct: "/product",
  listNewProduct: "/product/new",
  listProductWithStatus: "/product/job/company",
  listProductOffers: "/product/offer/all",
  createProduct: "/product",
  detailProduct: (id) => `/product/${id}`,
  editProduct: (id) => `/product/${id}`,
  deleteProduct: (id) => `/product/${id}`,
  recommendedSellers: (code) => `/product/${code}/recommended`,
  jobsOfSellerCount: () => `/job/count`,
  jobsOfCompanyCount: (search) => `/product/job/company/count?search=${search}`,
  sellersOfProductCount: (code) => `/product/${code}/seller-listing/count`,
  sellersOfProduct: (code, search) =>
    `/product/${code}/seller-listing?${search}`,

  //company
  createCompany: "/companies/",
  updateCompany: (id) => `/companies/${id}`,
  fetchCompany: (id) => `/companies/${id}`,
  getCompanyCode: "/companies/codes",
  fixedRatePayment: ({ dealCode }) => `/payments/fixed-rate/${dealCode}`,
  companyUpdateProof: ({ paymentCode }) => `/payments/proof/${paymentCode}`,
  companyFindPayment: ({ paymentCode }) => `/payments/info/${paymentCode}`,

  //brand
  createBrand: "/brands",
  updateBrand: (id) => `/brands/${id}`,
  getBrand: (id) => `/brands/${id}`,
  deleteBrand: (id) => `/brands/${id}`,

  // job
  createJob: "/job",
  getMyJobs: "/job/myjob",
  sellersOfCampaign: (jobCode, search = "") =>
    `/job/myseller/${jobCode}?${search}`,
  sellersOfCampaignStatusCount: (jobCode) => `/job/${jobCode}/myseller/count`,
  verifyJob: ({ hostCode, jobCode, dealCode }) =>
    `/deals/${dealCode}/job/${jobCode}/seller/${hostCode}/verified`,

  // deal
  createDeal: "/deals",
  createDealByCompany: "/deals/company",
  createDealByHost: "/deals/seller",
  sellerActivities: (jobCode, hostCode) =>
    `/deals/job/${jobCode}/seller/${hostCode}`,
  acceptDeal: (dealCode) => `/deals/${dealCode}/accept`,
  rejectDeal: (dealCode) => `/deals/${dealCode}/reject`,

  //campaign
  createCampaign: "/campaign/",
  updateCampaign: (jobCode) => `/campaign/${jobCode}`,
  getDetailCampaign: (productCode) => `/product/${productCode}/campaign-detail`,
  getCampaignAndDeal: (jobCode) => `/campaign/${jobCode}`,
  completeCampaign: (jobCode) => `/campaign/${jobCode}/complete`,
  deleteCampaign: (jobCode) => `/campaign/${jobCode}`,
  fetchCampaignsByProductCode: ({ productCode, size, page }) =>
    `/product/${productCode}/campaign-listing?per_page=${size}&page=${page}`,
  getOpenBid: "/job/campaign",
  getCampaignCardByProductAndSeller: (jobCode, productCode) =>
    `/job/campaign/${jobCode}/product/${productCode}`,

  //deal
  updateDealCampaign: (jobCode) => `/deals/${jobCode}/campaign`,
  submitDeal: "/deals/bid-submitted",
  getListDeal: (jobCode) => `/deals/job/${jobCode}`,
  completeJob: (dealCode, jobCode) =>
    `/deals/${dealCode}/job/${jobCode}/complete`,

  //marketplace
  getProductTrending: "/job/campaign/product-trending",
  getFeaturedProduct: "/job/campaign/product-featured",

  //payment
  getPayments: "/payments/all",
  getSellerTotalIncome: "/orders/income",

  //joborder
  getJobOrders: (jobCode) => `/orders/${jobCode}`,
  getJobOrderManagement: (jobCode) => `/orders/${jobCode}/managerment`,
  getJobOrderStatistic: (jobCode) => `/orders/${jobCode}/statistic`,
  getOrderPricing: (jobCode) => `/orders/${jobCode}/calculate`,
  submitOrder: (jobCode) => `/orders/${jobCode}/submit`,
  importOrder: (jobCode) => `/orders/${jobCode}/import`,
  removeOrder: (jobCode, orderCode) => `/orders/${jobCode}/${orderCode}`,
  editOrder: (jobCode, orderCode) => `/orders/${jobCode}/${orderCode}`,
  addOrder: (jobCode) => `/orders/${jobCode}`,

  // Conversation
  conversationDetail: (jobCode, hostCode) =>
    `/conversations/job/${jobCode}/seller/${hostCode}`,
  conversationDetailForHost: (jobCode) => `/conversations/job/${jobCode}`,
  message: (conversationCode) => `/messages/${conversationCode}`,
};

const apiPathIgnoreAuthentication = [
  //aut
  apiPath.login,
  apiPath.createPassword,
  apiPath.verify,

  apiPath.getTags,
];

export const router = {
  home: "/",
  signUp: "/sign-up",
  selectRole: "/sign-in",
  signIn: "/sign-in/:role",
  signUpHost: "/sign-up-host",
  flowSignUp: "/sign-up-onboarding",
  verifyEmail: "/verify-email/:token",
  createPassword: "/create-password",
  resetPassword: "/reset-password",
  resetPasswordSuccess: "/reset-password-success",
  thanks: "/thanks",
  allIcons: "/all-icons",

  //host
  hostList: "/host-list",
  importHost: "/import-host",
  hostProfile: "/host-profile",
  resetPasswordHost: "/reset-password-host",
  recommenderSeller: "/recommender-seller",
  myJobs: "/my-jobs",
  campaigns: "/campaigns",
  campaignDetail: "/detail-campaign/:code",

  // product
  products: "/products",
  featureProducts: "/featured-products",
  createProduct: "/create-product",
  detailProduct: "/detail-product/:code",
  editProduct: "/edit-product/:code",
  marketPlace: "/market-place",
  sellers: "/sellers",
  orders: (jobCode = ":jobCode", hostCode = ":hostCode") =>
    `/orders/${jobCode}/${hostCode}`,

  // company
  companyOnboarding: "/company-onboarding",
  signUpCompany: "/sign-up-company",
  companyProfile: "/company-profile",

  //viewCampaign
  viewCampaign: "/view-campaign/:jobCode",

  //payment
  payment: "/payment",
  jobOrder: "/job-order/:code",
  // 404 page
  notFound: "*",
};

const routeIgnoreAuthentication = [
  router.signIn,
  router.resetPassword,
  router.resetPasswordSuccess,
  router.signUp,
  router.signUpCompany,
  router.createPassword,
  router.signUpHost,
  router.thanks,
  router.verifyEmail,
  router.selectRole,

  // router.hostProfile,
  router.resetPasswordHost,

  // TODO : Need to make Authentication for that route // Author's Comment : phunguyen
];

const UserGroups = {
  admin: "ADMIN",
  host: "HOST",
  brand: "BRAND",
};

const userRoles = {
  ADMIN: "admin",
  HOST: "host",
  BRAND: "brand",
  COMPANY: "company",
};

const category = {
  Food: "Food",
  OrganicFood: "Organic Food",
  RawFood: "Raw Food",
  ReadytoEat: "Ready to Eat",
  Snacks: "Snacks",
  HealthProducts: "Health Products",
  Electronics: "Electronics",
  UtilityGadgets: "Utility Gadgets",
  HomeAppliances: "Home Appliances",
  Gaming: "Gaming",
  Audio: "Audio",
  Parenting: "Parenting",
  BabyFurniture: "Baby Furniture's",
  BabyToys: "Baby Toys",
  BabyFood: "Baby Food",
  BabyWear: "Baby Wear",
  ChildrenEducationandToys: "Children Education and Toys",
  BabyGoods: "Baby Goods",
  ToddlerProducts: "Toddler Products",
  InfantCare: "Infant Care",
  Toys: "Toys",
  EducationalProducts: "Educational Products",
  Fashion: "Fashion",
  Bags: "Bags",
  SportsFashion: "Sports Fashion",
  Eyewear: "Eyewear",
  Watches: "Watches",
  Accessories: "Accessories",
  Male: "Male",
  Female: "Female",
  HealthandWellness: "Health and Wellness",
  Supplements: "Supplements",
};

const career = [
  "Architecture and Engineering Occupations",
  "Arts, Design, Entertainment, Sports, and Media Occupations",
  "Building and Grounds Cleaning and Maintenance Occupations",
  "Business and Financial Operations Occupations",
  "Community and Social Services Occupations",
  "Computer and Mathematical OccupationsCon",
  "Education, Training, and Library Occupations",
  "Farming, Fishing, and Forestry Occupations",
  "Food Preparation and Serving Related Occupations",
  "Healthcare Practitioners and Technical Occupations",
  "Healthcare Support Occupations",
  "Installation, Maintenance, and Repair Occupations",
  "Legal Occupations",
  "Life, Physical, and Social Science Occupations",
  "Management Occupations",
  "Military Specific Occupations",
  "Office and Administrative Support Occupations",
  "Personal Care and Service Occupations",
  "Production Occupations",
  "Protective Service Occupations",
  "Sales and Rela",
];

const PAGINATION = {
  PAGE: 0,
  PER_PAGE: 10,
};

const FILTER_DEFAULT = {
  tags: { title: "All Categories" },
  experience: {
    groups: {
      all: "All years",
      2: "2 years+",
      3: "3 years+",
      5: "5 years+",
      10: "10 years+",
    },
    min: 0,
    max: 30,
    all: "all",
    title: "All Years",
  },
  gender: {
    male: "male",
    female: "female",
    title: "Not Specific",
  },
  age: {
    min: 0,
    max: 100,
    title: "All Ages",
  },
  rates: { title: "All Rates" },
};

const mainStyle = {
  gutter: { xs: 8, sm: 16, md: 24, lg: 32 },
};

export const SESSION_TIMEOUT_OPTIONS = [
  { text: "5 mins", value: 5 },
  { text: "10 mins", value: 10 },
  { text: "15 mins", value: 15 },
  { text: "30 mins", value: 30 },
  { text: "1hr", value: 60 },
  { text: "3hr", value: 180 },
  { text: "6hr", value: 360 },
  { text: "12hr", value: 720 },
  { text: "24hr", value: 1440 },
  { text: "48hr", value: 2880 },
];

export const TIME_RANGE = {
  WEEK: 7,
  HALF_MONTH: 15,
  MONTH: 30,
  TWO_MONTHS: 60,
};

export const LoginMethods = {
  GOOGLE: "Google",
  ACCOUNT: "Account",
  FACEBOOK: "Facebook",
};

const STREAM_TYPES = {
  "1 Cam + 1 Host": "1 Cam + 1 Host",
  "1 Cam + 2 Host": "1 Cam + 2 Host",
  "2 Cam + 1 Host": "2 Cam + 1 Host",
  "2 Cam + 2 Host": "2 Cam + 2 Host",
};

const PLATFORM_TYPES = [
  {
    value: "Shopee",
    text: "Shopee",
  },
  {
    value: "Lazada",
    text: "Lazada",
  },
  {
    value: "Facebook",
    text: "Facebook",
  },
  {
    value: "Instagram",
    text: "Instagram",
  },
];

const AGREED_PRICINGS = {
  // Negotiable: "Negotiable",
  Commissions: "Campaign Sales Commissions",
  "Hourly Rate": "Fixed Rate",
  "Hourly Rate + Commissions": "Fixed Rate + Campaign Sales Commissions",
};

export const statusJob = {
  offer: "offer",
  submitted: "submitted",
  negotiating: "negotiating",
  execute: "execute",
  complete: "complete",
  rejected: "rejected",
  verify: "verify",
};

const JobStatusText = {
  offer: "Direct Offer",
  submitted: "Pitch Submission",
  negotiating: "Negotiating",
  execute: "Waiting to execute",
  complete: "Completed",
  rejected: "Rejected",
  verify: "Completed",
};

export const statusDeal = {
  opens: "opens",
  deal: "deal",
  proposal: "proposal",
  accepted: "accepted",
  rejected: "rejected",
  complete: "complete",
  verified: "verified",
};

const CAMPAIGN_STATUS = {
  NOT_STARTED: "notStarted",
  COMPLETE: "complete",
  IN_PROGRESS: "inProgress",
};

const dateFormat = "DD MMM YYYY";

export const RATE_SCORES = ["0", "1", "2", "3", "4", "5"];

export const constants = {
  platform: "web-client",
  api: {
    path: apiPath,
    pathIgnoreAuthentication: apiPathIgnoreAuthentication,
    statusCode: {
      SUCCESS: 200,
      SUCCESS_: 201,
      BAD_REQUEST: 400,
      UNAUTHORIZED: 401,
      FORBIDDEN: 403,
      NOT_FOUND: 404,
      CONFLICT: 409,
      INTERNAL_SERVER_ERROR: 500,
    },
  },
  notificationLength: {
    LENGTH_SHORT: 3,
    LENGTH_LONG: 6,
  },
  router,
  routeIgnoreAuthentication,
  UserGroups,
  userRoles,
  category,
  PAGINATION,
  FILTER_DEFAULT,
  TOTAL_HOST_ONBOARDING_STEP: 5,
  MEDIA_TYPE: {
    HOST: "HOST",
    PRODUCT: "PRODUCT",
    COMPANY: "COMPANY",
  },
  mainStyle,
  FILE_TYPES: ".jpeg,.jpg,.png,.mp4,.webm,.ogv,.m3u8",
  career: career,
  STREAM_TYPES,
  AGREED_PRICINGS,
  statusJob,
  statusDeal,
  PLATFORM_TYPES,
  dateFormat,
  CAMPAIGN_STATUS,
  JobStatusText,
};
