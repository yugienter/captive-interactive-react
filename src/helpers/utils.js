/**
 * Author : phu.nguyenluu@gmail.com
 * Setup : 19/08/2021
 */

import { notification } from "antd";
import config from "config";
import moment from "moment";
import { constants } from "./constants";

const { API_END_POINT } = config;
const { api } = constants;

const showNotification = ({
  variant = "success",
  message = "",
  description = "",
  duration = constants.notificationLength.LENGTH_LONG,
  key,
  callback,
}) => {
  notification[variant]({ message, description, duration, key });
  if (callback) callback();
};

const validateEmail = (email) => {
  // return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(email);
  return /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i.test(
    email
  );
  // this allow the "+" sign in string email
  // yugienter.nguyen@gmail.com - true
  // yugienter.nguyen+1@gmail.com - true
  // yugienter.nguyen+@gmail.com - false
  // yugienter.nguyen/!@#$@gmail.com - false
};

const validatePassword = (password = "") => {
  return password.match(/^(?=.*[0-9])(?=.*[a-zA-Z])/); // at least one number or symbol
};

const camelCaseToUnderScore = (string) => {
  return string
    .replace(/\.?([A-Z]+)/g, (x, y) => {
      return "_" + y.toLowerCase();
    })
    .replace(/^_/, "");
};

const stringToUnderScore = (string) => {
  return string.toLowerCase.replace(/'/g, " ").replace(/ /g, "_");
};

const getFirsDigit = (number) => {
  while (number >= 10) {
    number /= 10;
  }
  return Math.floor(number);
};

const log10 = (n) => {
  return Math.round((100 * Math.log(n)) / Math.log(10)) / 100;
};

const getSocialLink = (type, name) => {
  if (!name) return "";
  if (name.includes("http")) return name;
  switch (type) {
    case "facebook":
      name = name[0] === "@" ? name.substring(1) : name;
      return `https://facebook.com/${name}`;

    case "instagram":
      name = name[0] === "@" ? name.substring(1) : name;
      return `https://instagram.com/${name}`;

    case "tiktok":
      name = name[0] !== "@" ? `@${name}` : name;
      return `https://tiktok.com/${name}`;

    default:
      return "";
  }
};

const transGoogleDriveLink = (driverLinkImage) => {
  const id = _getIdGoogleDriveFromUrl(driverLinkImage)[0];
  return `https://drive.google.com/uc?export=view&id=${id}`;
};

const _getIdGoogleDriveFromUrl = (url) => {
  return url.match(/[-\w]{25,}/);
};

const kFormatter = (num) => {
  try {
    const number = parseInt(num);
    if (Number.isNaN(number)) return num;

    return Math.abs(num) > 99999
      ? Math.sign(num) * (Math.abs(num) / 1000000).toFixed(1) + 'm'
      : Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + 'k'
      : Math.sign(num) * Math.abs(num) + '';
  } catch (err) {
    return num;
  }
};

const ucwords = (str) => {
  return (str + "").replace(/^([a-z])|\s+([a-z])/g, function ($1) {
    return $1.toUpperCase();
  });
};

const getYearOfExperience = (startDateString) => {
  try {
    if (!startDateString) return;

    const startDate = new Date(startDateString);
    return new Date(new Date() - startDate).getFullYear() - 1970;
  } catch (err) {
    console.log("[getYearOfExperience]", err);
  }
};

const getExperienceGroup = (yearExperience) => {
  try {
    if (yearExperience === null || yearExperience === undefined) return;

    const groups = constants.FILTER_DEFAULT.experience.groups;
    if (yearExperience >= 10) return groups[10];
    if (yearExperience >= 5) return groups[5];
    if (yearExperience >= 3) return groups[3];
    return groups[2];
  } catch (err) {
    console.log("[getExperienceGroup]", err);
  }
};

const acronym = (string) => {
  const matches = (string + "").match(/\b(\w)/g);
  return matches.join("");
};

const numberWithCommas = (x) => {
  if (!x && x != 0) return;
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const calculateHostProfileCurrentStep = (data) => {
  let step = 1;
  if (!data) return step;

  if (!data.avatar || !data.aboutMe) {
    return step;
  }

  step += 1; // step 2
  const isInStep2 = [
    data.country,
    data.phoneNumber,
    data.sex,
    data.dob,
    data.weight,
    data.height,
  ].some((value) => value === null || value === undefined);
  if (isInStep2) return step;

  step += 1; // step 3
  if (!data.media || data.media.length === 0) return step;

  step += 1; // step 4
  if (data.tags.length === 0) return step;

  step += 1; // step 5
  const social = data.social;
  if (!social.facebook || !social.instagram || !social.tiktok || !social.other)
    return step;

  return (step += 1); // Complete
};

const calculateHostProfileCompleteness = (data) => {
  let percentage = 0;
  if (!data) return percentage;
  if (data.avatar && data.aboutMe) percentage += 20;

  const isCompleteStep2 = [
    data.country,
    data.phoneNumber,
    data.sex,
    data.dob,
    data.weight,
    data.height,
    data.experience,
    data.rates,
  ].every((value) => value !== null || value !== undefined);
  if (isCompleteStep2) percentage += 20;

  if (data.media && data.media.length !== 0) percentage += 20;

  if (data.tags.length !== 0) percentage += 20;

  const social = data.social;
  if (social.facebook || social.instagram || social.tiktok || social.other)
    percentage += 20;

  return percentage;
};

function getMediaUrl(url) {
  if (!url) return;
  if (url.includes("drive.google")) return utils.transGoogleDriveLink(url);
  if (url.includes("./media")) {
    return `${API_END_POINT}${api.path.getLocalMedia(encodeURIComponent(url))}`;
  }

  return url;
}

function getAge(dob) {
  try {
    return dob ? moment().diff(moment(dob), "years") : "";
  } catch (err) {
    console.error("[getAge]", err);
    return dob;
  }
}

function isImage(url) {
  if (!url) return false;
  return /\.(jpeg|jpg|gif|png)$/i.test(url);
}

function isVideo(url) {
  if (!url) return false;
  return /\.(mp4|webm|ogv|m3u8|mpd)$/i.test(url);
}
function formatNumber(n) {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function handleFormatInput(input, numberOfDecimal) {
  if (input === "") {
    return "";
  }
  if (input.indexOf(".") >= 0) {
    var decimalPos = input.indexOf(".");
    var leftSide = input.substring(0, decimalPos);
    var rightSide = input.substring(decimalPos);
    leftSide = formatNumber(leftSide);
    rightSide = formatNumber(rightSide);
    rightSide = rightSide.substring(0, numberOfDecimal);
    return leftSide + "." + rightSide;
  }
  return formatNumber(input);
}
const handleFormatRightSide = (input, numberOfDecimal) => {
  if (input === "") {
    return "";
  }
  if (input.indexOf(".") >= 0) {
    return handleFormatInput(
      input + "0".repeat(numberOfDecimal - 1),
      numberOfDecimal
    );
  }
  return input + "." + "0".repeat(numberOfDecimal);
};

const formatMoney = (number) => {
  return Number(number)
    .toFixed(2)
    .replace(/\d(?=(\d{3})+\.)/g, "$&,");
};

const validateUrl = (url) => {
  const expression =
    /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
  const regex = new RegExp(expression);
  return url.match(regex);
};

const convertPeriodToArrayAndMoment = (from, to) => {
  from = from ? moment(from) : null;
  to = from ? moment(to) : null;
  return [from, to];
};

const toDateString = (
  value,
  { format = "DD MMM, YYYY", defaultValue = "" } = {}
) => {
  try {
    const date = moment(value);
    return date.isValid() ? date.format(format) : defaultValue;
  } catch (err) {
    return err.message;
  }
};

export const utils = {
  showNotification,
  validatePassword,
  validateEmail,
  camelCaseToUnderScore,
  stringToUnderScore,
  getFirsDigit,
  log10,
  getSocialLink,
  transGoogleDriveLink,
  kFormatter,
  ucwords,
  getYearOfExperience,
  getExperienceGroup,
  acronym,
  numberWithCommas,
  calculateHostProfileCurrentStep,
  calculateHostProfileCompleteness,
  getMediaUrl,
  isImage,
  isVideo,
  handleFormatInput,
  handleFormatRightSide,
  getAge,
  formatMoney,
  validateUrl,
  convertPeriodToArrayAndMoment,
  toDateString,
};
