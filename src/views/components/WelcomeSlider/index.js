import { Carousel } from "antd";

// temp data
import welcomeImage from "./media/welcome.png";
const WelcomeSlider = () => (
  <Carousel className="welcome-slider">
    <div className="welcome-slider-item">
      <img src={welcomeImage} alt="captive" />
      <h3>Welcome to the age of the Live Commerce</h3>
      <p>
        Engage with your consumers. Add a dimension LIVE to digital marketing.
      </p>
    </div>
    <div className="welcome-slider-item">
      <img src={welcomeImage} alt="captive" />
      <h3>Welcome to the age of the Live Commerce</h3>
      <p>
        Engage with your consumers. Add a dimension LIVE to digital marketing.
      </p>
    </div>
    <div className="welcome-slider-item">
      <img src={welcomeImage} alt="captive" />
      <h3>Welcome to the age of the Live Commerce</h3>
      <p>
        Engage with your consumers. Add a dimension LIVE to digital marketing.
      </p>
    </div>
  </Carousel>
);
export default WelcomeSlider;
