/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import React from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { initAuth, logout } from 'redux/common';
import { utils, constants } from 'helpers';
import jwtDecode from 'jwt-decode';
import NotFoundPage from 'views/pages/NotFound';

import { Helmet } from "react-helmet";
import { Layout, message } from "antd";
import Loading from "views/components/Loading";
import Menu from "views/components/Menu";
import "styles/style.less";

import { getTags } from 'views/pages/Host/actions';
import { fetchHostProfile } from 'views/pages/HostProfile/actions';
import { getCompany, fetchCompany } from 'views/pages/Company/actions'

message.config({ maxCount: 1 });

const { routeIgnoreAuthentication, userRoles, router } = constants;
const { Content } = Layout;

class Page extends React.Component {
  _timeoutSession = null;

  constructor(props) {
    super(props);
    this.state = {
      open: true,
      authLoading: false,
      accessDenied: false
    };
  }

  async componentDidMount() {
    const {
      initAuthAction,
      match,
      logout,
      callbackDidMount,
      requiredAccess,
      getTags,
      tags,
      hostProfile,
      fetchHostProfile,
      getCompany,
      history,
      company
    } = this.props;
    this.setState({ authLoading: true });

    const { path } = match;
    let myInfo = {};
    const isIgnore = routeIgnoreAuthentication.includes(path);

    try {
      const jwtToken = (localStorage.token || '').replace('Bearer ', '');

      if (!jwtToken && !isIgnore) {
        return logout();
      }

      const decoded = jwtDecode(jwtToken);
      const expiredTime = decoded.exp * 1000;
      const currentTime = Date.now();

      this._timeoutSession = setTimeout(() => {
        utils.showNotification({
          message: 'Your session has expired. please log in again',
          variant: 'warning',
          duration: 6
        });
        logout();
      }, expiredTime - currentTime);
    } catch (e) {
      console.log('Error Component Page', e.message);
      return;
    }

    try {
      myInfo = await initAuthAction(isIgnore);
    } catch (error) {
      console.log('InitAuthAction Error', error.message);
    }
    this.setState({ authLoading: false });
    const { role = '', email } = myInfo;
    // Check requiredAccess here
    if (role !== userRoles.ADMIN) {
      if (requiredAccess && !requiredAccess.includes(role)) {
        // Access Denined!
        this.setState({ accessDenied: true });
        return;
      }
    }

    if (!tags || !tags.length) await getTags();

    if (role === userRoles.HOST) {
      let host = hostProfile;
      if (!host) {
        host = await fetchHostProfile(email);
      }
      if (host && host.step < 3 && !host.finishedOnboarding) {
        history.replace(router.flowSignUp);
        return;
      }
    }

    if (role === userRoles.COMPANY) {
      let companyProfile = company;
      companyProfile = await getCompany()
      if (!companyProfile) {
        history.replace(router.companyOnboarding);
      }
    }

    callbackDidMount && callbackDidMount();
  }

  componentWillUnmount() {
    clearTimeout(this._timeoutSession);
  }

  render() {
    const { myInfo, children, helmet, layout, selectedKeys, alignTop, loadingPage } = this.props;

    const { accessDenied, authLoading } = this.state;
    if (accessDenied) return <NotFoundPage />;

    const contentClass = alignTop ? "align-top" : undefined;
    if (layout === "split") {
      return (
        <div className="captive split with-bg">
          {helmet && <Helmet title={helmet} />}
          {loadingPage && <Loading />}
          <Layout>
            <Content className={contentClass}>{children}</Content>
          </Layout>
        </div>
      );
    }

    const isRenderChild = !!myInfo.email;

    return (
      <div className="captive">
        {helmet && <Helmet title={helmet} />}
        {loadingPage && <Loading />}
        <Layout>
          <Menu selectedKeys={selectedKeys} />
          {
            authLoading
              ? 'Loading ...'
              :
              <Content className={contentClass}>
                {isRenderChild && children}
              </Content>
          }
        </Layout>
      </div>
    );
  }
}

// redux
const mapStateToProps = ({ common, hostList, hostProfile, company }) => ({
  isOnInitAuth: common.isOnInitAuth,
  myInfo: common.user,
  tags: hostList.tags,
  hostProfile: hostProfile.data,
  company: company.data
});

const mapDispatchToProps = {
  initAuthAction: initAuth,
  logout,
  getTags,
  fetchHostProfile,
  fetchCompany,
  getCompany
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Page));
