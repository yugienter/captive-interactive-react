import React from "react";
// import { Avatar } from "antd";
// import { UserOutlined } from "@ant-design/icons";
import { utils } from "helpers";

const UserAvatar = ({ user = {}, name }) => {
  const { role } = user;
  const displayNameInCircle = utils.ucwords(utils.acronym(name).substring(0, 2));

  return <div className={`txtName ${role}`}>{displayNameInCircle}</div>;

  // if (user) {
  //   let displayName = "";
  //   if (user.lastName) {
  //     displayName = user.lastName.charAt(0);
  //     return (
  //       <Avatar size="large" src={user?.avatar}>
  //         {displayName}
  //       </Avatar>
  //     );
  //   }
  //   if (user.username) {
  //     displayName = user.username.charAt(0);
  //     return (
  //       <Avatar size="large" src={user?.avatar}>
  //         {displayName}
  //       </Avatar>
  //     );
  //   }
  // }
  // return <Avatar icon={<UserOutlined />} size="large" src={user?.avatar} />;
};
export default UserAvatar;
