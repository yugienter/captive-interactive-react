import React from "react";
import { IconOutsideLink } from "views/components/Icon";
const 
TitleSubtitle = ({ title, subtitle, reverse, icon, link, style = {} }) => {
  if (!reverse) {
    if (icon) {
      return (
        <div className="title-subtitle with-icon">
          <div className="icon">{icon}</div>
          <div>
            <div className="title">{title}</div>
            <div className="subtitle">{subtitle}</div>
          </div>
        </div>
      );
    }
    return (
      <div className="title-subtitle">
        <div className="title">{title}</div>
        <div className="subtitle">{subtitle}</div>
      </div>
    );
  } else {
    if (icon) {
      return (
        <div className="title-subtitle with-icon">
          <div className="icon">{icon}</div>
          <div>
            <div className="subtitle">
              {subtitle}
              {link && (
                <a href={link} target="_blank" rel="noreferrer">
                  <IconOutsideLink />
                </a>
              )}
            </div>
            <div className="title">{title}</div>
          </div>
        </div>
      );
    }
    return (
      <div className="title-subtitle" style={style}>
        <div className="subtitle">
          {subtitle}
          {link && (
            <a href={link}>
              <IconOutsideLink />
            </a>
          )}
        </div>
        <div className="title">{title}</div>
      </div>
    );
  }
};

export default TitleSubtitle;
