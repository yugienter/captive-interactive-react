import React from "react";
import LogoFull from "./media/SCX-logo.svg";

const LogoVertical = ({ branding = {} }) => {
  return (
    <div className="logo logo--horizontal-full">
      <img src={LogoFull} alt="" />
    </div>
  );
};

export default LogoVertical;
