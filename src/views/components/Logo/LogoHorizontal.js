import React from "react";
import LogoHorz from "./media/SCX-logo.svg";

const LogoHorizontal = () => {
  return (
    <div className="logo logo--horizontal">
      <img src={LogoHorz} alt="logo" />
    </div>
  );
};

export default LogoHorizontal;
