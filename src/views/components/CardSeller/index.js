import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Card } from "antd";
import PictureCover from "./components/PictureCover";
import { IconRating } from "views/components/Icon/recommender";
import HostTag from "views/pages/Host/parts/HostTag";
import { utils } from "helpers";
import DrawerSeller from "views/components/ModalRight";
import { addSeller, removeSeller } from "views/pages/RecommenderSeller/actions";
import { BorderOutlined, CheckSquareFilled } from "@ant-design/icons";
import DirectOfferDrawer from "../DirectOfferDrawer";

const { Meta } = Card;
const LIMIT = 2;

const CardSeller = ({
  seller,
  selectedSellers,
  index,
  addSeller,
  removeSeller,
}) => {
  const [showDrawer, setShowDrawer] = useState(false);
  const [checked, setChecked] = useState(false);
  const [directOffer, setDirectOffer] = useState(false);

  useEffect(() => {
    setDirectOffer(!!selectedSellers.length);
  }, [selectedSellers, setDirectOffer]);

  useEffect(() => {
    const value = !!selectedSellers[index];
    setChecked(value);
  }, [selectedSellers, setChecked, index]);

  function onChange() {
    const isChecked = !checked;
    if (isChecked) {
      addSeller(index, seller.code);
    } else {
      removeSeller(index);
    }
  }

  const onshowDrawer = (event) => {
    const element = event.target;
    const sellerCode = selectedSellers.some((code) => !!code);
    if (!sellerCode && !["button-checkbox"].includes(element.className)) {
      setShowDrawer(true);
    }
  };

  const onCloseDrawer = () => {
    setShowDrawer(false);
  };

  const experienceYear = utils.getExperienceGroup(
    utils.getYearOfExperience(seller.startDate)
  );
  const tagLength = seller.tags ? seller.tags.length : 0;
  return (
    <>
      <Card
        hoverables
        style={{ width: 325 }}
        className="recommender-card-border"
        onClick={onshowDrawer}
        cover={
          <>
            <PictureCover media={seller.media} />
            <div className="rating">
              <IconRating />
              4.8
            </div>
            <div className="seller-check">
              <button
                className="button-checkbox"
                style={{ border: "none", background: "none", padding: 0 }}
                onClick={onChange}
              >
                {checked ? (
                  <CheckSquareFilled
                    style={{ color: "#0085ff", pointerEvents: "none" }}
                  />
                ) : (
                  <BorderOutlined
                    style={{ color: "#0085ff", pointerEvents: "none" }}
                  />
                )}
              </button>
            </div>
          </>
        }
      >
        <Meta
          avatar={<img src={utils.getMediaUrl(seller.avatar)} alt="" />}
          title={seller.name}
          description={
            <>
              <p>Age: {utils.getAge(seller.dob)}</p>
              <ul className="list-tags">
                {seller.tags &&
                  seller.tags.slice(0, LIMIT).map((tag) => {
                    return (
                      <li key={tag.code} className="list-tags__item">
                        {HostTag({ tag: tag.name }).props.icon} {tag.name}
                      </li>
                    );
                  })}
                {tagLength > LIMIT && (
                  <li className="list-tags__item list-tags__item--num">
                    +{tagLength - LIMIT}
                  </li>
                )}
              </ul>
              <hr style={{ borderColor: "#E8E8EA" }} />
              <ul className="list-info">
                <li className="list-info__item">
                  <div className="list-info__item--title">Exp. Years</div>
                  <div className="list-info__item--content">
                    {experienceYear ? experienceYear.replace(" years", "") : ""}
                  </div>
                </li>
                <li className="list-info__item">
                  <div className="list-info__item--title">Followers</div>
                  <div className="list-info__item--content">
                    {utils.numberWithCommas(seller.followers)}
                  </div>
                </li>
                <li className="list-info__item">
                  <div className="list-info__item--title">Hourly Rate</div>
                  <div className="list-info__item--content">
                    ${utils.numberWithCommas(seller.rates)}
                  </div>
                </li>
              </ul>
            </>
          }
        />
      </Card>
      <DrawerSeller
        showDrawer={showDrawer}
        isDirectOffer={false}
        onCloseDrawer={onCloseDrawer}
        seller={seller}
      />
      <DirectOfferDrawer
        showDrawer={showDrawer && directOffer}
        onCloseDrawer={() => setDirectOffer(false)}
      />
    </>
  );
};

const mapStateToProps = ({ recommenderSeller: { selectedSellers } }) => ({
  selectedSellers,
});
const mapDispatchToProps = {
  addSeller,
  removeSeller,
};
export default connect(mapStateToProps, mapDispatchToProps)(CardSeller);
