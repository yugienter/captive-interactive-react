import { utils } from 'helpers';
import React from 'react'
import { ImageDefaultSeller } from 'views/components/Icon/recommender'

const classNames = [
  ['', 'picture-cover__full', 'picture-cover__half', 'picture-cover'],
  ['picture-cover_first', 'picture-cover_second', 'picture-cover_end']
];
const defaultImage = <div className='picture-cover__default picture-cover_first'><ImageDefaultSeller /></div>
const PictureCover = ({ media }) => {
  return media && media.length ? media.filter(item => utils.isImage(item.url)).slice(0, 3).map((item, i, array) => {
    const length = array.length;
    return (
      <div key={item.code} className={`${classNames[0][length]} ${classNames[1][i]}`}>
        <img
          className='picture-cover__img'
          alt="example"
          src={utils.getMediaUrl(item.url)}
        />
      </div>
    );
  }) : defaultImage;
}

export default PictureCover
