import { useEffect } from 'react';
import { Drawer, Button, Form, Input, DatePicker } from 'antd';
import { IconCloseDrawer } from 'views/components/Icon/drawer';
import {
  IconCalendar,
  IconRemove,
  IconAddPlus,
  IconUploadAttachment,
} from 'views/components/Icon/pages';
import {
  IconPricing,
  IconStream,
  IconSendProposal,
} from 'views/components/Icon/myjobs';
import _ from 'lodash';
import moment from 'moment';
import { completeJob } from 'views/pages/MyJobs/actions';
import { connect } from 'react-redux';

const { TextArea } = Input;

function DrawerCompleteJob({ showDrawer, onCloseDrawer, deal, completeJob }) {
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({
      streamTime: moment(_.get(deal, 'streamTime.description')),
      attachments: _.get(deal, 'streamMedia'),
      links: _.get(deal, 'streamLink'),
    });
  }, [form, deal]);

  const onSubmit = async (values) => {
    await completeJob({
      jobCode: deal.jobCode,
      dealCode: deal.code,
      params: values,
    });
    onCloseDrawer && onCloseDrawer();
  };

  return (
    <>
      <Drawer
        className={'drawer-main drawer-complete-job'}
        placement="right"
        width={660}
        onClose={onCloseDrawer}
        visible={showDrawer}
        closeIcon={<IconCloseDrawer />}
        title={
          <p className="fontStyle24 drawer-submit-proposal__header">
            Complete this job
          </p>
        }
      >
        <div className="drawer-submit-proposal__content">
          <Form form={form} layout="vertical" onFinish={onSubmit}>
            <div>
              <p className="title">
                <IconStream /> Stream Details
              </p>
              <div>
                <Form.Item label="Streamed Datetime" name="streamTime">
                  <DatePicker
                    style={{ fontWeight: 'normal' }}
                    suffixIcon={<IconCalendar />}
                  />
                </Form.Item>
              </div>
            </div>
            <div>
              <p className="title">
                <IconPricing /> Stream Upload
              </p>
              <div>
                <Form.List name="attachments">
                  {(fields, { add, remove }) => (
                    <div>
                      {fields.map((field, index) => {
                        return (
                          <Form.Item
                            {...field}
                            label={index === 0 ? 'Attachments' : ''}
                          >
                            <Input
                              size="large"
                              className="input-basic"
                              style={{
                                marginBottom: 15,
                              }}
                              suffix={
                                <a onClick={() => remove(field.name)}>
                                  <IconRemove />
                                </a>
                              }
                            />
                          </Form.Item>
                        );
                      })}
                      <Button
                        onClick={() => add()}
                        type="primary"
                        className="add-btn"
                      >
                        <IconUploadAttachment />
                        Upload file
                      </Button>
                    </div>
                  )}
                </Form.List>

                <Form.List name="links">
                  {(fields, { add, remove }) => (
                    <div>
                      {fields.map((field, index) => {
                        return (
                          <Form.Item
                            {...field}
                            label={index === 0 ? 'Streamed Links' : ''}
                          >
                            <Input
                              size="large"
                              className="input-basic"
                              style={{
                                marginBottom: 15,
                              }}
                              type="url"
                              suffix={
                                <a onClick={() => remove(field.name)}>
                                  <IconRemove />
                                </a>
                              }
                            />
                          </Form.Item>
                        );
                      })}
                      <Button
                        onClick={() => add()}
                        type="primary"
                        className="add-btn"
                      >
                        <IconAddPlus />
                        Add link
                      </Button>
                    </div>
                  )}
                </Form.List>

                <Form.Item label="Note" name="note">
                  <TextArea
                    style={{
                      height: 82,
                    }}
                    rows={3}
                    className="input-basic"
                  />
                </Form.Item>
              </div>
            </div>
          </Form>
        </div>
        <div className="drawer-submit-proposal__action">
          <div>
            <a>Cancel</a>
            <a onClick={() => form.submit()}>
              <IconSendProposal />
              Submit
            </a>
          </div>
        </div>
      </Drawer>
    </>
  );
}

const mapDispatchToProps = {
  completeJob,
};

export default connect(null, mapDispatchToProps)(DrawerCompleteJob);
