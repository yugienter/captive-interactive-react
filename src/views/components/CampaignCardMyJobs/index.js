import { connect } from 'react-redux';
import { Tag, Progress } from 'antd';
import {
  IconPricing,
  IconStream,
  IconClock,
} from 'views/components/Icon/myjobs';
import _ from 'lodash';
import moment from 'moment';
import { utils } from 'helpers';
import { useEffect, useState } from 'react';
import { getCampaignAndDealByJobCode } from 'views/pages/MyJobs/actions';

function CampaignCard({ jobCode }) {
  const [campaign, setCampaign] = useState(null);
  const [deal, setDeal] = useState(null);
  const [job, setJob] = useState(null);

  useEffect(() => {
    async function getCampaign() {
      const data = await getCampaignAndDealByJobCode(jobCode);
      setJob(_.get(data, 'data'));
      setCampaign(_.get(data.data, 'campaign'));
      setDeal(_.get(data.data, 'deal'));
    }
    getCampaign();
  }, [jobCode]);

  const getDiffDate = (now, then) => {
    return `${moment(now).diff(moment(then), 'days')} days`;
  };

  const TAG_COLORS = {
    'Fixed Rate': '#55acee',
    'Hourly Rate': '#55acee',
    Commissions: '#FF9A4D',
  };
  console.log('deal', deal);

  return (
    <div className="drawer-submit-proposal__result">
      <div className="drawer-submit-proposal__result__tags">
        {(_.get(deal, 'streamPricings') || '').split(' + ').map((tag) => (
          <Tag key={tag} color={TAG_COLORS[tag]}>
            {tag}
          </Tag>
        ))}
      </div>
      <div className="drawer-submit-proposal__result__title">
        <p>{_.get(campaign, 'name')}</p>
        <p>{_.get(campaign, 'description')}</p>
      </div>
      <div className="drawer-submit-proposal__result__duration">
        <p>
          <IconClock />{' '}
          {getDiffDate(
            _.get(campaign, 'timeEnd'),
            _.get(campaign, 'timeStart'),
          )}
        </p>
        <Progress strokeColor="#1BD2A4" percent={50} showInfo={false} />
      </div>
      <div className="drawer-submit-proposal__result__deal">
        <p>
          {'$' +
            (_.get(deal, 'streamHourlyRateRange.start') || 0) +
            ' - ' +
            '$' +
            (_.get(deal, 'streamHourlyRateRange.end') || 0) +
            '/hr'}
        </p>
        <p>
          Commission:{' '}
          <span>
            {_.get(deal, 'streamCommission.negiable')
              ? 'Negotiable'
              : `${_.get(deal, 'streamCommission.description')}${_.get(
                  deal,
                  'streamCommissionUnit',
                )}`}
          </span>
        </p>
      </div>
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconPricing /> Product Price
        </p>
        <table>
          <tr>
            <td>Public RRP</td>
            <td>${_.get(job, 'publicRRP')}</td>
          </tr>
          <tr>
            <td>Discount</td>
            <td>{_.get(job, 'discountPercentage')}%</td>
          </tr>
          <tr>
            <td>Discounted Price</td>
            <td>
              $
              {_.get(job, 'publicRRP') *
                (1 - _.get(job, 'discountPercentage') / 100)}
            </td>
          </tr>
          <tr>
            <td>Discount Period</td>
            <td>
              {utils.toDateString(_.get(job, 'discountPeriodStart'))} -{' '}
              {utils.toDateString(_.get(job, 'discountPeriodEnd'))}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconStream /> Stream Details
        </p>
        <table>
          <tr>
            <td>Time Suggestion</td>
            <td>{utils.toDateString(_.get(deal, 'streamTime.description'))}</td>
          </tr>
          <tr>
            <td>Type of Stream</td>
            <td>{_.get(deal, 'streamType.description')}</td>
          </tr>
          <tr>
            <td>Elements</td>
            <td>
              {deal && _.get(deal, 'streamElement.description').join(' + ')}
            </td>
          </tr>
          <tr>
            <td>Platform</td>
            <td>
              {deal && _.get(deal, 'streamPlatform.description').join(' + ')}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__message">
        <p className="sub-title">Message</p>
        <p className="fontStyle13">{_.get(deal, 'note')}</p>
      </div>
    </div>
  );
}

const mapStateToProps = ({ jobReducer: { selectedJob = {} } }) => ({
  job: selectedJob,
});

const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(CampaignCard);
