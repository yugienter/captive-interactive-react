import React, { useState } from "react";
import { connect } from "react-redux";
import {
  Drawer,
  Row,
  Col,
  Form,
  Radio,
  Space,
  Input,
  Select,
  DatePicker,
  Checkbox,
  Tag,
  Progress,
  message,
  Button,
} from "antd";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import { IconCalendar } from "views/components/Icon/pages";
import CampaignBidCard from "views/components/CampaignBidCard";
import {
  IconPricing,
  IconStream,
  IconSendProposal,
  IconClock,
} from "views/components/Icon/myjobs";
import { utils, constants, ReactUtils } from "helpers";
import { addDeal } from "views/pages/MyJobs/actions";
import { submitDeal } from "views/pages/Marketplace/actions";
import moment from "moment";
import _ from "lodash";

const { STREAM_TYPES, AGREED_PRICINGS } = constants;

const { Option } = Select;
const { TextArea } = Input;

function DrawerBidJob({ visible, handleClose, data, submitDeal, jobCode }) {
  const [form] = Form.useForm();
  const [value, setValue] = useState(1);
  const [commissionUnit, setCommissionUnit] = useState("%");
  const [streamCommission, setStreamCommission] = useState(0);
  const [agreedPricing, setAgreedPricing] = useState(
    AGREED_PRICINGS["Hourly Rate + Commissions"]
  );

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const onChangeAgreedPricing = (e) => {
    setAgreedPricing(e.target.value);
  };
  const onChangeCommissionUnit = (e) => {
    setCommissionUnit(e.target.value);
  };
  const onChangeStreamCommission = (e) => {
    setStreamCommission(e.target.value);
  };

  const onFinish = async (values) => {
    const payload = {
      jobCode: jobCode,
      //   hostCode: code,
      streamTime: {
        description: values.streamTime ? values.streamTime.utc().format() : "",
        negiable: true,
      },
      streamType: {
        description: values.streamType || "",
        negiable: true,
      },
      streamElement: {
        description: values.streamElement || [],
        negiable: true,
      },
      streamPlatform: {
        description: values.streamPlatform || [],
        negiable: true,
      },
      streamPricings: values.streamPricings || "",
      streamHourlyRate: {
        description: values.streamHourlyRate || "",
        negiable: true,
      },
      streamCommission: {
        description: streamCommission || "",
        negiable: true,
      },
      streamCommissionUnit: commissionUnit === "%" ? "%" : "USD",
      note: values.message,
    };

    const error = await submitDeal(payload);
    console.log(error);
    if (error && error.message) {
      ReactUtils.messageWarn({ content: error.message });
    } else {
      ReactUtils.messageSuccess({ content: "Bid Sucessfully" });
      handleClose();
    }
  };

  return (
    <>
      <Drawer
        className={"drawer-main drawer-submit-proposal"}
        placement="right"
        width={860}
        onClose={handleClose}
        visible={visible}
        closeIcon={<IconCloseDrawer />}
        title={
          <p className="fontStyle24 drawer-submit-proposal__header">
            Submit your proposal
          </p>
        }
      >
        <div className="drawer-submit-proposal__content">
          <Form
            form={form}
            layout="vertical"
            onFinish={onFinish}
            autoComplete="off"
          >
            <div className="drawer-submit-proposal__deal-section">
              <Row justify="space-between">
                <Col span={11}>
                  <div>
                    <p className="title">
                      <IconPricing /> Seller Fees
                    </p>
                    <Form.Item label="Fee Type" name="streamPricings">
                      <Radio.Group
                        onChange={onChangeAgreedPricing}
                        value={value}
                      >
                        <Space direction="vertical">
                          {Object.keys(AGREED_PRICINGS).map((value) => (
                            <Radio key={value} value={value}>
                              {AGREED_PRICINGS[value]}
                            </Radio>
                          ))}
                        </Space>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Campaign Sales Commissions" name="streamCommission">
                      <Input.Group compact>
                        <Select
                          className="select-basic"
                          style={{ width: "60px" }}
                          size="large"
                          onChange={onChangeCommissionUnit}
                          defaultValue="percent"
                          disabled={agreedPricing === "Hourly Rate"}
                          name="streamCommissionUnit"
                        >
                          <Option value="percent">%</Option>
                          <Option value="price">USD</Option>
                        </Select>
                        <Input
                          style={{ width: "calc(100% - 60px)" }}
                          className="input-basic"
                          size="large"
                          disabled={agreedPricing === "Hourly Rate"}
                          name="streamCommission"
                          onChange={onChangeStreamCommission}
                        />
                      </Input.Group>
                    </Form.Item>
                    <Form.Item label="Fixed Campaign Fee" name="streamHourlyRate">
                      <Input
                        size="large"
                        className="input-basic"
                        prefix="SGD"
                        disabled={agreedPricing === "Commissions"}
                        placeholder=""
                      />
                    </Form.Item>
                  </div>
                  <div>
                    <p className="title">
                      <IconStream /> Stream Details
                    </p>
                    <Form.Item label="Stream Time" name="streamTime">
                      <DatePicker
                        style={{ fontWeight: "normal" }}
                        suffixIcon={<IconCalendar />}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Type of Stream (Required by Company)"
                      name="streamType"
                    >
                      <Radio.Group>
                        <Space direction="vertical">
                          <Radio value="1 Cam + 1 Host">1 Cam + 1 Host</Radio>
                          <Radio value="1 Cam + 2 Host">1 Cam + 2 Host</Radio>
                          <Radio value="2 Cam + 1 Host">2 Cam + 1 Host</Radio>
                          <Radio value="2 Cam + 2 Host">2 Cam + 2 Host</Radio>
                        </Space>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item
                      label="Elements (Required by Company)"
                      name="streamElement"
                    >
                      <Checkbox.Group>
                        <Row justify="space-between">
                          <Col span={24}>
                            <Checkbox value="Studio">
                              <div className="cb-group">
                                <p>Studio</p>
                                <p>Including Location</p>
                              </div>
                            </Checkbox>
                          </Col>
                          <Col span={24}>
                            <Checkbox value="Overlay">
                              <div className="cb-group">
                                <p>Overlay</p>
                                <p>Including Graphics</p>
                              </div>
                            </Checkbox>
                          </Col>
                        </Row>
                      </Checkbox.Group>
                    </Form.Item>
                    <Form.Item label="Platform" name="streamPlatform">
                      <Checkbox.Group>
                        <Row justify="space-between">
                          <Col span={24}>
                            <Checkbox value="Shopee">Shopee</Checkbox>
                          </Col>
                          <Col span={24}>
                            <Checkbox value="Lazada">Lazada</Checkbox>
                          </Col>
                          <Col span={24}>
                            <Checkbox value="Facebook">Facebook</Checkbox>
                          </Col>
                          <Col span={24}>
                            <Checkbox value="Instagram">Instagram</Checkbox>
                          </Col>
                        </Row>
                      </Checkbox.Group>
                    </Form.Item>
                  </div>
                </Col>
                <Col span={11}>
                  <CampaignBidCard />
                </Col>
              </Row>
            </div>
            <Form.Item label="Send a message to the merchant here" name="message">
              <TextArea
                style={{
                  height: 82,
                }}
                rows={3}
                className="input-basic"
              />
            </Form.Item>
          </Form>
        </div>
        <div className="drawer-submit-proposal__action">
          <div>
            <a onClick={handleClose}>Cancel</a>
            <a href="javascript:void(0)" onClick={() => form.submit()}>
              <IconSendProposal />
              Send your pitch!
            </a>
          </div>
        </div>
      </Drawer>
    </>
  );
}

const mapStateToProps = () => {};
const mapDispatchToProps = {
  submitDeal,
};
export default connect(mapStateToProps, mapDispatchToProps)(DrawerBidJob);
