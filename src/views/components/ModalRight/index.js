import { useState } from 'react';
import { Drawer } from 'antd';
import { connect } from 'react-redux';
import { IconCloseDrawer } from 'views/components/Icon/drawer';
import { resetSelectedSellers } from 'views/pages/RecommenderSeller/actions';
import DrawerSellerInfo from './components/DrawerSellerInfo';
import DrawerContentSendDirect from './components/DrawerContentSendDirect';
import DrawerTitle from './components/DrawerTitle';
import DrawerTitleSendDirect from './components/DrawerTitleSendDirect';
import DrawerTitleMyJobs from './components/DrawerTitleMyJobs';
import DrawerContentMyJobs from './components/DrawerContentMyJobs';

const DrawerSeller = ({
  showDrawer,
  onCloseDrawer,
  isDirectOffer,
  seller,
  resetSelectedSellers,
  isListOffers,
}) => {
  const [showButtonSend, setShowButtonSend] = useState(false);
  const [directOffer, setShowDirectOffer] = useState(isDirectOffer);
  const [listOffers, setDrawerMyJobs] = useState(isListOffers);

  const isProfile = !(isDirectOffer || isListOffers);

  const handleClose = () => {
    if (isProfile) {
      resetSelectedSellers();
      setShowDirectOffer(false);
    }

    onCloseDrawer && onCloseDrawer();
  };

  const renderDrawerContent = () => {
    if (listOffers)
      return (
        <>
          <DrawerContentMyJobs />
          {/* <OlderActivities /> */}
        </>
      );
    if (directOffer)
      return <DrawerContentSendDirect onCloseDrawer={handleClose} />;
    return <DrawerSellerInfo showButtonSend={showButtonSend} seller={seller} />;
  };

  const renderDrawerTitle = () => {
    if (directOffer) return <DrawerTitleSendDirect />;
    if (listOffers) return <DrawerTitleMyJobs />;
    return (
      <DrawerTitle
        showButtonSend={showButtonSend}
        seller={seller}
        onSend={setShowDirectOffer}
      />
    );
  };

  return (
    <>
      <Drawer
        className={
          listOffers ? 'drawer-main drawer-main-myjobs' : 'drawer-main'
        }
        placement="right"
        width={listOffers ? 420 : 660}
        onClose={handleClose}
        visible={showDrawer}
        closeIcon={<IconCloseDrawer />}
        title={renderDrawerTitle()}
      >
        {renderDrawerContent()}
      </Drawer>
    </>
  );
};

const mapDispatchToProps = {
  resetSelectedSellers,
};
export default connect(null, mapDispatchToProps)(DrawerSeller);
