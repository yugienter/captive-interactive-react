import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Dropdown,
  Form,
  message,
  Menu,
  Input,
  Select,
  DatePicker,
  Radio,
  Space,
  Checkbox,
  Button,
} from "antd";
import {
  IconBox,
  IconDown,
  IconSearch,
  IconBoxSmall,
  IconDetail,
  IconPricings,
  IconMessage,
} from "views/components/Icon/drawer";
import { getListProductOffers } from "views/pages/Products/actions";
import { addJob, addDeal } from "views/pages/MyJobs/actions";
import { resetSelectedSellers } from "views/pages/RecommenderSeller/actions";
import { utils, constants } from "helpers";

const { Option } = Select;
const { RangePicker } = DatePicker;

const DISCOUNT_UNITS = {
  PERCENTAGE: "percent",
  PRICE: "price",
};

const ELEMENTS = [
  {
    label: (
      <span>
        <span className="first">Studio</span> <br />
        <span className="last">Including Location</span>
      </span>
    ),
    value: "Studio",
  },
  {
    label: (
      <span>
        <span className="first">Overlay</span> <br />
        <span className="last">Including Graphics</span>
      </span>
    ),
    value: "Overlay",
  },
];

const PLATFORMS = ["Shopee", "Lazada", "Facebook", "Instagram"];

const { STREAM_TYPES, AGREED_PRICINGS } = constants;

const DrawerContentSendDirect = ({
  selectedSellers,
  getListProductOffers,
  listProductOffers,
  currentProduct,
  addJob,
  addDeal,
  onCloseDrawer,
  resetSelectedSellers,
}) => {
  const sellerCodes = selectedSellers.filter((item) => !!item);
  const [form] = Form.useForm();
  const [value, setValue] = useState(1);
  const [selectedProduct, setProduct] = useState(currentProduct);

  form.setFieldsValue({
    commissionUnit: DISCOUNT_UNITS.PERCENTAGE,
  });

  useEffect(() => {
    async function getProducts() {
      await getListProductOffers();
    }

    getProducts();
  }, [getListProductOffers]);

  const onFinish = async (values) => {
    for (let i = 0, n = sellerCodes.length; i < n; i++) {
      const code = sellerCodes[i];
      if (!code) return;

      const discountPeriod = values["discount-period"];
      const discountUnit = values.discountUnit;
      const job = await addJob({
        publicRRP: Number(values.rrp),
        productCode: selectedProduct.code,
        hostCode: code,
        discountPrice:
          discountUnit === DISCOUNT_UNITS.PRICE
            ? Number(values.discount)
            : undefined,
        discountPercentage:
          discountUnit === DISCOUNT_UNITS.PERCENTAGE
            ? Number(values.discount)
            : undefined,
        discountPercentageUnit: discountUnit,
        discountPeriodStart: discountPeriod ? discountPeriod[0] : undefined,
        discountPeriodEnd: discountPeriod ? discountPeriod[1] : undefined,
      });

      if (!job || !job.code) return;

      await addDeal({
        jobCode: job.code,
        hostCode: code,
        streamTime: {
          description: values.streamTime,
          negiable: values.streamTimeChecked,
        },
        streamType: {
          description: values.streamType,
          negiable: values.streamTypeChecked,
        },
        streamElement: {
          description: values.element || [],
          negiable: values.elementChecked,
        },
        streamPlatform: {
          description: values.platform || [],
          negiable: values.platformChecked,
        },
        streamPricings: values.agreed,
        streamHourlyRate: {
          description: values.hourlyRate,
          negiable: values.hourlyRateChecked,
        },
        streamCommission: {
          description: values.commission,
          negiable: values.commissionChecked,
        },
        streamCommissionUnit: values.commissionUnit,
        note: values.message,
      });
    }

    message.success("Submit success!");
    resetSelectedSellers();
    onCloseDrawer && onCloseDrawer();
  };

  const onFinishFailed = () => {
    message.error("Submit failed!");
  };

  const prefixSelector = (name, onChange) => (
    <Form.Item name={name} noStyle>
      <Select
        style={{
          width: 75,
        }}
        onChange={onChange || function () {}}
      >
        <Option value="percent">%</Option>
        <Option value="price">USD</Option>
      </Select>
    </Form.Item>
  );

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const handleMenuClick = (product) => {
    setProduct(product);
  };

  const menu = (
    <Menu className="sendDirect-dropdown-menu">
      <Input
        placeholder="Basic usage"
        onClick={(e) => e.stopPropagation()}
        suffix={<IconSearch />}
      />
      <div className="sendDirect-box-menu">
        <p className="title">
          <IconBoxSmall />
          All products
        </p>
        <Menu.Divider />
        {listProductOffers.map((product) => (
          <Menu.Item
            key={product.code}
            onClick={() => handleMenuClick(product)}
          >
            <figure>
              <img
                className="dropdown-img"
                alt={product.name}
                src={utils.getMediaUrl((product.media[0] || {}).url)}
              />
            </figure>
            {product.name}
          </Menu.Item>
        ))}
      </div>
    </Menu>
  );

  const calculateDiscountedPrice = () => {
    const price = form.getFieldValue("rrp");
    const discount = form.getFieldValue("discount");
    const discountUnit = form.getFieldValue("discountUnit");
    if (![price, discount, discountUnit].includes(undefined)) {
      const priceNumber = parseFloat(price);
      const discountNumber = parseFloat(discount);
      let discountedPrice = 0;
      if (discountUnit === DISCOUNT_UNITS.PRICE) {
        discountedPrice = priceNumber - discountNumber;
      } else if (discountUnit === DISCOUNT_UNITS.PERCENTAGE) {
        discountedPrice = priceNumber - (discountNumber * priceNumber) / 100;
      }
      form.setFieldsValue({ discountedPrice: discountedPrice || 0 });
    }
  };

  const onPriceChange = (e) => {
    calculateDiscountedPrice();
  };

  const onDiscountChange = (e) => {
    calculateDiscountedPrice();
  };

  const onUnitChange = (value) => {
    calculateDiscountedPrice();
  };

  return (
    <div className="drawer-main__content">
      <Form
        layout="vertical"
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div className="product-title">
          <IconBox />
          Product Setup
        </div>
        <div className="drawer-main__content--wrapper">
          <div className="form-sendDirect">
            <Form.Item label="Product" className="form-sendDirect__label">
              <Dropdown
                overlay={menu}
                trigger={["click"]}
                className="form-sendDirect__control"
              >
                <span>
                  <figure>
                    <img
                      className="dropdown-img"
                      alt={selectedProduct.name}
                      src={utils.getMediaUrl(
                        (selectedProduct.media[0] || {}).url
                      )}
                    />
                  </figure>
                  {selectedProduct.name}
                  <IconDown />
                </span>
              </Dropdown>
            </Form.Item>
            <Form.Item
              label="Public RRP"
              name="rrp"
              className="form-sendDirect__label"
            >
              <Input
                addonBefore="USD"
                style={{ width: "100%" }}
                min="1"
                step="0.01"
                onChange={onPriceChange}
                type="number"
                className="form-sendDirect__control"
              />
            </Form.Item>
            <Form.Item
              label="Discount"
              name="discount"
              className="form-sendDirect__label"
            >
              <Input
                addonBefore={prefixSelector("discountUnit", onUnitChange)}
                style={{ width: "100%" }}
                type="number"
                onChange={onDiscountChange}
                placeholder="Discount Percentage"
                className="form-sendDirect__control"
              />
            </Form.Item>
            <Form.Item
              label="Discounted Price"
              name="discountedPrice"
              className="form-sendDirect__label"
            >
              <Input
                addonBefore="USD"
                style={{ width: "100%" }}
                className="form-sendDirect__control disabled"
                disabled
              />
            </Form.Item>
            <Form.Item
              label="Discount Period"
              name="discount-period"
              className="form-sendDirect__label"
            >
              <RangePicker
                format="DD/MM/YYYY"
                className="form-sendDirect__control"
              />
            </Form.Item>
          </div>
        </div>
        <hr style={{ border: "2px dashed #E8E8EA", margin: "0 0 30px 0" }} />
        <div className="product-title">
          <IconDetail />
          Stream Details
        </div>
        <div className="drawer-main__content--wrapper">
          <div className="form-sendDirect">
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Stream Time</label>
                  <Form.Item name="streamTimeChecked" valuePropName="checked">
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="streamTime"
              className="form-sendDirect__label--checkbox"
            >
              <DatePicker className="form-sendDirect__control" />
            </Form.Item>
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Type of Stream</label>
                  <Form.Item
                    name={`${"streamType"}Checked`}
                    valuePropName="checked"
                  >
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="streamType"
              className="form-sendDirect__label--checkbox"
            >
              <Radio.Group onChange={onChange} value={value}>
                <Space direction="vertical">
                  {Object.keys(STREAM_TYPES).map((value) => (
                    <Radio key={value} value={value}>
                      {STREAM_TYPES[value]}
                    </Radio>
                  ))}
                </Space>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Elements</label>
                  <Form.Item
                    name={`${"element"}Checked`}
                    valuePropName="checked"
                  >
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="element"
              className="form-sendDirect__label--checkbox"
            >
              <Checkbox.Group options={ELEMENTS} />
            </Form.Item>
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Platform</label>
                  <Form.Item
                    name={`${"platform"}Checked`}
                    valuePropName="checked"
                  >
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="platform"
              className="form-sendDirect__label--checkbox"
            >
              <Checkbox.Group options={PLATFORMS} />
            </Form.Item>
          </div>
        </div>
        <hr style={{ border: "2px dashed #E8E8EA", margin: "0 0 30px 0" }} />
        <div className="product-title">
          <IconPricings />
          Seller Fees
        </div>
        <div className="drawer-main__content--wrapper">
          <div className="form-sendDirect">
            <Form.Item
              label={
                <div className="box-lbl" style={{ marginBottom: "10px" }}>
                  <label>Fee Type</label>
                </div>
              }
              name="agreed"
              className="form-sendDirect__label--checkbox"
            >
              <Radio.Group onChange={onChange} value={value}>
                <Space direction="vertical">
                  {Object.keys(AGREED_PRICINGS).map((value) => (
                    <Radio key={value} value={value}>
                      {AGREED_PRICINGS[value]}
                    </Radio>
                  ))}
                </Space>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Fixed Rate</label>
                  <Form.Item
                    name={`${"hourlyRate"}Checked`}
                    valuePropName="checked"
                  >
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="hourlyRate"
              className="form-sendDirect__label--checkbox"
            >
              <Input
                type="number"
                addonBefore="USD"
                style={{ width: "100%" }}
                className="form-sendDirect__control"
              />
            </Form.Item>
            <Form.Item
              label={
                <div className="box-lbl">
                  <label>Commission</label>
                  <Form.Item
                    name={`${"commission"}Checked`}
                    valuePropName="checked"
                    valuePropName="checked"
                  >
                    <Checkbox>Negotiable</Checkbox>
                  </Form.Item>
                </div>
              }
              name="commission"
              className="form-sendDirect__label--checkbox"
            >
              <Input
                type="number"
                addonBefore={prefixSelector("commissionUnit")}
                style={{ width: "100%" }}
                placeholder="10"
                className="form-sendDirect__control"
              />
            </Form.Item>
          </div>
        </div>
        <hr style={{ border: "2px dashed #E8E8EA" }} />
        <div className="drawer-main__content--wrapper">
          <div className="form-sendDirect">
            <Form.Item
              label="Message"
              name="message"
              className="form-sendDirect__label"
            >
              <Input.TextArea placeholder="Any marketing info on the product? (e.g. Made in mountains of taiwan, handcrafted, etc.)" />
            </Form.Item>
          </div>
        </div>
        <hr style={{ border: "1px solid #E8E8EA", margin: 0 }} />
        <div
          className="drawer-main__header--actions bottom"
          style={{ marginTop: "30px" }}
        >
          <Button className="cancel">Cancel</Button>
          <Button type="primary" className="send" onClick={() => form.submit()}>
            <IconMessage />
            Send an Offer
          </Button>
        </div>
      </Form>
    </div>
  );
};

const mapStateToProps = ({
  detailProduct: { currentProduct },
  recommenderSeller: { selectedSellers },
  products: { listProductOffers },
}) => ({
  selectedSellers,
  listProductOffers,
  currentProduct,
});
const mapDispatchToProps = {
  getListProductOffers,
  addJob,
  addDeal,
  resetSelectedSellers,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerContentSendDirect);
