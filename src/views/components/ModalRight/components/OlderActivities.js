import React from 'react';
import { Collapse } from 'antd';
import { IconUpCollapse } from 'views/components/Icon/myjobs';
import ContentDeal from '../../CollapseMyJobs/components/ContentDeal';
import HeaderDeal from '../../CollapseMyJobs/components/HeaderDeal';

const { Panel } = Collapse;

const OlderActivities = ({ seller, deals = [] }) => {
  return (
    <div className="older-activities">
      <div className="title-older">Older Activities</div>
      {deals.map((deal) => (
        <Collapse
          bordered={false}
          expandIcon={() => <IconUpCollapse />}
          expandIconPosition="right"
          className="site-collapse-custom-collapse older-activities-collapse collapse-main"
        >
          <Panel
            header={<HeaderDeal deal={deal} seller={seller} />}
            key={deal.code + deal.status}
            className="site-collapse-custom-panel"
          >
            <ContentDeal deal={deal} />
          </Panel>
        </Collapse>
      ))}
    </div>
  );
};

export default OlderActivities;
