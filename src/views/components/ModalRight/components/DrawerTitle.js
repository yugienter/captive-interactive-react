import React from 'react'
import { connect } from "react-redux";
import { Button } from 'antd'
import { IconMessage } from 'views/components/Icon/drawer'
import { IconRating } from 'views/components/Icon/recommender'
import { addSeller, removeSeller } from 'views/pages/RecommenderSeller/actions';
import { utils } from 'helpers'

const DrawerTitle = ({ showButtonSend, seller = {}, onSend, addSeller }) => {
  const handleSendOffer = () => {
    addSeller(0, seller.code);
    onSend && onSend(true);
  }

  return (
    <div className='drawer-main__header'>
      <div className='drawer-main__header--info'>
        <div className='avatar'>
          <img className='avatar__image' alt="example" src={utils.getMediaUrl(seller.avatar)} />
        </div>
        <div className='info'>
          <p className='info-name'><strong>{seller.name}</strong><span><IconRating />4.8</span></p>
          <p className='info-jobs'>{seller.title}</p>
        </div>
      </div>
      <div className='drawer-main__header--actions'>
        {/* <Button type='primary' className='contacting'>Contacting</Button> */}
        {showButtonSend && (
          <Button type='primary' className='send' onClick={handleSendOffer}><IconMessage />Send an Offer</Button>
        )}
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  addSeller,
  removeSeller
};
export default connect(null, mapDispatchToProps)(DrawerTitle);
