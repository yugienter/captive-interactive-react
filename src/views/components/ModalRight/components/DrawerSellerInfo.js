import React from "react";
import Carousel from "react-multi-carousel";
import { connect } from 'react-redux';
import {
  IconFacebook,
  IconInstagram,
  IconTikTok,
  IconLink,
  IconPlay,
  IconMessage,
} from "views/components/Icon/drawer";
import { Button } from "antd";
import { _mapDataHostResponse } from "views/pages/Host/actions";
import { addSeller } from "views/pages/RecommenderSeller/actions";
import HostTag from "views/pages/Host/parts/HostTag";
import "react-multi-carousel/lib/styles.css";
import { utils } from "helpers";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    paritialVisibilityGutter: 30,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
    paritialVisibilityGutter: 50,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    paritialVisibilityGutter: 30,
  },
};

const DrawerContent = ({ showButtonSend, seller = {}, tagsById, addSeller }) => {
  const hostProfile = seller.experienceGroup ? seller : _mapDataHostResponse(seller);

  const onSendOfferClick = () => {
    addSeller(2, seller.code);
  }

  return (
    <div className="drawer-main__content drawer-main-tabs__content">
      <div className="box-info">
        <div className="box-info__col">
          <p className="box-info__col--name">Age</p>
          <p className="box-info__col--result">{hostProfile.age}</p>
        </div>
        <div className="box-info__col">
          <p className="box-info__col--name">Height</p>
          <p className="box-info__col--result">{hostProfile.physique.height}</p>
        </div>
        <div className="box-info__col">
          <p className="box-info__col--name">Weight</p>
          <p className="box-info__col--result">{hostProfile.physique.weight}</p>
        </div>
        <div className="box-info__col">
          <p className="box-info__col--name">Gender</p>
          <p className="box-info__col--result">{hostProfile.gender}</p>
        </div>
      </div>
      <div className="box-medias">
        <h5>Photos & Videos</h5>
        <Carousel
          responsive={responsive}
          partialVisible
          deviceType={"desktop"}
          ssr={true}
          slidesToSlide={1}
          minimumTouchDrag={80}
          containerClass="carousel-container carousel-main"
          itemClass="image-item"
        >
          {(hostProfile.gallery || []).map((url, key) => {
            if (utils.isImage(url))
              return (
                <img
                  key={key}
                  alt="img"
                  style={{
                    width: "152px",
                    height: "152px",
                    borderRadius: "12px",
                  }}
                  src={url}
                />
              );

            if (utils.isVideo(url))
              return (
                <video
                  style={{
                    width: "152px",
                    height: "152px",
                    borderRadius: "12px",
                  }}
                  key={key}
                  alt="Video"
                  controls
                  src={url}
                />
              );
          })}
        </Carousel>
      </div>
      <div className="box-followers">
        <h5>Followers</h5>
        <div className="box-followers__content">
          <div className="box-followers__content--col">
            <div className="left">
              <IconFacebook />
            </div>
            <div className="right">
              <p className="right-title">
                Facebook
                <IconLink />
              </p>
              <p className="right-result">
                {utils.numberWithCommas(hostProfile.facebookFollowers)}
              </p>
            </div>
          </div>
          <div className="box-followers__content--col">
            <div className="left">
              <IconInstagram />
            </div>
            <div className="right">
              <p className="right-title">
                Instagram
                <IconLink />
              </p>
              <p className="right-result">
                {utils.numberWithCommas(hostProfile.instagramFollowers)}
              </p>
            </div>
          </div>
          <div className="box-followers__content--col">
            <div className="left">
              <IconTikTok />
            </div>
            <div className="right">
              <p className="right-title">
                TikTok
                <IconLink />
              </p>
              <p className="right-result">
                {utils.numberWithCommas(hostProfile.tiktokFollowers)}
              </p>
            </div>
          </div>
          <div className="box-followers__content--col">
            <div className="left">
              <IconTikTok />
            </div>
            <div className="right">
              <p className="right-title">
                Other
                <IconLink />
              </p>
              <p className="right-result">
                {utils.numberWithCommas(hostProfile.otherFollowers)}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="box-portfolio">
        <h5>Portfolio (Showreel or Website)</h5>
        <div className="play">
          <IconPlay />
          <a href={hostProfile.porfolio}>{hostProfile.porfolio}</a>
        </div>
      </div>
      <div className="box-category">
        <h5>Category</h5>
        <div className="box-category__content">
          {(hostProfile.tags || []).map((value) => {
            let tag = value;
            if (typeof value === 'string') {
              tag = tagsById[value];
            }

            const icon = HostTag({ tag: tag.name }).props.icon;
            return (
              <span className="tags-read">
                {icon}
                {tag.name}
              </span>
            );
          })}
        </div>
      </div>
      <div className="box-country">
        <div className="box-country__content">
          <div className="box-country__content--col">
            <p className="info">Country</p>
            <p className="result">{hostProfile.country}</p>
          </div>
          <div className="box-country__content--col">
            <p className="info">Phone Number</p>
            <p className="result">+{hostProfile.phone}</p>
          </div>
        </div>
      </div>
      <div className="box-biography">
        <h5>Biography</h5>
        <div className="box-biography__content">{hostProfile.about}</div>
      </div>
      <div className="box-country">
        <div className="box-country__content">
          <div className="box-country__content--col">
            <p className="info">Experience</p>
            <p className="result">{hostProfile.experienceGroup}</p>
          </div>
          <div className="box-country__content--col">
            <p className="info">Hourly Rate</p>
            <p className="result">{hostProfile.rates}</p>
          </div>
        </div>
      </div>
      <hr style={{ border: "1px solid #E8E8EA", margin: "30px 0" }} />
      <div className="drawer-main__header--actions bottom">
        {showButtonSend ? (
          <Button type="primary" className="send send_disable" onClick={onSendOfferClick}>
            <IconMessage />
            Send an Offer
          </Button>
        ) : (
          <Button type="primary" className="send" onClick={onSendOfferClick}>
            <IconMessage />
            Send an Offer
          </Button>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ hostList }) => ({
  tagsById: hostList.tagsById
});

const mapDispatchToProps = {
  addSeller,
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);