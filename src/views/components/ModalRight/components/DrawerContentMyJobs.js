import React from "react";
import CollapseMyJobs from "views/components/CollapseMyJobs";

const DrawerContentMyJobs = () => {
  return (
    <div className="ant-drawer-body__myjobs">
      <CollapseMyJobs />
    </div>
  );
};

export default DrawerContentMyJobs;
