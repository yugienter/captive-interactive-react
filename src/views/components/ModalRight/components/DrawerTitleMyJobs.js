import React from 'react'

const DrawerTitleMyJobs = () => {
  return (
    <div className='drawer-main__header--myjobs'>
      Activities
    </div>
  )
}

export default DrawerTitleMyJobs
