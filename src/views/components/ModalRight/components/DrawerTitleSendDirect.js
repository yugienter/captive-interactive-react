import React from 'react'
import { connect } from "react-redux";

const DrawerTitleSendDirect = ({ selectedSellers }) => {
  const sellerCodes = selectedSellers.filter(item => !!item);
  return (
    <div className='drawer-main__header' style={{ borderWidth: '1px' }}>
      <div className='drawer-main__header--info' style={{ display: 'flex', alignContent: 'center' }}>
        <p className='info-name'><strong>Send direct offer</strong><label>{sellerCodes.length}</label></p>
      </div>
    </div>
  )
}

const mapStateToProps = ({
  recommenderSeller: { selectedSellers }
}) => ({
  selectedSellers
});
const mapDispatchToProps = {
};
export default connect(mapStateToProps, mapDispatchToProps)(DrawerTitleSendDirect)
