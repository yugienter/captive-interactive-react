import { Drawer } from "antd";
import { connect } from "react-redux";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import { resetSelectedSellers } from "views/pages/RecommenderSeller/actions";
import DirectOfferDrawerContent from "./DirectOfferDrawerContent";

const DirectOfferDrawer = ({
  showDrawer,
  onCloseDrawer,
  selectedSellers,
}) => {
  const handleClose = () => {
    onCloseDrawer && onCloseDrawer();
  };

  const renderDrawerTitle = () => {
    const sellerCodes = selectedSellers.filter(item => !!item);
    return (
      <div className='drawer-main__header' style={{ borderWidth: '1px' }}>
        <div className='drawer-main__header--info' style={{ display: 'flex', alignContent: 'center' }}>
          <p className='info-name'><strong>Send direct offer</strong><label>{sellerCodes.length}</label></p>
        </div>
      </div>
    )
  };

  return (
    <>
      <Drawer
        className={"drawer-main drawer-main-tabs"}
        placement="right"
        width={660}
        onClose={handleClose}
        visible={showDrawer}
        closeIcon={<IconCloseDrawer />}
        title={renderDrawerTitle()}
      >
        <DirectOfferDrawerContent onCloseDrawer={handleClose} />
      </Drawer>
    </>
  );
};

const mapStateToProps = ({
  recommenderSeller: { selectedSellers }
}) => ({
  selectedSellers
});

const mapDispatchToProps = {
  resetSelectedSellers,
};
export default connect(mapStateToProps, mapDispatchToProps)(DirectOfferDrawer);
