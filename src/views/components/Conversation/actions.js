import { constants, client } from "helpers";

import * as types from "./actionTypes";

const { api } = constants;

export const getConversation =
  (jobCode, hostCode = false) =>
  async (dispatch, getState) => {
    try {
      dispatch({ type: types.IS_FETCH_CONVERSATION, payload: true });
      let conversationData = false;
      if (hostCode) {
        const { data } = await client.request({
          path: api.path.conversationDetail(jobCode, hostCode),
          method: "get",
        });
        conversationData = data;
      } else {
        const { data } = await client.request({
          path: api.path.conversationDetailForHost(jobCode),
          method: "get",
        });
        conversationData = data;
      }
      dispatch({
        type: types.FETCH_CONVERSATION_SUCCESS,
        payload: { ...conversationData },
      });
    } catch (err) {
      console.log("[getConversation]", err);
      dispatch({ type: types.FETCH_CONVERSATION_FAILED, payload: err.message });
    } finally {
      dispatch({ type: types.IS_FETCH_CONVERSATION, payload: false });
    }
  };

export const getMessage = (conversationCode) => async (dispatch, getState) => {
  try {
    dispatch({ type: types.IS_FETCH_MESSAGES, payload: true });
    const { data: messageData } = await client.request({
      path: api.path.message(conversationCode),
      method: "get",
    });

    dispatch({
      type: types.FETCH_MESSAGES_SUCCESS,
      payload: [...messageData.data],
    });
  } catch (err) {
    console.log("[getMessages]", err);
    dispatch({ type: types.FETCH_MESSAGES_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_MESSAGES, payload: false });
  }
};

export const resetMessage = () => (dispatch, getState) => {
  console.log("[resetMessage]");
  dispatch({ type: types.IS_FETCH_CONVERSATION, payload: true });
  dispatch({
    type: types.FETCH_CONVERSATION_SUCCESS,
    payload: false,
  });
  dispatch({
    type: types.FETCH_MESSAGES_SUCCESS,
    payload: [],
  });
};

export const createMessage =
  (conversationCode, message) => async (dispatch, getState) => {
    try {
      dispatch({ type: types.IS_CREATE_MESSAGE, payload: true });
      const { data: messageData } = await client.request({
        path: api.path.message(conversationCode),
        data: {
          message,
        },
        method: "post",
      });
      dispatch({
        type: types.CREATE_MESSAGE_SUCCESS,
        payload: { ...messageData.data },
      });
    } catch (err) {
      console.log("[createMessage]", err);
      dispatch({ type: types.CREATE_MESSAGE_FAILED, payload: err.message });
    } finally {
      dispatch({ type: types.IS_CREATE_MESSAGE, payload: false });
    }
  };
