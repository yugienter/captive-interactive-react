import { Button } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import { _mapDataHostResponse } from "views/pages/Host/actions";
import { IconSendMessage } from "../Icon/conversation";
import { getConversation, getMessage, resetMessage, createMessage } from "./actions";
import "styles/conversation.less";
import moment from "moment";
import { utils } from "helpers";

const Conversation = ({
  jobCode,
  seller = false,
  conversationInfo,
  messages,
  type = "company",
  onCloseDrawer,
  showDrawer,
  getConversation, getMessage, resetMessage, createMessage,
  company,
  isLoading,
}) => {
  const messagesEndRef = useRef(null);
  const [hostProfile, setHostProfile] = useState({});
  const [messageData, setMessageData] = useState([]);
  const [message, setMessage] = useState("");
  let intervalGetMessage = null;

  const sendMessage = async () => {
    await createMessage(conversationInfo.code, message);
    await getMessage(conversationInfo.code);
    setMessage("");
  };

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView();
  }

  const cancelAction = async () => {
    // resetMessage();
    // setMessageData([]);
    onCloseDrawer();
  }

  const getConversationData = () => {
    getConversation(jobCode, type === "company" ? seller.code : false);
  };

  const getMessageData = () => {
    getMessage(conversationInfo.code);
  };

  const getHostInfo = () => {
    const hostProfileData = _mapDataHostResponse(seller);
    setHostProfile(hostProfileData);
  }

  const getCompanyInfo = () => {
    setHostProfile({...company, avatar: company.logo });
  }

  useEffect(() => {
    if (!showDrawer) {
      setMessageData([]);
      return;
    }
    if (conversationInfo && conversationInfo.jobCode === jobCode) {
      getMessageData();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showDrawer]);

  useEffect(() => {
    setMessageData(messages);
    setTimeout(() => {
      scrollToBottom();
    }, 100);
  }, [messages]);

  useEffect(() => {
    if (!jobCode) return;
    getConversationData();
    if (seller) {
      getHostInfo();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [jobCode, seller]);

  useEffect(() => {
    if (!company) return;
    getCompanyInfo();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [company]);

  useEffect(() => {
    if (!conversationInfo) return;
    getMessageData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [conversationInfo]);

  return (
    <div className="message-page">
      <div className="message-page-content">
        {messageData.map((item) => (
          <div
            key={item.code}
            className={
              "message " + (item.sender === type ? "my-message" : "info")
            }
          >
            {item.sender !== type && (
              <div className="message-profile">
                <img
                  title={hostProfile.name}
                  alt={hostProfile.name}
                  className="rounded-circle message-avatar"
                  src={utils.getMediaUrl(hostProfile.avatar)}
                />
                <h4>{hostProfile.name}</h4>
              </div>
            )}
            <div className="message-body">
              <div className="message-body-inner">
                <div className="message-info">
                  <h5>
                    <time className="time-ago">{moment(item.createdAt).format("DD MMM YYYY [at] h:mm a")}</time>
                  </h5>
                </div>
                <div className="message-text">{item.message}</div>
              </div>
            </div>
          </div>
        ))}
        <div ref={messagesEndRef} />
      </div>
      <div className="message-page-footer">
        <textarea
          rows={3}
          onChange={(e) => setMessage(e.target.value)}
          value={message}
          placeholder="Type your message here..."
          style={{
            width: "100%",
            padding: "8px 16px",
            fontSize: "13px",
            background: "#FFFFFF",
            border: "1px solid #E8E8EA",
            borderRadius: "12px",
            marginBottom: "30px",
          }}
        ></textarea>
        <hr style={{ border: "1px solid #E8E8EA", margin: 0 }} />
        <div
          className="drawer-main__header--actions bottom"
          style={{ marginTop: "30px" }}
        >
          <Button className="cancel" onClick={cancelAction}>Cancel</Button>
          <Button
            type="primary"
            className="send-message"
            onClick={() => sendMessage()}
          >
            <IconSendMessage />
            Send Message
          </Button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ conversation }) => ({
  conversationInfo: conversation?.conversation?.data,
  company: conversation?.conversation?.company || false,
  messages: conversation.messages || [],
  isLoading: conversation.isLoading || false,
});

const mapDispatchToProps = {
  getConversation, getMessage, resetMessage, createMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Conversation);
