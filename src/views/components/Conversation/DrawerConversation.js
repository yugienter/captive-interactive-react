import React, { memo, useEffect } from "react";
import { Drawer } from "antd";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import Conversation from "./index";
import { connect } from "react-redux";
import { utils } from "helpers";

function DrawerConversation({
  record,
  showDrawer,
  onCloseDrawer,
  company,
  isLoading,
}) {

  return (
    <Drawer
      className={"drawer-main drawer-complete-job"}
      placement="right"
      width={660}
      onClose={onCloseDrawer}
      visible={showDrawer}
      closeIcon={<IconCloseDrawer />}
      title={
        company && (
          <div className="conversation-drawer-header">
            <img className="company-logo" src={utils.getMediaUrl(company.logo)} alt={company.name} />
            <h4 className="company-title">{company.name}</h4>
          </div>
        )
      }
    >
      <div className="conversation-drawer-content">
      {record && <Conversation
        showDrawer={showDrawer}
        onCloseDrawer={onCloseDrawer}
        type="host"
        jobCode={record.code}
      />}
      </div>
    </Drawer>
  );
}
const mapStateToProps = ({ conversation }) => ({
  company: conversation?.conversation?.company || false,
  isLoading: conversation.isLoading || false,
});

const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(DrawerConversation);
