import * as types from "./actionTypes";

const initialState = {
  isLoading: false,
  error: null,
  messages: [],
  conversation: false,
};

const createConversationReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.IS_FETCH_CONVERSATION:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_CONVERSATION_SUCCESS:
      return {
        ...state,
        conversation: { ...payload },
      };
    case types.FETCH_CONVERSATION_FAILED:
      return {
        ...state,
        error: payload,
      };

    case types.IS_FETCH_MESSAGES:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: [...payload],
      };
    case types.FETCH_MESSAGES_FAILED:
      return {
        ...state,
        error: payload,
      };

    case types.IS_CREATE_MESSAGE:
      return {
        ...state,
        isLoading: payload,
      };
    case types.CREATE_MESSAGE_SUCCESS:
      return {
        ...state,
      };
    case types.CREATE_MESSAGE_FAILED:
      return {
        ...state,
        error: payload,
      };

    default:
      return state;
  }
};

export default createConversationReducer;
