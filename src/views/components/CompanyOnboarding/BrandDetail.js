import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react';
import { Tag, Row, Col, Form, Input, Select, Button, Slider, Dropdown } from 'antd';
import HostTag from 'views/pages/Host/parts/HostTag';
import { ReactUtils } from 'helpers';
import { IconTag, IconTargetAudiences, IconBestSeller, IconSelectDown, IconAddPlus, IconRemove } from 'views/components/Icon/pages';
import { constants } from "helpers";



const { career } = constants

const { Option } = Select;

const { CheckableTag } = Tag;

function BrandDetail({ updateBrand, tags, brand, company, next }, ref) {
    const [tagsData, setTagsData] = useState([]);
    const [selectedTags, setSelectedTags] = useState([]);
    const [sellers, setSellers] = useState([]);
    const [formAudiences] = Form.useForm();
    const [formSeller] = Form.useForm();
    const [age, setAge] = useState([0, 100]);

    useImperativeHandle(ref, () => ({
        onContinue: () => {
            formAudiences.submit()
        }
    }));

    useEffect(() => {
        const selectedTags = [];
        const data = tags.map(parent => {
            return {
                id: parent.id,
                categories: parent.name,
                data: parent.item.map(child => {
                    const tag = {
                        id: child.id,
                        icon: HostTag({ tag: child.name }).props.icon,
                        name: child.name
                    };
                    return tag;
                })
            };
        });
        setTagsData(data);
        setSelectedTags(selectedTags);
    }, [tags, setTagsData, setSelectedTags]);

    const handleSelectedTags = (tag, checked) => {
        let nextSelectedTags = selectedTags;
        if (checked && selectedTags.length < 5) {
            nextSelectedTags = [...selectedTags, tag];
        } else if (!checked) {
            nextSelectedTags = selectedTags.filter(t => t !== tag);
        } else {
            ReactUtils.messageWarn({ content: 'Limit of 5 categories reached' });
        }
        setSelectedTags(nextSelectedTags)
    }

    const addBestSeller = () => {
        setSellers(seller => {
            const temp = [...seller]
            temp.unshift('')
            return temp;
        })
    }

    const removeBestSeller = index => {
        setSellers(seller => {
            const temp = [...seller]
            temp.splice(index, 1)
            return temp;
        })
    }

    const onFinish = async (values) => {
        let isUpdated = false
        if (selectedTags && selectedTags.length > 0) {
            brand.tags = selectedTags.map(item => item = item.id)
            isUpdated = true
        }
        if (sellers && sellers.length > 0) {
            brand.bestSellers = sellers
            isUpdated = true
        }

        if (Object.keys(values).length > 0) {
            brand.targetAudiences = {
                genders: [values.genders],
                ageMin: age[0],
                ageMax: age[1],
                carrer: values.career,
                insights: values.insights
            }
            isUpdated = true
        }

        if (isUpdated) {
            await updateBrand(brand)
        }
        next && next()
    }

    const handleBestSellerChange = (e, index) => {
        setSellers(sel => {
            sel = [...sellers]
            sel[index] = e.target.value
            return sel
        })
    }

    const handleChangeAge = (e) => {
        setAge(e)
    }

    return (
        <>
            <h2>Tell us more about the brand you represent</h2>
            <div className='text-bold mb36'>You can also add more brand, edit brand information in Company Profile</div>
            <div>
                <p><span className="mr10"><IconTag /></span>Brand Tags</p>
                <div className="pl30">
                    {tagsData.map(tag => (
                        company && company.tags && company.tags.includes(tag.id) &&
                        <>
                            <div className='topics-categories'>{tag.categories}</div>

                            <div className='topics-tags'>
                                {tag.data.map(item => (
                                    <CheckableTag
                                        key={item.id}
                                        className={selectedTags.length > 5 ? 'none-cheked' : ''}
                                        checked={selectedTags.indexOf(item) > -1}
                                        onChange={checked => handleSelectedTags(item, checked)}
                                    >
                                        {item.icon} {item.name}
                                    </CheckableTag>
                                ))}
                            </div>
                        </>
                    ))}
                </div>
            </div>
            <div>
                <p><span className="mr10"><IconTargetAudiences /></span>Target Audiences</p>
                <div className="pl30">

                    <Form onFinish={onFinish} form={formAudiences} layout="vertical">
                        <Row justify="space-between">
                            <Col span={7}>
                                <Dropdown overlay={<div style={{
                                    background: '#FFF',
                                    height: 60,
                                    padding: 10
                                }}><Slider marks={{
                                    0: '0',
                                    100: '100'
                                }} onChange={handleChangeAge} range value={age} /></div>} placement="bottomLeft">
                                    <Form.Item label="Age Range">
                                        <Input
                                            readOnly
                                            value={`${age[0]} - ${age[1]}`}
                                            size="large"
                                            className="input-basic"
                                        />
                                    </Form.Item>
                                </Dropdown>
                            </Col>
                            <Col span={7}>
                                <Form.Item name="genders" label="Gender">
                                    <Select
                                        placeholder="Select gender"
                                        className='select-noicon'
                                        style={{ width: '100%' }}
                                        suffixIcon={<IconSelectDown />}
                                    >
                                        <Option value="all">All</Option>
                                        <Option value="female">Female</Option>
                                        <Option value="male">Male</Option>
                                        <Option value="ohthers">Ohthers</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={7}>
                                {/* <Form.Item name="career" label="Career">
                                    <Select
                                        placeholder="Select career"
                                        className='select-noicon'
                                        style={{ width: '100%' }}
                                        suffixIcon={<IconSelectDown />}
                                    >
                                        {
                                            career && career.map(item => (
                                                <Option value={item}>{item}</Option>
                                            ))
                                        }

                                    </Select>
                                </Form.Item> */}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item name="insights" label="Insights">
                                    <Input
                                        size="large"
                                        className="input-basic"
                                        placeholder="Locals from all walks of life"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
            <div>
                <p><span className="mr10"><IconBestSeller /></span>Best Sellers</p>
                <div className="pl30">
                    <Button onClick={addBestSeller} type="primary" className="add-more"><IconAddPlus />Add more</Button>
                    <Form form={formSeller} layout="vertical">
                        {
                            sellers && sellers.map((item, index) => (
                                <Form.Item >
                                    <Input
                                        onChange={(e) => handleBestSellerChange(e, index)}
                                        value={item}
                                        size="large"
                                        className="input-basic"
                                        placeholder="Enter Seller Name"
                                        suffix={
                                            <a onClick={() => removeBestSeller(index)}><IconRemove /></a>
                                        }
                                    />
                                </Form.Item>
                            ))
                        }
                    </Form>
                </div>
            </div>
        </>
    );
}

export default forwardRef(BrandDetail);