import React, { useState, useImperativeHandle, useEffect, forwardRef } from 'react';
import { Form, Checkbox, Row, Col } from "antd";


function CompanyCategories({ tags, company, updateCompany, next, errorMsg }, ref) {
    const [tagsData, setTagsData] = useState([]);
    const [selectedTags, setSelectedTags] = useState(company ? company.tags : []);
    const [form] = Form.useForm();

    useImperativeHandle(ref, () => ({
        onContinue: () => {
            form.submit();
        }
    }));

    useEffect(() => {
        const data = tags.map(t => {
            return {
                id: t.id,
                categories: t.name,
            };
        });
        setTagsData(data);
        selectedTags && selectedTags.forEach((item) => {
            form.setFieldsValue({ [`${item}`]: true });
        })
    }, [tags, setTagsData, setSelectedTags]);

    const isNonUpdated = (array1, array2) => {
        const array2Sorted = array2.slice().sort();
        return array1.length === array2.length && array1.slice().sort().every(function (value, index) {
            return value === array2Sorted[index];
        });
    }

    const onFinish = async (values) => {
        const categories = []
        let res = ''
        Object.keys(values).length > 0 && Object.keys(values).forEach(item => {
            if (values[item]) {
                categories.push(item)
            }
        })
        if (!isNonUpdated(categories, company ? company.tags || [] : [])) {
            const payload = { ...company }
            payload.tags = categories
            res = await updateCompany(payload)
        }
        if (!res) {
            next && next();
        }
    }
    return (
        <div>
            <h2>Tell us more about the products you represent</h2>
            <div className='text-bold mb36'>What categories do they belong to?</div>
            { errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
            <Form form={form} layout="vertical" onFinish={onFinish}>
                <Row justify="space-between">
                    {
                        tagsData && tagsData.map((item, index) => (
                            <Col key={index} span={11}>
                                <Form.Item
                                    name={item.id}
                                    valuePropName="checked"
                                >
                                    <Checkbox key={item.id} checked={selectedTags && selectedTags.includes(item.id)} value={item.id} className="checkbox-basic">{item.categories}</Checkbox>
                                </Form.Item>
                            </Col>
                        ))
                    }
                </Row>
            </Form>
        </div>
    );
}

export default forwardRef(CompanyCategories);