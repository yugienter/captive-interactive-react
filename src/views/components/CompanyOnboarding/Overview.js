import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react'
import { Form, Input, Row, Col, Upload, message, Image, DatePicker } from "antd";
import { constants, COUNTRY_CODE } from 'helpers';
import { IconPorfolio, IconUpload, IconFlagAlt, IconCalendar, IconNumber } from 'views/components/Icon/pages';
import { LoadingOutlined } from '@ant-design/icons';
import { CountryDropdown } from 'react-country-region-selector';
import ReactPhoneInput from "react-phone-input-2";
import moment from 'moment'
import { utils, ReactUtils } from 'helpers'
const { TextArea } = Input;
const { getMediaUrl } = utils

const { MEDIA_TYPE } = constants;

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}


function Overview({ user, uploadMedia, next, company, createCompany, updateCompany, errorMsg }, ref) {
    const [form1] = Form.useForm();
    const [form2] = Form.useForm();
    const [imageUrl, setImageUrl] = useState(company && company.logo);
    const [logoUrl, setLogoUrl] = useState(company && company.logo);
    const [loading, setLoading] = useState(false);
    const [country, setCountry] = useState(company ? company.country : 'Singapore');
    const [countryCode, setCountryCode] = useState('sg');

    useImperativeHandle(ref, () => ({
        onContinue: () => {
            form1.submit();
            form2.submit();
        }
    }));

    useEffect(() => {
        form2.setFieldsValue({
            name: company ? company.name : '',
            registrationNo: company ? company.registrationNo : '',
            countryOrigin: company ? company.countryOrigin : 'Singapore',
            phoneNumber: company ? company.phoneNumber : '',
            yearEstablisment: (company && company.yearEstablisment) ? moment(`1/1/${company.yearEstablisment}`) : '',
            website: company ? company.website : '',
        });
        setCountry(company ? company.countryOrigin : 'Singapore');
        setImageUrl(company ? getMediaUrl(company.logo) : null);
        setLogoUrl(company ? company.logo : null);

    }, [company, form2]);

    const handleChangeCountry = (value) => {
        setCountryCode(COUNTRY_CODE[value]);
        setCountry(value);
        form2.setFieldsValue({ countryOrigin: value });
    }


    const validateAvatar = (rule, value, callback) => {
        if (!imageUrl) return callback('Avatar is missing');
        callback();
    }
    const upload = () => {
        const url = uploadMedia({ ownerType: MEDIA_TYPE.COMPANY, ownerCode: user.code, noCreate: true });
        return url;
    }

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            setLogoUrl(info.file.response.data.url)
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                setImageUrl(imageUrl)
            },
                setLoading(false)
            );
            form1.resetFields();
        }
    };

    const onFinish = async (values) => {
        const update = {}
        let res = ''
        let cloneCompany = company || {}
        if (logoUrl !== cloneCompany.logo) {
            update.logo = logoUrl;
        }
        if (values.registrationNo !== cloneCompany.registrationNo) {
            update.registrationNo = values.registrationNo;
        }
        if (values.countryOrigin !== cloneCompany.countryOrigin) {
            update.countryOrigin = values.countryOrigin;
        }
        if (values.yearEstablisment && values.yearEstablisment.year() !== cloneCompany.yearEstablisment) {
            update.yearEstablisment = values.yearEstablisment.year();
        }
        if (values.phoneNumber !== cloneCompany.phoneNumber) {
            update.phoneNumber = values.phoneNumber;
        }
        if (values.website !== cloneCompany.website) {
            update.website = values.website;
        }
        if (Object.keys(update).length) {
            if (cloneCompany && cloneCompany.registrationNo) {
                res = await updateCompany({ ...cloneCompany, ...update });
            }
            else {
                update.name = 'Captive';
                res = await createCompany(update);
            }
        }
        if (!res) {
            next && next();
        }
    };

    const uploadButton = (
        <div className='upload'>
            {loading ? <LoadingOutlined /> : <IconUpload />}
            <div className='text'>Upload</div>
        </div>
    );
    const disabledDate = (current) => {
        return current && current.valueOf() >= Date.now();
    }
    return (
        <>
            <h2>We'd love to know more about your company</h2>
            <div className='text-bold mb36'>Here are some details we'll need to get started</div>
            { errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
            <Row>
                <Col span={8}>
                    <Form form={form1} layout="vertical">
                        <Form.Item
                            label="Company Logo"
                            name="logo"
                            className='form-textarea section-scroll_overview'
                            rules={[{
                                required: true,
                                validator: validateAvatar
                            }]}
                        >

                            <Upload
                                accept={constants.FILE_TYPES}
                                name="file"
                                listType="picture-card"
                                className="avatar-uploader avatar-image"
                                showUploadList={false}
                                action={upload}
                                beforeUpload={beforeUpload}
                                onChange={handleChange}
                            >
                                {imageUrl ? <Image width={194} preview={false} src={imageUrl} className='avatar-image_view' alt="Company Logo" /> : uploadButton}
                            </Upload>

                        </Form.Item>
                    </Form>
                </Col>
                <Col span={16}>
                    <Form onFinish={onFinish} form={form2} layout="vertical">
                        <Form.Item name="registrationNo" label="Company Registration No." rules={[{ required: true, message: 'Registration No is missing' }]}>
                            <Input
                                size="large"
                                className="input-basic"
                                prefix={<IconNumber />}
                                placeholder="eg: UEN 202X12345A"
                            />
                        </Form.Item>
                        <Form.Item
                            label="Country"
                            className='form-select'
                            name="countryOrigin"
                            rules={[{ required: true, message: 'Country is missing' }]}
                        >
                            <CountryDropdown
                                name="my-country-field"
                                classes="select-style"
                                showDefaultOption={true}
                                value={country}
                                onChange={handleChangeCountry}
                            />
                            <IconFlagAlt />
                        </Form.Item>
                        <Form.Item
                            label="Phone Number"
                            name="phoneNumber"
                            rules={[{ required: true, message: 'Phone Number is missing' }]}
                        >
                            <ReactPhoneInput
                                country={countryCode}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Year of Establishment"
                            name="yearEstablisment"
                            rules={[{ required: true, message: 'Year of Establishment is missing' }]}
                        >
                            <DatePicker
                                placeholder='YYYY'
                                style={{ fontWeight: 'normal' }}
                                format="YYYY"
                                disabledDate={disabledDate}
                                picker="year"
                                suffixIcon={<IconCalendar />}
                            />
                        </Form.Item>
                        <Form.Item name="website" label="Website"
                            rules={[
                                { validator: ReactUtils.urlValidator }
                            ]}
                        >
                            <Input
                                size="large"
                                className="input-basic"
                                prefix={<IconPorfolio />}
                                placeholder="(Optional)"
                            />
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </>
    );
}

export default forwardRef(Overview);