import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react'
import { Form, Input, Row, Col, Upload, message, Image, DatePicker } from "antd";
import { constants, COUNTRY_CODE } from 'helpers';
import { IconPorfolio, IconUpload, IconFlagAlt, IconCalendar, IconNumber } from 'views/components/Icon/pages';
import { LoadingOutlined } from '@ant-design/icons';
import { CountryDropdown } from 'react-country-region-selector';
import { utils, ReactUtils } from 'helpers'
const { TextArea } = Input;
const { getMediaUrl } = utils

const { MEDIA_TYPE } = constants;

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function CreateBrand({ company, uploadMedia, next, createBrand, brand, updateBrand, errorMsg }, ref) {
    const [form1] = Form.useForm();
    const [form2] = Form.useForm();
    const [imageUrl, setImageUrl] = useState();
    const [loading, setLoading] = useState(false);
    const [country, setCountry] = useState(company ? company.country : 'Singapore');
    const [countryCode, setCountryCode] = useState('sg');
    const [logoUrl, setLogoUrl] = useState();

    useImperativeHandle(ref, () => ({
        onContinue: () => {
            form1.submit();
            form2.submit();
        }
    }));

    const handleChangeCountry = (value) => {
        setCountryCode(COUNTRY_CODE[value]);
        setCountry(value);
        form2.setFieldsValue({ countryOrigin: value });
    }

    useEffect(() => {
        form2.setFieldsValue({
            logo: logoUrl,
            name: brand ? brand.name : '',
            concept: brand ? brand.concept : '',
            countryOrigin: brand ? brand.countryOrigin : 'Singapore',
            facebook: brand ? brand.facebook : '',
            instagram: brand ? brand.instagram : '',
        });
        setImageUrl((brand && brand.logo) ? getMediaUrl(brand.logo) : '');
        setCountry(brand ? brand.countryOrigin : 'Singapore');
        setLogoUrl(brand ? brand.logo : null);

    }, [brand, setImageUrl, form2]);

    const upload = () => {
        const url = uploadMedia({ ownerType: MEDIA_TYPE.HOST, noCreate: true });
        return url;
    }

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            setLogoUrl(info.file.response.data.url)
            getBase64(info.file.originFileObj, imageUrl =>
                setImageUrl(imageUrl),
                setLoading(false)
            );
            form1.resetFields();
        }
    };

    const onFinish = async (values) => {
        const update = {}
        let res = ''
        const cloneBrand = brand || {}
        if (logoUrl !== cloneBrand.logo) {
            update.logo = logoUrl
        }
        if (values.name !== cloneBrand.name) {
            update.name = values.name
        }
        if (values.concept !== cloneBrand.concept) {
            update.concept = values.concept
        }
        if (values.countryOrigin !== cloneBrand.countryOrigin) {
            update.countryOrigin = values.countryOrigin
        }
        if (values.facebook !== cloneBrand.facebook) {
            update.facebook = values.facebook
        }
        if (values.instagram !== cloneBrand.instagram) {
            update.instagram = values.instagram
        }

        if (Object.keys(update).length) {
            update.companyCode = company.code
            if (cloneBrand && cloneBrand.code) {
                res = await updateBrand({ ...cloneBrand, ...update });
            }
            else {
                res = await createBrand(update);
            }
        }
        if (!res) {
            next && next();
        }
    };

    const uploadButton = (
        <div className='upload'>
            {loading ? <LoadingOutlined /> : <IconUpload />}
            <div className='text'>Upload</div>
        </div>
    );

    return (
        <>
            <h2>Tell us more about a brand that you represent or own</h2>
            <div className='text-bold mb36'>Don't worry, you can change or add to this information anytime on the SCX platform</div>
            { errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
            <Row>
                <Col span={8}>
                    <Form form={form1} layout="vertical">
                        <Form.Item
                            label="Brand Logo"
                            name="logo"
                            className='form-textarea section-scroll_overview'
                        >

                            <Upload
                                accept={constants.FILE_TYPES}
                                name="file"
                                listType="picture-card"
                                className="avatar-uploader avatar-image"
                                showUploadList={false}
                                action={upload}
                                beforeUpload={beforeUpload}
                                onChange={handleChange}
                            >
                                {imageUrl ? <Image width={194} preview={false} src={imageUrl} className='avatar-image_view' alt="Company Logo" /> : uploadButton}
                            </Upload>

                        </Form.Item>
                    </Form>
                </Col>
                <Col span={16}>
                    <Form onFinish={onFinish} form={form2} layout="vertical">
                        <Form.Item name="name" label="Brand Name" rules={[{ required: true, message: 'Brand Name is missing' }]}>
                            <Input
                                size="large"
                                className="input-basic"
                                placeholder="e.g. Captive LIVE+"
                            />
                        </Form.Item>
                        <Form.Item
                            name="concept"
                            label="Brand Concept"
                        >
                            <TextArea style={{
                                height: 82
                            }} rows={3} />
                        </Form.Item>
                        <Form.Item
                            label="Country"
                            className='form-select'
                            name="countryOrigin"
                        >
                            <CountryDropdown
                                name="my-country-field"
                                classes="select-style"
                                showDefaultOption={true}
                                value={country}
                                onChange={handleChangeCountry}
                            />
                            <IconFlagAlt />
                        </Form.Item>
                        <Form.Item name="facebook" label="Facebook"
                            rules={[
                                { validator: ReactUtils.urlValidator }
                            ]}>
                            <Input
                                size="large"
                                className="input-basic"
                                prefix={<IconPorfolio />}
                                placeholder="(Optional)"
                            />
                        </Form.Item>
                        <Form.Item name="instagram" label="Instagram"
                            rules={[
                                { validator: ReactUtils.urlValidator }
                            ]}>
                            <Input
                                size="large"
                                className="input-basic"
                                prefix={<IconPorfolio />}
                                placeholder="(Optional)"
                            />
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </>
    );
}

export default forwardRef(CreateBrand);