import arrowRight from "./basic/arrowRight.svg";
import arrowRightWhite from "./basic/arrowRightWhite.svg";

const IconLock = () => (
  <span role="img" className="anticon">
    <svg
      width="13"
      height="15"
      viewBox="0 0 13 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.5 6.5V3.5C3.5 1.84315 4.84315 0.5 6.5 0.5C8.15685 0.5 9.5 1.84315 9.5 3.5V6.5M1.5 6.5H11.5C12.0523 6.5 12.5 6.94772 12.5 7.5V13.5C12.5 14.0523 12.0523 14.5 11.5 14.5H1.5C0.947715 14.5 0.5 14.0523 0.5 13.5V7.5C0.5 6.94772 0.947715 6.5 1.5 6.5Z"
        stroke="#77757F"
      />
    </svg>
  </span>
);

const IconArrowRight = () => (
  <span role="img" className="anticon">
    <img src={arrowRight} width="14" alt="icon" />
  </span>
);

const IconArrowRightWhite = () => (
  <span role="img" className="anticon opacity0">
    <img src={arrowRightWhite} width="14" alt="icon" />
  </span>
);

const IconDown = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="15"
      height="10"
      viewBox="0 0 15 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M14 1.5L7.5 8.5L1 1.5" stroke="#1D1929" strokeLinecap="square" />
    </svg>
  </span>
);

const IconStar = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="15"
      height="15"
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.5 12.0405L3.17354 14.3151L4 9.4973L0.5 6.08562L5.33677 5.38324L7.5 1L9.66323 5.38324L14.5 6.08562L11 9.4973L11.8265 14.3151L7.5 12.0405Z"
        stroke="#77757F"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  </span>
);

const IconRefresh = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="12"
      height="12"
      viewBox="0 0 12 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.16666 1.54449C3.61388 1.54449 1.54444 3.61392 1.54444 6.16671H0.833328C0.833328 3.22119 3.22114 0.833374 6.16666 0.833374C7.71212 0.833374 9.10413 1.49105 10.0778 2.54072V0.833374H10.7889V3.67782L7.94444 3.67782V2.96671L9.50209 2.96671C8.66053 2.0897 7.47735 1.54449 6.16666 1.54449ZM6.16666 10.7889C8.71944 10.7889 10.7889 8.71949 10.7889 6.16671H11.5C11.5 9.11223 9.11218 11.5 6.16666 11.5C4.62121 11.5 3.22919 10.8424 2.25555 9.7927V11.5H1.54444V8.6556H4.38888V9.36671H2.83123C3.67279 10.2437 4.85597 10.7889 6.16666 10.7889Z"
        fill="white"
      />
    </svg>
  </span>
);

const IconUserName = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="11"
      height="15"
      viewBox="0 0 11 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.5 3.49804C8.5 5.15396 7.157 6.49609 5.5 6.49609C3.843 6.49609 2.5 5.15396 2.5 3.49804C2.5 1.84212 3.843 0.5 5.5 0.5C7.157 0.5 8.5 1.84212 8.5 3.49804Z"
        stroke="#77757F"
        stroke-linecap="square"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.5 14.4909H0.5C0.5 13.7808 0.5 13.1053 0.5 12.4936C0.5 10.8368 1.84315 9.49414 3.5 9.49414H7.5C9.15685 9.49414 10.5 10.8368 10.5 12.4936C10.5 13.1053 10.5 13.7808 10.5 14.4909Z"
        stroke="#77757F"
        stroke-linecap="square"
      />
    </svg>
  </span>
);

export {
  IconLock,
  IconArrowRight,
  IconArrowRightWhite,
  IconDown,
  IconStar,
  IconRefresh,
  IconUserName,
};
