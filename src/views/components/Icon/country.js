import singapore from './flag/singapore.svg'
const FlagSingapore = ({ margin }) => (
    <span role="img" className="anticon">
        <img src={singapore} width="20" height="15" alt="icon" />
    </span>

)
export {
    FlagSingapore
}
