const IconConversation = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="28"
      height="28"
      viewBox="0 0 28 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 13.9824C6 9.56414 9.58172 5.98242 14 5.98242C18.4183 5.98242 22 9.56414 22 13.9824V21.9824H14C9.58172 21.9824 6 18.4007 6 13.9824ZM10.2667 14.5158H11.3333V13.4491H10.2667V14.5158ZM17.7333 14.5158H16.6667V13.4491H17.7333V14.5158ZM13.4667 14.5158H14.5333V13.4491H13.4667V14.5158Z"
        fill="#615E69"
      />
    </svg>
  </span>
);
const IconConversationNew = () => (
  <span role="img" className="anticon opacity0">
    <svg
      width="28"
      height="28"
      viewBox="0 0 28 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 13.9824C6 9.56414 9.58172 5.98242 14 5.98242C18.4183 5.98242 22 9.56414 22 13.9824V21.9824H14C9.58172 21.9824 6 18.4007 6 13.9824ZM10.2667 14.5158H11.3333V13.4491H10.2667V14.5158ZM17.7333 14.5158H16.6667V13.4491H17.7333V14.5158ZM13.4667 14.5158H14.5333V13.4491H13.4667V14.5158Z"
        fill="#615E69"
      />
      <rect
        x="17"
        y="3"
        width="8"
        height="8"
        rx="4"
        fill="#F42829"
        stroke="white"
      />
    </svg>
  </span>
);

const IconSendMessage = () => (
  <span role="img" className="anticon absolute left">
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M19.3327 0.666748L19.8842 0.9031C19.9808 0.677594 19.9304 0.415967 19.7569 0.242484C19.5835 0.0690004 19.3218 0.0186158 19.0963 0.115261L19.3327 0.666748ZM0.666016 8.66675L0.429664 8.11526C0.222565 8.20402 0.0826494 8.40132 0.0673926 8.62612C0.0521359 8.85092 0.16411 9.06532 0.357318 9.18124L0.666016 8.66675ZM11.3327 19.3334L10.8182 19.6421C10.9341 19.8353 11.1485 19.9473 11.3733 19.932C11.5981 19.9168 11.7954 19.7769 11.8842 19.5698L11.3327 19.3334ZM19.0963 0.115261L0.429664 8.11526L0.902367 9.21824L19.569 1.21824L19.0963 0.115261ZM0.357318 9.18124L7.02398 13.1812L7.64138 12.1523L0.974713 8.15225L0.357318 9.18124ZM6.81819 12.9754L10.8182 19.6421L11.8472 19.0247L7.84718 12.358L6.81819 12.9754ZM11.8842 19.5698L19.8842 0.9031L18.7812 0.430396L10.7812 19.0971L11.8842 19.5698ZM18.9084 0.242484L6.90842 12.2425L7.75695 13.091L19.7569 1.09101L18.9084 0.242484Z"
        fill="white"
      />
    </svg>
  </span>
);

const IconSendMessageSeller = () => (
  <span role="img" className="anticon absolute left">
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g opacity="0.8">
        <path
          d="M9.46654 9.98229H10.5332M6.26654 9.98229H7.3332M12.6665 9.98229H13.7332M17.4665 17.449H9.99987C5.87614 17.449 2.5332 14.106 2.5332 9.98229C2.5332 5.85857 5.87614 2.51562 9.99987 2.51562C14.1236 2.51562 17.4665 5.85857 17.4665 9.98229V17.449Z"
          stroke="#615E69"
        />
      </g>
    </svg>
  </span>
);

export {
  IconConversation,
  IconConversationNew,
  IconSendMessage,
  IconSendMessageSeller,
};
