import React, { useState, forwardRef, useImperativeHandle } from 'react'
import { Upload, Modal } from "antd";
import { IconUploadMedia } from 'views/components/Icon/pages';
import { constants, utils, ReactUtils } from 'helpers';

const { MEDIA_TYPE } = constants;

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const PhotosVideos = ({ addHostProfileMedia, uploadMedia, deleteMedia, removeHostProfileMedia, host, next }, ref) => {
  useImperativeHandle(ref, () => ({
    onContinue: () => {
      next && next()
    }
  }));

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');

  const media = host ? host.media.map(item => {
    return {
      uid: item.code,
      url: utils.getMediaUrl(item.url),
    };
  }) : [];
  const [fileList, setFileList] = useState(media);
  const handleCancel = () => setPreviewVisible(false)
  const handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview)
    setPreviewVisible(true)
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1))
  };
  const handleChange = ({ file, fileList }) => {
    if (file) {
      if (file.status === 'done') {
        const data = file.response.data;
        file.url = utils.getMediaUrl(data.url);
        file.uid = data.code;
        addHostProfileMedia(data);
      } else if (file.status === 'removed') {
        removeHostProfileMedia(file.uid);
      } else if (file.status === 'error') {
        file.thumbUrl = null;
      }
    }
    setFileList(fileList);
  };
  const uploadButton = (
    <div>
      <IconUploadMedia />
      <div>Add photo or video</div>
    </div>
  );

  const upload = () => {
    const url = uploadMedia({ ownerType: MEDIA_TYPE.HOST, ownerCode: host.code });
    return url;
  }

  const handleRemove = file => {
    return deleteMedia(file.uid);
  }

  const beforeUpload = (file, newFileList) => {
    if (newFileList.length + fileList.length > 8) {
      ReactUtils.messageWarn({ content: 'You can only upload total 8 files' });
      return Upload.LIST_IGNORE;
    }
  }

  return (
    <>
      <h2>Photos and Videos</h2>
      <div className='text-bold mb36'>Help your portfolio looks attractive and professional (Ideal photo size is 1024x1024px and Max video size is 5MB)</div>
      <div className='box-media'>
        <Upload
          accept={constants.FILE_TYPES}
          listType="picture-card"
          fileList={fileList}
          beforeUpload={beforeUpload}
          onPreview={handlePreview}
          onChange={handleChange}
          onRemove={handleRemove}
          showUploadList={{ showRemoveIcon: true }}
          action={upload}
          multiple
          className='personal-infomation'
        >
          {fileList.length >= 8 ? null : uploadButton}
        </Upload>
        <div
          disabled={fileList.length < 0}
          className={fileList.length <= 0 ? 'before-upload-box' : 'before-upload-box hidden'}
        >
          <div className='box-none-file'>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        {
          utils.isImage(previewImage) ? (
            <img alt="example" style={{ width: '100%' }} src={previewImage} />
          ) : utils.isVideo(previewImage) ? (
            <video
              controls
              width={'100%'}
              src={previewImage}
            />
          ) : ''
        }
      </Modal>
    </>
  )
}

export default forwardRef(PhotosVideos);