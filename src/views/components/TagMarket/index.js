
const TagMarket = ({ isHideFixedRate, isHideComission }) => {

    return (
        <div className="tags-market">
            {isHideFixedRate || <span className="blue-tab">Fixed Rate</span>}
            {isHideComission || <span className="orange-tab" >Commission</span>}
        </div>

    )
};

export default TagMarket;