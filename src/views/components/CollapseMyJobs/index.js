import React, { useState } from "react";
import { Collapse } from "antd";
import { IconUpCollapse } from "views/components/Icon/myjobs";
import HeaderReceived from "./components/HeaderReceived";
import HeaderProposal from "./components/HeaderProposal";
import ContentDeal from "./components/ContentDeal";

const { Panel } = Collapse;

const Index = () => {
  const [showProposal, setShowProposal] = useState(true);
  function callback(key) {
    console.log(key);
  }

  return (
    <>
      <Collapse
        defaultActiveKey={["1"]}
        onChange={callback}
        expandIcon={() => <IconUpCollapse />}
        expandIconPosition="right"
        className="collapse-main"
      >
        <Panel
          header={showProposal ? <HeaderProposal /> : <HeaderReceived />}
          key="1"
        >
          <ContentDeal />
        </Panel>
      </Collapse>
    </>
  );
};

export default Index;
