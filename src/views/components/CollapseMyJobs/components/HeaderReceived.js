import React from "react";
import { IconMessageSend } from "views/components/Icon/myjobs";

const HeaderReceived = () => {
  return (
    <div className="header-main">
      <div className="default">
        <div className="first">
          <IconMessageSend />
        </div>
        <div className="center">
          <p className="send">
            <strong>Company</strong> sent you a deal
          </p>
          <p className="date">Today at 11:23am</p>
        </div>
      </div>
    </div>
  );
};

export default HeaderReceived;
