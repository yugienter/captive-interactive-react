import React, { useEffect } from "react";
import {
  IconUpBig,
  IconUploadProof,
  IconDeleteProof,
  IconDownloadProof,
  IconCopy,
} from "views/components/Icon/pages";
import { Modal } from "antd";
import _ from "lodash";
import { connect, useSelector } from "react-redux";
import {
  resetProof,
  uploadMedia,
  removeProof,
} from "../../../pages/JobOrder/actions";
import { utils, ReactUtils, constants } from "helpers";
import { useParams } from "react-router-dom";
import axios from "axios";
import { updateProofByCompany } from "views/pages/DetailProduct/components/actions";

const { MEDIA_TYPE } = constants;
const { getMediaUrl } = utils;

function PopupConfirmOrder({
  payment,
  visible,
  handleClose,
  handleOK,
  orderPricing,
  resetProof,
  proof,
  uploadMedia,
  removeProof,
}) {
  const { code } = useParams();
  const { user } = useSelector((state) => state.common);

  useEffect(() => {
    resetProof();
  }, []);

  const onProofChange = async (e) => {
    const file = _.get(e, "target.files[0]");
    const formData = new FormData();
    formData.append("file", file);
    const params = {
      ownerType: MEDIA_TYPE.COMPANY,
      ownerCode: user.code,
    };
    const url = await uploadMedia(formData, params);
    await updateProof([...proof, url]);
  };

  const copyToClipBoard = (text) => {
    ReactUtils.messageSuccess({ content: "Copied to clipboard" });
    navigator.clipboard.writeText(text);
  };

  const download = (uri) => {
    try {
      axios({
        url: uri,
        method: "GET",
        responseType: "blob",
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        uri = uri.split(".");
        const type = uri[uri.length - 1];
        link.setAttribute("download", "proof." + type);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
    } catch (err) {
      console.log(err);
    }
  };

  const deleteProof = async (url) => {
    await removeProof(url);
    await updateProof(proof.filter(x => x !== url));
  }

  const updateProof = async (proof) => {
    const result = await updateProofByCompany({ paymentCode: payment.code, proof });
    if (!result.code) {
      ReactUtils.messageWarn({ content: 'Update proof fail' });
      return;
    }
    ReactUtils.messageSuccess({ content: 'Update proof success' });
  }

  return (
    <Modal
      centered
      visible={visible}
      footer={(null, null)}
      onCancel={handleClose}
    >
      <div className="confirm-order payment-detail__container">
        <div className="payment-detail__info">
          <h4>Payment Sent</h4>
          <span>
            <IconUpBig />
          </span>
          <p className="payment-detail__info__title">Fixed Rate</p>
          <p>Send the amount shown to the banking details below:</p>
          <p>Pay amount:</p>
          <p className="payment-detail__info__amount">
            ${payment.total}
          </p>
        </div>
        <div className="payment-detail__card">
          <div className="payment-detail__card__status">
            <table>
              <tr>
                <td>Payment ID</td>
                <td>
                  <strong>{`PR${payment.code}`}</strong>&nbsp;
                  <a onClick={() => copyToClipBoard(`PR${payment.code}`)}>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__info">
            <table>
              <tr>
                <td>Account Holder</td>
                <td><strong>Captive Interactive Co.</strong></td>
              </tr>
              <tr>
                <td>Bank Name</td>
                <td><strong>International Bank of Singapore</strong></td>
              </tr>
              <tr>
                <td>City</td>
                <td><strong>Singapore</strong></td>
              </tr>
              <tr>
                <td>Bank Account No.</td>
                <td>
                <strong>SGB1232085455555</strong>{" "}
                  <a onClick={() => copyToClipBoard("SGB1232085455555")}>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__proof">
            {!(proof && proof.length > 0) ? (
              <div className="payment-detail__card__proof--nofile">
                <p>
                Remember to check the numbers carefully and include the Payment ID in your transfer’s note.
                </p>
              </div>
            ) : (
              <>
                <p className="payment-detail__card__proof__title">
                  Proof Upload
                </p>
                {proof.map((item, index) => (
                  <div
                    style={{
                      marginBottom: 15,
                    }}
                  >
                    <div className="payment-detail__card__proof__file">
                      <div className="payment-detail__card__proof__file__preview">
                        <img src={getMediaUrl(item)} alt="proof" />
                      </div>
                      <div className="payment-detail__card__proof__file__info">
                        <p>Proof {index + 1}</p>
                      </div>
                      <div className="payment-detail__card__proof__file__action">
                        {/* <a><IconEditProof /></a> */}
                        <a onClick={() => download(getMediaUrl(item))}>
                          <IconDownloadProof />
                        </a>
                        <a onClick={() => deleteProof(item)}>
                          <IconDeleteProof />
                        </a>
                      </div>
                    </div>
                  </div>
                ))}
              </>
            )}
            <div
              className="payment-detail__card__proof--nofile"
              style={{
                marginTop: 15,
              }}
            >
              <input
                onChange={onProofChange}
                style={{ display: "none" }}
                id="proof"
                type="file"
                name="myImage"
                accept="image/png, image/gif, image/jpeg, application/pdf"
              />
              <div>
                <label
                  for="proof"
                  style={{
                    width: "50%",
                  }}
                  className="payment-detail__card__proof__action"
                >
                  <span>
                    <IconUploadProof />
                  </span>{" "}
                  Upload Proof
                </label>
                <a onClick={handleOK}>Done</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
}

const mapStateToProps = ({ jobOrder }) => ({
  job: jobOrder.job,
  orderPricing: jobOrder.orderPricing,
  proof: jobOrder.proof,
});

const mapDispatchToProps = {
  resetProof,
  removeProof,
  uploadMedia,
};
export default connect(mapStateToProps, mapDispatchToProps)(PopupConfirmOrder);
