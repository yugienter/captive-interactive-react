import { useState } from 'react';
import { connect } from 'react-redux';
import {
  IconYoutube,
  IconBtnPropose,
  IconBtnAccept,
  IconBtnReject,
  IconBtnVerify
} from 'views/components/Icon/collapse';
import BoxStatus from './BoxStatus';
import moment from 'moment';
import { constants, RATE_SCORES, ReactUtils, statusJob, utils } from 'helpers';
import {
  acceptDeal,
  rejectDeal,
  verifyJob,
  rateHost,
  submitFixedRatePayment,
  findPaymentByCode,
} from 'views/pages/DetailProduct/components/actions';
import DrawerProposeDeal from 'views/components/DrawerProposeDeal';
import { IconExpand } from 'views/components/Icon/myjobs';
import { Button, Rate } from 'antd';
import {
  IconLiveStream,
  IconRating, IconSendMessage, IconStreamUpload
} from 'views/components/Icon/status'
import TextExpandable from 'views/components/TextExpandable';
import PopupConfirmOrder from './PopupConfirmOrder';
import DrawerSubmitFixedRate from './DrawerSubmitFixedRate';
import { getSellersOfCampaign } from "../../../pages/DetailProduct/components/actions";
import DrawerPaymentDetail from 'views/components/DrawerPaymentDetail/DrawerPaymentFixedRateDetail';
import { setProof } from 'views/pages/JobOrder/actions';

const { statusDeal } = constants;

const ContentDeal = ({
  getSellersOfCampaign,
  deal,
  seller = false,
  noFooter = false,
  acceptDeal,
  rejectDeal,
  verifyJob,
  rateHost,
  setProof,
}) => {
  const [showMore, setShowMore] = useState(false);
  const [submitFixedRatePopup, setSubmitFixedRatePopup] = useState(false);
  const [confirmOrderPopup, setConfirmOrderPopup] = useState(false)
  const [showProposeADeal, setShowProposeADeal] = useState(false);
  const [rateScore, setRateScore] = useState(0);
  const [fixedRatePayment, setFixedRatePayment] = useState(false);
  const [paymentPopup, setPaymentPopup] = useState(false);
  const [payment, setPayment] = useState(false);

  if (!deal) return '';
  console.log('deal', deal)

  const proposeADeal = () => {
    setShowProposeADeal(true);
  };

  const closeProposeADealModal = () => {
    setShowProposeADeal(false);
  };

  const isCompleteNotVerify = deal.status === statusDeal.complete && !([statusJob.verify, statusJob.complete].includes(deal.statusJob));

  const streamUploads = (
    <>
      <h4 style={{ marginTop: 18 }}>
        <IconStreamUpload />
        Stream Uploads
      </h4>
      <div className="box-details">
        <div className="left">Attachments</div>
      </div>
      {(deal.streamMedia || []).map(link => {
        return (
          <div>
            <IconLiveStream />
            <Button type="link" href={utils.getMediaUrl(link)} target="_blank">{link}</Button>
          </div>
        )
      })}
      <div className="box-details">
        <div className="left">Stream Links</div>
      </div>
      {(deal.streamLink || []).map(link => {
        return (
          <div>
            <IconExpand />
            <Button type="link" href={link} target="_blank">{link}</Button>
          </div>
        )
      })}
      <hr style={{ border: '2px dashed #E8E8EA', margin: 0 }} />
    </>
  );

  const handleAcceptDeal = async () => {
    if (deal?.streamHourlyRate?.description) {
      setSubmitFixedRatePopup(true);
      return;
    }
    await acceptDeal(deal.code);
  }

  const handleRejectDeal = async () => {
    await rejectDeal(deal.code);
  }

  const handleVerifyJob = async () => {
    await verifyJob({ dealCode: deal.code, jobCode: deal.jobCode, hostCode: deal.hostCode });
  }

  const submitRating = async () => {
    await rateHost({
      dealCode: deal.code,
      jobCode: deal.jobCode,
      hostCode: deal.hostCode,
      score: rateScore
    });
  }

  const handlePayForFixedRate = () => {
    setSubmitFixedRatePopup(true);
  }

  const createPayment = async () => {
    const result = await submitFixedRatePayment({ dealCode: deal.code });
    if (!result.code) {
      ReactUtils.messageWarn({ content: 'Create fixed rate payment fail' });
      return;
    }
    setFixedRatePayment(result);
    setConfirmOrderPopup(true);
  }

  const onSubmitOrder = async () => {
    setSubmitFixedRatePopup(false);
    setConfirmOrderPopup(false);
    closeProposeADealModal();
    ReactUtils.messageSuccess({ content: 'Submit fixed rate payment successfully' });
    await getSellersOfCampaign(deal.jobCode);
  }

  const closePopupConfirmOrder = async () => {
    setConfirmOrderPopup(false);
    closeProposeADealModal();
    await getSellersOfCampaign(deal.jobCode);
  }

  const findPayment = async () => {
    const data = await findPaymentByCode({ paymentCode: deal.fixedRatePaymentCode });
    if (!data) {
      ReactUtils.messageWarn({ content: 'Find payment fail' });
    }
    setProof(data.proof);
    setPayment(data);
    setPaymentPopup(true);
  }

  const handleSendMessage = async () => {
    console.log('click handleSendMessage');
  }

  const RatingComponent = () => (
    <div className='rating-group'>
      <Rate character={<IconRating />} onChange={setRateScore} value={rateScore} className='rating-actions' />
      <div className='rating-result'>
        Rating this Seller:<strong className="ant-rate-text">{RATE_SCORES[rateScore]}</strong>
      </div>
    </div>
  );

  if (deal.status === statusDeal.complete && deal.statusJob === statusJob.complete) {
    return (
      <div className="main-content">
        <BoxStatus deal={deal} />
        <RatingComponent />
        <hr className='linear-divider' />
        <button className="btn-submit" onClick={submitRating}>
          Submit
        </button>
      </div>
    )
  }

  const renderFooter = () => {
    if (noFooter) return;
    if (deal.fixedRatePaymentCode) return;
    if (deal.fixedRatePaymentRequired) {
      return (
        <>
        <hr className='linear-divider' />
          <button className="btn-received complete" onClick={handlePayForFixedRate}>
            Pay for Fixed Rate
          </button>
        </>
      )
    }

    if ([statusDeal.proposal].includes(deal.status)) return (
      <>
        <hr className='linear-divider' />
        <button className="btn-received propose" onClick={proposeADeal}>
          <IconBtnPropose />
          Propose a deal
        </button>
        <div className="group-button">
          <button className="btn-received accept" onClick={handleAcceptDeal}>
            <IconBtnAccept />
            Accept
          </button>
          <button className="btn-received reject" onClick={handleRejectDeal}>
            <IconBtnReject />
            Reject
          </button>
        </div>
      </>
    );

    if (isCompleteNotVerify) return (
      <>
        <hr className='linear-divider' />
        <button className="btn-received complete" onClick={handleVerifyJob}>
          <IconBtnVerify />
          Verify this job
        </button>
        <button className="btn-received propose" onClick={handleSendMessage} style={{ marginTop: 18 }}>
          <IconSendMessage />
          Send message to Seller
        </button>
      </>
    );
  }

  return (
    <>
      <div className="main-content">
        <BoxStatus deal={deal} findPayment={findPayment} />
        <hr className='dash-divider' />
        <h4>
          <IconYoutube />
          Stream Details
        </h4>
        <div className="box-details">
          <div className="left">Time Suggestion</div>
          <div className="right">
            {moment(deal.streamTime?.description).format('DD MMM, YYYY')}
          </div>
        </div>
        <div className="box-details">
          <div className="left">Type of Stream</div>
          <div className="right">{deal.streamType?.description}</div>
        </div>
        <div className="box-details">
          <div className="left">Elements</div>
          <div className="right">
            {deal.streamElement?.description?.map((element, index) => {
              return index > 0 ? ` + ${element}` : element;
            })}
          </div>
        </div>
        <div className="box-details">
          <div className="left">Platform</div>
          <div className="right">
            {deal.streamPlatform?.description?.map((element, index) => {
              return index > 0 ? `, ${element}` : element;
            })}
          </div>
        </div>
        <hr className='dash-divider' />
        {isCompleteNotVerify && streamUploads}
        <h5 className='message'>Message</h5>
        <TextExpandable text={deal.note} />
        {renderFooter()}
      </div>
      <DrawerProposeDeal
        deal={deal}
        visible={showProposeADeal}
        handleClose={closeProposeADealModal}
      />
      <DrawerSubmitFixedRate
        visible={submitFixedRatePopup}
        deal={deal}
        seller={seller}
        handleClose={() => setSubmitFixedRatePopup(false)}
        onContinue={() => createPayment()} />
      <PopupConfirmOrder 
        payment={fixedRatePayment}
        handleOK={onSubmitOrder}
        handleClose={closePopupConfirmOrder} 
        visible={confirmOrderPopup} />
      <DrawerPaymentDetail
        handleClose={() => setPaymentPopup(false)}
        visible={paymentPopup}
        dataSource={payment}
        ></DrawerPaymentDetail>
    </>
  );
};

const mapStateToProps = ({ jobReducer: { deals } }) => ({});

const mapDispatchToProps = {
  acceptDeal,
  rejectDeal,
  verifyJob,
  rateHost,
  setProof,
  getSellersOfCampaign,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContentDeal);
