import React, { useEffect } from "react";
import { Drawer, Radio, Space } from "antd";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import {
  IconFeeInfo,
  IconNext,
  IconBank,
  IconCard,
} from "views/components/Icon/pages";
import { Collapse } from "antd";
import { IconPricing } from "views/components/Icon/myjobs";
import { connect } from "react-redux";
import { utils } from "helpers";

function DrawerSubmitFixedRate({
  currentProduct,
  deal,
  seller,
  handleClose,
  visible,
  onContinue,
  job,
  orderPricing,
}) {
  const [fixedRate, setFixedRate] = React.useState(0);
  const [platformFee, setPlatformFee] = React.useState(0);
  const [total, setTotal] = React.useState(0);

  useEffect(() => {
    if (!deal?.streamHourlyRate?.description) {
      return;
    }
    const fixedRateValue = parseFloat(deal?.streamHourlyRate?.description).toFixed(2);
    setFixedRate(fixedRateValue);
    const platformFeeValue = (fixedRateValue / 100 * 3 + 0.5).toFixed(2);
    setPlatformFee(platformFeeValue);
    setTotal(parseFloat(fixedRateValue) + parseFloat(platformFeeValue));
  }, [deal]);

  return (
    <Drawer
      className={"drawer-main payment-drawer payment-detail submit-order"}
      placement="right"
      width={660}
      onClose={handleClose}
      visible={visible}
      closeIcon={<IconCloseDrawer />}
      title={<div className="payment-detail__title">Pay for Fixed Rate</div>}
    >
      <div className="payment-detail__container drawer-submit-fixed-rate">
        <div className="payment-detail__order">
          <div className="payment-detail__order__detail">
            <div className="payment-detail__order__detail__job">
              <div className="payment-detail__order__detail__job__image">
                <img
                    alt={seller.name}
                    className="dropdown-img"
                    src={utils.getMediaUrl(seller.avatar)}
                  />
              </div>
              <div className="payment-detail__order__detail__job__info">
                <h4>{seller.name}</h4>
                <p>
                  Job Name:: <span>{ currentProduct?.name }</span>
                </p>
              </div>
            </div>
            <div>
              <table>
                <tr>
                  <td>Fixed Rate</td>
                  <td>
                    ${fixedRate}
                  </td>
                </tr>
                <tr>
                  <td>
                    Platform Fee{" "}
                    <a>
                      <IconFeeInfo />
                    </a>
                  </td>
                  <td>
                    + ${platformFee}
                  </td>
                </tr>
              </table>
              <div className="payment-detail__order__detail__total">
                <span>Total Payment</span>
                <span>
                  ${total}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="payment-method">
          <p className="title">
            <IconPricing /> Payment method
          </p>
          <div className="payment-method__options">
            <Radio.Group value={"bank"}>
              <Space direction="vertical">
                <Radio value="bank" checked={true}>
                  <div className="payment-method__option">
                    <div>
                      <IconBank />
                    </div>
                    <div className="payment-method__option__detail">
                      <p>Bank Transfer</p>
                      <p>It can take 2-5 business days</p>
                    </div>
                  </div>
                </Radio>
                <Radio value="xfers" disabled={true}>
                  <div className="payment-method__option">
                    <div>
                      <IconCard />
                    </div>
                    <div className="payment-method__option__detail">
                      <p>XFers</p>
                      <p>Require XFers account</p>
                    </div>
                  </div>
                </Radio>
              </Space>
            </Radio.Group>
          </div>
        </div>
        <div className="drawer-submit-proposal__action">
          <div>
            <a onClick={handleClose}>Cancel</a>
            <a onClick={onContinue}>
              <IconNext />
              Confirm Payment
            </a>
          </div>
        </div>
      </div>
    </Drawer>
  );
};

const mapStateToProps = ({ detailProduct: { currentProduct } }) => ({
  currentProduct: currentProduct,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerSubmitFixedRate);
