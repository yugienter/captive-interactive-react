import React from "react";
import {
  IconReceived,
  IconProposal,
  IconHeadAccept,
  IconHeadReject,
  IconDropDown,
} from "views/components/Icon/collapse";
import { constants, router, statusJob } from "helpers";
import {
  IconCompleteYellow,
  IconPropose,
} from "views/components/Icon/myjobs";
import IconComplete from "views/pages/HostProfile/ViewProfile/IconComplete";
import { FixedRatePaymentIcon, IconBlueCompletement, IconOrganReceived, IconYellowHelpUs } from "views/components/Icon/status";
import { useHistory } from "react-router-dom";
import { Dropdown, Menu } from "antd";

const { statusDeal } = constants;


const BoxStatus = ({ deal, findPayment = false }) => {
  const history = useHistory();
  if (!deal) return '';

  const renderContent = (child, color = 'black', noCommission = false) => {
    return (
      <div className="box-status">
        {child}
        {!noCommission && deal.streamHourlyRate && (
          <>
            <div className={`spend-hours ${color}`}>
              {`$${deal.streamHourlyRate.description}/hr`}
            </div>
            <div className="commission">
              Commission:{" "}
              <span className={color}>
                {`${deal.streamCommission.description || 0} ${deal.streamCommissionUnit
                  }`}
              </span>
            </div>
          </>
        )}
      </div>
    );
  }

  const viewPayment = async (record) => {
    if (!findPayment) return;
    findPayment();
  }

  const showInPaymentProcessing = (record) => {

  }

  const overlayPayment = (record) => {
    return <Menu>
      <div className="popup-row-actions">
        <Menu.Item key="0" onClick={() => viewPayment(record)}>
          View Payment
        </Menu.Item>
        <Menu.Item key="1" onClick={() => showInPaymentProcessing(record)}>
          Show in Payment Processing
        </Menu.Item>
      </div>
    </Menu>
  }

  const status = deal.status;
  switch (status) {
    case statusDeal.opens:
    case statusDeal.deal: {
      return renderContent((
        <div className="status grey">
          <IconProposal />
          Your deal was sent
        </div>
      ), 'black');
    }
    case statusDeal.proposal: {
      if (deal.verifyPaymentRequired) {
        return renderContent((
          <div className="status grey inline-flex justify-center">
            <FixedRatePaymentIcon />
            You submitted a fixed rate payment. 
            <Dropdown overlay={overlayPayment(deal)} trigger={["click"]}>
              <span className="clickable blue-text inline-flex justify-center">
                View <IconDropDown />
              </span>
            </Dropdown>
          </div>
        ), 'black');
      }
      if (deal.fixedRatePaymentRequired) {
        return renderContent((
          <div className="status grey inline-flex justify-center">
            <FixedRatePaymentIcon />
            Waiting pay for fixed rate. 
          </div>
        ), 'black');
      }
      return renderContent((
        <div className="status orange">
          <IconOrganReceived />
          You received a proposal
        </div>
      ), 'blue');
    }
    case statusDeal.accepted: {
      if (deal.verifyPaymentRequired) {
        return renderContent((
          <div className="status grey inline-flex justify-center">
            <FixedRatePaymentIcon />
            You submitted a fixed rate payment. 
            <Dropdown overlay={overlayPayment(deal)} trigger={["click"]}>
              <span className="clickable blue-text inline-flex justify-center">
                View <IconDropDown />
              </span>
            </Dropdown>
          </div>
        ), 'black');
      }
      if (deal.fixedRatePaymentRequired) {
        return renderContent((
          <div className="status grey inline-flex justify-center">
            <FixedRatePaymentIcon />
            Waiting pay for fixed rate. 
          </div>
        ), 'black');
      }
      return renderContent((
        <div className="status green">
          <IconHeadAccept />
          The deal is accepted. 
          <span onClick={() =>
                history.push({
                  pathname: router.orders(deal?.jobCode, deal?.hostCode),
                })
              } className="view-order-text">
            &nbsp;&nbsp;View Order
          </span>
        </div>
      ), 'green');
    }
    case statusDeal.rejected: {
      return renderContent((
        <div className="status red">
          <IconHeadReject />
          The deal is rejected
        </div>
      ), 'black');
    }
    case statusDeal.complete: {
      if (deal.statusJob === statusJob.complete) {
        return renderContent((
          <div className="status yellow">
            <IconYellowHelpUs />
            Help us for rating this seller
          </div>
        ), 'yellow', true);
      }

      return renderContent((
        <div className="status blue">
          <IconBlueCompletement />
          Complement is submitted
        </div>
      ), 'black');
    }

    default:
      return null;
  }
};

export default BoxStatus;
