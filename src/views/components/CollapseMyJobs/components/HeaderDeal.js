import { statusDeal, statusJob, utils } from "helpers";
import { IconMessageSend } from "views/components/Icon/myjobs";
import { IconGreyGerald, IconVerify } from "views/components/Icon/status";

const HeaderDeal = ({ seller, deal }) => {
  if (!deal || !seller) {
    return '';
  }

  const timeString = utils.toDateString(deal.createdAt, { format: 'DD MMM, YYYY hh:mm' });
  const renderIcon = () => {
    if (deal.status === statusDeal.complete) {
      if (deal.statusJob === statusJob.complete) {
        return <IconVerify />
      }
      return <IconGreyGerald />
    }
    return <IconMessageSend />
  }

  const renderContent = () => {
    switch (deal.status) {
      case statusDeal.proposal: {
        return (
          <p className="send">
            <strong>{seller.name}</strong> submitted a proposal
          </p>
        );
      }
      case statusDeal.opens:
      case statusDeal.deal: {
        return (
          <p className="send">
            <strong>You</strong> sent a deal
          </p>
        );
      }
      case statusDeal.accepted: {
        return (
          <p className="send">
            <strong>{seller.name}</strong> accepted a deal
          </p>
        );
      }
      case statusDeal.rejected: {
        return (
          <p className="send">
            <strong>{seller.name}</strong> sent you a deal
          </p>
        );
      }
      case statusDeal.complete: {
        if (deal.statusJob === statusJob.complete) {
          return (
            <p className="send">
              <strong>You</strong> verified this job
            </p>
          );
        }

        return (
          <p className="send">
            <strong>{seller.name}</strong> completed this job
          </p>
        );
      }
      default: {
        return (
          <p className="send">
            {deal.status} {deal.statusJob}
          </p>
        );
      }
    }
  }

  return (
    <div className="header-main">
      <div className="default">
        <div className="first">
          {renderIcon()}
        </div>
        <div className="center">
          {renderContent()}
          <p className="date">{timeString}</p>
        </div>
      </div>
    </div >
  )
};

export default HeaderDeal;
