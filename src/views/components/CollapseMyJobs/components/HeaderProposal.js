import React from "react";
import { IconMessageSend } from "views/components/Icon/myjobs";

const HeaderProposal = () => {
  return (
    <div className="header-main">
      <div className="default">
        <div className="first">
          <IconMessageSend />
        </div>
        <div className="center">
          <p className="send">
            <strong>You</strong> sent you a proposal
          </p>
          <p className="date">Today at 11:23am</p>
        </div>
      </div>
    </div>
  );
};

export default HeaderProposal;
