import { Modal } from "antd";

const PromptModal = ({ title, handleOk, handleClose, visible, text, okText }) => {
  return (
    <Modal
      title={title}
      centered
      visible={visible}
      onOk={handleOk}
      onCancel={handleClose}
      okText={okText || 'OK'}
    >
      <p>{text}</p>
    </Modal>
  );
};

export default PromptModal;
