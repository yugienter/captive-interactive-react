import React from "react";
import { Drawer } from "antd";
import moment from "moment";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import {
  IconUpBig,
  IconDownBig,
  IconUploadProof,
  IconEditProof,
  IconDeleteProof,
  IconDownloadProof,
  IconPaid,
  IconCancel,
  IconCopy,
  IconOrder,
  IconFeeInfo,
} from "views/components/Icon/pages";
import { Collapse } from "antd";
import { IconMessageSend, ExpandIcon } from "views/components/Icon/myjobs";
import reportWebVitals from "reportWebVitals";
import { utils } from "helpers";
const { getMediaUrl } = utils;

const { Panel } = Collapse;

const PAYMENT_STATUS = {
  // verify: {
  //   id: "verify",
  //   label: "Verifying",
  //   color: "#55ADFF",
  // },
  submitted: {
    id: "submitted",
    label: "Verifying",
    color: "#FF9A4D",
  },
  approved: {
    id: "approved",
    label: "Paid",
    color: "#1D1929",
  },
  rejected: {
    id: "rejected",
    label: "Canceled",
    color: "#F76969",
  },
};

function DrawerPaymentDetail({ dataSource, handleClose, visible }) {
  if (!dataSource) return <div />;
  const date = dataSource.updatedAt
    ? moment(dataSource.updatedAt).format("DD MMM YYYY")
    : null;

  const payment = {
    paymentID: dataSource.code,
    type: "Order Submission",
    name: dataSource.jobName,
    status: dataSource.status,
    amount: `$${parseFloat(dataSource.productAmount).toFixed(2)}`,
    date,
    isIncrease: true,
  };
  const hasProof = dataSource.proof && dataSource.proof.length > 0;
  const renderProof = (proof) => {
    const rows = [];
    for (var url of proof) {
      const name = url.split("/").at(-1);
      rows.push(
        <div className="payment-detail__card__proof__file">
          <div className="payment-detail__card__proof__file__preview">
            <img src={getMediaUrl(url)} alt={name} />
          </div>
          <div className="payment-detail__card__proof__file__info">
            <p>{name}</p>
            {/* <p>12 Dec 2021, 10:32</p> */}
          </div>
          <div className="payment-detail__card__proof__file__action">
            <a>
              <IconEditProof />
            </a>
            <a>
              <IconDownloadProof />
            </a>
            <a>
              <IconDeleteProof />
            </a>
          </div>
        </div>
      );
    }
    return rows;
  };
  return (
    <Drawer
      className={"drawer-main payment-drawer payment-detail"}
      placement="right"
      width={460}
      onClose={handleClose}
      visible={visible}
      closeIcon={<IconCloseDrawer />}
      title={<div className="payment-detail__title">Payment detail</div>}
    >
      <div className="payment-detail__container">
        <div className="payment-detail__info">
          {payment.isIncrease ? (
            <span>
              <IconUpBig />
            </span>
          ) : (
            <span>
              <IconDownBig />
            </span>
          )}
          <p className="payment-detail__info__title">Order Submission</p>
          <p>Send the amount shown to the banking details below:</p>
          <p>Pay amount:</p>
          <p className="payment-detail__info__amount">{payment.amount}</p>
          {payment.status === PAYMENT_STATUS.submitted.id ? (
            <p>
              Submited Date:{" "}
              {moment(dataSource.createdAt).format("DD MMM YYYY")}
            </p>
          ) : (
            <p>
              Verified Date:{" "}
              {moment(dataSource.updatedAt).format("DD MMM YYYY")}
            </p>
          )}
          {/* <p>Verified Date: 12 Dec 2021, 10:32am</p> */}
        </div>
        <div className="payment-detail__card">
          <div className="payment-detail__card__status">
            <p
              className="payment-detail__card__status__tag"
              style={{
                border: `1px solid ${PAYMENT_STATUS[payment.status]?.color}`,
                color: PAYMENT_STATUS[payment.status]?.color,
              }}
            >
              {PAYMENT_STATUS[payment.status]?.label}
            </p>
            <table>
              <tr>
                <td>Payment ID</td>
                <td>
                  {dataSource.code}{" "}
                  <a>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__info">
            <table>
              <tr>
                <td>Account Holder</td>
                <td>Captive Interactive Co.</td>
              </tr>
              <tr>
                <td>Bank Name</td>
                <td>International Bank of Singapore</td>
              </tr>
              <tr>
                <td>City</td>
                <td>Singapore</td>
              </tr>
              <tr>
                <td>Bank Account No.</td>
                <td>
                  SGB1232085455555{" "}
                  <a>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__proof">
            {!hasProof ? (
              <div className="payment-detail__card__proof--nofile">
                <p>
                  Remember to check the numbers carefully and include the
                  Payment ID in your transfer’s note.
                </p>
                <a className="payment-detail__card__proof__action">
                  <span>
                    <IconUploadProof />
                  </span>{" "}
                  Upload Proof
                </a>
              </div>
            ) : (
              <div>
                <p className="payment-detail__card__proof__title">
                  Proof Upload
                </p>
                {renderProof(dataSource.proof)}
              </div>
            )}
          </div>
          <div className="payment-detail__card__action">
            {/* <a className="payment-detail__card__action--paid" >
              <span>
                <IconPaid />
              </span>
              Paid
            </a> */}
            {/* <a className="payment-detail__card__action--cancel">
              <span>
                <IconCancel />
              </span>
              Cancelled
            </a> */}
          </div>
        </div>
        <div className="payment-detail__order">
          <Collapse
            bordered={false}
            expandIcon={(panelProps) => <ExpandIcon {...panelProps} />}
            expandIconPosition="right"
            className="site-collapse-custom-collapse older-activities-collapse collapse-main"
          >
            <Panel
              header={
                <div className="payment-detail__order__header">
                  <div>
                    <IconOrder />
                  </div>
                  <div className="payment-detail__order__header__info">
                    <p>Order sent</p>
                    <p>12 Dec 2021, 10:32am</p>
                  </div>
                </div>
              }
              className="site-collapse-custom-panel"
            >
              <div className="payment-detail__order">
                <div className="payment-detail__order__detail">
                  <div className="payment-detail__order__detail__job">
                    <div className="payment-detail__order__detail__job__image">
                      <img
                        src="https://dienanhtrongtamtay.com/wp-content/uploads/2021/04/Icon-la-gi.jpg"
                        alt="proof"
                      />
                    </div>
                    <div className="payment-detail__order__detail__job__info">
                      <p>Black Chicken Mushroom Soup</p>
                      <p>
                        Public RRP:{" "}
                        <span>
                          ${parseFloat(dataSource.productPublicRRP).toFixed(2)}
                        </span>
                      </p>
                    </div>
                  </div>
                  <div>
                    <table>
                      <tr>
                        <td>Order Total</td>
                        <td>
                          ${parseFloat(dataSource.productAmount).toFixed(2)}
                        </td>
                      </tr>
                      <tr>
                        <td>Commision</td>
                        <td>
                          - ${parseFloat(dataSource.commission).toFixed(2)}
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Platform Fee{" "}
                          <a>
                            <IconFeeInfo />
                          </a>
                        </td>
                        <td>
                          + ${parseFloat(dataSource.platformFee).toFixed(2)}
                        </td>
                      </tr>
                    </table>
                    <div className="payment-detail__order__detail__total">
                      <span>Total Payment</span>
                      <span>${parseFloat(dataSource.total).toFixed(2)}</span>
                    </div>
                  </div>
                </div>
              </div>
            </Panel>
          </Collapse>
        </div>
      </div>
    </Drawer>
  );
}

export default DrawerPaymentDetail;
