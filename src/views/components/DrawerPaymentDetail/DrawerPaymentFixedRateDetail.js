import React from "react";
import { Drawer } from "antd";
import moment from "moment";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import {
  IconUpBig,
  IconDownBig,
  IconUploadProof,
  IconDownloadProof,
  IconCopy,
  IconDeleteProof,
} from "views/components/Icon/pages";
import {
  resetProof,
  uploadMedia,
  removeProof,
} from "../../pages/JobOrder/actions";
import { Collapse } from "antd";
import { IconMessageSend, ExpandIcon } from "views/components/Icon/myjobs";
import reportWebVitals from "reportWebVitals";
import { constants, ReactUtils, utils } from "helpers";
import axios from "axios";
import { connect, useSelector } from "react-redux";
import _ from "lodash";
import { updateProofByCompany } from "views/pages/DetailProduct/components/actions";
import { useEffect } from "react";
const { getMediaUrl } = utils;
const { MEDIA_TYPE } = constants;
const { Panel } = Collapse;

const PAYMENT_STATUS = {
  // verify: {
  //   id: "verify",
  //   label: "Verifying",
  //   color: "#55ADFF",
  // },
  submitted: {
    id: "submitted",
    label: "Verifying",
    color: "#FF9A4D",
  },
  approved: {
    id: "approved",
    label: "Paid",
    color: "#1D1929",
  },
  rejected: {
    id: "rejected",
    label: "Canceled",
    color: "#F76969",
  },
};

function DrawerPaymentFixedRateDetail({
  dataSource,
  handleClose,
  visible,
  uploadMedia,
  removeProof,
  resetProof,
}) {
  const { user } = useSelector((state) => state.common);
  const { proof } = useSelector((state) => state.jobOrder);

  const date = dataSource.updatedAt
    ? moment(dataSource.updatedAt).format("DD MMM YYYY")
    : null;

  const payment = {
    paymentID: `PR${dataSource.code}`,
    type: "Fixed Rate",
    name: dataSource.jobName,
    status: dataSource.status,
    amount: `$${parseFloat(dataSource.total).toFixed(2)}`,
    date,
    isIncrease: true,
  };
  const hasProof = dataSource.proof && dataSource.proof.length > 0;

  const onProofChange = async (e) => {
    const file = _.get(e, "target.files[0]");
    const formData = new FormData();
    formData.append("file", file);
    const params = {
      ownerType: MEDIA_TYPE.COMPANY,
      ownerCode: user.code,
    };
    const url = await uploadMedia(formData, params);
    await updateProof([...proof, url]);
  };

  const download = (uri) => {
    try {
      axios({
        url: uri,
        method: "GET",
        responseType: "blob",
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        uri = uri.split(".");
        const type = uri[uri.length - 1];
        link.setAttribute("download", "proof." + type);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
    } catch (err) {
      console.log(err);
    }
  };

  const deleteProof = async (url) => {
    await removeProof(url);
    await updateProof(proof.filter(x => x !== url));
  };

  const updateProof = async (proof) => {
    if (!dataSource.code) return;
    const result = await updateProofByCompany({
      paymentCode: dataSource.code,
      proof,
    });
    if (!result.code) {
      ReactUtils.messageWarn({ content: "Update proof fail" });
      return;
    }
    ReactUtils.messageSuccess({ content: "Update proof success" });
  };

  if (!dataSource) return <div />;

  return (
    <Drawer
      className={"drawer-main payment-drawer payment-detail"}
      placement="right"
      width={460}
      onClose={handleClose}
      visible={visible}
      closeIcon={<IconCloseDrawer />}
      title={<div className="payment-detail__title">Pay for Fixed Rate</div>}
    >
      <div className="payment-detail__container">
        <div className="payment-detail__info">
          {payment.isIncrease ? (
            <span>
              <IconUpBig />
            </span>
          ) : (
            <span>
              <IconDownBig />
            </span>
          )}
          <p className="payment-detail__info__title">Fixed Rate</p>
          <p>Send the amount shown to the banking details below:</p>
          <p>Pay amount:</p>
          <p className="payment-detail__info__amount">{payment.amount}</p>
          {payment.status === PAYMENT_STATUS.submitted.id ? (
            <p>
              Submited Date:{" "}
              {moment(dataSource.createdAt).format("DD MMM YYYY")}
            </p>
          ) : (
            <p>
              Verified Date:{" "}
              {moment(dataSource.updatedAt).format("DD MMM YYYY")}
            </p>
          )}
          {/* <p>Verified Date: 12 Dec 2021, 10:32am</p> */}
        </div>
        <div className="payment-detail__card">
          <div className="payment-detail__card__status">
            <p
              className="payment-detail__card__status__tag"
              style={{
                border: `1px solid ${PAYMENT_STATUS[payment.status]?.color}`,
                color: PAYMENT_STATUS[payment.status]?.color,
              }}
            >
              {PAYMENT_STATUS[payment.status]?.label}
            </p>
            <table>
              <tr>
                <td>Payment ID</td>
                <td>
                  PR{dataSource.code}{" "}
                  <a>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__info">
            <table>
              <tr>
                <td>Account Holder</td>
                <td>Captive Interactive Co.</td>
              </tr>
              <tr>
                <td>Bank Name</td>
                <td>International Bank of Singapore</td>
              </tr>
              <tr>
                <td>City</td>
                <td>Singapore</td>
              </tr>
              <tr>
                <td>Bank Account No.</td>
                <td>
                  SGB1232085455555{" "}
                  <a>
                    <IconCopy />
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div className="payment-detail__card__proof">
            {!hasProof && (
              <div className="payment-detail__card__proof--nofile">
                <p>
                  Remember to check the numbers carefully and include the
                  Payment ID in your transfer’s note.
                </p>
              </div>
            )}
            <div>
              <p className="payment-detail__card__proof__title">Proof Upload</p>
              {proof.map((item, index) => (
                <div
                  style={{
                    marginBottom: 15,
                  }}
                >
                  <div className="payment-detail__card__proof__file">
                    <div className="payment-detail__card__proof__file__preview">
                      <img src={getMediaUrl(item)} alt="proof" />
                    </div>
                    <div className="payment-detail__card__proof__file__info">
                      <p>Proof {index + 1}</p>
                    </div>
                    <div className="payment-detail__card__proof__file__action">
                      {/* <a><IconEditProof /></a> */}
                      <a onClick={() => download(getMediaUrl(item))}>
                        <IconDownloadProof />
                      </a>
                      <a onClick={() => deleteProof(item)}>
                        <IconDeleteProof />
                      </a>
                    </div>
                  </div>
                </div>
              ))}
              <br />
              <input
                onChange={onProofChange}
                style={{ display: "none" }}
                id="proof"
                type="file"
                name="myImage"
                accept="image/png, image/gif, image/jpeg, application/pdf"
              />
              <div>
                <label
                  htmlFor="proof"
                  className="payment-detail__card__proof__action"
                >
                  <span>
                    <IconUploadProof />
                  </span>{" "}
                  Upload Proof
                </label>
              </div>
            </div>
          </div>
          <div className="payment-detail__card__action"></div>
        </div>
      </div>
    </Drawer>
  );
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  resetProof,
  removeProof,
  uploadMedia,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerPaymentFixedRateDetail);
