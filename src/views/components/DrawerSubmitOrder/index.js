import React from "react";
import { Drawer, Radio, Space } from "antd";
import { IconCloseDrawer } from "views/components/Icon/drawer";
import { IconFeeInfo, IconNext, IconBank, IconCard } from "views/components/Icon/pages"
import { Collapse } from 'antd';
import {
    IconPricing,
} from "views/components/Icon/myjobs";
import { connect } from 'react-redux';
import { utils } from 'helpers';
import _ from "lodash"

const { getMediaUrl } = utils

const { Panel } = Collapse;

const PAYMENT_STATUS = {
    verifying: {
        id: 'verifying',
        label: 'Verifying Payment',
        color: '#55ADFF'
    },
    pending: {
        id: 'pending',
        label: 'Pending payment',
        color: '#FF9A4D'
    },
    paid: {
        id: 'paid',
        label: 'Paid',
        color: '#1D1929'
    },
    canceled: {
        id: 'canceled',
        label: 'Canceled',
        color: '#F76969'
    }
}

function DrawerSubmitOrder({
    handleClose,
    visible,
    onContinue,
    job,
    orderPricing
}) {

    return (
        <Drawer
            className={"drawer-main payment-drawer payment-detail submit-order"}
            placement="right"
            width={660}
            onClose={handleClose}
            visible={visible}
            closeIcon={<IconCloseDrawer />}
            title={
                (
                    <div className="payment-detail__title">
                        Submit Order
                    </div>
                )
            }
        >
            <div className="payment-detail__container">
                <div className="payment-detail__order">
                    <div className="payment-detail__order__detail">
                        <div className="payment-detail__order__detail__job">
                            <div className="payment-detail__order__detail__job__image">
                                <img src={getMediaUrl(_.get(job, 'product.media[0].url'))} alt="proof" />
                            </div>
                            <div className="payment-detail__order__detail__job__info">
                                <p>{_.get(job, 'product.name')}</p>
                                <p>Public RRP: <span>${_.get(job, 'publicRRP')}</span></p>
                            </div>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td>Order Total</td>
                                    <td>${_.get(orderPricing, 'productAmount') ? _.get(orderPricing, 'productAmount').toFixed(2) : 0}</td>
                                </tr>
                                <tr>
                                    <td>Commision</td>
                                    <td>- {_.get(orderPricing, 'commissionUnit')}{_.get(orderPricing, 'commission') ? _.get(orderPricing, 'commission').toFixed(2) : 0}</td>
                                </tr>
                                <tr>
                                    <td>Platform Fee <a><IconFeeInfo /></a></td>
                                    <td>+ ${_.get(orderPricing, 'platformFee') ? _.get(orderPricing, 'platformFee').toFixed(2) : 0}</td>
                                </tr>
                            </table>
                            <div className="payment-detail__order__detail__total">
                                <span>Total Payment</span>
                                <span >${_.get(orderPricing, 'total') ? _.get(orderPricing, 'total').toFixed(2) : 0}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="payment-method">
                    <p className="title">
                        <IconPricing /> Payment method
                    </p>
                    <div className="payment-method__options">
                        <Radio.Group>
                            <Space direction="vertical">
                                <Radio value="bank">
                                    <div className="payment-method__option">
                                        <div>
                                            <IconBank />
                                        </div>
                                        <div className="payment-method__option__detail">
                                            <p>Bank Transfer</p>
                                            <p>It can take 2-5 business days</p>
                                        </div>

                                    </div>
                                </Radio>
                                <Radio value="xfers">
                                    <div className="payment-method__option">
                                        <div>
                                            <IconCard />
                                        </div>
                                        <div className="payment-method__option__detail">
                                            <p>XFers</p>
                                            <p>Require XFers account</p>
                                        </div>

                                    </div>
                                </Radio>
                            </Space>
                        </Radio.Group>
                    </div>
                </div>
                <div className="drawer-submit-proposal__action">
                    <div>
                        <a onClick={handleClose}>Cancel</a>
                        <a onClick={onContinue}>
                            <IconNext />
                            Confirm Order
                        </a>
                    </div>
                </div>
            </div>
        </Drawer>
    );
}

const mapStateToProps = ({ jobOrder }) => ({
    job: jobOrder.job,
    orderPricing: jobOrder.orderPricing
});

const mapDispatchToProps = {

};
export default
    connect(mapStateToProps, mapDispatchToProps)(DrawerSubmitOrder)
    ;
