import {
    CloseCircleFilled
} from "@ant-design/icons";
import { useEffect, useState } from "react";
import { connect } from 'react-redux'
import { handleUpdateCategories, getTags } from "views/pages/CreateProduct/actions";
import HostTag from 'views/pages/Host/parts/HostTag';

const InputCategories = ({ data, handleUpdateCategories, detail, getTags, tags }) => {
    const [arrayTag, setArrayTag] = useState([])
    useEffect(() => {
        const result = []
        tags.forEach((tagItem) => {
            const arrayTags = tagItem.item.filter(item => data.ids.indexOf(item.id) !== -1)
            arrayTags.length === tagItem.item.length ? result.push(tagItem) : result.push(...arrayTags)
        })
        setArrayTag(result)
    }, [tags, data])

    useEffect(() => {
        tags.length < 1 && getTags();
    }, [getTags])

    const handleRemove = (item) => {
        const indexParent = tags.findIndex(element => element.id === item.id)
        let tempIds = [...data.ids]
        let tempNames = [...data.names]
        const arrayIndex = []
        indexParent !== -1 ? (arrayIndex.push(...tags[indexParent].item.map(tagItem => data.ids.indexOf(tagItem.id)))) : (arrayIndex.push(data.ids.indexOf(item.id)))

        arrayIndex.forEach(indexTag => {
            if (indexTag > -1) {
                tempIds = tempIds.filter((_, index) => index != indexTag)
                tempNames = tempNames.filter((_, index) => index != indexTag)
            }
        })

        handleUpdateCategories({
            ids: tempIds,
            names: tempNames
        })
    }
    return (
        arrayTag && arrayTag.map((item, index) => (
            <span className="categoties-item" key={index}>
                {HostTag({ tag: item.name }).props.icon}
                <span className="item-name">{item.name}</span>
                {!detail && <CloseCircleFilled onClick={() => handleRemove(item)} />}
            </span>
        ))
    )
};
const mapStateToProps = ({ createProduct: { category, tags } }) => ({
    category,
    tags
});
const mapDispatchToProps = {
    handleUpdateCategories,
    getTags
};
export default connect(mapStateToProps, mapDispatchToProps)(InputCategories);