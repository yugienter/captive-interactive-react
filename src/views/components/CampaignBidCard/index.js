import { connect } from "react-redux";
import { IconPen } from "views/components/Icon/market";
import { Tag, Progress, Button } from "antd";
import {
  IconPricing,
  IconStream,
  IconClock,
} from "views/components/Icon/myjobs";
import _ from "lodash";
import moment from "moment";
import { utils } from "helpers";
import { useEffect, useState } from "react";

function CampaignBidCard({ data, isShowBidButton, onBid }) {
  const [deal, setDeal] = useState({});
  const [job, setJob] = useState({});
  const [campaign, setCampaign] = useState({});
  useEffect(() => {
    setCampaign(_.get(data, "data.campaign") || {});
    setDeal(_.get(data, "dealData") || {});
    setJob(_.get(data, "data") || {});
  }, [data]);

  console.log(data);
  const getDiffDate = (now, then) => {
    return `${moment(now).diff(moment(then), "days")} days`;
  };

  const renderStreamCommission = () => {
    const negiable = _.get(deal, "streamCommission.negiable", "Negotiable");
    const streamCommission = _.get(deal, "streamCommission.description");
    const streamCommissionUnit = _.get(deal, "streamCommissionUnit");
    if (streamCommissionUnit && streamCommission && negiable !== "Negotiable") {
      return `${streamCommission}${streamCommissionUnit}`;
    }
    return "Negotiable";
  };

  return (
    <div className="drawer-submit-proposal__result">
      <div className="drawer-submit-proposal__result__tags">
        {_.get(deal, "streamHourlyRateRange.start") && (
          <Tag color="#55acee">Fixed Rate</Tag>
        )}
        {_.get(deal, "streamCommission.description") && (
          <Tag color="#FF9A4D">Commission</Tag>
        )}
      </div>
      <div className="drawer-submit-proposal__result__title">
        <p>{_.get(campaign, "name")}</p>
        <p>{_.get(campaign, "description")}</p>
      </div>
      <div className="drawer-submit-proposal__result__duration">
        <p>
          <IconClock />{" "}
          {getDiffDate(
            _.get(campaign, "timeEnd"),
            _.get(campaign, "timeStart")
          )}
        </p>
        <Progress strokeColor="#1BD2A4" percent={50} showInfo={false} />
      </div>
      <div className="drawer-submit-proposal__result__deal">
        {deal?.streamHourlyRateRange?.start && (
          <p className="fixed-price-text">
            {"$" +
              (_.get(deal, "streamHourlyRateRange.start") || 0) +
              " - " +
              "$" +
              (_.get(deal, "streamHourlyRateRange.end") || 0) +
              "/hr"}
          </p>
        )}
        <p>
          Commission: <span>{renderStreamCommission()}</span>
        </p>
      </div>
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconPricing /> Product Price
        </p>
        <table>
          <tr>
            <td>Public RRP</td>
            <td>${_.get(job, "publicRRP")}</td>
          </tr>
          <tr>
            <td>Discount</td>
            <td>{_.get(job, "discountPercentage")}%</td>
          </tr>
          <tr>
            <td>Discounted Price</td>
            <td>
              $
              {_.get(job, "publicRRP") *
                (1 - _.get(job, "discountPercentage") / 100)}
            </td>
          </tr>
          <tr>
            <td>Discount Period</td>
            <td>
              {utils.toDateString(_.get(job, "discountPeriodStart"))} -{" "}
              {utils.toDateString(_.get(job, "discountPeriodEnd"))}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconStream /> Stream Details
        </p>
        <table>
          <tr>
            <td>Time Suggestion</td>
            <td>{utils.toDateString(_.get(deal, "streamTime.description"))}</td>
          </tr>
          <tr>
            <td>Type of Stream</td>
            <td>{_.get(deal, "streamType.description")}</td>
          </tr>
          <tr>
            <td>Elements</td>
            <td>
              {_.get(deal, "streamElement.description") &&
                _.get(deal, "streamElement.description").join(" + ")}
            </td>
          </tr>
          <tr>
            <td>Platform</td>
            <td>
              {_.get(deal, "streamPlatform.description") &&
                _.get(deal, "streamPlatform.description").join(" + ")}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__message">
        <p className="sub-title">Message</p>
        <p className="fontStyle13">{_.get(deal, "note")}</p>
      </div>
      <div>
        {isShowBidButton && (
          <Button
            onClick={() => {
              onBid();
            }}
            style={{
              background:
                "linear-gradient(254.81deg, #1A5AFF 8.13%, #1EE9B6 92.46%)",
              borderRadius: 12,
              width: "100%",
              height: 48,
            }}
            type="primary"
            size="large"
          >
            <IconPen />
            Pitch for this product
          </Button>
        )}
      </div>
    </div>
  );
}

const mapStateToProps = ({ marketplace }) => ({
  data: marketplace.campaign,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CampaignBidCard);
