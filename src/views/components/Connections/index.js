import React, { useImperativeHandle, forwardRef } from 'react'
import { Form, Input, Row, Col } from "antd";
import { constants } from 'helpers';

const Connections = ({ updateHostProfile, host, next }, ref) => {
  const [form] = Form.useForm();

  form.setFieldsValue({
    facebook: host ? host.social.facebook : null,
    instagram: host ? host.social.instagram : null,
    tiktok: host ? host.social.tiktok : null,
    other: host ? host.social.other : null,
    facebookFollowers: host ? host.facebookFollowers : null,
    instagramFollowers: host ? host.instagramFollowers : null,
    tiktokFollowers: host ? host.tiktokFollowers : null,
    otherFollowers: host ? host.otherFollowers : null,
  });

  useImperativeHandle(ref, () => ({
    onContinue: () => {
      form.submit();
    }
  }));

  const onFinish = async (values) => {
    await updateHostProfile({
      social: {
        facebook: values.facebook,
        instagram: values.instagram,
        tiktok: values.tiktok,
        other: values.other,
      },
      facebookFollowers: parseInt(values.facebookFollowers),
      instagramFollowers: parseInt(values.instagramFollowers),
      tiktokFollowers: parseInt(values.tiktokFollowers),
      otherFollowers: parseInt(values.otherFollowers),
      finishedOnboarding: true
    });
    next && next();
  };

  return (
    <>
      <h2>Connections</h2>
      <div className='text-bold mb36'>Connect to social accounts to enhance your portfolio</div>
      <Form form={form} onFinish={onFinish} layout="vertical">
        <Row gutter={constants.mainStyle.gutter}>
          <Col className="gutter-row" span={12}>
            <Form.Item
              label="Facebook"
              name="facebook"
            >
              <Input
                size="large"
                className="input-basic"
                placeholder="https://facebook.com/exampleURL"
              />
            </Form.Item>
            <Form.Item label="Instagram" name="instagram">
              <Input
                size="large"
                className="input-basic"
                placeholder="Paste URL here"
              />
            </Form.Item>
            <Form.Item label="TikTok" name="tiktok">
              <Input
                size="large"
                className="input-basic"
                placeholder="Paste URL here"
              />
            </Form.Item>
            <Form.Item label="Other" name="other">
              <Input
                size="large"
                className="input-basic"
                placeholder="Paste URL here"
              />
            </Form.Item>
          </Col>
          <Col className="gutter-row" span={12}>
            <Form.Item label="Followers" name="facebookFollowers">
              <Input
                size="large"
                className="input-basic"
                placeholder="Numbers of Followers"
                type="number"
              />
            </Form.Item>
            <Form.Item label="Followers" name="instagramFollowers">
              <Input
                size="large"
                className="input-basic"
                placeholder="Numbers of Followers"
                type="number"
              />
            </Form.Item>
            <Form.Item label="Followers" name="tiktokFollowers">
              <Input
                size="large"
                className="input-basic"
                placeholder="Numbers of Followers"
                type="number"
              />
            </Form.Item>
            <Form.Item label="Followers" name="otherFollowers">
              <Input
                size="large"
                className="input-basic"
                placeholder="Numbers of Followers"
                type="number"
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  )
}

export default forwardRef(Connections);