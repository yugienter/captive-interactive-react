import React, { useState, useEffect } from 'react';
import moment from 'moment';

const Timer = ({ timeEnd }) => {
  const endDate = moment(timeEnd);
  const now = moment();
  const [count, setCount] = useState(endDate.diff(now));

  useEffect(() => {
    const id = setInterval(() => {
      setCount((c) => c - 1000);
    }, 1000);
    return () => clearInterval(id);
  }, []);

  const duration = moment.duration(count, 'milliseconds');
  const days = duration.days();
  const hours = duration.hours();
  const minutes = duration.minutes();
  const seconds = duration.seconds();
  return (
    <>
      {days && <span>{days} days </span>}
      {(days || hours) && <span>{hours}h </span>}
      {(hours || minutes) && <span>{minutes}m </span>}
      {(minutes || seconds) && <span>{seconds}s </span>}
    </>
  );
};

export default Timer;
