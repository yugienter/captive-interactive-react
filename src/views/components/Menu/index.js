/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 25/08/2021
 */

import React from "react";
import { connect } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import { Menu, Layout, Badge, Popover, Divider } from "antd";
import { BellOutlined } from "@ant-design/icons";
import LogoHorizontal from "views/components/Logo/LogoHorizontal";
import UserAvatar from "views/components/UserAvatar";
import {
  // IconHome,
  IconUsers,
  IconCog,
  IconFeedback,
  IconPassword,
  IconUsersCircle,
} from "views/components/Icon/essentials";
import { IconBox } from "views/components/Icon/authoring";
import { IconLogout } from "views/components/Icon/arrows";
import { IconDown } from "views/components/Icon/basic";
import { IconKanban, IconPayment } from "views/components/Icon/pages";
import { client, constants } from "helpers";
import { setTab } from "views/pages/HostProfile/actions";
import { setTab as setTabCompany } from "views/pages/Company/actions";
import FeedbackModal from "./feedback-modal";

const { Header } = Layout;
const { router, userRoles, api } = constants;

const menuDefault = [
  // {
  //   name: "Dashboard",
  //   link: router.home,
  //   key: "dashboard",
  //   icon: <IconHome />,
  // },
  {
    name: "Products",
    link: router.products,
    key: "products",
    accessRequired: [userRoles.COMPANY],
    icon: <IconBox />,
  },
  {
    name: "Marketplace",
    link: router.marketPlace,
    key: "market-place",
    accessRequired: [userRoles.HOST],
    icon: <IconBox />,
  },
  {
    name: "Seller",
    link: router.hostList,
    key: "host-list",
    accessRequired: [userRoles.COMPANY],
    icon: <IconUsers />,
  },
  {
    name: "My Jobs",
    link: router.myJobs,
    key: "my-jobs",
    accessRequired: [userRoles.HOST],
    icon: <IconKanban />,
  },
  {
    name: "Payment",
    link: router.payment,
    key: "payment",
    accessRequired: [userRoles.HOST],
    icon: <IconPayment />,
  },
  {
    name: "Seller",
    link: router.recommenderSeller,
    key: "recommended-sellers",
    accessRequired: [userRoles.BRAND],
    icon: <IconUsers />,
  },
];

const logOut = (routerUrl) => {
  window.localStorage.clear();
  window.location.href = routerUrl;
};

const MainMenu = ({
  user,
  selectedKeys,
  notificationData = [],
  setProfileTab,
  setCompanyProfileTab,
  tab,
  name,
}) => {
  const [showFeedback, setShowFeedback] = React.useState(false);
  const { role, email } = user;
  const menu =
    role === userRoles.ADMIN
      ? menuDefault
      : menuDefault.filter(
          (item) => !item.accessRequired || item.accessRequired.includes(role)
        );

  const sendFeedback = async () => {
    await client.request({
      path: api.path.feedback,
      method: "post",
      data: {},
    });
  };

  return (
    <React.Fragment>
      <Header>
        <Link to="/">
          <LogoHorizontal />
        </Link>
        <Menu
          theme="light"
          mode="horizontal"
          className="ant-mod-main-menu"
          selectedKeys={selectedKeys}
          overflowedIndicator={false}
        >
          {menu.map((item) => (
            <Menu.Item key={item.key} icon={item.icon}>
              <Link to={item.link}>{item.name} </Link>
            </Menu.Item>
          ))}
        </Menu>
        <Menu
          theme="light"
          mode="horizontal"
          className="ant-mod-action-menu"
          overflowedIndicator={false}
        >
          {
            notificationData.length > 0 ? (
              <Menu.Item key="7">
                <Popover
                  placement="topLeft"
                  title="Notifications"
                  content={
                    <div className="menu-notification-content">Tesst</div>
                  }
                  trigger="click"
                  overlayClassName="menu-notification-popover"
                >
                  <Badge
                    count={
                      notificationData.filter((item) => !item.isRead).length
                    }
                  >
                    <BellOutlined style={{ fontSize: "24px" }} />
                  </Badge>
                </Popover>
              </Menu.Item>
            ) : null
            // <Menu.Item key="7">
            //   <Popover
            //     placement="topLeft"
            //     title="Notification"
            //     content={<div style={{ width: "320px" }}>Test</div>}
            //     trigger="click"
            //   >
            //     <BellOutlined style={{ fontSize: "24px" }} />
            //   </Popover>
            // </Menu.Item>
          }
        </Menu>
        <Popover
          content={
            <UserMenu
              setProfileTab={setProfileTab}
              setCompanyProfileTab={setCompanyProfileTab}
              setShowFeedback={setShowFeedback}
              tab={tab}
              user={user}
            />
          }
          overlayClassName="user-menu-popover"
        >
          <div className="user-menu">
            <UserAvatar user={user} name={name} />
            <div className="user-info">
              <div className="username">{name}</div>
              <div className="email">{email}</div>
            </div>
            <IconDown />
          </div>
        </Popover>
        <FeedbackModal
          visible={showFeedback}
          handleClose={() => setShowFeedback(false)}
          handleOk={() => sendFeedback()}
        />
      </Header>
    </React.Fragment>
  );
};

const UserMenu = ({
  setShowFeedback,
  setProfileTab,
  setCompanyProfileTab,
  tab,
  user,
}) => {
  const { role } = user;

  const routeLink = () => {
    if (role === "company") return router.companyProfile;
    return router.hostProfile;
  };

  const changeTab = (tab) => {
    if (role === "company") return setCompanyProfileTab(tab);
    return setProfileTab(tab);
  };

  return (
    <div className="user-menu-popover-content">
      <div className="user-menu-item">
        <NavLink
          className={tab === "1" ? "selected" : ""}
          to={routeLink()}
          onClick={() => changeTab("1")}
        >
          <IconUsersCircle /> My Profile
        </NavLink>
      </div>
      <div className="user-menu-item">
        <NavLink
          className={tab === "2" ? "selected" : ""}
          to={routeLink()}
          onClick={() => changeTab("2")}
        >
          <IconCog /> Account Settings
        </NavLink>
      </div>
      <Divider />
      <div
        className="user-menu-item"
        onClick={() => logOut(router.resetPassword)}
      >
        <IconPassword /> Reset Password
      </div>
      <div className="user-menu-item" onClick={() => setShowFeedback(true)}>
        <IconFeedback /> Feedback & Support
      </div>
      <Divider />
      <div className="user-menu-item" onClick={() => logOut(router.selectRole)}>
        <IconLogout /> Sign Out
      </div>
    </div>
  );
};

const mapStateToProps = ({ common, hostProfile }) => ({
  user: common.user,
  name: hostProfile.data && hostProfile.data.name,
  tab: hostProfile.tab,
});

const mapDispatchToProps = {
  setProfileTab: setTab,
  setCompanyProfileTab: setTabCompany,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
