import { Button, Col, Modal, Row } from "antd";

import { FeedbackIcon, FeedbackSendIcon } from "./icon";
import React from "react";

const FeedbackModal = ({ handleOk, handleClose, visible }) => {
  const [isSend, setIsSend] = React.useState(false);

  const handleClickOk = () => {
    setIsSend(true);
    handleOk();
  };

  React.useEffect(() => {
    setIsSend(false);
  }, [visible]);

  return (
    <Modal
      centered
      visible={visible}
      footer={false}
      closable={true}
      onCancel={handleClose}
      className="modal-feedback"
    >
      {isSend ? (
        <>
          <FeedbackSendIcon />
          <h4>Your request was sent</h4>
        </>
      ) : (
        <>
          <FeedbackIcon />
          <h4>Feedback and Support</h4>
          <p>
            If you need any feedback or support, please contact us:{" "}
            <a href="mailto:colin@captiveinteractive.sg">
              colin@captiveinteractive.sg
            </a>
          </p>
        </>
      )}
      {!isSend && (
        <Row>
          <Col span={12}>
            <Button onClick={handleClose} primary="true">
              Cancel
            </Button>
          </Col>
          <Col span={12}>
            <Button
              onClick={handleClickOk}
              type="primary"
              className="btn-approve"
            >
              Send request
            </Button>
          </Col>
        </Row>
      )}
    </Modal>
  );
};
export default FeedbackModal;
