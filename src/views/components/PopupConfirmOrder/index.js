import React, { useEffect } from 'react';
import { IconUpBig, IconDownBig, IconUploadProof, IconEditProof, IconDeleteProof, IconDownloadProof, IconPaid, IconCancel, IconCopy, IconOrder, IconFeeInfo } from "views/components/Icon/pages"
import { Collapse } from 'antd';
import { Modal, Button } from 'antd';
import _ from "lodash";
import { connect } from 'react-redux';
import { resetProof, uploadMedia, removeProof } from '../../pages/JobOrder/actions'
import { utils, ReactUtils, constants } from 'helpers';
import { useParams } from "react-router-dom"
import axios from "axios"

const { MEDIA_TYPE } = constants
const { getMediaUrl } = utils

const PAYMENT_STATUS = {
    verifying: {
        id: 'verifying',
        label: 'Verifying Payment',
        color: '#55ADFF'
    },
    pending: {
        id: 'pending',
        label: 'Pending payment',
        color: '#FF9A4D'
    },
    paid: {
        id: 'paid',
        label: 'Paid',
        color: '#1D1929'
    },
    canceled: {
        id: 'canceled',
        label: 'Canceled',
        color: '#F76969'
    }
}
const payment = {
    paymentID: 'PO10001',
    type: 'Order Submission',
    name: 'Black Chicken Mushroom Soup',
    status: PAYMENT_STATUS.canceled.id,
    amount: '$1,295.30',
    date: '01 Dec, 2021',
    isIncrease: false,
}

function PopupConfirmOrder({ visible, handleClose, handleOK, orderPricing, resetProof, proof, uploadMedia, removeProof }) {
    const { code } = useParams();
    useEffect(() => {
        resetProof()
    }, [])
    const onProofChange = async (e) => {
        const file = _.get(e, 'target.files[0]')
        const formData = new FormData();
        formData.append("file", file)
        const params = {
            ownerType: MEDIA_TYPE.HOST,
            ownerCode: code,
        }
        await uploadMedia(formData, params);
        uploadMedia({})
    }

    const copyToClipBoard = (text) => {
        ReactUtils.messageSuccess({ content: 'Copied to clipboard' });
        navigator.clipboard.writeText(text)
    }

    const download = (uri) => {
        try {
            axios({
                url: uri,
                method: 'GET',
                responseType: 'blob'
            })
                .then((response) => {
                    const url = window.URL
                        .createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    uri = uri.split('.')
                    const type = uri[uri.length - 1]
                    link.setAttribute('download', 'proof.' + type);
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                })
        }
        catch (err) {
            console.log(err)
        }
    }

    return (
        <Modal
            centered
            visible={visible}
            footer={null, null}
            onCancel={handleClose}
        >
            <div className="confirm-order payment-detail__container">
                <div className="payment-detail__info">
                    {payment.isIncrease ? <span><IconUpBig /></span> : <span><IconDownBig /></span>}
                    <p className="payment-detail__info__title">Order Submission</p>
                    <p>Send the amount shown to the banking details below:</p>
                    <p>Pay amount:</p>
                    <p className="payment-detail__info__amount">${_.get(orderPricing, 'total') ? _.get(orderPricing, 'total').toFixed(2) : 0}</p>
                </div>
                <div className="payment-detail__card">
                    <div className="payment-detail__card__status">
                        {/* <p className="payment-detail__card__status__tag" style={{
                            border: `1px solid ${PAYMENT_STATUS.canceled.color}`,
                            color: PAYMENT_STATUS.canceled.color
                        }}></p> */}
                        <table>
                            <tr>
                                <td>Payment ID</td>
                                <td>PO1004 <a onClick={() => copyToClipBoard('PO1004')}><IconCopy /></a></td>
                            </tr>
                        </table>
                    </div>
                    <div className="payment-detail__card__info">
                        <table>
                            <tr>
                                <td>Account Holder</td>
                                <td>Captive Interactive Co.</td>
                            </tr>
                            <tr>
                                <td>Bank Name</td>
                                <td>International Bank of Singapore</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>Singapore</td>
                            </tr>
                            <tr>
                                <td>Bank Account No.</td>
                                <td>SGB1232085455555 <a onClick={() => copyToClipBoard('SGB1232085455555')}><IconCopy /></a></td>
                            </tr>
                        </table>
                    </div>
                    <div className="payment-detail__card__proof">
                        {
                            !(proof && proof.length > 0) ? <div className="payment-detail__card__proof--nofile">
                                <p>Remember to check the numbers carefully and include the Payment ID in your transfer’s note.</p>

                            </div> : <>
                                    <p className="payment-detail__card__proof__title">Proof Upload</p>
                                    {
                                        proof.map((item, index) => (
                                            <div style={{
                                                marginBottom: 15
                                            }}>

                                                <div className="payment-detail__card__proof__file">
                                                    <div className="payment-detail__card__proof__file__preview">
                                                        <img src={getMediaUrl(item)} alt="proof" />
                                                    </div>
                                                    <div className="payment-detail__card__proof__file__info">
                                                        <p>Proof {index + 1}</p>
                                                    </div>
                                                    <div className="payment-detail__card__proof__file__action">
                                                        {/* <a><IconEditProof /></a> */}
                                                        <a onClick={() => download(getMediaUrl(item))}><IconDownloadProof /></a>
                                                        <a onClick={() => removeProof(item)}><IconDeleteProof /></a>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }

                                </>
                        }
                        <div className="payment-detail__card__proof--nofile" style={{
                            marginTop: 15
                        }}>
                            <input onChange={onProofChange} style={{ display: 'none' }} id="proof" type="file" name="myImage" accept="image/png, image/gif, image/jpeg, application/pdf" />
                            <div>
                                <label for="proof" style={
                                    {
                                        width: '50%'
                                    }
                                } className="payment-detail__card__proof__action"><span><IconUploadProof /></span> Upload Proof</label>
                                <a onClick={handleOK}>Done</a>
                            </div>
                        </div>
                    </div>
                    {/* <div className="payment-detail__card__action">
                        <a className="payment-detail__card__action--paid"><span><IconPaid /></span>Paid</a>
                        <a className="payment-detail__card__action--cancel"><span><IconCancel /></span>Cancelled</a>
                    </div> */}
                </div>
            </div>
        </Modal>
    );
}

const mapStateToProps = ({ jobOrder }) => ({
    job: jobOrder.job,
    orderPricing: jobOrder.orderPricing,
    proof: jobOrder.proof
});

const mapDispatchToProps = {
    resetProof,
    removeProof,
    uploadMedia
};
export default
    connect(mapStateToProps, mapDispatchToProps)(PopupConfirmOrder)
    ;
