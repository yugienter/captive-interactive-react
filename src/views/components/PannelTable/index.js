import React from 'react'
import { Row, Col, Tag, Progress } from 'antd'
import RightTable from './components/RightTable'
import {
  IconPricing,
  IconStream,
  IconClock,
  IconClockGrey,
  IconAppoint,
} from "views/components/Icon/myjobs";

const PannelTable = () => {
  return (
    <div className='pannel-table'>
      <Row style={{width: '100%'}}>
        <Col span={7}>
          <div className="drawer-submit-proposal__result" style={{marginRight: '30px'}}>
            <div className="drawer-submit-proposal__result__title">
              <p>Campaign 1</p>
              <p>3 bidders with lowest rate will be selected</p>
            </div>
            <div className="drawer-submit-proposal__result__duration">
              <p className='green'>
                <IconClock /> 12 days 8h 40m 2s
              </p>
              {/* <p className='grey'>
                <IconClockGrey /> 12 days 8h 40m 2s
              </p>
              <p className='green'>
                <IconAppoint /> 12 days 8h 40m 2s
              </p> */}
              <Progress
                strokeColor="#fff"
                percent={50}
                showInfo={false}
              />
              {/* <Progress
                strokeColor="#fff"
                percent={0}
                showInfo={false}
                className='progress-grey'
              />
              <Progress
                strokeColor="#fff"
                percent={0}
                showInfo={false}
                className='progress-green'
              /> */}
            </div>
            <div className="drawer-submit-proposal__result__deal">
              <p>$20-$120/hr</p>
              <p>
                Commission: <span>Negotiable</span>
              </p>
            </div>
            <div className="drawer-submit-proposal__result__table">
              <p>
                <IconPricing /> Product Price
              </p>
              <table>
                <tr>
                  <td>Public RRP</td>
                  <td>$14.99</td>
                </tr>
                <tr>
                  <td>Discount</td>
                  <td>10%</td>
                </tr>
                <tr>
                  <td>Discounted Price</td>
                  <td>$13.49</td>
                </tr>
                <tr>
                  <td>Discount Period</td>
                  <td>12 Jan, 2021 - 12 Mar, 2021</td>
                </tr>
              </table>
            </div>
            <div className="drawer-submit-proposal__result__table">
              <p>
                <IconStream /> Stream Details
              </p>
              <table>
                <tr>
                  <td>Time Suggestion</td>
                  <td>12 Jan, 2021</td>
                </tr>
                <tr>
                  <td>Type of Stream</td>
                  <td>1 Cam + 1 Host</td>
                </tr>
                <tr>
                  <td>Elements</td>
                  <td>Studio + Overlay</td>
                </tr>
                <tr>
                  <td>Platform</td>
                  <td>Shopee, Lazada, Facebook, Instagram</td>
                </tr>
              </table>
            </div>
            <div className="drawer-submit-proposal__result__message">
              <p className="sub-title">Message</p>
              <p className="fontStyle13">
                Sign up for our monthly newsletter to get more details
                such as featured articles, upcoming training and
                webinars, free. Sign up for our monthly newsletter to
                get more details such as featured articles, upcoming
                training and webinars, free
              </p>
            </div>
          </div>
        </Col>
        <Col span={17}>
          <div className='wrapper-filter'>
            <div className='item selected'>
              All Statuses <span>(54)</span>
            </div>
            <div className='item'>
              Bid Received <span>(19)</span>
            </div>
            <div className='item'>
              Negotiating <span>(5)</span>
            </div>
            <div className='item'>
              Waiting to Execute <span>(3)</span>
            </div>
            <div className='item'>
              Completed <span>(12)</span>
            </div>
            <div className='item reject'>
              Reject <span>(15)</span>
            </div>
          </div>
          <RightTable />
        </Col>
      </Row>
    </div>
  )
}

export default PannelTable
