import React, { useState } from "react";
import { Table, Space, Menu } from "antd";
import {
  IconExpand,
  IconNoted,
  IconDots,
  IconCompleteGrey,
  IconCompleteYellow,
  IconNotedFull,
  IconPropose,
  IconTickGreen,
  IconX,
  IconStar,
  IconStarYellow,
} from "views/components/Icon/myjobs";

const RightTable = () => {
  const columns = [
    {
      title: "Seller",
      dataIndex: "seller",
      key: "seller",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Followers",
      dataIndex: "followers",
      key: "followers",
    },
    {
      title: "Fixed Rate",
      dataIndex: "hourly",
      key: "hourly",
    },
    {
      title: "Commission",
      dataIndex: "commission",
      key: "commission",
    },
    {
      title: "Date Modified",
      dataIndex: "modified",
      key: "modified",
    },
    {
      title: "Stream Date",
      dataIndex: "stream",
      key: "stream",
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (text, record) => (
        console.log("record", record),
        (
          <Space size="middle">
            <a>
              <IconExpand />
            </a>
            <>{renderIconNoted(record)}</>
            <>{renderIcon(record)}</>
          </Space>
        )
      ),
    },
  ];
  const data = [
    {
      key: "1",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  className="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Albert Flores</p>
            </div>
          </div>
          <div className="last"></div>
        </div>
      ),
      status: <div className="status direct">Direct Offer</div>,
      followers: <div className="followers">184.3K</div>,
      hourly: <div className="hourly">$850</div>,
      commission: <div className="commission"></div>,
      modified: <div className="modified">Today</div>,
      stream: <div className="stream">--</div>,
    },
    {
      key: "2",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Arlene McCoy</p>
            </div>
          </div>
          {/* <div className='last'><span className='bid green'></span></div> */}
        </div>
      ),
      status: <div className="status received">Bid Received</div>,
      followers: <div className="followers">27.7K</div>,
      hourly: <div className="hourly"></div>,
      commission: <div className="commission">10%</div>,
      modified: <div className="modified">2 days ago</div>,
      stream: <div className="stream">--</div>,
    },
    {
      key: "3",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Jane Cooper</p>
            </div>
          </div>
          {/* <div className='last'><span className='bid green'></span></div> */}
        </div>
      ),
      status: <div className="status negotiating bidSubmited">Negotiating</div>,
      followers: <div className="followers">1.2K</div>,
      hourly: <div className="hourly"></div>,
      commission: <div className="commission">20%</div>,
      modified: <div className="modified">5 days ago</div>,
      stream: <div className="stream">--</div>,
    },
    {
      key: "4",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Theresa Webb</p>
            </div>
          </div>
          <div className="last"></div>
        </div>
      ),
      status: <div className="status negotiating">Negotiating</div>,
      followers: <div className="followers">13.7K</div>,
      hourly: <div className="hourly">$20</div>,
      commission: <div className="commission">10%</div>,
      modified: <div className="modified">02 Aug, 2021</div>,
      stream: <div className="stream">--</div>,
    },
    {
      key: "5",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Cody Fisher</p>
            </div>
          </div>
          <div className="last"></div>
        </div>
      ),
      status: <div className="status waiting">Waiting to Execute</div>,
      followers: <div className="followers">956</div>,
      hourly: <div className="hourly"></div>,
      commission: <div className="commission">10%</div>,
      modified: <div className="modified">01 May, 2021</div>,
      stream: <div className="stream">01 May, 2021</div>,
    },
    {
      key: "6",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Ralph Edwards</p>
            </div>
          </div>
          {/* <div className='last'><span className='bid grey'></span></div> */}
        </div>
      ),
      status: <div className="status waiting">Waiting to Execute</div>,
      followers: <div className="followers">956</div>,
      hourly: <div className="hourly">$95</div>,
      commission: <div className="commission"></div>,
      modified: <div className="modified">31 Mar, 2021</div>,
      stream: <div className="stream">31 Mar, 2021</div>,
    },
    {
      key: "7",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Floyd Miles</p>
            </div>
          </div>
        </div>
      ),
      status: <div className="status complete">Completed</div>,
      followers: <div className="followers">22K</div>,
      hourly: <div className="hourly"></div>,
      commission: <div className="commission">7.5%</div>,
      modified: <div className="modified">07 Feb, 2021</div>,
      stream: <div className="stream">07 Feb, 2021</div>,
    },
    {
      key: "8",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Robert Fox</p>
            </div>
          </div>
          {/* <div className='last'><span className='bid green'></span></div> */}
        </div>
      ),
      status: <div className="status complete">Completed</div>,
      followers: <div className="followers">22K</div>,
      hourly: <div className="hourly">$120</div>,
      commission: <div className="commission"></div>,
      modified: <div className="modified">07 Feb, 2021</div>,
      stream: <div className="stream">07 Feb, 2021</div>,
    },
    {
      key: "9",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Cameron Williamson</p>
            </div>
          </div>
        </div>
      ),
      status: <div className="status complete">Completed</div>,
      followers: <div className="followers">22K</div>,
      hourly: <div className="hourly">$20</div>,
      commission: <div className="commission">12%</div>,
      modified: <div className="modified">22 Feb, 2021</div>,
      stream: <div className="stream">22 Feb, 2021</div>,
    },
    {
      key: "10",
      seller: (
        <div className="box-products-wrapper">
          <div className="box-products">
            <div className="box-products__left">
              <figure>
                <img
                  class="dropdown-img"
                  src="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=60"
                />
              </figure>
            </div>
            <div className="box-products__right">
              <p className="last">Bessie Cooper</p>
            </div>
          </div>
          {/* <div className='last'><span className='bid grey'></span></div> */}
        </div>
      ),
      status: <div className="status reject">Rejected</div>,
      followers: <div className="followers">22K</div>,
      hourly: <div className="hourly">$110</div>,
      commission: <div className="commission">10%</div>,
      modified: <div className="modified">27 Jan, 2021</div>,
      stream: <div className="stream">27 Jan, 2021</div>,
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const popupIconDefault = (
    <div className="box-default__popup">
      <Menu>
        <Menu.Item key="0">
          <a href="">
            <IconPropose />
            Propose a deal
          </a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <a href="">
            <IconTickGreen />
            Accept
          </a>
        </Menu.Item>
        <Menu.Item key="3">
          <IconX />
          Reject
        </Menu.Item>
      </Menu>
    </div>
  );

  function renderIcon(record) {
    // <IconStar />
    // <IconStarYellow />
    const iconDefault = (
      <a className="box-default">
        <IconDots />
      </a>
    );
    if (record.status.props.children === "Bid Submitted") {
      return (
        <a className="box-tooltip">
          {
            <div className="box-tooltip__content">
              Wait for Seller to response your proposed deal
            </div>
          }
        </a>
      );
    } else if (
      record.status.props.children === "Negotiating" &&
      record.commission.props.children === undefined
    ) {
      return (
        <a className="box-tooltip">
          {
            <div className="box-tooltip__content">
              Wait for Seller to response your proposed deal
            </div>
          }
        </a>
      );
    } else if (
      record.status.props.children === "Completed" &&
      record.commission.props.children === undefined
    ) {
      return (
        <a className="box-tooltip">
          <IconCompleteGrey />
          {
            <div className="box-tooltip__content box-tooltip__content--verified">
              Wait for verifying
            </div>
          }
        </a>
      );
    } else if (
      record.status.props.children === "Completed" &&
      record.followers.props.children === undefined
    ) {
      return (
        <a className="box-tooltip">
          <IconCompleteYellow />
          {
            <div className="box-tooltip__content box-tooltip__content--verified">
              Verified by company
            </div>
          }
        </a>
      );
    } else if (record.status.props.children === "Rejected") {
      return "";
    } else {
      return iconDefault;
    }
  }
  function renderIconNoted(record) {
    const iconDefault = (
      <a>
        <IconNoted />
      </a>
    );
    if (
      record.followers.props.children === undefined ||
      record.commission.props.children === undefined ||
      record.status.props.children === "Rejected"
    )
      return (
        <a>
          <IconNotedFull />
        </a>
      );
    return iconDefault;
  }
  function onChange(pagination, filters, sorter, extra) {
    console.log("params", pagination, filters, sorter, extra);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
    onSelect: (record, selected, selectedRows) => {
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      console.log(selected, selectedRows, changeRows);
    },
  };
  const onRow = (r) => ({
    onClick: () => {
      console.log(r);
      if (r.status.props.children === "Bid Submitted") {
        return alert("a");
      } else {
        return;
      }
    },
  });
  return (
    <div className="table-sellers">
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        pagination={{ pageSize: 10, showSizeChanger: true }}
        onRow={onRow}
      />
    </div>
  );
};

export default RightTable;
