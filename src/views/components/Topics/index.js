import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react';
import { Tag } from 'antd';
import HostTag from 'views/pages/Host/parts/HostTag';
import { ReactUtils } from 'helpers';

const { CheckableTag } = Tag;

const Topics = ({ updateHostProfile, tags, host, next }, ref) => {
  const [tagsData, setTagsData] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  useEffect(() => {
    if (!host || !host.tags) return;
    const selectedTags = [];
    const data = tags.map(parent => {
      return {
        id: parent.id,
        categories: parent.name,
        data: parent.item.map(child => {
          const tag = {
            id: child.id,
            icon: HostTag({ tag: child.name }).props.icon,
            name: child.name
          };
          if (host.tags.includes(tag.id)) {
            selectedTags.push(tag);
          }
          return tag;
        })
      };
    });
    setTagsData(data);
    setSelectedTags(selectedTags);
  }, [tags, setTagsData, setSelectedTags, host]);

  useImperativeHandle(ref, () => ({
    onContinue: async () => {
      await updateHostProfile({ tags: selectedTags.map(tag => tag.id) });
      next && next();
    }
  }));

  const handleSelectedTags = (tag, checked) => {
    let nextSelectedTags = selectedTags;
    if (checked && selectedTags.length < 5) {
      nextSelectedTags = [...selectedTags, tag];
    } else if (!checked) {
      nextSelectedTags = selectedTags.filter(t => t !== tag);
    } else {
      ReactUtils.messageWarn({ content: 'Limit of 5 categories reached' });
    }
    setSelectedTags(nextSelectedTags)
  }
  return (
    <>
      <h2>What is your Topics?</h2>
      <div className='text-bold mb36'>Select up to 5 that best describe the areas of your interest</div>
      {/* <Tags /> */}
      {tagsData.map(tag => (
        <>
          <div className='topics-categories'>{tag.categories}</div>
          <div className='topics-tags'>
            {tag.data.map(item => (
              <CheckableTag
                key={item.id}
                className={selectedTags.length > 5 ? 'none-cheked' : ''}
                checked={selectedTags.indexOf(item) > -1}
                onChange={checked => handleSelectedTags(item, checked)}
              >
                {item.icon} {item.name}
              </CheckableTag>
            ))}
          </div>
        </>
      ))}
    </>
  )
}

export default forwardRef(Topics);