import { Collapse } from 'antd';
import HeaderDeal from './HeaderDeal';
import ContentDeal from './ContentDeal';
import { ExpandIcon } from 'views/components/Icon/myjobs';

const { Panel } = Collapse;

const CollapseSellerActivity = ({ deal }) => {
  return (
    <Collapse
      bordered={false}
      expandIcon={(panelProps) => <ExpandIcon {...panelProps} />}
      expandIconPosition="right"
      className="site-collapse-custom-collapse older-activities-collapse collapse-main"
    >
      <Panel
        header={<HeaderDeal deal={deal} />}
        className="site-collapse-custom-panel"
      >
        <ContentDeal deal={deal} />
      </Panel>
    </Collapse>
  );
};

export default CollapseSellerActivity;
