import { useState } from "react";
import { connect } from "react-redux";
import {
  IconYoutube,
  IconBtnPropose,
  IconBtnAccept,
  IconBtnReject,
  IconBtnComplete,
} from "views/components/Icon/collapse";
import BoxStatus from "./BoxStatus";
import moment from "moment";
import { constants, utils } from "helpers";
import { IconExpand } from "views/components/Icon/myjobs";
import { Button } from "antd";
import { IconLiveStream, IconStreamUpload } from "views/components/Icon/status";
import TextExpandable from "../TextExpandable";
import DrawerCompleteJob from "../DrawerCompleteJob";
import { IconSendMessageSeller } from "../Icon/conversation";
import DrawerConversation from "../Conversation/DrawerConversation";
import DrawerSubmitProposalMyJobs from "../DrawerSubmitProposalMyJobs";
import { acceptDeal, rejectDeal } from "views/pages/MyJobs/actions";
import { useHistory } from "react-router-dom"
import _ from "lodash"

const { statusDeal } = constants;

const ContentDeal = ({ deal, noFooter = false, acceptDeal, rejectDeal }) => {
  const history = useHistory()
  const [showProposeADeal, setShowProposeADeal] = useState(false);
  const [completeDeal, setCompleteDeal] = useState(false);
  const [showDrawerConversation, setShowDrawerConversation] = useState(false);
  const onShowDrawerConversation = () => {
    setShowDrawerConversation(true);
  };
  const onCloseDrawerConversation = () => {
    setShowDrawerConversation(false);
  };

  if (!deal) return "";

  const proposeADeal = () => {
    setShowProposeADeal(true);
  };

  const closeProposeADealModal = () => {
    setShowProposeADeal(false);
  };

  const streamUploads = (
    <>
      <h4 style={{ marginTop: 18 }}>
        <IconStreamUpload />
        Stream Uploads
      </h4>
      <div className="box-details">
        <div className="left">Attachments</div>
      </div>
      {(deal.streamMedia || []).map((link) => {
        return (
          <div>
            <IconLiveStream />
            <Button type="link" href={utils.getMediaUrl(link)} target="_blank">
              {link}
            </Button>
          </div>
        );
      })}
      <div className="box-details">
        <div className="left">Stream Links</div>
      </div>
      {(deal.streamLink || []).map((link) => {
        return (
          <div>
            <IconExpand />
            <Button type="link" href={link} target="_blank">
              {link}
            </Button>
          </div>
        );
      })}
      <hr
        className="dash-divider"
        style={{ border: "2px dashed #E8E8EA", margin: 0 }}
      />
    </>
  );

  const handleAcceptDeal = async () => {
    await acceptDeal(deal.code);
  };

  const handleRejectDeal = async () => {
    await rejectDeal(deal.code);
  };

  const handleCompleteJob = async () => {
    setCompleteDeal(deal);
  };

  const handleSubmitOrder = () => {
    history.push('/job-order/' + _.get(deal, 'jobCode'))
  }

  if (deal.status === statusDeal.verified) {
    return (
      <div className="main-content">
        <BoxStatus deal={deal} />
        <div>
          <span style={{ color: "#A5A3A9", fontSize: 13 }}>
            Estimated Payout Date:
          </span>{" "}
          <strong>{utils.toDateString(deal.createdAt)}</strong>
        </div>
      </div>
    );
  }

  const renderFooter = () => {
    if (noFooter) return;

    if ([statusDeal.opens, statusDeal.deal].includes(deal.status))
      return (
        <>
          <hr className="linear-divider" />
          <button className="btn-received propose" onClick={proposeADeal}>
            <IconBtnPropose />
            Propose a deal
          </button>
          <div className="group-button">
            <button className="btn-received accept" onClick={handleAcceptDeal}>
              <IconBtnAccept />
              Accept
            </button>
            <button className="btn-received reject" onClick={handleRejectDeal}>
              <IconBtnReject />
              Reject
            </button>
          </div>
        </>
      );

    if (deal.status === statusDeal.accepted && !deal.fixedRatePaymentRequired)
      return (
        <>
          <hr className="linear-divider" />
          {/* <Button
            className="view-activity-button"
            style={{
              marginBottom: "20px",
              background: "#fff",
            }}
            onClick={onShowDrawerConversation}
          >
            <IconSendMessageSeller />
            &nbsp;&nbsp;Send Message
          </Button> */}
          <button className="btn-received complete" onClick={handleSubmitOrder}>
            <IconBtnComplete />
            Submit order
          </button>
        </>
      );
  };

  return (
    <>
      <div className="main-content">
        <BoxStatus deal={deal} />
        <hr className="dash-divider" />
        <h4>
          <IconYoutube />
          Stream Details
        </h4>
        <div className="box-details">
          <div className="left">Time Suggestion</div>
          <div className="right">
            {moment(deal.streamTime?.description).format("DD MMM, YYYY")}
          </div>
        </div>
        <div className="box-details">
          <div className="left">Type of Stream</div>
          <div className="right">{deal.streamType?.description}</div>
        </div>
        <div className="box-details">
          <div className="left">Elements</div>
          <div className="right">
            {deal.streamElement?.description?.map((element, index) => {
              return index > 0 ? ` + ${element}` : element;
            })}
          </div>
        </div>
        <div className="box-details">
          <div className="left">Platform</div>
          <div className="right">
            {deal.streamPlatform?.description?.map((element, index) => {
              return index > 0 ? `, ${element}` : element;
            })}
          </div>
        </div>
        <hr className="dash-divider" />
        {deal.status === statusDeal.complete && streamUploads}
        <h5 className="message">Message</h5>
        <TextExpandable text={deal.note} />
        {renderFooter()}
      </div>
      <DrawerSubmitProposalMyJobs
        deal={deal}
        visible={showProposeADeal}
        handleClose={closeProposeADealModal}
      />
      <DrawerCompleteJob
        showDrawer={completeDeal}
        onCloseDrawer={() => setCompleteDeal(false)}
        deal={completeDeal}
      />
      <DrawerConversation
        showDrawer={showDrawerConversation}
        onCloseDrawer={onCloseDrawerConversation}
        record={{ ...deal, code: deal.jobCode }}
      />
    </>
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  acceptDeal,
  rejectDeal,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContentDeal);
