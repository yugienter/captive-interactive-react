import { statusDeal, utils } from 'helpers';
import { IconMessageSend } from 'views/components/Icon/myjobs';
import {
  Icon2,
  IconGreyGerald,
  IconVerify,
} from 'views/components/Icon/status';

const HeaderDeal = ({ deal }) => {
  if (!deal) {
    return '';
  }

  const timeString = utils.toDateString(deal.createdAt, {
    format: 'DD MMM, YYYY hh:mm',
  });
  const renderIcon = () => {
    if (deal.status === statusDeal.complete) {
      return <IconGreyGerald />;
    }
    if (deal.status === statusDeal.verified) {
      return <IconVerify />;
    }
    if (deal.status === statusDeal.accepted) {
      return <Icon2 />;
    }
    return <IconMessageSend />;
  };

  const renderContent = () => {
    switch (deal.status) {
      case statusDeal.proposal: {
        return (
          <p className="send">
            <strong>You</strong> sent a pitch
          </p>
        );
      }
      case statusDeal.opens:
      case statusDeal.deal: {
        return (
          <p className="send">
            <strong>Company</strong> sent you a deal
          </p>
        );
      }
      case statusDeal.accepted: {
        return (
          <p className="send">
            <strong>You</strong> accepted a deal
          </p>
        );
      }
      case statusDeal.rejected: {
        return (
          <p className="send">
            <strong>Company</strong> rejected a deal
          </p>
        );
      }
      case statusDeal.complete: {
        return (
          <p className="send">
            <strong>You</strong> completed this job
          </p>
        );
      }
      case statusDeal.verified: {
        return (
          <p className="send">
            <strong>Company</strong> verified completement
          </p>
        );
      }
      default: {
        return (
          <p className="send">
            {deal.status} {deal.statusJob}
          </p>
        );
      }
    }
  };

  return (
    <div className="header-main">
      <div className="default">
        <div className="first">{renderIcon()}</div>
        <div className="center">
          {renderContent()}
          <p className="date">{timeString}</p>
        </div>
      </div>
    </div>
  );
};

export default HeaderDeal;
