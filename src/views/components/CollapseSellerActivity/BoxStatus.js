import {
  IconProposal,
  IconHeadAccept,
  IconHeadReject,
} from "views/components/Icon/collapse";
import { constants } from "helpers";
import {
  Icon3,
  IconBlueCompletement,
  IconOrganReceived,
} from "views/components/Icon/status";

const { statusDeal } = constants;

const BoxStatus = ({ deal }) => {
  if (!deal) return "";

  const renderContent = (child, color = "black", noCommission = false) => {
    const streamCommissionText = () => {
      if (deal?.streamCommission?.description) {
        return `${deal.streamCommission.description || 0}${
          deal.streamCommissionUnit
        }`;
      }
      if (deal.streamCommission.negiable) {
        return "Negotiable";
      }
      return "";
    }
    return (
      <div className="box-status">
        {child}
        {!noCommission && (
          <>
            <div className={`spend-hours ${color}`}>
              {`$${deal?.streamHourlyRate?.description || "0"}/hr`}
            </div>
            <div className="commission">
              Commission:{" "}
              <strong>
                <span className={color}>
                  {streamCommissionText()}
                </span> 
              </strong>
            </div>
          </>
        )}
      </div>
    );
  };

  const status = deal.status;
  switch (status) {
    case statusDeal.opens:
    case statusDeal.deal: {
      return renderContent(
        <div className="status orange">
          <IconOrganReceived />
          <strong>You received a deal</strong>
        </div>,
        "blue"
      );
    }
    case statusDeal.proposal: {
      return renderContent(
        <div className="status grey">
          <IconProposal />
          Your proposal was sent
        </div>,
        "black"
      );
    }
    case statusDeal.accepted: {
      if (deal.fixedRatePaymentRequired) {
        return renderContent(
          <div className="status grey">
            <IconHeadAccept />
            Waiting for Company pay for fixed rate
          </div>,
          "black"
        );
      }
      return renderContent(
        <div className="status green">
          <IconHeadAccept />
          The deal is approved
        </div>,
        "green"
      );
    }
    case statusDeal.rejected: {
      return renderContent(
        <div className="status red">
          <IconHeadReject />
          The deal is rejected
        </div>,
        "black"
      );
    }
    case statusDeal.complete: {
      return renderContent(
        <div className="status blue">
          <IconBlueCompletement />
          You submitted completement
        </div>,
        "black"
      );
    }
    case statusDeal.verified: {
      return renderContent(
        <div className="status yellow">
          <Icon3 />
          Processing for payment
        </div>,
        "yellow",
        true
      );
    }
    default:
      return null;
  }
};

export default BoxStatus;
