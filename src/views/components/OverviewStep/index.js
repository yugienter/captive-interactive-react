import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react'
import { Form, Input, Row, Col, Upload, message, Image } from "antd";
import { IconPorfolio, IconUpload } from 'views/components/Icon/pages';
import { LoadingOutlined } from '@ant-design/icons';
import { constants } from 'helpers';

const { MEDIA_TYPE } = constants;
const { TextArea } = Input;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const OverviewStep = ({ updateHostProfile, uploadMedia, email, host, next }, ref) => {
  const [form1] = Form.useForm();
  const [form2] = Form.useForm();

  useImperativeHandle(ref, () => ({
    onContinue: () => {
      form1.submit();
      form2.submit();
    }
  }));

  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState(host ? host.avatar : null);

  useEffect(() => {
    form2.setFieldsValue({
      aboutMe: host ? host.aboutMe : '',
      porfolio: host ? host.porfolio : '',
    });
    setImageUrl(host ? host.avatar : null);

  }, [host, setImageUrl, form2]);
  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      updateHostProfile({ avatar: info.file.response.data.url });

      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        setImageUrl(imageUrl),
        setLoading(false)
      );
      form1.resetFields();
    }
  };
  const uploadButton = (
    <div className='upload'>
      {loading ? <LoadingOutlined /> : <IconUpload />}
      <div className='text'>Upload</div>
    </div>
  );

  const onFinish = async (values) => {
    const update = {};
    if (values.aboutMe !== host.aboutMe) {
      update.aboutMe = values.aboutMe;
    }

    if (values.porfolio !== host.porfolio) {
      update.porfolio = values.porfolio;
    }

    Object.keys(update).length && await updateHostProfile(update);
    next && next();
  };

  const upload = () => {
    const url = uploadMedia({ ownerType: MEDIA_TYPE.HOST, ownerCode: host.code, noCreate: true });
    return url;
  }

  const validateAvatar = (rule, value, callback) => {
    if (!imageUrl) return callback('Avatar is missing');
    callback();
  }

  return (
    <>
      <h2>Hi, {host ? host.name : ''}! Tell us more about you</h2>
      <div className='text-bold mb36'>This information will help us to get you started quickly</div>
      <Row>
        <Col span={8}>
          <Form form={form1} layout="vertical">
            <Form.Item
              label="Avatar"
              name="avatar"
              className='form-textarea section-scroll_overview'
              rules={[{
                required: true,
                validator: validateAvatar
              }]}
            >
              
                <Upload
                  accept={constants.FILE_TYPES}
                  name="file"
                  listType="picture-card"
                  className="avatar-uploader avatar-image"
                  showUploadList={false}
                  action={upload}
                  beforeUpload={beforeUpload}
                  onChange={handleChange}
                >
                  {imageUrl ? <Image width={194} preview={false} src={imageUrl} className='avatar-image_view' alt="avatar"  /> : uploadButton}
                </Upload>
              
            </Form.Item>
          </Form>
        </Col>
        <Col span={14}>
          <Form onFinish={onFinish} form={form2} layout="vertical">
            <Form.Item
              name="aboutMe"
              rules={[{ required: true, message: 'Biography is missing' }]}
              label="Biography" className='form-textarea'
            >
              <TextArea rows={9} />
            </Form.Item>
            <Form.Item name="porfolio" label="Portfolio (Showreel or website)">
              <Input
                size="large"
                className="input-basic"
                prefix={<IconPorfolio />}
                placeholder="(Optional)"
              />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </>
  )
}

export default forwardRef(OverviewStep);