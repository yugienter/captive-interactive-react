import { Carousel } from "antd";

// temp data
import influencdeImage from "./media/influencde.png";
import { IconTickCircle } from "views/components/Icon/pages"

const InfluencdeSlider = () => (
  <Carousel className="welcome-slider">
    <div className="welcome-slider-item">
      <img src={influencdeImage} alt="influencde" />
      <h3>Influence and Creativity, On Demand</h3>
      <ul>
        <li><IconTickCircle />Showcase your skills and earn money for your talents</li>
        <li><IconTickCircle />Take control of your creative partnerships</li>
        <li><IconTickCircle />Name your price and gain visibility with companies looking to collaborate</li>
      </ul>
    </div>
    <div className="welcome-slider-item">
      <img src={influencdeImage} alt="influencde" />
      <h3>Influence and Creativity, On Demand</h3>
      <p>
        Engage with your consumers. Add a dimension LIVE to digital marketing.
      </p>
    </div>
    <div className="welcome-slider-item">
      <img src={influencdeImage} alt="influencde" />
      <h3>Influence and Creativity, On Demand</h3>
      <p>
        Engage with your consumers. Add a dimension LIVE to digital marketing.
      </p>
    </div>
  </Carousel>
);
export default InfluencdeSlider;
