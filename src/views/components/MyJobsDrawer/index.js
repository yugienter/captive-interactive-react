import { Drawer } from 'antd';
import { connect } from 'react-redux';
import { IconCloseDrawer } from 'views/components/Icon/drawer';
import { getSellerActivities } from 'views/pages/DetailProduct/components/actions';
import CollapseSellerActivity from '../CollapseSellerActivity';

const MyJobsDrawer = ({ showDrawer, onCloseDrawer, deals }) => {
  const handleClose = () => {
    onCloseDrawer && onCloseDrawer();
  };

  const currentDeal = deals[0];
  const oldDeals = deals.slice(1) || [];
  return (
    <>
      <Drawer
        className="drawer-main drawer-main-myjobs"
        placement="right"
        width={420}
        onClose={handleClose}
        visible={showDrawer}
        closeIcon={<IconCloseDrawer />}
        title={<div className="drawer-main__header--myjobs">Activities</div>}
      >
        <div
          className="drawer-main-myjobs"
          style={{ width: 360, margin: '0 auto' }}
        >
          {currentDeal && <CollapseSellerActivity deal={currentDeal} />}
          {!!oldDeals.length && (
            <div className="older-activities">
              <div className="title-older">Older Activities</div>
              {oldDeals.map((activity) => (
                <CollapseSellerActivity deal={activity} />
              ))}
            </div>
          )}
        </div>
      </Drawer>
    </>
  );
};

const mapStateToProps = ({ jobReducer: { deals = [] } }) => ({
  deals,
});

const mapDispatchToProps = {
  getSellerActivities,
};
export default connect(mapStateToProps, mapDispatchToProps)(MyJobsDrawer);
