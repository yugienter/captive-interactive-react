import { Progress, Tag } from "antd";
import { connect } from "react-redux";
import { constants, utils } from "helpers";
import moment from "moment";
import "styles/product-campaign-summary.less";
import {
  IconAppoint,
  IconClock,
  IconClockGrey,
  IconPricing,
  IconStream,
} from "../Icon/myjobs";
import Timer from "../Timer";
import _ from "lodash";

const { CAMPAIGN_STATUS } = constants;

const CampaignDetailPanel = ({ jobDetail }) => {
  const { campaign = {}, deal: dealDetail = {} } = jobDetail || {};

  const getProgressPercentage = () => {
    try {
      const end = moment(campaign.timeEnd);
      const start = moment(campaign.timeStart);
      const now = moment();
      const duration = end.diff(start);
      const timeLeft = end.diff(now);
      return ((duration - timeLeft) / duration) * 100;
    } catch (err) {
      return 0;
    }
  };

  const getDiscountedPrice = () => {
    try {
      const originalPrice = parseFloat(jobDetail.publicRRP);
      const percent = parseFloat(jobDetail.discountPercentage);
      if (jobDetail.discountPercentageUnit === "amount") {
        return originalPrice - percent;
      }
      return (originalPrice - (originalPrice * percent) / 100).toFixed(2);
    } catch (err) {
      return 0;
    }
  };

  const unitText = (unit, value) => {
    if (unit === "percentage") return `${value}%`;
    if (unit === "amount") return `$${value}`;
    if (!unit) return `$${value}`;
    return `${value}${unit}`;
  }

  return (
    <div
      className="drawer-submit-proposal__result"
      style={{ marginRight: "30px" }}
    >
      <div className="drawer-submit-proposal__result__tags">
        {_.get(dealDetail, 'streamHourlyRateRange.start') && <Tag color="#55acee">Fixed Rate</Tag>}
        {_.get(dealDetail, 'streamCommission.description') && <Tag color="#FF9A4D">Commission</Tag>}
      </div>
      <div className="drawer-submit-proposal__result__title">
        <p>{campaign.name}</p>
        <p>{campaign.description}</p>
      </div>
      <div className="drawer-submit-proposal__result__duration">
        {campaign.status === CAMPAIGN_STATUS.NOT_STARTED ? (
          <>
            <p className="green">
              <IconClock /> Start on: {utils.toDateString(campaign.timeStart)}
            </p>
            <Progress
              strokeColor="#fff"
              percent={0}
              showInfo={false}
              className="progress-green"
            />
          </>
        ) : campaign.status === CAMPAIGN_STATUS.IN_PROGRESS ? (
          <>
            <p className="green">
              <IconAppoint /> <Timer timeEnd={campaign.timeEnd} />
            </p>
            <Progress
              strokeColor="#fff"
              percent={getProgressPercentage()}
              showInfo={false}
            />
          </>
        ) : (
          <>
            <p className="grey">
              <IconClockGrey /> {utils.toDateString(campaign.timeStart)} -{" "}
              {utils.toDateString(campaign.timeEnd)}
            </p>
            <Progress
              strokeColor="#fff"
              percent={0}
              showInfo={false}
              className="progress-grey"
            />
          </>
        )}
      </div>
      {dealDetail && (
        <div className="drawer-submit-proposal__result__deal">
          {dealDetail?.streamHourlyRateRange?.start && (
            <p className="fixed-price-text">
              ${(dealDetail.streamHourlyRateRange || {}).start}-$
              {(dealDetail.streamHourlyRateRange || {}).end}/hr
            </p>
          )}
          {!!(dealDetail.streamCommission || {}).negotiable && (
            <p>
              Commission: <strong>Negotiable</strong>
            </p>
          )}
          {dealDetail && dealDetail.streamCommissionUnit === "%" && (
            <p>
              Commission:{" "}
              <strong>{dealDetail.streamCommission.description}%</strong>
            </p>
          )}
        </div>
      )}
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconPricing /> Product Price
        </p>
        <table>
          <tr>
            <td>Public RRP</td>
            <td>${jobDetail.publicRRP}</td>
          </tr>
          <tr>
            <td>Discount</td>
            <td>{unitText(jobDetail.discountPercentageUnit, jobDetail.discountPercentage)}</td>
          </tr>
          <tr>
            <td>Discounted Price</td>
            <td>${getDiscountedPrice()}</td>
          </tr>
          <tr>
            <td>Discount Period</td>
            <td>
              {utils.toDateString(jobDetail.discountPeriodStart)} -{" "}
              {utils.toDateString(jobDetail.discountPeriodEnd)}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__table">
        <p>
          <IconStream /> Stream Details
        </p>
        <table>
          <tr>
            <td>Time Suggestion</td>
            <td>
              {utils.toDateString((dealDetail?.streamTime || {}).description)}
            </td>
          </tr>
          <tr>
            <td>Type of Stream</td>
            <td>{(dealDetail?.streamType || {}).description}</td>
          </tr>
          <tr>
            <td>Elements</td>
            <td>
              {((dealDetail?.streamElement || {}).description || []).join(" + ")}
            </td>
          </tr>
          <tr>
            <td>Platform</td>
            <td>
              {((dealDetail?.streamPlatform || {}).description || []).join(", ")}
            </td>
          </tr>
        </table>
      </div>
      <div className="drawer-submit-proposal__result__message">
        <p className="sub-title">Message</p>
        <p className="fontStyle13">{dealDetail?.note}</p>
      </div>
    </div>
  );
};

const mapStateToProps = ({ campaign: { campaignDetail } }) => ({
  jobDetail: campaignDetail || {},
});

export default connect(mapStateToProps)(CampaignDetailPanel);
