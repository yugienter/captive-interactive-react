import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Drawer,
  Row,
  Col,
  Form,
  Radio,
  Space,
  Input,
  Select,
  DatePicker,
  Checkbox,
} from "antd";
import { IconCloseDrawer, IconPricings } from "views/components/Icon/drawer";
import { IconCalendar } from "views/components/Icon/pages";
import { IconStream, IconSendProposal } from "views/components/Icon/myjobs";
import {
  addDealByCompany,
  getSellerActivities,
} from "views/pages/DetailProduct/components/actions";
import ContentDeal from "../CollapseMyJobs/components/ContentDeal";
import moment from "moment";
import { constants } from "helpers";

const { STREAM_TYPES } = constants;

const { Option } = Select;
const { TextArea } = Input;

const AGREED_PRICINGS = {
  Commissions: "Commissions",
  "Hourly Rate": "Fixed Rate",
  "Hourly Rate + Commissions": "Fixed Rate + Commissions",
};

const ELEMENTS = [
  {
    label: (
      <span>
        <span className="first">Studio</span> <br />
        <span className="last">Including Location</span>
      </span>
    ),
    value: "Studio",
  },
  {
    label: (
      <span>
        <span className="first">Overlay</span> <br />
        <span className="last">Including Graphics</span>
      </span>
    ),
    value: "Overlay",
  },
];

const PLATFORMS = ["Shopee", "Lazada", "Facebook", "Instagram"];

function DrawerProposeDeal({
  visible,
  handleClose,
  addDealByCompany,
  deal,
  getSellerActivities,
}) {
  const [form] = Form.useForm();
  const [value, setValue] = useState(1);
  const [streamPricingSwitcher, setStreamPricingSwitcher] = useState(3);

  useEffect(() => {
    form.setFieldsValue({
      streamTime: deal.streamTime && moment(deal.streamTime.description),
      streamType: deal.streamType && deal.streamType.description,
      element: deal.streamElement && deal.streamElement.description,
      platform: deal.streamPlatform && deal.streamPlatform.description,
      streamHourlyRate:
        deal.streamHourlyRate && deal.streamHourlyRate.description,
      agreed: deal.streamPricings,
      commission: deal.streamCommission && deal.streamCommission.description,
      hourlyRate: deal.streamHourlyRate && deal.streamHourlyRate.description,
      commissionUnit: deal.streamCommissionUnit,
    });
  }, [deal, form]);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const onStreamPricingChange = (e) => {
    switch (e.target.value) {
      case "Commissions":
        setStreamPricingSwitcher(1);
        break;
      case "Hourly Rate":
        setStreamPricingSwitcher(2);
        break;
      default:
        setStreamPricingSwitcher(3);
        break;
    }
    onChange(e);
  };

  const onFinish = async (values) => {
    if (!deal.jobCode || !deal.hostCode) return;
    console.log("on finish", values);
    console.log("on finish 1", {
      jobCode: deal.jobCode,
      hostCode: deal.hostCode,
      streamTime: {
        description: values.streamTime,
        negiable: deal.streamTime.negiable,
      },
      streamType: {
        description: values.streamType,
        negiable: deal.streamType.negiable,
      },
      streamElement: {
        description: values.element || [],
        negiable: deal.streamElement.negiable,
      },
      streamPlatform: {
        description: values.platform || [],
        negiable: deal.streamPlatform.negiable,
      },
      streamPricings: values.agreed,
      streamHourlyRate: {
        description: values.hourlyRate,
        negiable: deal.streamHourlyRate.negiable,
      },
      streamCommission: {
        description: values.commission,
        negiable: deal.streamCommission.negiable,
      },
      streamCommissionUnit: values.commissionUnit,
      note: values.message,
    });
    await addDealByCompany({
      jobCode: deal.jobCode,
      hostCode: deal.hostCode,
      streamTime: {
        description: values.streamTime,
        negiable: deal.streamTime.negiable,
      },
      streamType: {
        description: values.streamType,
        negiable: deal.streamType.negiable,
      },
      streamElement: {
        description: values.element || [],
        negiable: deal.streamElement.negiable,
      },
      streamPlatform: {
        description: values.platform || [],
        negiable: deal.streamPlatform.negiable,
      },
      streamPricings: values.agreed,
      streamHourlyRate: {
        description: values.hourlyRate,
        negiable: deal.streamHourlyRate.negiable,
      },
      streamCommission: {
        description: values.commission,
        negiable: deal.streamCommission.negiable,
      },
      streamCommissionUnit: values.commissionUnit,
      note: values.message,
    });

    handleClose();
  };

  const prefixSelector = (name, onChange) => (
    <Form.Item name={name} noStyle>
      <Select
        style={{
          width: 75,
        }}
        onChange={onChange || function () {}}
        disabled={streamPricingSwitcher === 2 && name === "commissionUnit"}
      >
        <Option value="percent">%</Option>
        <Option value="$">USD</Option>
      </Select>
    </Form.Item>
  );

  return (
    <>
      <Drawer
        className={"drawer-main drawer-submit-proposal"}
        placement="right"
        width={860}
        onClose={handleClose}
        visible={visible}
        closeIcon={<IconCloseDrawer />}
        title={
          <p className="fontStyle24 drawer-submit-proposal__header">
            Submit a deal
          </p>
        }
      >
        <div className="drawer-submit-proposal__content">
          <Form
            form={form}
            layout="vertical"
            onFinish={onFinish}
            autoComplete="off"
          >
            <div className="drawer-submit-proposal__deal-section">
              <Row justify="space-between">
                <Col span={11}>
                  <div>
                    <p className="title">
                      <IconPricings /> Seller Fees
                    </p>
                    <Form.Item label="Fee Type" name="agreed">
                      <Radio.Group
                        onChange={onStreamPricingChange}
                        value={value}
                      >
                        <Space direction="vertical">
                          {Object.keys(AGREED_PRICINGS).map((value) => (
                            <Radio key={value} value={value}>
                              {AGREED_PRICINGS[value]}
                            </Radio>
                          ))}
                        </Space>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Commission Fee" name="commission">
                      <Input
                        addonBefore={prefixSelector("commissionUnit")}
                        className="input-basic"
                        size="large"
                        type="number"
                        disabled={streamPricingSwitcher === 2}
                      />
                    </Form.Item>
                    <Form.Item label="Fixed Rate" name="hourlyRate">
                      <Input
                        size="large"
                        className="input-basic"
                        prefix="USD"
                        placeholder="e.g. CO12345"
                        type="number"
                        disabled={streamPricingSwitcher === 1}
                      />
                    </Form.Item>
                  </div>
                  <div>
                    <p className="title">
                      <IconStream /> Stream Details
                    </p>
                    <Form.Item label="Stream Time" name="streamTime">
                      <DatePicker
                        style={{ fontWeight: "normal" }}
                        suffixIcon={<IconCalendar />}
                        disabled={!deal.streamTime.negiable}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Type of Stream (Required by Company)"
                      name="streamType"
                    >
                      <Radio.Group
                        onChange={onChange}
                        value={value}
                        disabled={
                          !(deal.streamType && deal.streamType.negiable)
                        }
                      >
                        <Space direction="vertical">
                          {Object.keys(STREAM_TYPES).map((value) => (
                            <Radio key={value} value={value}>
                              {STREAM_TYPES[value]}
                            </Radio>
                          ))}
                        </Space>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item
                      label="Elements (Required by Company)"
                      name="element"
                    >
                      <Checkbox.Group
                        options={ELEMENTS}
                        disabled={
                          !(deal.streamElement && deal.streamElement.negiable)
                        }
                      />
                    </Form.Item>
                    <Form.Item label="Platform" name="platform">
                      <Checkbox.Group
                        options={PLATFORMS}
                        disabled={
                          !(deal.streamPlatform && deal.streamPlatform.negiable)
                        }
                      />
                    </Form.Item>
                  </div>
                </Col>
                <Col span={10}>
                  <div className="collapse-main">
                    <div className="site-collapse-custom-panel">
                      <div className="ant-collapse-content">
                        <div
                          className="ant-collapse-content-box"
                          style={{ paddingTop: 20 }}
                        >
                          <ContentDeal deal={deal} noFooter={true} />
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
            <Form.Item label="Message" name="message">
              <TextArea
                style={{
                  height: 82,
                }}
                rows={3}
                className="input-basic"
              />
            </Form.Item>
          </Form>
        </div>
        <div className="drawer-submit-proposal__action">
          <div>
            <a onClick={handleClose}>Cancel</a>
            <a href="javascript:void(0)" onClick={() => form.submit()}>
              <IconSendProposal />
              Send Deal
            </a>
            {/* <Button className="cancel">Cancel</Button>
            <Button
              type="primary"
              className="send"
              onClick={() => form.submit()}
            >
              <IconSendProposal />
              Send an Offer
            </Button> */}
          </div>
        </div>
      </Drawer>
    </>
  );
}

const mapStateToProps = () => {};
const mapDispatchToProps = {
  addDealByCompany,
  getSellerActivities,
};
export default connect(mapStateToProps, mapDispatchToProps)(DrawerProposeDeal);
