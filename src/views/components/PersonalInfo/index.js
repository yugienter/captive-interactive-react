import React, {
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";
import { Form, Input, Row, Col, Select, DatePicker } from "antd";
import {
  IconFlagAlt,
  IconSelectDown,
  IconCalendar,
} from "views/components/Icon/pages";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { CountryDropdown } from "react-country-region-selector";
import moment from "moment";
import { COUNTRY_CODE } from "helpers";
import { constants } from "helpers";

const { Option } = Select;
const dateFormat = "DD/MM/YYYY";

const PersonalInfo = ({ updateHostProfile, email, host, next }, ref) => {
  const [form] = Form.useForm();

  useImperativeHandle(ref, () => ({
    onContinue: () => {
      form.submit();
    },
  }));

  const [country, setCountry] = useState((host && host.country) || "Singapore");
  const [countryCode, setCountryCode] = useState("sg");

  const handleChangeCountry = (value) => {
    console.log("handleChangeCountry", form.getFieldsValue());
    setCountryCode(COUNTRY_CODE[value]);
    setCountry(value);
    form.setFieldsValue({ country: value });
  };

  useEffect(() => {
    form.setFieldsValue({
      country: (host && host.country) || "Singapore",
      dob: host && host.dob ? moment(host.dob) : null,
      experience: host ? host.experience : null,
      height: host ? host.height : null,
      phoneNumber: host ? host.phoneNumber : null,
      rates: host ? host.rates : null,
      gender: host ? host.sex : null,
      weight: host ? host.weight : null,
    });
  }, [form, host]);

  const disabledDate = (current) => {
    return current && current.valueOf() >= Date.now();
  };
  const suffixWeight = <div className="suffix-custom">kg</div>;
  const suffixHeight = <div className="suffix-custom">cm</div>;
  const suffixYears = <div className="suffix-custom">years</div>;
  const perfixPrice = <div className="perfix-custom">SGD</div>;

  const onFinish = async (values) => {
    const update = {};
    if (values.country !== host.country) {
      update.country = values.country;
    }
    if (values.phoneNumber !== host.phoneNumber) {
      update.phoneNumber = values.phoneNumber;
    }
    if (values.dob.toISOString() !== host.dob) {
      update.dob = values.dob.toISOString();
    }
    if (parseInt(values.experience) !== host.experience) {
      update.experience = parseInt(values.experience);
    }
    if (parseInt(values.height) !== host.height) {
      update.height = parseInt(values.height);
    }
    if (parseInt(values.weight) !== host.weight) {
      update.weight = parseInt(values.weight);
    }
    if (parseFloat(values.rates) !== host.rates) {
      update.rates = parseInt(values.rates);
    }
    if (values.gender !== host.sex) {
      update.sex = values.gender;
    }
    Object.keys(update).length && (await updateHostProfile(update));
    next && next();
  };

  return (
    <>
      <h2>Personal Information</h2>
      <div className="text-bold mb36">
        Give our merchants an idea of who you are and stand out in the
        marketplace
      </div>

      <Form form={form} onFinish={onFinish} layout="vertical">
        <Row gutter={constants.mainStyle.gutter}>
          <Col className="gutter-row" span={12}>
            <Form.Item
              label="Country"
              className="form-select"
              name="country"
              rules={[{ required: true, message: "Country is missing" }]}
            >
              <CountryDropdown
                name="my-country-field"
                classes="select-style"
                showDefaultOption={true}
                value={country}
                onChange={handleChangeCountry}
              />
              <IconFlagAlt />
            </Form.Item>
            <Form.Item
              name="gender"
              label="Gender"
              rules={[{ required: true, message: "Gender is missing" }]}
            >
              <Select
                placeholder="Select gender"
                className="select-noicon"
                style={{ width: "100%" }}
                suffixIcon={<IconSelectDown />}
              >
                <Option value="male">Male</Option>
                <Option value="female">Female</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Weight"
              name="weight"
              rules={[{ required: false, message: "Weight is missing" }]}
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixWeight}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <Form.Item
              label="How long has your community be active?"
              name="experience"
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixYears}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <div className="experience">
              It will be added 1 after one year.
            </div>
          </Col>
          <Col className="gutter-row" span={12}>
            <Form.Item
              label="Phone Number"
              name="phoneNumber"
              rules={[{ required: true, message: "Phone Number is missing" }]}
            >
              <ReactPhoneInput country={countryCode} />
            </Form.Item>
            <Form.Item
              label="Date of Birth"
              name="dob"
              rules={[{ required: true, message: "Date of Birth is missing" }]}
            >
              <DatePicker
                placeholder="DD/MM/YYYY"
                style={{ fontWeight: "normal" }}
                // defaultValue={moment()}
                format={dateFormat}
                suffixIcon={<IconCalendar />}
                disabledDate={disabledDate}
              />
            </Form.Item>
            <Form.Item
              label="Height"
              name="height"
              rules={[{ required: false, message: "Height is missing" }]}
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixHeight}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <Form.Item label="Fixed Rate" name="rates">
              <Input
                size="large"
                className="input-basic"
                prefix={perfixPrice}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default forwardRef(PersonalInfo);
