import { useEffect } from 'react';
import { Drawer, Tabs, Collapse, Rate } from 'antd';
import { connect } from 'react-redux';
import { IconCloseDrawer } from 'views/components/Icon/drawer';
import { getSellerActivities } from 'views/pages/DetailProduct/components/actions';
import DrawerSellerInfo from 'views/components/ModalRight/components/DrawerSellerInfo';
import DrawerTitle from 'views/components/ModalRight/components/DrawerTitle';
import OlderActivities from 'views/components/ModalRight/components/OlderActivities';
import { ExpandIcon, IconUpCollapse } from '../Icon/myjobs';
import ContentDeal from '../CollapseMyJobs/components/ContentDeal';
import HeaderDeal from '../CollapseMyJobs/components/HeaderDeal';
import { RATE_SCORES, statusDeal, statusJob } from 'helpers';
import { IconRating } from 'views/components/Icon/status';
import Conversation from '../Conversation';

const { Panel } = Collapse;
const { TabPane } = Tabs;

const CampaignDetailDrawer = ({
  showDrawer,
  onCloseDrawer,
  seller,
  getSellerActivities,
  jobCode,
  activities,
}) => {
  useEffect(() => {
    async function getActivities(hostCode) {
      await getSellerActivities(hostCode, jobCode);
    }

    if (seller && seller.code && jobCode) {
      getActivities(seller.code);
    }
  }, [seller, jobCode, getSellerActivities]);

  function onTabChange(key) {
    console.log(key);
  }

  const handleClose = () => {
    onCloseDrawer && onCloseDrawer();
  };

  const renderDrawerTitle = () => {
    return <DrawerTitle seller={seller} />;
  };

  const oldActivities = activities.slice(1);
  const currentActivity = activities[0];
  const isCompleteVerified =
    currentActivity &&
    currentActivity.status === statusDeal.complete &&
    currentActivity.statusJob === statusJob.verify;
  return (
    <>
      <Drawer
        className="drawer-main drawer-main-tabs"
        placement="right"
        width={660}
        onClose={handleClose}
        visible={showDrawer}
        closeIcon={<IconCloseDrawer />}
        title={renderDrawerTitle()}
      >
        <Tabs
          defaultActiveKey="1"
          onChange={onTabChange}
          className="recommender-tabs"
        >
          <TabPane tab={<div className="item-tab">Seller Profile</div>} key="1">
            <DrawerSellerInfo seller={seller} />
          </TabPane>
          <TabPane
            tab={
              <div className="item-tab">
                Activities<span className="num">{activities.length}</span>
              </div>
            }
            key="2"
          >
            <div
              className="drawer-main-myjobs"
              style={{ width: 420, margin: '0 auto' }}
            >
              {currentActivity &&
                (isCompleteVerified ? (
                  <div className="box-rating">
                    <Rate
                      character={<IconRating />}
                      value={currentActivity.rate}
                      className="rating-actions"
                      disabled
                    />
                    <div className="rating-result">
                      You rated this Seller:
                      <strong className="ant-rate-text">
                        {RATE_SCORES[currentActivity.rate]}
                      </strong>
                    </div>
                  </div>
                ) : (
                  <Collapse
                    bordered={false}
                    expandIcon={(panelProps) => <ExpandIcon {...panelProps} />}
                    expandIconPosition="right"
                    className="site-collapse-custom-collapse older-activities-collapse collapse-main"
                  >
                    <Panel
                      header={
                        <HeaderDeal deal={currentActivity} seller={seller} />
                      }
                      className="site-collapse-custom-panel"
                    >
                      <ContentDeal deal={currentActivity} seller={seller} />
                    </Panel>
                  </Collapse>
                ))}
              {!!oldActivities.length && (
                <OlderActivities deals={oldActivities} seller={seller} />
              )}
            </div>
          </TabPane>
          <TabPane tab={<div className="item-tab">Messages</div>} key="3">
            <Conversation
              showDrawer={showDrawer}
              onCloseDrawer={onCloseDrawer}
              seller={seller}
              jobCode={jobCode}
            />
          </TabPane>
        </Tabs>
      </Drawer>
    </>
  );
};

const mapStateToProps = ({ campaign: { sellerActivities } }) => ({
  activities: sellerActivities || [],
});

const mapDispatchToProps = {
  getSellerActivities,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CampaignDetailDrawer);
