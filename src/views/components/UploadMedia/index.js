import React, { useState } from 'react'
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Upload, Modal } from "antd";
import { IconUploadMedia } from 'views/components/Icon/pages';
import { constants, ReactUtils, utils } from 'helpers';
import { addHostProfileMedia, removeHostProfileMedia, uploadMedia, deleteMedia } from 'views/pages/HostProfile/actions';

const { MEDIA_TYPE } = constants;

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const UploadMedia = ({ addHostProfileMedia, removeHostProfileMedia, host, showRemoveIcon = false, deleteMedia }) => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');

  const media = host ? host.media.map(item => {
    return {
      uid: item.code,
      url: utils.getMediaUrl(item.url),
    };
  }) : [];
  const [fileList, setFileList] = useState(media);
  const handleCancel = () => setPreviewVisible(false)
  const handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview)
    setPreviewVisible(true)
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1))
  };

  const handleChange = async ({ file, fileList }) => {
    if (file) {
      if (file.status === 'done') {
        const data = file.response.data;
        file.url = utils.getMediaUrl(data.url);
        file.uid = data.code;
        addHostProfileMedia(data);
      } else if (file.status === 'removed') {
        removeHostProfileMedia(file.uid);
      } else if (file.status === 'error') {
        file.thumbUrl = null;
      }
    }
    setFileList(fileList);
  };
  const uploadButton = (
    <div>
      <IconUploadMedia />
      <div>Add photo or video</div>
    </div>
  );

  const upload = () => {
    const url = uploadMedia({ ownerType: MEDIA_TYPE.HOST, ownerCode: host.code });
    return url;
  }

  const handleRemove = file => {
    return deleteMedia(file.uid);
  }

  const beforeUpload = (file, newFileList) => {
    if (newFileList.length + fileList.length > 8) {
      ReactUtils.messageWarn({ content: 'You can only upload total 8 files' });
      return Upload.LIST_IGNORE;
    }
  }

  return (
    <>
      <div className='box-media'>
        <Upload
          listType="picture-card"
          fileList={fileList}
          accept={constants.FILE_TYPES}
          beforeUpload={beforeUpload}
          onPreview={handlePreview}
          onChange={handleChange}
          onRemove={handleRemove}
          showUploadList={{ showRemoveIcon }}
          action={upload}
          multiple
          className='personal-infomation'
        >
          {fileList.length >= 8 ? null : uploadButton}
        </Upload>
        <div
          disabled={fileList.length < 0}
          className={fileList.length <= 0 ? 'before-upload-box' : 'before-upload-box hidden'}
        >
          <div className='box-none-file'>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        {
          utils.isImage(previewImage) ? (
            <img alt="example" style={{ width: '100%' }} src={previewImage} />
          ) : utils.isVideo(previewImage) ? (
            <video
              controls
              width={'100%'}
              src={previewImage}
            />
          ) : ''
        }

      </Modal>
    </>
  )
}

const mapStateToProps = ({ hostProfile }) => ({
  host: hostProfile.data,
});

const mapDispatchToProps = {
  addHostProfileMedia,
  deleteMedia,
  removeHostProfileMedia,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UploadMedia)
);