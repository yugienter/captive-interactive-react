import React from 'react';
import { Tag, Progress } from "antd";
import {
    IconPricing,
    IconStream,
    IconSendProposal,
    IconClock,
} from "views/components/Icon/myjobs";
import _ from "lodash";
import moment from "moment"
function CampaignCard({ data }) {

    const getDiffDate = (now, then) => {
        return `${moment(now).diff(moment(then), 'days')} days`
    }

    return (
        <div className="drawer-submit-proposal__result">
            <div className="drawer-submit-proposal__result__tags">
                <Tag color="#55acee">Fixed Rate</Tag>
                <Tag color="#FF9A4D">Commission</Tag>
            </div>
            <div className="drawer-submit-proposal__result__title">
                <p>{_.get(data, 'job.campaign.name')}</p>
                <p>{_.get(data, 'job.campaign.description')}</p>
            </div>
            <div className="drawer-submit-proposal__result__duration">
                <p>
                    <IconClock /> {getDiffDate(_.get(data, 'job.campaign.timeEnd'), _.get(data, 'job.campaign.timeStart'))}
                </p>
                <Progress
                    strokeColor="#1BD2A4"
                    percent={50}
                    showInfo={false}
                />
            </div>
            <div className="drawer-submit-proposal__result__deal">
                <p>{'$' + _.get(data, 'deal.streamHourlyRateRange.start') + ' - ' + '$' + _.get(data, 'deal.streamHourlyRateRange.end') + '/hr'}</p>
                <p>
                    Commission: <span>Negotiable</span>
                </p>
            </div>
            <div className="drawer-submit-proposal__result__table">
                <p>
                    <IconPricing /> Product Price
                      </p>
                <table>
                    <tr>
                        <td>Public RRP</td>
                        <td>${_.get(data, 'job.publicRRP')}</td>
                    </tr>
                    <tr>
                        <td>Discount</td>
                        <td>{_.get(data, 'job.discount')}</td>
                    </tr>
                    <tr>
                        <td>Discounted Price</td>
                        <td>{_.get(data, 'job.discount')}</td>
                    </tr>
                    <tr>
                        <td>Discount Period</td>
                        <td>{_.get(data, 'job.discount')}</td>
                    </tr>
                </table>
            </div>
            <div className="drawer-submit-proposal__result__table">
                <p>
                    <IconStream /> Stream Details
                      </p>
                <table>
                    <tr>
                        <td>Time Suggestion</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Type of Stream</td>
                        <td>1 Cam + 1 Host</td>
                    </tr>
                    <tr>
                        <td>Elements</td>
                        <td>Studio + Overlay</td>
                    </tr>
                    <tr>
                        <td>Platform</td>
                        <td>Shopee, Lazada, Facebook</td>
                    </tr>
                </table>
            </div>
            <div className="drawer-submit-proposal__result__message">
                <p className="sub-title">Message</p>
                <p className="fontStyle13">
                    {_.get(data, 'deal.note')}
                </p>
            </div>
        </div>
    );
}

export default CampaignCard;