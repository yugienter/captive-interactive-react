import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { connect } from 'react-redux';
import _ from "lodash";
 
const { Option } = Select;

function ListFilter({ tags, onChooseTag }) {
    const [selectedTag, setSelectedTag] = useState('')
    return (
        <div className="list-filter">
            <div className="list-filter__tags">
                <ul>
                    <li className={`list-filter__tag${selectedTag === undefined ? ' list-filter__tag--selected' : ''}`}>
                        <a onClick={() => {
                            setSelectedTag(undefined)
                            onChooseTag(undefined)
                            }}>All categories</a></li>
                    {
                        tags && tags.map(item => (
                            <li key={_.get(item, 'id')} className={`list-filter__tag${_.get(item, 'id') === selectedTag ? ' list-filter__tag--selected' : ''}`}>
                                <a onClick={() => {
                                    setSelectedTag(_.get(item, 'id'))
                                    onChooseTag(_.get(item, 'id'))
                                }}>{_.get(item, 'name')}</a></li>
                        ))
                    }
                </ul>
            </div>
            <div className="list-filter__filter">
                <Select defaultValue="popular" style={{ minWidth: 126 }}>
                    <Option value="popular">Popular</Option>
                    <Option value="startDate">Start Date</Option>
                    <Option value="endDate">End Date</Option>
                    <Option value="highestDate">Highest Rate</Option>
                    <Option value="bidsPlaced">Pitch Placed</Option>
                </Select>
            </div>
        </div>
    );
}

const mapStateToProps = ({ hostList }) => ({
    tags: hostList.tags
});

export default connect(mapStateToProps)(ListFilter);