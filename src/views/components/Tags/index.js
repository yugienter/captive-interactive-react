import React, { useState, forwardRef, useImperativeHandle, useEffect } from 'react';
import { Tag } from 'antd';
import { IconOrganic, IconRaw, IconReady, 
  IconUnion, IconHeathly, IconUtility, IconHomeApp, IconGaming, IconAudio, IconBaby, IconBabyToys,
  IconBabyWear, IconBabyFood, IconBabyGoods, IconToddler, IconInfant, IconToys, 
  IconEducational, IconBags, IconSports, IconEyeWear, IconWatches, IconAccessories, 
  IconGentleman, IconLady, IconSupplement } from "views/components/Icon/topics"
import HostTag from 'views/pages/Host/parts/HostTag';

const { CheckableTag } = Tag;
const tagsData = [
  {
    categories: 'foods',
    data: [
      {
        icon: <IconOrganic />,
        name: 'Organic Food'
      },
      {
        icon: <IconRaw />,
        name: 'Raw Food'
      },
      {
        icon: <IconReady />,
        name: 'Ready to Eat'
      },
      {
        icon: <IconUnion />,
        name: 'Snack'
      },
      {
        icon: <IconHeathly />,
        name: 'Healthy Products'
      }
    ]
  },
  {
    categories: 'Electronics',
    data: [
      {
        icon: <IconUtility />,
        name: 'Utility Gadget'
      },
      {
        icon: <IconHomeApp />,
        name: 'Home Appliance'
      },
      {
        icon: <IconGaming />,
        name: 'Gaming'
      },
      {
        icon: <IconAudio />,
        name: 'Audio'
      }
    ]
  },
  {
    categories: 'Parenting',
    data: [
      {
        icon: <IconBaby />,
        name: 'Baby Furniture'
      },
      {
        icon: <IconBabyToys />,
        name: 'Baby Toy'
      },
      {
        icon: <IconBabyWear />,
        name: 'Baby Wear'
      },
      {
        icon: <IconBabyFood />,
        name: 'Baby Food'
      }
    ]
  },
  {
    categories: 'Children Education and Toys',
    data: [
      {
        icon: <IconBabyGoods />,
        name: 'Baby Goods'
      },
      {
        icon: <IconToddler />,
        name: 'Toddler Products'
      },
      {
        icon: <IconInfant />,
        name: 'Infant Care'
      },
      {
        icon: <IconToys />,
        name: 'Toys'
      },
      {
        icon: <IconEducational />,
        name: 'Educational products'
      }
    ]
  },
  {
    categories: 'Fashion',
    data: [
      {
        icon: <IconBags />,
        name: 'Bags'
      },
      {
        icon: <IconSports />,
        name: 'Sports Fashion'
      },
      {
        icon: <IconEyeWear />,
        name: 'Eyewear'
      },
      {
        icon: <IconWatches />,
        name: 'Watches'
      },
      {
        icon: <IconAccessories />,
        name: 'Accessories'
      },
      {
        icon: <IconGentleman />,
        name: 'Gentleman'
      },
      {
        icon: <IconLady />,
        name: 'Lady'
      }
    ]
  },
  {
    categories: 'Health and Wellness',
    data: [
      {
        icon: <IconSupplement />,
        name: 'Supplements'
      },
    ]
  }
]

const Tags = () => {
  const [selectedTags, setSelectedTags] = useState([])
  const handleSelectedTags = (tag, checked) => {
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag)
    setSelectedTags(nextSelectedTags)
  }
  console.log(selectedTags)

  return (
    <>
      {tagsData.map(tag => (
        <>
          <div className='topics-categories'>{tag.categories}</div>
          <div className='topics-tags'>
            {/* className='cursor'  */}
            {tag.data.map(item => (
              <CheckableTag
                key={item.id}
                className={selectedTags.length > 5 ? 'none-cheked' : ''}
                checked={selectedTags.indexOf(item) > -1}
                onChange={checked => handleSelectedTags(item, checked)}
              >
                {item.icon} {item.name}
              </CheckableTag>
            ))}
          </div>
        </>
      ))}
    </>
  )
}

export default forwardRef(Tags);