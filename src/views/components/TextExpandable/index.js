import { useState } from 'react';

const TextExpandable = ({ text = '', limit = 118 }) => {
  const [isTrim, setTrim] = useState(text.length > limit);

  return (
    <div className="more">
      {text.length > limit ? (
        <>
          {isTrim ? `${text.substring(0, limit)}...` : text}
          <button className="btnMore" onClick={() => setTrim(!isTrim)}>
            {isTrim ? 'more' : 'less'}
          </button>
        </>
      ) : (
        text
      )}
    </div>
  );
};
export default TextExpandable;
