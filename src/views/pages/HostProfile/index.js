import React, { useState, useEffect } from 'react';
import { Button, Form } from "antd";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
// import qs from 'query-string';
import Page from "views/components/Page";
import { constants } from "helpers";
import { IconEdit, IconCancelBtn, IconSaveBtn } from 'views/components/Icon/pages'
import PromptModal from 'views/components/PromptModal';
import ViewProfile from './ViewProfile';
import EditProfile from './EditProfile';

import { fetchHostProfile } from './actions';

const { userRoles } = constants;

const HostProfile = ({ hostProfile, email, fetchHostProfile }) => {
  // const history = useHistory()
  // const { location } = history;
  // const params = qs.parse(location.search);
  // console.log(params);

  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [changeMode, setChangeMode] = useState(false);
  const handleChangeMode = () => {
    setChangeMode(!changeMode);
    setVisible(false);
  }

  const onSave = () => {
    form.submit();
  }

  useEffect(() => {
    window.addEventListener('beforeunload', alertUser)
    return () => {
      window.removeEventListener('beforeunload', alertUser)
    }
  })

  const alertUser = (event) => {
    const update = getUpdates();
    if (Object.keys(update).length > 0) {
      event.preventDefault();
      event.returnValue = ''
    }
  }

  useEffect(() => {
    async function getHost() {
      await fetchHostProfile(email);
    }
    getHost();
  }, [email, fetchHostProfile]);

  const getUpdates = () => {
    const values = form.getFieldsValue();
    const update = {};
    // Overview
    if (values.name !== hostProfile.name) {
      update.name = values.name;
    }
    if (values.aboutMe !== hostProfile.aboutMe) {
      update.aboutMe = values.aboutMe;
    }
    if (values.porfolio !== hostProfile.porfolio) {
      update.porfolio = values.porfolio;
    }
    // Personal Information
    if (values.country !== hostProfile.country) {
      update.country = values.country;
    }
    if (values.phoneNumber !== hostProfile.phoneNumber) {
      update.phoneNumber = values.phoneNumber;
    }
    if (values.dob.toISOString() !== hostProfile.dob) {
      update.dob = values.dob.toISOString();
    }
    if (values.experience != hostProfile.experience) {
      update.experience = parseInt(values.experience);
    }
    if (values.height != hostProfile.height) {
      update.height = parseInt(values.height);
    }
    if (values.weight != hostProfile.weight) {
      update.weight = parseInt(values.weight);
    }
    if (values.rates != hostProfile.rates) {
      update.rates = parseInt(values.rates);
    }
    if (values.gender !== hostProfile.sex) {
      update.sex = values.gender;
    }
    // Connection
    if (values.facebookFollowers != hostProfile.facebookFollowers) {
      update.facebookFollowers = parseInt(values.facebookFollowers);
    }
    if (values.instagramFollowers != hostProfile.instagramFollowers) {
      update.instagramFollowers = parseInt(values.instagramFollowers);
    }
    if (values.tiktokFollowers != hostProfile.tiktokFollowers) {
      update.tiktokFollowers = parseInt(values.tiktokFollowers);
    }
    if (values.otherFollowers != hostProfile.otherFollowers) {
      update.otherFollowers = parseInt(values.otherFollowers);
    }
    if (values.facebook !== hostProfile.social.facebook) {
      update.facebook = values.facebook;
    }
    if (values.instagram !== hostProfile.social.instagram) {
      update.instagram = values.instagram;
    }
    if (values.tiktok !== hostProfile.social.tiktok) {
      update.tiktok = values.tiktok;
    }
    if (values.other !== hostProfile.social.other) {
      update.other = values.other;
    }
    return update;
  };

  const handleCancel = () => {
    const update = getUpdates();
    if (Object.keys(update).length > 0) {
      setVisible(true);
    } else {
      handleChangeMode();
    }
  }

  const handleClose = () => {
    setVisible(false);
  }

  if (!hostProfile) return (
    <Page
      helmet="Setting Profile Page"
      alignTop
      selectedKeys={["host-profile"]}
      requiredAccess={[userRoles.HOST]}
    />
  )

  return (
    <Page
      helmet="Setting Profile Page"
      alignTop
      selectedKeys={["host-profile"]}
      requiredAccess={[userRoles.HOST]}
    >
      <div className='host-profile_page'>
        <div className='main-header_title host-profile_title'>
          <div className='main-header_name'>
            {changeMode ? 'Edit Profile' : 'Settings'}
          </div>
          <div className='main-header_actions host-profile_actions'>
            {changeMode ? (
              <>
                <Button
                  type="primary"
                  className="control cancel"
                  block
                  onClick={handleCancel}
                >
                  <IconCancelBtn />Cancel
                </Button>
                <Button
                  type="primary"
                  className="control save"
                  block
                  onClick={onSave}
                >
                  <IconSaveBtn />Save
                </Button>
              </>
            ) :
              <Button
                type="primary"
                className="control cancel"
                block
                onClick={handleChangeMode}
              >
                <IconEdit />Edit Profile
              </Button>}
          </div>
        </div>
        {changeMode ?
          <EditProfile form={form} handleChangeMode={handleChangeMode} />
          :
          <ViewProfile handleChangeMode={handleChangeMode} />
        }
        <PromptModal
          title="Confirm all discard changes"
          text="Are you sure you want to discard the changes?"
          visible={visible}
          okText="Discard"
          handleOk={handleChangeMode}
          handleClose={handleClose}
        />
      </div>
    </Page>
  )
}

const mapStateToProps = ({ hostProfile, common }) => ({
  hostProfile: hostProfile.data,
  email: common.user.email,
});

const mapDispatchToProps = {
  fetchHostProfile
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(HostProfile)
);