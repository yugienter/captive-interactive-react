import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Row, Col, Input, Form, Upload, message, Image, Tag } from "antd";
import {
  IconPersonalInfo, IconPhoto, IconTopics, IconConnections, IconOverviews,
  IconUpload
} from 'views/components/Icon/pages'
import HostTag from 'views/pages/Host/parts/HostTag';
import { updateHostProfile } from 'views/pages/HostProfile/actions';
import MenuScroll from '../MenuScroll';
import PersonalInfoForm from './personalInfoForm';
import UploadMedia from 'views/components/UploadMedia';
import { uploadMedia } from 'views/pages/HostProfile/actions';

import { LoadingOutlined } from '@ant-design/icons';

import { constants, ReactUtils } from 'helpers';

const { mainStyle: { gutter }, MEDIA_TYPE } = constants

const { TextArea } = Input;
const { CheckableTag } = Tag;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const EditProfile = ({ tags, hostProfile, form, updateHostProfile, handleChangeMode, uploadMediaAction }) => {
  form.setFieldsValue({
    name: hostProfile.name,
    aboutMe: hostProfile.aboutMe,
    porfolio: hostProfile.porfolio,
    facebook: hostProfile.social.facebook,
    instagram: hostProfile.social.instagram,
    tiktok: hostProfile.social.tiktok,
    other: hostProfile.social.other,
    facebookFollowers: hostProfile.facebookFollowers,
    instagramFollowers: hostProfile.instagramFollowers,
    tiktokFollowers: hostProfile.tiktokFollowers,
    otherFollowers: hostProfile.otherFollowers,
  });

  const [tagsData, setTagsData] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  useEffect(() => {
    const selectedTags = [];
    const data = tags.map(parent => {
      return {
        id: parent.id,
        categories: parent.name,
        data: parent.item.map(child => {
          const tag = {
            id: child.id,
            icon: HostTag({ tag: child.name }).props.icon,
            name: child.name
          };
          if (hostProfile.tags.includes(tag.id)) {
            selectedTags.push(tag);
          }
          return tag;
        })
      };
    });
    setTagsData(data);
    setSelectedTags(selectedTags);
  }, [tags, setTagsData, setSelectedTags, hostProfile]);

  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        setImageUrl(imageUrl),
        setLoading(false)
      );
      updateHostProfile({ avatar: info.file.response.data.url });
    }
  };

  const uploadButton = (
    <div className='upload' style={{
      position: 'absolute',
      left: '50%',
      transform: 'translate(-50%, 0)',
      cursor: 'pointer',
    }}>
      {loading ? <LoadingOutlined /> : <IconUpload />}
      <div className='text'>Change</div>
    </div>
  );

  const handleSelectedTags = (tag, checked) => {
    let nextSelectedTags = selectedTags;
    if (checked && selectedTags.length < 5) {
      nextSelectedTags = [...selectedTags, tag];
    } else if (!checked) {
      nextSelectedTags = selectedTags.filter(t => t !== tag);
    } else {
      ReactUtils.messageWarn({ content: 'Limit of 5 categories reached' });
    }
    setSelectedTags(nextSelectedTags)
  }

  const onFinish = async values => {
    const update = {};
    // Overview
    if (values.name !== hostProfile.name) {
      update.name = values.name;
    }
    if (values.aboutMe !== hostProfile.aboutMe) {
      update.aboutMe = values.aboutMe;
    }
    if (values.porfolio !== hostProfile.porfolio) {
      update.porfolio = values.porfolio;
    }
    // Personal Information
    if (values.country !== hostProfile.country) {
      update.country = values.country;
    }
    if (values.phoneNumber !== hostProfile.phoneNumber) {
      update.phoneNumber = values.phoneNumber;
    }
    if (values.dob.toISOString() !== hostProfile.dob) {
      update.dob = values.dob.toISOString();
    }
    if (parseInt(values.experience) !== hostProfile.experience) {
      update.experience = parseInt(values.experience);
    }
    if (parseInt(values.height) !== hostProfile.height) {
      update.height = parseInt(values.height);
    }
    if (parseInt(values.weight) !== hostProfile.weight) {
      update.weight = parseInt(values.weight);
    }
    if (parseFloat(values.rates) !== hostProfile.rates) {
      update.rates = parseInt(values.rates);
    }
    if (values.gender !== hostProfile.sex) {
      update.sex = values.gender;
    }
    // Connection
    if (parseInt(values.facebookFollowers) !== hostProfile.facebookFollowers) {
      update.facebookFollowers = parseInt(values.facebookFollowers);
    }
    if (parseInt(values.instagramFollowers) !== hostProfile.instagramFollowers) {
      update.instagramFollowers = parseInt(values.instagramFollowers);
    }
    if (parseInt(values.tiktokFollowers) !== hostProfile.tiktokFollowers) {
      update.tiktokFollowers = parseInt(values.tiktokFollowers);
    }
    if (parseInt(values.otherFollowers) !== hostProfile.otherFollowers) {
      update.otherFollowers = parseInt(values.otherFollowers);
    }

    await updateHostProfile({
      tags: selectedTags.map(tag => tag.id),
      social: {
        facebook: values.facebook,
        instagram: values.instagram,
        tiktok: values.tiktok,
        other: values.other,
      },
      ...update
    });
    handleChangeMode();
  }

  const upload = () => {
    return uploadMedia({ ownerType: MEDIA_TYPE.HOST, ownerCode: hostProfile.code, noCreate: true });
  }

  return (
    <>
      <section className='main-content host-profile_content'>
        <Row gutter={gutter} style={{ display: 'flex', width: '100%' }} className='row-host-profile'>
          <Col className="gutter-row" md={8} lg={8} xl={6} style={{ marginTop: '20px' }}>
            <MenuScroll tabMode='1' />
          </Col>
          <Col className="gutter-row" md={12} lg={16} xl={18} style={{ marginTop: '20px' }}>
            <div id="overview" className='section-scroll section-scroll_overview'>
              <h1>
                <IconOverviews />Overview<br />
                <label>This information will help us to get you started quickly</label>
              </h1>
              <Row gutter={gutter} style={{ padding: '0 26px' }}>
                <Col className="gutter-row input-with-lbl-hori" span={8}>
                  <Form layout="vertical">
                    <Form.Item
                      label="Avatar"
                      name="avatar"
                      className='form-textarea'
                    >
                      <Image
                        width={194}
                        src={imageUrl || hostProfile.avatar}
                      />
                      <Upload
                        accept={constants.FILE_TYPES}
                        name="file"
                        listType="picture-card"
                        className="avatar-uploader avatar-image"
                        showUploadList={false}
                        action={upload}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                      >
                        {uploadButton}
                      </Upload>
                    </Form.Item>
                  </Form>
                </Col>
                <Col className="gutter-row" span={16}>
                  <Form form={form} layout="vertical" onFinish={onFinish} >
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{width: '100%'}}>
                        <Form.Item name="name" label="Full Name">
                          <Input placeholder="Jessica Chen" />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{width: '100%'}}>
                        <Form.Item
                          name="aboutMe"
                          rules={[{ required: true, message: 'Biography is missing' }]}
                          label="Biography"
                        >
                          <TextArea rows={6} />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" span={24}>
                        <Form.Item name="porfolio" label="Portfolio (Showreel or website)">
                          <Input placeholder="https://facebook.com/exampleURL" />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </div>
            <div id="personal-info" className='section-scroll'>
              <h1>
                <IconPersonalInfo />Personal Information<br />
                <label>Give Companies an idea of who you are and help them when searching in Seller Marketplace</label>
              </h1>
              <div className='auth-form'><PersonalInfoForm form={form} /></div>
            </div>
            <div id="media" className='section-scroll'>
              <h1>
                <IconPhoto />Photos & Videos
                <br />
                <label>Help your portfolio looks attractive and professional (Ideal photo size is 1024x1024px and Max 5MB for video)</label>
              </h1>
              <UploadMedia showRemoveIcon={true} />
            </div>
            <div id="topics" className='section-scroll'>
              <h1>
                <IconTopics />Topics
                <br />
                <label>Select up to 5 that best describe the areas of your interest</label>
                <div className='topics-tags-edit'>
                  {tagsData.map(tag => (
                    <>
                      <div className='topics-categories'>{tag.categories}</div>
                      <div className='topics-tags'>
                        {tag.data.map(item => (
                          <CheckableTag
                            key={item.id}
                            className={selectedTags.length > 5 ? 'none-cheked' : ''}
                            checked={selectedTags.indexOf(item) > -1}
                            onChange={checked => handleSelectedTags(item, checked)}
                          >
                            {item.icon} {item.name}
                          </CheckableTag>
                        ))}
                      </div>
                    </>
                  ))}</div>
              </h1>
            </div>
            <div id='connections' className='section-scroll'>
              <h1>
                <IconConnections />Connections
                <br />
                <label>Connect to social accounts to enhance your portfolio</label>
              </h1>
              <Form form={form} onFinish={onFinish} layout="vertical" style={{ marginLeft: '26px' }}>
                <Row gutter={gutter}>
                  <Col className="gutter-row input-with-lbl-hori" span={12}>
                    <Form.Item
                      label="Facebook"
                      name="facebook"
                    >
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="https://facebook.com/exampleURL"
                      />
                    </Form.Item>
                    <Form.Item label="Instagram" name="instagram">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Paste URL here"
                      />
                    </Form.Item>
                    <Form.Item label="TikTok" name="tiktok">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Paste URL here"
                      />
                    </Form.Item>
                    <Form.Item label="Other" name="other">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Paste URL here"
                      />
                    </Form.Item>
                  </Col>
                  <Col className="gutter-row input-with-lbl-hori" span={12}>
                    <Form.Item label="Followers" name="facebookFollowers">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Numbers of Followers"
                        type="number"
                      />
                    </Form.Item>
                    <Form.Item label="Followers" name="instagramFollowers">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Numbers of Followers"
                        type="number"
                      />
                    </Form.Item>
                    <Form.Item label="Followers" name="tiktokFollowers">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Numbers of Followers"
                        type="number"
                      />
                    </Form.Item>
                    <Form.Item label="Followers" name="otherFollowers">
                      <Input
                        size="large"
                        className="input-basic"
                        placeholder="Numbers of Followers"
                        type="number"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </div>
          </Col>
        </Row>
      </section>
    </>
  )
}

const mapStateToProps = ({ hostProfile, hostList }) => ({
  hostProfile: hostProfile.data,
  tagsById: hostList.tagsById,
  tags: hostList.tags,
});

const mapDispatchToProps = {
  updateHostProfile
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EditProfile)
);