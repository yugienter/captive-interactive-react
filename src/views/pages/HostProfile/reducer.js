import * as types from './actionTypes';

const initialState = {
  isFetching: false,
  isUpdating: false,
  isUploadingMedia: false,
  data: null,
  error: null,
  tab: '1'
};

const hostProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.IS_FETCH_PROFILE:
      return { ...state, isFetching: action.payload };
    case types.FETCH_PROFILE_SUCCESS:
      return { ...state, data: action.payload };

    case types.IS_UPDATE_PROFILE:
      return { ...state, isUpdating: action.payload };
    case types.UPDATE_PROFILE_SUCCESS:
      return { ...state, data: { ...state.data, avatar: action.payload } };

    case types.SET_PROFILE_VALUES:
      return { ...state, data: { ...state.data, ...action.payload } };

    case types.ADD_PROFILE_MEDIA:
      return { ...state, data: { ...state.data, media: [...state.data.media, action.payload] } };

    case types.REMOVE_PROFILE_MEDIA:
      return { ...state, data: { ...state.data, media: state.data.media.filter(media => media.code !== action.payload) } };

    case types.IS_UPLOAD_MEDIA:
      return { ...state, isUploadingMedia: action.payload };

    case types.SET_ERROR:
      return { ...state, err: action.payload };

    case types.SET_TAB:
      return { ...state, tab: action.payload };

    default:
      return state;
  }
};

export default hostProfileReducer;
