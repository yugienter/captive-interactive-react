import { constants, client, utils } from 'helpers';
import config from 'config';

import * as types from './actionTypes';

const { api } = constants;
const { API_END_POINT } = config;

function _calculateHostProfile(host) {
  host.step = utils.calculateHostProfileCurrentStep(host);
  localStorage.setItem('step', host.step);

  host.completePercentage = utils.calculateHostProfileCompleteness(host);
  host.avatar = utils.getMediaUrl(host.avatar);
  const yearExperience = utils.getYearOfExperience(host.startDate);
  host.yearExperience = yearExperience;

  host.social.facebook = utils.getSocialLink('facebook', host.social.facebook);
  host.social.instagram = utils.getSocialLink('instagram', host.social.instagram);
  host.social.tiktok = utils.getSocialLink('tiktok', host.social.tiktok);
}

export const fetchHostProfile = (email) => async dispatch => {
  if (!email) return;
  try {
    dispatch({ type: types.IS_FETCH_PROFILE, payload: true });

    const { data } = await client.request({
      path: api.path.getHostProfile(email),
      method: 'get',
    });

    if (data.data) {
      const host = data.data;
      _calculateHostProfile(host);
      dispatch({ type: types.FETCH_PROFILE_SUCCESS, payload: host });
      return host;
    }

    dispatch({ type: types.SET_ERROR, payload: 'Profile not exist' });
  } catch (err) {
    console.log('[fetchHostProfile]', err);
    dispatch({ type: types.SET_ERROR, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_PROFILE, payload: false });
  }
};

export const getHost = async (email) => {
  if (!email) return;
  try {
    const { data } = await client.request({
      path: api.path.getHostProfile(email),
      method: 'get',
    });

    if (data.data) {
      const host = data.data;
      _calculateHostProfile(host);
      return host;
    }

  } catch (err) {
    console.log('[getHost]', err);
  }
};

export const updateHostProfile = (payload) => async dispatch => {
  try {
    dispatch({ type: types.IS_UPDATE_PROFILE, payload: true });

    const { data } = await client.request({
      path: api.path.updateHostProfile,
      method: 'post',
      data: payload
    });

    if (data.data) {
      const host = data.data;
      _calculateHostProfile(host);
      dispatch({ type: types.FETCH_PROFILE_SUCCESS, payload: host });
      return host;
    }

    dispatch({ type: types.SET_ERROR, payload: 'Profile not exist' });
  } catch (err) {
    console.log('[fetchHostProfile]', err);
    dispatch({ type: types.SET_ERROR, payload: err.message });
  } finally {
    dispatch({ type: types.IS_UPDATE_PROFILE, payload: false });
  }
};

export const setHostProfileValues = (values) => dispatch => {
  dispatch({ type: types.SET_PROFILE_VALUES, payload: values });
};

export const addHostProfileMedia = (item) => dispatch => {
  dispatch({ type: types.ADD_PROFILE_MEDIA, payload: item });
};

export const removeHostProfileMedia = (code) => dispatch => {
  dispatch({ type: types.REMOVE_PROFILE_MEDIA, payload: code });
};

export const uploadMedia = (params) => {
  const url = new URL(`${API_END_POINT}${api.path.uploadMedia}`);
  url.search = new URLSearchParams(params);
  return url.toString();
};

export const deleteMedia = (code) => async dispatch => {
  try {
    dispatch({ type: types.IS_DELETE_MEDIA, payload: true });

    const { data } = await client.request({
      path: api.path.getLocalMedia(code),
      method: 'delete',
    });

    if (data.data) {
      const media = data.data;
      dispatch({ type: types.DELETE_MEDIA_SUCCESS, payload: media });
      return media;
    }

    dispatch({ type: types.SET_ERROR, payload: 'Delete failed' });
  } catch (err) {
    console.log('[deleteMedia]', err);
    dispatch({ type: types.SET_ERROR, payload: err.message });
  } finally {
    dispatch({ type: types.IS_DELETE_MEDIA, payload: false });
  }
};

export const setTab = (tab) => dispatch => {
  dispatch({ type: types.SET_TAB, payload: tab });
};