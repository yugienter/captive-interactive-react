import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Tabs } from "antd";
import { setTab } from 'views/pages/HostProfile/actions';
import Profile from './_profile';
import Account from './_account';

const { TabPane } = Tabs;

const ViewProfile = ({ hostProfile, tagsById, handleChangeMode, tab, setTab, history }) => {
  const [tabMode, setTabMode] = useState(tab);

  useEffect(() => {
    setTabMode(tab);
  }, [tab]);

  const callback = (key) => { setTab(key) }

  if (!hostProfile) {
    return <section className='main-content host-profile_content'></section>;
  }

  return (
    <>
      <section className='main-content host-profile_content'>
        <Tabs defaultActiveKey="1" onChange={callback} activeKey={tabMode}>
          <TabPane tab="Profile" key="1">
            <Profile
              history={history}
              hostProfile={hostProfile}
              tagsById={tagsById}
              handleChangeMode={handleChangeMode}
              tabMode={tabMode}
            />
          </TabPane>
          <TabPane tab="Account" key="2">
            <Account email={hostProfile.email} tabMode={tabMode} />
          </TabPane>
        </Tabs>
      </section>
    </>
  )
}

const mapStateToProps = ({ hostProfile, hostList }) => ({
  hostProfile: hostProfile.data,
  tab: hostProfile.tab,
  tagsById: hostList.tagsById
});

const mapDispatchToProps = {
  setTab
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewProfile)
);