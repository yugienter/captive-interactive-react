import React from "react";
import { Tag, Row, Col, Progress, Image, Button } from "antd";
import {
  IconPersonalInfo,
  IconPhoto,
  IconTopics,
  IconConnections,
  IconOverviews,
} from "views/components/Icon/pages";
import HostTag from "views/pages/Host/parts/HostTag";
import MenuScroll from "../MenuScroll";
import IconComplete from "./IconComplete";
import { utils, constants, router } from "helpers";

const { mainStyle } = constants;
const { CheckableTag } = Tag;

const Profile = (props) => {
  const { hostProfile, tagsById, tabMode, history } = props;
  const tagsData = hostProfile.tags.map((tagId) => {
    const tag = tagsById[tagId];
    return {
      icon: HostTag({ tag: tag.name }).props.icon,
      name: tag.name,
    };
  });

  const views = {
    colLeft: [
      { label: "Country", value: hostProfile.country },
      {
        label: "Gender",
        value: hostProfile.sex === "male" ? "Male" : "Female",
      },
      { label: "Weight", value: hostProfile.weight ? `${hostProfile.weight} kg` : '' },
      {
        label: "Experience",
        value: hostProfile.yearExperience
          ? `${hostProfile.yearExperience} years`
          : null,
      },
    ],
    colRight: [
      { label: "Phone Number", value: `+${hostProfile.phoneNumber}` },
      {
        label: "Date of Birth",
        value: `${
          hostProfile.dob
            ? new Date(hostProfile.dob).toLocaleDateString()
            : null
        }`,
      },
      { label: "Height", value: hostProfile.height ? `${hostProfile.height}cm` : '' },
      {
        label: "Hourly Rate",
        value: hostProfile.rates ? `$${hostProfile.rates}` : null,
      },
    ],
  };

  const onCompleteProfile = () => {
    history.replace(router.flowSignUp);
  };

  return (
    <>
      <Row gutter={mainStyle.gutter} className="row-host-profile">
        <Col className="gutter-row" md={8} lg={8} xl={6}>
          {hostProfile.completePercentage < 100 && (
            <div className="profile-completion">
              <Row gutter={mainStyle.gutter}>
                <Col className="gutter-row" span={8}>
                  {hostProfile.completePercentage >= 100 ? (
                    <IconComplete />
                  ) : (
                    <Progress
                      type="circle"
                      percent={hostProfile.completePercentage}
                      width={80}
                      strokeColor={"white"}
                      trailColor={"#ffffff17"}
                    />
                  )}
                </Col>
                <Col className="gutter-row" span={16}>
                  <div className="profile-completion_title">
                    Profile{" "}
                    {hostProfile.completePercentage >= 100
                      ? "Completed!"
                      : "Completion"}
                  </div>
                  <div className="profile-completion_content">
                    {hostProfile.completePercentage < 100
                      ? "Complete your profile to be on top in Seller Marketplace searching"
                      : "Keep building your profile to expand your reputation and reach."}
                  </div>
                </Col>
              </Row>
              {hostProfile.completePercentage < 100 && (
                <Button
                  type="primary"
                  className="control complete"
                  block
                  onClick={onCompleteProfile}
                >
                  Complete My Profile
                </Button>
              )}
            </div>
          )}
          <MenuScroll tabMode={tabMode} />
        </Col>
        <Col className="gutter-row" md={12} lg={16} xl={18}>
          <div id="overview" className="section-scroll section-scroll_overview">
            <h1>
              <IconOverviews />
              Overview
            </h1>
            <Row gutter={mainStyle.gutter} style={{ padding: "0 26px" }}>
              <Col className="gutter-row" span={8} className="avatar-image">
                <div className="lbl-hozi">Avatar</div>
                <Image
                  width={194}
                  src={hostProfile.avatar}
                  className="avatar-image_view"
                />
              </Col>
              <Col className="gutter-row" span={16}>
                <div className="lbl-hozi">Full Name</div>
                <div className="txt-hozi mb26">{hostProfile.name}</div>

                <div className="lbl-hozi">Biography</div>
                <div className="txt-hozi mb26">{hostProfile.aboutMe}</div>

                <div className="lbl-hozi">Portfolio (Showreel or website)</div>
                <div className="txt-hozi blue">
                  {hostProfile.porfolio ? (
                    <a
                      href={hostProfile.porfolio}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {hostProfile.porfolio}
                    </a>
                  ) : (
                    ""
                  )}
                </div>
              </Col>
            </Row>
          </div>
          <div id="personal-info" className="section-scroll">
            <h1>
              <IconPersonalInfo />
              Personal Information
            </h1>
            <Row gutter={mainStyle.gutter} style={{ padding: "0 26px" }}>
              <Col className="gutter-row" span={12}>
                {views.colLeft.map((item, key) => (
                  <Row
                    key={key}
                    gutter={mainStyle.gutter}
                    style={{ display: "flex", alignItems: "baseline" }}
                  >
                    <Col className="gutter-row" span={6}>
                      <div className="lbl-vert">{item.label}</div>
                    </Col>
                    <Col className="gutter-row" span={18}>
                      <div className="txt-vert">{item.value}</div>
                    </Col>
                  </Row>
                ))}
              </Col>
              <Col className="gutter-row" span={12}>
                {views.colRight.map((item, key) => (
                  <Row
                    key={key}
                    gutter={mainStyle.gutter}
                    style={{ display: "flex", alignItems: "baseline" }}
                  >
                    <Col className="gutter-row" span={9}>
                      <div className="lbl-vert">{item.label}</div>
                    </Col>
                    <Col className="gutter-row" span={15}>
                      <div className="txt-vert">{item.value}</div>
                    </Col>
                  </Row>
                ))}
              </Col>
            </Row>
          </div>
          <div id="media" className="section-scroll section-scroll_overview">
            <h1>
              <IconPhoto />
              Photos & Videos
            </h1>
            <div className="box-medias avatar-image152">
              {hostProfile.media.map((media) => {
                const url = utils.getMediaUrl(media.url);
                if (utils.isImage(url))
                  return (
                    <Image
                      className="avatar-image152_view"
                      key={media.code}
                      width={152}
                      src={url}
                    />
                  );

                if (utils.isVideo(url))
                  return (
                    <div>
                      <video
                        key={media.code}
                        width={152}
                        height={152}
                        style={{
                          background: "#cdcdcd",
                        }}
                        controls
                        src={url}
                      />
                    </div>
                  );

                return "";
              })}
            </div>
          </div>
          <div id="topics" className="section-scroll">
            <h1>
              <IconTopics />
              Topics
            </h1>
            <div className="topics-tags-view">
              {tagsData.map((tag) => (
                <CheckableTag key={tag.name}>
                  {tag.icon} {tag.name}
                </CheckableTag>
              ))}
            </div>
          </div>
          <div id="connections" className="section-scroll">
            <h1>
              <IconConnections />
              Connections
            </h1>
            <Row gutter={mainStyle.gutter} style={{ padding: "0 26px" }}>
              <Col className="gutter-row" span={15}>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={6}>
                    <div className="lbl-vert">Facebook</div>
                  </Col>
                  <Col className="gutter-row" span={18}>
                    <div className="txt-vert blue">
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={hostProfile.social.facebook}
                      >
                        {hostProfile.social.facebook}
                      </a>
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={6}>
                    <div className="lbl-vert">Instagram</div>
                  </Col>
                  <Col className="gutter-row" span={18}>
                    <div className="txt-vert blue">
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={hostProfile.social.instagram}
                      >
                        {hostProfile.social.instagram}
                      </a>
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={6}>
                    <div className="lbl-vert">TikTok</div>
                  </Col>
                  <Col className="gutter-row" span={18}>
                    <div className="txt-vert blue">
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={hostProfile.social.tiktok}
                      >
                        {hostProfile.social.tiktok}
                      </a>
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={6}>
                    <div className="lbl-vert">Other</div>
                  </Col>
                  <Col className="gutter-row" span={18}>
                    <div className="txt-vert blue">
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={hostProfile.social.other}
                      >
                        {hostProfile.social.other}
                      </a>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col className="gutter-row" span={9}>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={9}>
                    <div className="lbl-vert">Followers</div>
                  </Col>
                  <Col className="gutter-row" span={15}>
                    <div className="txt-vert">
                      {utils.numberWithCommas(hostProfile.facebookFollowers)}
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={9}>
                    <div className="lbl-vert">Followers</div>
                  </Col>
                  <Col className="gutter-row" span={15}>
                    <div className="txt-vert">
                      {utils.numberWithCommas(hostProfile.instagramFollowers)}
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={9}>
                    <div className="lbl-vert">Followers</div>
                  </Col>
                  <Col className="gutter-row" span={15}>
                    <div className="txt-vert">
                      {utils.numberWithCommas(hostProfile.tiktokFollowers)}
                    </div>
                  </Col>
                </Row>
                <Row
                  gutter={mainStyle.gutter}
                  style={{ display: "flex", alignItems: "baseline" }}
                >
                  <Col className="gutter-row" span={9}>
                    <div className="lbl-vert">Followers</div>
                  </Col>
                  <Col className="gutter-row" span={15}>
                    <div className="txt-vert">
                      {utils.numberWithCommas(hostProfile.otherFollowers)}
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Profile;
