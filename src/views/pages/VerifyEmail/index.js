import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from "react-router-dom";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import { IconVerified, IconBorder, IconVerifiFailed } from "views/components/Icon/pages";
import { IconArrowRightWhite } from 'views/components/Icon/basic'
import { IconArrRightCircle } from "views/components/Icon/pages";
import { constants } from 'helpers';
import { Button } from 'antd';

import { verifyToken } from '../CreatePassword/actions';

const { router } = constants;
const HOST_ROUTER = router.signIn.replace(':role', 'host');

const VerifyEmail = ({ history, match: { params }, verifyToken, errorMessage }) => {
  const [count, setCount] = useState(15);
  const [isVerified, setVerified] = useState(false);

  useEffect(() => {
    if (count === 0) {
      history.replace(HOST_ROUTER);
    }
  }, [count, history]);

  useEffect(() => {
    async function verifyEmail() {
      const result = await verifyToken(params.token);
      setVerified(result);
    }

    verifyEmail();
  }, [params, verifyToken]);

  useEffect(() => {
    if (errorMessage) {
      const id = setInterval(() => {
        setCount(c => c - 1);
      }, 1000);
      return () => clearInterval(id);
    }
  }, [errorMessage]);

  const handleContinue = () => {
    history.replace(router.flowSignUp);
  }

  return (
    <Page helmet="Thank You" layout="split">
      <div className="primary-section w80">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            {isVerified && (
              <div>
                <IconVerified />
                <h2 style={{ marginTop: '15px' }}>Verified!</h2>
                <div className='text-bold mt4'>Account verified<br />The world of Social Commerce awaits you!</div>
                <div style={{ margin: '20px 0 24px' }}><IconBorder /></div>
                <div className='text-normal'>
                  <Button
                    type="primary"
                    size="large"
                    className="cta continue"
                    style={{ width: '230px' }} block
                    onClick={handleContinue}
                  >Continue<IconArrowRightWhite /></Button>
                </div>
              </div>
            )}
            {errorMessage && (
              (
                <div>
                  <IconVerifiFailed />
                  <h2 style={{ marginTop: '15px' }}>Verification Failed!</h2>
                  <div className='text-bold mt4'>The user does not exist or is verified.</div>
                  <div style={{ margin: '20px 0 22px' }}><IconBorder /></div>
                  <div className='text-normal'>
                    <IconArrRightCircle /> Redirect to <Link to={HOST_ROUTER}>Log in</Link> page in <strong>{count}</strong> seconds
                  </div>
                </div>
              )
            )}
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section w20"></div>
    </Page>
  )
}

const mapStateToProps = ({ createPassword }) => ({
  isProcessing: createPassword.isProcessing,
  errorMessage: createPassword.message
});

const mapDispatchToProps = {
  verifyToken
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(VerifyEmail)
);