const REDUCER = 'signUp';

export const SET_ON_SIGN_UP = `@@${REDUCER}/SET_ON_SIGN_UP`;
export const UPDATE_SIGN_UP_ERR_MSG = `@@${REDUCER}/UPDATE_SIGN_UP_ERR_MSG`;
