import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Row, Col } from "antd";
import Page from "views/components/Page";
// import LogoWhite from "./media/white-logo.png";
import LogoWhite from "./media/SCX-logo.svg";
import Company from "./media/company.png";
import Host from "./media/host.png";

index.propTypes = {};

function index(props) {
  return (
    <Page helmet="Select role" layout="split">
      <div className="secondary-section-host full">
        <div className="select-role">
          <div className="select-role__header">
            <img src={LogoWhite} alt="logo" width={180}/>
            <p className="select-role__header__title">
              Social Commerce Exchange
              <br />
              Your Gateway into the Social Commerce World
            </p>
          </div>
          <div className="select-role__content">
            <Row justify="space-around">
              <Col span={10}>
                <Link to="sign-in/company">
                  <div className="select-role__content__role">
                    <img src={Company} alt="company" />
                    <p className="select-role__content__role__title">
                      We're a brand owner
                    </p>
                    <p className="select-role__content__role__description">
                      We are interested in working with social commerce sellers
                      <br />
                      to sell our products
                    </p>
                  </div>
                </Link>
              </Col>
              <Col span={10}>
                {" "}
                <Link to="sign-in/host">
                  <div className="select-role__content__role">
                    <img src={Host} alt="host" />
                    <p className="select-role__content__role__title">
                      I’m a Seller
                    </p>
                    <p className="select-role__content__role__description">
                      We're interested to work with brands and discover
                      salesworthy,
                      <br />
                      best value and upcoming products
                    </p>
                  </div>
                </Link>
              </Col>
            </Row>
          </div>
          <div className="select-role__footer">
            <p>© 2021 Captive Interactive. All Rights Reserved.</p>
          </div>
        </div>
      </div>
    </Page>
  );
}

export default index;
