import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import { Table } from "antd";
import { search, setStatus, updateSort, setSelectedPayment } from "../actions";
import moment from "moment";
import {
  IconUp,
  IconDown,
  IconReceipt,
  IconAttach,
} from "views/components/Icon/pages";
import DrawerPaymentDetail from "views/components/DrawerPaymentDetail/DrawerPaymentDetail";

const PAYMENT_STATUS = {
  submitted: {
    id: "submitted",
    label: "Verifying Payment",
    color: "#FF9A4D",
  },
  // verify: {
  //   id: "verify",
  //   label: "Verifying Payment",
  //   color: "#55ADFF",
  // },
  approved: {
    id: "approved",
    label: "Paid",
    color: "#1D1929",
  },
  rejected: {
    id: "rejected",
    label: "Canceled",
    color: "#F76969",
  },
};

// const sampleData = [
//   {
//     paymentID: "PO10001",
//     type: "Order Submission",
//     name: "Black Chicken Mushroom Soup",
//     status: PAYMENT_STATUS.canceled.id,
//     amount: "$1,295.30",
//     date: "01 Dec, 2021",
//     isIncrease: true,
//   },
//   {
//     paymentID: "PO10002",
//     type: "Order Submission",
//     name: "Black Chicken Mushroom Soup",
//     status: PAYMENT_STATUS.verifying.id,
//     amount: "$1,295.30",
//     date: "01 Dec, 2021",
//     isIncrease: false,
//   },
//   {
//     paymentID: "PO10003",
//     type: "Order Submission",
//     name: "Black Chicken Mushroom Soup",
//     status: PAYMENT_STATUS.paid.id,
//     amount: "$1,295.30",
//     date: "01 Dec, 2021",
//     isIncrease: true,
//   },
//   {
//     paymentID: "PO10004",
//     type: "Order Submission",
//     name: "Black Chicken Mushroom Soup",
//     status: PAYMENT_STATUS.canceled.id,
//     amount: "$1,295.30",
//     date: "01 Dec, 2021",
//     isIncrease: false,
//   },
//   {
//     paymentID: "PO10005",
//     type: "Order Submission",
//     name: "Black Chicken Mushroom Soup",
//     status: PAYMENT_STATUS.pending.id,
//     amount: "$1,295.30",
//     date: "01 Dec, 2021",
//     isIncrease: false,
//   },
// ];

function MainTable({
  payments,
  selectedPayment,
  pagination,
  search,
  isLoading,
  status,
  statusCount,
  setStatus,
  updateSort,
  setSelectedPayment,
}) {
  const [openDetailPopup, setOpenDetailPopup] = useState(false);

  useEffect(() => {
    const params = { page: pagination.page, perPage: pagination.perPage };
    if (status) params.status = status;
    search(params);
  }, [search, status]);

  const handleTableChange = (pagination, filter, sorter) => {
    let sort = null;
    if (!_.isEmpty(sorter)) {
      sort = `${sorter.field}:${sorter.order === "descend" ? -1 : 1}`;
    }
    updateSort(sort);

    const params = {
      page: pagination.current,
      perPage: pagination.pageSize,
      sort,
    };
    search(params);
  };

  const onChangeStatus = (status) => {
    search({
      page: 1,
    });
  };

  const onRow = (payment) => ({
    onClick: () => {
      setSelectedPayment(payment);
      setOpenDetailPopup(true);
    },
  });

  const renderDate = (dateStr) => {
    if (!dateStr) return dateStr;
    const curDate = new Date(dateStr);
    if (isToday(curDate)) {
      return "Today";
    }
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const now = new Date();

    const diffDays = Math.round(Math.abs((now - curDate) / oneDay));
    if (diffDays < 7) {
      return `${diffDays} day${diffDays > 1 ? "s" : ""} ago`;
    }
    const diffWeeks = Math.round(diffDays / 7);
    if (diffWeeks < 4) {
      return `${diffWeeks} week${diffWeeks > 1 ? "s" : ""} ago`;
    }

    return moment(dateStr).format("DD MMM YYYY");
  };
  const isToday = (someDate) => {
    const today = new Date();
    return (
      someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
    );
  };

  const columns = [
    {
      title: "",
      key: "icon",
      render: (payment) => (
        <span>
          <IconUp />
        </span>
      ),
      // payment.isIncrease ? (
      //   <span>
      //     <IconUp />
      //   </span>
      // ) : (
      //   <span>
      //     <IconDown />
      //   </span>
      // ),
    },
    {
      title: "Payment ID",
      key: "code",
      dataIndex: "code",
    },
    {
      title: "Type",
      key: "type",
      render: (payment) => <span>Order Submission</span>,
      //   dataIndex: "type",
      //   sorter: (a, b) => a.age - b.age,
    },
    {
      title: "Product/Job Name",
      key: "jobName",
      dataIndex: "jobName",
    },
    {
      title: "Payment Status",
      key: "status",
      dataIndex: "status",
      sorter: (a, b) => a.status - b.status,
      render: (status) => {
        return (
          PAYMENT_STATUS[status] && (
            <span style={{ color: PAYMENT_STATUS[status].color }}>
              {PAYMENT_STATUS[status].label}
            </span>
          )
        );
      },
    },
    {
      title: "Amount",
      key: "amount",
      // dataIndex: "total",
      sorter: (a, b) => a.total - b.total,
      render: (payment) => {
        return payment.source === "host" ? (
          <span style={{ color: "#DC2425" }}>
            - {parseFloat(payment.total).toFixed(2)}
          </span>
        ) : (
          <span style={{ color: "#1BD2A4" }}>
            + {parseFloat(payment.total).toFixed(2)}
          </span>
        );
      },
      // payment.isIncrease ? (
      //   <span style={{ color: "#DC2425" }}>- {payment.amount}</span>
      // ) : (
      //   <span style={{ color: "#1BD2A4" }}>+ {payment.amount}</span>
      // ),
    },
    {
      title: "Date",
      key: "updatedAt",
      dataIndex: "updatedAt",
      sorter: (a, b) => a.updatedAt - b.updatedAt,
      render: (updatedAt) => <span>{renderDate(updatedAt)}</span>,
    },
    {
      title: "Action",
      key: "action",
      render: (payment) => (
        <div className="payment__table__action">
          <span>
            <IconReceipt />
          </span>
          <span>
            <IconAttach />
          </span>
          {/* <a className="payment__table__action--verify">Verify as</a> */}
        </div>
      ),
    },
  ];

  return (
    <>
      <div className="payment__table-container">
        <div className="payment__table-filter">
          <div className="payment__list-filter">
            <div className="payment__list-filter__tags">
              <ul>
                <li
                  className={`payment__list-filter__tag${
                    status === undefined
                      ? " payment__list-filter__tag--selected"
                      : ""
                  }`}
                >
                  <a
                    onClick={() => {
                      setStatus(undefined);
                      onChangeStatus(undefined);
                    }}
                  >
                    All Transactions ({statusCount.total || 0})
                  </a>
                </li>
                {Object.keys(PAYMENT_STATUS).map((item) => (
                  <li
                    key={_.get(PAYMENT_STATUS[item], "id")}
                    className={`payment__list-filter__tag${
                      _.get(PAYMENT_STATUS[item], "id") === status
                        ? " payment__list-filter__tag--selected"
                        : ""
                    }`}
                  >
                    <a
                      onClick={() => {
                        setStatus(_.get(PAYMENT_STATUS[item], "id"));
                        onChangeStatus(_.get(PAYMENT_STATUS[item], "id"));
                      }}
                    >
                      {_.get(PAYMENT_STATUS[item], "label")}
                      <span>
                        {" "}
                        ({statusCount[_.get(PAYMENT_STATUS[item], "id")] || 0})
                      </span>
                    </a>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <div className="payment__table">
          <Table
            columns={columns}
            dataSource={payments}
            rowKey={(record) => record.code}
            onChange={handleTableChange}
            onRow={onRow}
            pagination={{
              position: ["bottomCenter"],
              showSizeChanger: true,
              showTotal: (total, range) =>
                `Showing ${range[0]}-${range[1]} of ${total} results`,
              size: "small",
              current: pagination.page,
              pageSize: pagination.perPage,
              total: pagination.total,
            }}
            loading={isLoading}
          />
        </div>
      </div>
      <DrawerPaymentDetail
        visible={openDetailPopup}
        dataSource={selectedPayment}
        handleClose={() => {
          setOpenDetailPopup(false);
        }}
      />
    </>
  );
}

const mapStateToProps = ({
  paymentReducer: {
    payments,
    pagination,
    isLoading,
    filter,
    statusCount,
    selectedPayment,
  },
}) => ({
  payments,
  pagination,
  isLoading,
  status: filter.status,
  statusCount,
  selectedPayment,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(
    {
      search,
      setStatus,
      updateSort,
      setSelectedPayment,
    },
    dispatch
  ),
});
export default connect(mapStateToProps, mapDispatchToProps)(MainTable);
