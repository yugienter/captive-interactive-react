import React from 'react';

function TopBar(props) {
    return (
        <div className='payment__top'>
            <h1>Payment Management</h1>
        </div>
    );
}

export default TopBar;