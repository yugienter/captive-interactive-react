import { useEffect, useState } from "react";
import { Tabs } from "antd";
import { constants } from "helpers";

const { TabPane } = Tabs;

const MainTabs = () => {
    const [status, SetStatus] = useState('')

    const changeTabByStatus = (status) => {
        SetStatus(status)
    };

    return (
        <>
            <Tabs
                defaultActiveKey="1"
                onChange={changeTabByStatus}
                className="payment__container--tabs"
            >
                <TabPane
                    tab={
                        <div>
                            Payment Management
            </div>
                    }
                    key="processing"
                >
                </TabPane>
                <TabPane
                    tab={
                        <div>
                            Banking detail
            </div>
                    }
                    key="banking"
                >
                </TabPane>
            </Tabs>
        </>
    );
};

export default MainTabs;
