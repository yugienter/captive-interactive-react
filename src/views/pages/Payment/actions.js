import { constants, client } from "helpers";
import * as types from "./actionTypes";
import _ from "lodash";

const { api, PAGINATION } = constants;

const _makeQuery = (options, statePaymentList) => (dispatch) => {
  const { pagination, filter, sort } = statePaymentList;
  const query = {};

  const { page = PAGINATION.PAGE, perPage = pagination.perPage } = options;
  query.page = page;
  query.per_page = perPage;

  if (filter.status) {
    query.status = filter.status;
  }

  if (sort) {
    query.sort = sort;
  }

  return query;
};
const _doSearch = async (query = {}) => {
  try {
    const res = await client.request({
      path: api.path.getPayments,
      method: "get",
      params: query,
    });

    return res;
  } catch (error) {
    return { data: [], pagination: {} };
  }
};

export const search = (options = {}) => async (dispatch, getState) => {
  dispatch({ type: types.UPDATE_LOADING, payload: true });
  const statePaymentReduce = getState().paymentReducer;
  let data = statePaymentReduce.payments;

  const query = _makeQuery(options, statePaymentReduce)(dispatch);
  const result = await _doSearch(query);
  data = result.data;
  dispatch({ type: types.UPDATE_PAYMENTS, payload: data });

  // update pagination
  const pagination = {
    page: query.page,
    perPage: query.per_page,
    total: data.total,
    lastPage: statePaymentReduce.pagination.page,
  };
  dispatch({ type: types.UPDATE_PAGINATION, payload: pagination });

  dispatch({ type: types.UPDATE_LOADING, payload: false });
  return data;
};

export const setStatus = (status) => async (dispatch, getState) => {
  dispatch({ type: types.UPDATE_FILTER, payload: { status } });
};

export const updateSort = (sort) => async (dispatch, getState) => {
  dispatch({ type: types.UPDATE_SORT, payload: sort });
};

export const setSelectedPayment = (payment) => async (dispatch, getState) => {
  dispatch({ type: types.UPDATE_SELECTED_PAYMENT, payload: payment });
};
