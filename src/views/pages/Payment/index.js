import React from 'react';
import Page from "views/components/Page";
import TopBar from './components/TopBar'
import MainTabs from './components/MainTabs'

import { constants } from "helpers";
import MainTable from './components/MainTable';

const { userRoles } = constants;

function index(props) {
  return (
    <Page
      helmet="Payment"
      alignTop
      requiredAccess={[userRoles.HOST]}
    >
      <div className="payment">
        <TopBar/>
        <MainTabs/>
        <MainTable/>
      </div>
    </Page>
  );
}

export default index;