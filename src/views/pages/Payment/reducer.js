import * as types from "./actionTypes";
import { constants } from "helpers";
const { PAGINATION } = constants;

const initialState = {
  payments: [],
  isLoading: false,
  filter: {
    status: null,
    sortStatus: null,
    sortAmound: null,
    sortDate: null,
  },
  pagination: {
    page: PAGINATION.PAGE + 1,
    perPage: PAGINATION.PER_PAGE,
    total: null,
    lastPage: null,
  },
  statusCount: {
    verify: 0,
    submitted: 0,
    approved: 0,
    rejected: 0,
    total: 0,
  },
  sort: null,
  selectedPayment: null,
};

const paymentReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    // for payment list pages
    case types.UPDATE_LOADING:
      return {
        ...state,
        isLoading: !!payload,
      };

    case types.UPDATE_PAYMENTS: {
      return {
        ...state,
        payments: payload.data,
        pagination: {
          ...state.pagination,
          total: payload.total,
        },
        statusCount: {
          ...state.statusCount,
          ...payload.statusCount,
        },
      };
    }

    case types.UPDATE_PAGINATION: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          ...payload,
        },
      };
    }

    case types.UPDATE_FILTER: {
      return {
        ...state,
        filter: {
          ...state.filter,
          ...payload,
        },
      };
    }

    case types.UPDATE_SORT: {
      return {
        ...state,
        sort: payload,
      };
    }

    case types.UPDATE_SELECTED_PAYMENT: {
      return {
        ...state,
        selectedPayment: payload,
      };
    }

    default:
      return state;
  }
};

export default paymentReducer;
