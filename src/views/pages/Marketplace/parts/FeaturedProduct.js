import TagMarket from "views/components/TagMarket";
import { Link, useHistory } from "react-router-dom";
import { constants } from "helpers";
import { connect, useDispatch } from 'react-redux';
import { getFeaturedProduct } from '../actions'
import _ from "lodash";
import { useEffect } from "react";
import moment from "moment"
import { utils, ReactUtils } from 'helpers'
import * as type from "../actionTypes"

const { getMediaUrl } = utils

const { router } = constants;

const FeaturedProduct = ({ products, getFeaturedProduct }) => {
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(async () => {
        await getFeaturedProduct({ page: 0, per_page: 8 })
    }, [])

    const getDiffDate = (now, then) => {
        return `${moment(now).diff(moment(then), 'days')} days`
    }

    return (
        <div className="maket-product">
            <div className="market-product__header">
                <h2>Featured Products</h2>
                <Link to={router.featureProducts}>View all products</Link>
            </div>
            <div className="market-product__list">
                {
                    products && products.map((item, index) => (
                        <div onClick={() => {
                            history.push(`/detail-campaign/${_.get(item, 'jobCode')}`)
                            dispatch({
                                type: type.SET_SELECTED_PRODUCT,
                                payload: _.get(item, 'product')
                            })
                        }}  className="item" key={index}>
                            <div className="item__bg" style={{ backgroundImage: `url(${getMediaUrl(_.get(item, 'product.media[0].url'))})` }}>
                                <div>
                                    {_.get(item, 'campaign.status') === 'complete' && <div className="item__tag blue">Pre-Streaming</div>}
                                    {_.get(item, 'campaign.submitted') === 'complete' && <div className="item__tag gray">Bid Submitted</div>}
                                    {_.get(item, 'campaign.negotiating') === 'complete' && <div className="item__tag orange">Dealing</div>}
                                    {!_.get(item, 'campaign.status') && <div className="item__tag gray">Job invited</div>}
                                </div>
                                <div>
                                    <TagMarket isHideFixedRate={_.get(item, 'hourlyRateRange.negiable')}
                                        isHideComission={!_.get(item, 'commission.negiable')} />
                                    <div className="bid">
                                        <span>{_.get(item, 'big') || 0}</span>
                                        <span>Pitch</span>
                                    </div>
                                </div>
                            </div>
                            <div className="item__content">
                                <div>
                                    <div className="avatar">
                                        <img src={getMediaUrl(_.get(item, 'company.logo'))}></img>
                                        <span>{_.get(item, 'company.name')}</span>
                                    </div>
                                    <h3>{_.get(item, 'product.name')}</h3>
                                </div>
                                <div>
                                    <span>
                                        <div>Ending in</div>
                                        <div>{getDiffDate(_.get(item, 'campaign.timeEnd'), _.get(item, 'campaign.timeStart'))}</div>
                                    </span>
                                    <span>
                                        <div>Fixed Rate</div>
                                        <div>{'$' + (_.get(item, 'hourlyRateRange.start') || 0) + ' - ' + '$' + (_.get(item, 'hourlyRateRange.end') || 0)}</div>
                                    </span>
                                </div>

                            </div>
                        </div>
                    ))}
            </div>
        </div>
    )
};

const mapDispatchToProps = {
    getFeaturedProduct
};

const mapStateToProps = ({ marketplace }) => ({
    products: _.get(marketplace, 'featuredProduct.data') || []
});

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedProduct);