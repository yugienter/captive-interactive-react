import Trending from './Trending'
import OpenBid from './OpenBid'
import FeaturedProduct from './FeaturedProduct'

export {
    Trending,
    OpenBid,
    FeaturedProduct
}