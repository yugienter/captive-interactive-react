import { Button } from "antd";
import { IconUsers } from "views/components/Icon/essentials";
import { IconClock, IconPen } from "views/components/Icon/market";
import { IconInfor, IconMoney } from "views/components/Icon/product";
import { Avatar } from "antd";
import TagMarket from "views/components/TagMarket";
import DrawerBidJob from "views/components/DrawerBidJob";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { getProductTrending } from "../actions";
import * as type from "../actionTypes";
import _ from "lodash";
import { utils, ReactUtils } from "helpers";
import moment from "moment";

const { getMediaUrl } = utils;

const Trending = ({ getProductTrending, trending }) => {
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const getDiffDate = (now, then) => {
    return `${moment(now).diff(moment(then), "days")} days`;
  };

  useEffect(() => {
    getProductTrending();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if (!trending || Object.keys(trending).length === 0) return <div></div>;
  return (
    <>
      <div className="market__trending">
        <h2>Trending</h2>
        <div className="market__trending__main">
          <div className="market__trending__main__picture">
            <img
              src={getMediaUrl(_.get(trending, "job.product.media[0].url"))}
              alt="trending"
            />
          </div>
          <div className="market__trending__main__content">
            <TagMarket
              isHideFixedRate={
                !_.get(trending, "deal.streamHourlyRate.negiable")
              }
              isHideComission={
                !_.get(trending, "deal.streamCommission.negiable")
              }
            />
            <h2>{_.get(trending, "job.product.name")}</h2>
            <div className="avatar">
              <img
                src={getMediaUrl(_.get(trending, "companyLogo"))}
                alt="trending-avatar"
              ></img>
              <span>{_.get(trending, "companyName")}</span>
            </div>
            <div className="infor">
              <span>
                <div>
                  <IconClock />
                  Ending in
                </div>
                <div>
                  {getDiffDate(
                    _.get(trending, "job.campaign.timeEnd"),
                    _.get(trending, "job.campaign.timeStart")
                  )}
                </div>
              </span>
              <span>
                <div>
                  <IconMoney width={15} height={15} />
                  Fixed Rate
                </div>
                <div>
                  {"$" +
                    _.get(trending, "deal.streamHourlyRateRange.start", 0) +
                    " - " +
                    "$" +
                    _.get(trending, "deal.streamHourlyRateRange.end", 0)}
                </div>
              </span>
              <span>
                <div>
                  <IconUsers width={15} height={15} />
                  Pitch
                </div>
                <div className="avatar-group">
                  <Avatar.Group>
                    {_.get(trending, "host") &&
                      _.get(trending, "host").map((item) => {
                        <Avatar src={getMediaUrl(_.get(item, "media"))} />;
                      })}
                  </Avatar.Group>
                  <span>{_.get(trending, "host.length")}</span>
                </div>
              </span>
            </div>
            <div className="action">
              <Button
                onClick={() => {
                  const payload = {};
                  payload.dealData = _.get(trending, "deal");
                  payload.data = {
                    campaign: _.get(trending, "job.campaign"),
                  };

                  setOpen(true);
                  dispatch({
                    type: type.SET_CAMPAIGN,
                    payload: payload,
                  });
                }}
                type="primary"
                size="large"
              >
                <IconPen />
                Pitch for this product
              </Button>
              <Button
                onClick={() => {
                  history.push(
                    `/detail-campaign/${_.get(trending, "job.code")}`
                  );
                  dispatch({
                    type: type.SET_SELECTED_PRODUCT,
                    payload: _.get(trending, "job.product"),
                  });
                }}
                type="primary"
                size="large"
              >
                <IconInfor width={20} height={20} />
                View Details
              </Button>
            </div>
          </div>
        </div>
      </div>
      <DrawerBidJob
        isSendProposal={true}
        jobCode={_.get(trending, "job.code")}
        visible={open}
        handleClose={() => setOpen(false)}
      />
    </>
  );
};

const mapDispatchToProps = {
  getProductTrending,
};

const mapStateToProps = ({ marketplace }) => ({
  trending: marketplace.productTrending || {},
});

export default connect(mapStateToProps, mapDispatchToProps)(Trending);
