import TagMarket from "views/components/TagMarket";
import { Link, useHistory } from "react-router-dom";
import { constants } from "helpers";
import { connect, useDispatch } from 'react-redux';
import { getOpenBidList } from '../actions'
import { useEffect } from "react";
import _ from "lodash";
import moment from "moment"
import { utils, ReactUtils } from 'helpers'
import * as type from "../actionTypes"

const { getMediaUrl } = utils


const { router } = constants;

const OpenBid = ({ getOpenBidList, campaigns }) => {
    const dispatch = useDispatch()
    const history = useHistory()
    useEffect(async () => {
        await getOpenBidList({ page: 0, per_page: 3 })
    }, [])

    const getDiffDate = (now, then) => {
        return `${moment(now).diff(moment(then), 'days')} days`
    }

    return (
        <div className="market-bid">
            <div className="market-bid__header">
                <h2>Opening Pitch</h2>
                <Link to={router.campaigns}>View all campaigns</Link>
            </div>
            <div className="market-bid__product">
                {
                    campaigns && campaigns.map((item, index) => (
                        <div onClick={() => {
                            history.push(`/detail-campaign/${_.get(item, 'jobCode')}`)
                            dispatch({
                                type: type.SET_SELECTED_PRODUCT,
                                payload: _.get(item, 'product')
                            })
                        }} className="market-bid__product-item" style={{ backgroundImage: `url(${getMediaUrl(_.get(item, 'product.media[0].url'))})` }} key={index}>
                            <div className="market-bid__product-item__header">
                                <div className="header__avatar">
                                    <span>
                                        <img src={getMediaUrl(_.get(item, 'company.logo'))} alt="avatar" />
                                    </span>
                                    <span>{_.get(item, 'company.name')}</span>
                                </div>
                                {_.get(item, 'submit') && <div className="header__tag">Bid Submitted</div>}
                            </div>
                            <div className="market-bid__product-item__content">
                                <TagMarket isHideFixedRate={_.get(item, 'hourlyRateRange.negiable')}
                                    isHideComission={!_.get(item, 'commission.negiable')} />
                                <div className="title">
                                    {_.get(item, 'product.name')}
                                </div>
                                <div className="infor">
                                    <div>
                                        <div className="infor-title">Ending in</div>
                                        <div className="infor-content">{getDiffDate(_.get(item, 'campaign.timeEnd'), _.get(item, 'campaign.timeStart'))}</div>
                                    </div>
                                    <div>
                                        <div className="infor-title">Fixed Rate</div>
                                        <div className="infor-content">{'$' + (_.get(item, 'hourlyRateRange.start') || 0) + ' - ' + '$' + (_.get(item, 'hourlyRateRange.end') || 0)}</div>
                                    </div>
                                    <div>
                                        <div className="infor-title">Pitch</div>
                                        <div className="infor-content">{_.get(item, 'big')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
            </div>
        </div>
    )
};

const mapDispatchToProps = {
    getOpenBidList
};

const mapStateToProps = ({ marketplace }) => ({
    campaigns: _.get(marketplace, 'openBid.data') || []
});

export default connect(mapStateToProps, mapDispatchToProps)(OpenBid);