import { constants, client, utils } from 'helpers';
import config from 'config';

import * as types from './actionTypes';

const { api } = constants;
const { API_END_POINT } = config;

export const getOpenBidList = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getOpenBid,
            method: "get",
            params: params,
        });
        if (data.data) {
            dispatch({
                type: types.SET_OPEN_BID,
                payload: data,
            });
        }
    } catch (err) {
        console.log("[getOpenBidList]", err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const getFeaturedProduct = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getFeaturedProduct,
            method: "get",
            params: params,
        });
        if (data.data) {
            dispatch({
                type: types.SET_FEATURED_PRODUCT,
                payload: data,
            });
        }
    } catch (err) {
        console.log("[getFeaturedProduct]", err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const getProductTrending = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });

        const { data } = await client.request({
            path: api.path.getProductTrending,
            method: "get",
        });
        if (data.data) {
            dispatch({
                type: types.SET_PRODUCT_TRENDING,
                payload: data.data,
            });
        }
    } catch (err) {
        console.log("[getProductTrending]", err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const getListDeal = (code) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });

        const { data } = await client.request({
            path: api.path.getListDeal(code),
            method: "get",
        });
        dispatch({
            type: types.SET_LIST_DEAL,
            payload: data,
        });

    } catch (err) {
        console.log("[getListDeal]", err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const getCampaignCardByProductAndSeller = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getCampaignCardByProductAndSeller(params.jobCode, params.productCode),
            method: "get",
        });
        dispatch({
            type: types.SET_CAMPAIGN,
            payload: data,
        });
    } catch (err) {
        console.log("[getCampaignCardByProductAndSeller]", err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const submitDeal = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });

        const { data } = await client.request({
            path: api.path.submitDeal,
            method: "post",
            data: params
        });
        return data
    } catch (err) {
        console.log("[submitDeal]", err);
        dispatch({ type: types.SET_SUBMIT_DEAL_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const completeJob = (params) => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });

        const { data } = await client.request({
            path: api.path.completeJob,
            method: "post",
            data: params
        });
    } catch (err) {
        console.log("[submitDeal]", err);
        dispatch({ type: types.SET_COMPLETE_JOB_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};