import Page from "views/components/Page";
import { OpenBid, Trending, FeaturedProduct } from "./parts";
import { constants } from "helpers";
import { connect } from "react-redux";
const { userRoles } = constants;

const MarketPlace = ({ isUpdating }) => {
  return (
    <Page
      loadingPage={isUpdating}
      helmet="Market Place Page"
      alignTop
      selectedKeys={["market-place"]}
      requiredAccess={[userRoles.HOST]}
    >
      <div className="market">
        <Trending />
        <OpenBid />
        <FeaturedProduct />
      </div>
    </Page>
  );
};

const mapStateToProps = ({ marketplace }) => ({
  isUpdating: marketplace.isUpdating,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MarketPlace);
