import * as types from './actionTypes';

const initialState = {
  isUpdating: false,
  errorMsg: '',
  productTrending: {},
  openBid: [],
  featuredProduct: [],
  submitDealErr: '',
  completeJobErr: '',
  listDeal: [],
  campaign: {},
  selectedProduct: {},
};

const companyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_LOADING:
      return { ...state, isUpdating: action.payload };
    case types.SET_ERROR:
      return { ...state, errorMsg: action.payload };
    case types.SET_OPEN_BID:
      return { ...state, openBid: action.payload };
    case types.SET_PRODUCT_TRENDING:
      return { ...state, productTrending: action.payload };
    case types.SET_SUBMIT_DEAL_ERROR:
      return { ...state, submitDealErr: action.payload };
    case types.SET_COMPLETE_JOB_ERROR:
      return { ...state, completeJobErr: action.payload };
    case types.SET_LIST_DEAL:
      return { ...state, listDeal: action.payload };
    case types.SET_CAMPAIGN:
      return { ...state, campaign: action.payload };
    case types.SET_FEATURED_PRODUCT:
      return { ...state, featuredProduct: action.payload };
    case types.SET_SELECTED_PRODUCT:
      return { ...state, selectedProduct: action.payload };
    default:
      return state;
  }
};

export default companyReducer;
