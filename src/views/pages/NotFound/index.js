import { Link } from "react-router-dom";
import Page from "views/components/Page";

import { Result, Button } from "antd";

const NotFound = () => (
  <Page className="centered page not-found-page">
    <div className="page-content">
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link to="/">
            <Button type="primary">Back Home</Button>
          </Link>
        }
      />
    </div>
  </Page>
);

export default NotFound;
