import React, { useRef, useEffect, useState } from "react";
import { connect } from 'react-redux';
import Page from "views/components/Page";
import { Row, Col, Steps, Button, message, Form } from 'antd';
import LogoFull from "views/components/Logo/LogoFull";
import OverviewStep from "views/components/OverviewStep";
import PersonalInfo from "views/components/PersonalInfo";
import PhotosVideos from "views/components/PhotoVideos";
import Connections from "views/components/Connections";
import Topics from "views/components/Topics";
import { IconArrowRightWhite } from 'views/components/Icon/basic'
import { IconArrowLeft } from 'views/components/Icon/pages'
import { updateHostProfile, uploadMedia, addHostProfileMedia, removeHostProfileMedia, deleteMedia } from 'views/pages/HostProfile/actions';
import { getTags } from 'views/pages/Host/actions';
import { ReactUtils, router } from 'helpers';

const { Step } = Steps;
const fallbackDelay = 500;

const FlowSignUp = (props) => {
  const {
    history,
    email,
    host,
    updateHostProfile,
    getTags,
    tags,
    tagsById,
    addHostProfileMedia,
    removeHostProfileMedia,
    deleteMedia,
    isUpdating
  } = props;

  useEffect(() => {
    getTags();
  }, [getTags])

  const localStep = parseInt(localStorage.getItem('step')) || 1;
  const [current, setCurrent] = useState((host ? host.step : localStep) - 1);
  const [profileUpdating, setProfileUpdating] = useState(false);

  useEffect(() => {
    if (host?.step < 3) {
      setCurrent(host.step - 1)
    }
  }, host)

  useEffect(() => {
    if (isUpdating) {
      setProfileUpdating(true);
      setTimeout(function () {
        setProfileUpdating(false);
      }, fallbackDelay);
    }
  }, isUpdating)

  const next = async () => {
    if (current === 4) {
      const profile = await updateHostProfile({ finishedOnboarding: true });
      if (profile.completePercentage >= 100) {
        ReactUtils.messageSuccess({ content: 'Update profile sucessfully' });
      }
      history.replace(router.home);
      return;
    }
    setCurrent(current + 1);
  };

  const stepRefs = [
    useRef(),
    useRef(),
    useRef(),
    useRef(),
    useRef()
  ]

  const stepProps = {
    updateHostProfile,
    uploadMedia,
    email,
    host,
    tags,
    tagsById,
    addHostProfileMedia,
    removeHostProfileMedia,
    deleteMedia,
    next
  };

  const steps = [
    {
      title: '1',
      content: <OverviewStep ref={stepRefs[0]} {...stepProps} />,
    },
    {
      title: '2',
      content: <PersonalInfo ref={stepRefs[1]} {...stepProps} />,
    },
    {
      title: '3',
      content: <PhotosVideos ref={stepRefs[2]} {...stepProps} />,
    },
    {
      title: '4',
      content: <Topics ref={stepRefs[3]} {...stepProps} />,
    },
    {
      title: '5',
      content: <Connections ref={stepRefs[4]} {...stepProps} />,
    },
  ];

  const onContinue = () => {
    const ref = stepRefs[current].current;
    if (ref && ref.onContinue) {
      ref.onContinue();
    }
  }

  const prev = () => {
    setCurrent(current - 1);
  };
  const customDot = (dot, { status, index }) => (
    <div className='customDot'></div>
  );

  if (!host && !localStorage.getItem('step')) {
    return (
      <Page helmet="Flow Sign Up" layout="split" />
    )
  }

  return (
    <Page helmet="Flow Sign Up" layout="split" loadingPage={isUpdating || profileUpdating}>
      <div className="primary-section w80 flow-signup">
        <div className="header w700">
          <LogoFull />
        </div>
        <div className="content w700 jc-fs">
          <div className="auth-form">
            <Steps size="small" current={current + 1} progressDot={customDot} style={{ margin: '55px 0 36px' }}>
              {Array(steps.length + 1).fill().map(item => (
                <Step />
              ))}
            </Steps>
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action">
              {current < steps.length - 1 && current === 0 ? (
                <Row style={{ width: '100%' }}>
                  <Col span={8}></Col>
                  <Col span={14}>
                    <Form layout="vertical" style={{ display: 'flex' }}>
                      <Form.Item>
                        <Button type="primary" onClick={onContinue} size="large" className="cta continue" block>
                          Continue <IconArrowRightWhite />
                        </Button>
                      </Form.Item>
                    </Form>
                  </Col>
                </Row>
              ) : (
                current === 4 ? (
                  <Button onClick={onContinue} size="large" className="cta control next" block>
                    Finish <IconArrowRightWhite />
                  </Button>
                ) : (
                  <Form.Item>
                    <Button type="primary" onClick={onContinue} size="large" className="cta control next" block>
                      Next <IconArrowRightWhite />
                    </Button>
                  </Form.Item>
                )
              )}
              {current >= 2 && (
                <Button size="large" className="cta control skip" block onClick={next}>
                  Skip
                </Button>
              )}
              {current > 0 && (
                <Button type="primary" onClick={() => prev()} size="large" className="cta control prev" block>
                  <IconArrowLeft /> Back
                </Button>
              )}
            </div>
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section w20"></div>
    </Page>
  )
}

const mapStateToProps = ({ common, hostProfile, hostList }) => ({
  email: common.user.email,
  host: hostProfile.data,
  tags: hostList.tags,
  tagsById: hostList.tagsById,
  isUpdating: hostProfile.isUpdating
});

const mapDispatchToProps = {
  updateHostProfile,
  getTags,
  addHostProfileMedia,
  removeHostProfileMedia,
  deleteMedia
};

export default connect(mapStateToProps, mapDispatchToProps)(FlowSignUp);