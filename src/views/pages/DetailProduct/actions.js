import { constants, client } from 'helpers';

import * as types from './actionTypes';
import {
  IS_FETCH_SELLERS_CAMPAIGN,
  FETCH_SELLERS_CAMPAIGN_SUCCESS,
  FETCH_SELLERS_CAMPAIGN_FAILED,
} from './components/actionTypes';

const { api } = constants;
export const getDetailProduct =
  ({ code }) =>
  async (dispatch) => {
    try {
      dispatch({ type: types.IS_FETCH_PRODUCT, payload: true });

      const { data } = await client.request({
        path: api.path.detailProduct(code),
        method: 'get',
      });
      if (data.data) {
        dispatch({ type: types.FETCH_PRODUCT_SUCCESS, payload: data.data });
      }
    } catch (err) {
      console.log('[getProductDetail]', err);
      dispatch({ type: types.FETCH_PRODUCT_FAILED, payload: err.message });
    } finally {
      dispatch({ type: types.IS_FETCH_PRODUCT, payload: false });
    }
  };

export const getRecommendedSellers = () => async (dispatch, getState) => {
  try {
    const detailProduct = getState().detailProduct;
    const { code: productCode } =
      (detailProduct && detailProduct.currentProduct) || {};
    if (!productCode) return;

    dispatch({ type: types.IS_FETCH_RECOMMENDED_SELLERS, payload: true });

    const { data } = await client.request({
      path: api.path.recommendedSellers(productCode),
      method: 'get',
    });
    if (data.data) {
      dispatch({
        type: types.FETCH_RECOMMENDED_SELLERS_SUCCESS,
        payload: data.data,
      });
    }
  } catch (err) {
    console.log('[getRecommendedSellers]', err);
    dispatch({
      type: types.FETCH_RECOMMENDED_SELLERS_FAILED,
      payload: err.message,
    });
  } finally {
    dispatch({ type: types.IS_FETCH_RECOMMENDED_SELLERS, payload: false });
  }
};

export const deleteProduct = async (code) => {
  try {
    const path = api.path.deleteProduct(code);
    const { data } = await client.request({
      path: path,
      method: 'delete',
    });
    return data;
  } catch (err) {
    console.error('[deleteProduct]', err);
  }
};

export const addOrUpdateCampaign =
  (isEdit, payload) => async (dispatch, getState) => {
    try {
      const { code: productCode } = getState().detailProduct.currentProduct;
      // dispatch({ type: types.IS_UPDATE_PROFILE, payload: true });
      const { deal, ...campaign } = payload;
      const [timeStart, timeEnd] = campaign.campaign.time;
      campaign.campaign.timeStart = timeStart;
      campaign.campaign.timeEnd = timeEnd;

      const [discountPeriodStart, discountPeriodEnd] =
        campaign?.discountPeriod || [];
      campaign.discountPeriodStart = discountPeriodStart;
      campaign.discountPeriodEnd = discountPeriodEnd;

      campaign.productCode = productCode;
      if (isEdit) {
        const { code: jobCode } = getState().campaign.campaignDetail;
        if (!campaign.discountUnit) {
          campaign.discountUnit = "amount";
        }
        const { data } = await client.request({
          path: api.path.updateCampaign(jobCode),
          method: 'patch',
          data: campaign,
        });
        // deal.streamHourlyRateRange = {
        //   start: 20,
        //   end: 30,
        //   unit: "USD",
        //   negative: false,
        // };
        // deal.streamHourlyRate = {
        //   negiable: true,
        //   description: "abc",
        // };
        const { data: data1 } = await client.request({
          path: api.path.updateDealCampaign(jobCode),
          method: 'patch',
          data: deal,
        });
        if (!data1) {
          return false;
        }
      } else {
        if (!campaign.discountUnit) {
          campaign.discountUnit = "amount";
        }
        const { data } = await client.request({
          path: api.path.createCampaign,
          method: 'post',
          data: campaign,
        });
        if (!data.jobCode) {
          return false;
        }
        deal.jobCode = data.jobCode;
        if (!deal.streamHourlyRateRange.negative) {
          deal.streamHourlyRateRange.negative = false;
          deal.streamHourlyRateRange.unit = "USD";
        }
        // deal.streamHourlyRateRange = {
        //   start: 20,
        //   end: 30,
        //   unit: 'USD',
        //   negative: false,
        // };
        // deal.streamHourlyRate = {
        //   negiable: true,
        //   description: 'abc',
        // };
        const { data: data1 } = await client.request({
          path: api.path.createDeal,
          method: 'post',
          data: deal,
        });
        if (!data1) {
          return false;
        }
      }

      return true;

      // dispatch({ type: types.SET_ERROR, payload: "Campaign not exist" });
    } catch (err) {
      console.log('[addOrUpdateCampaign]', err);
      // dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
      // dispatch({ type: types.IS_UPDATE_PROFILE, payload: false });
    }
  };

export const selectedTab = (tab) => async (dispatch) => {
  dispatch({ type: types.SELECT_TAB, payload: tab });
};

export const countSellersOfProduct = async (productCode) => {
  try {
    const { data = {} } = await client.request({
      path: api.path.sellersOfProductCount(productCode),
      method: 'get',
    });

    const value = data.data || {};
    const all = Object.keys(value).reduce((total, status) => {
      return value[status] + total;
    }, 0);
    value.all = all;
    return value;
  } catch (err) {
    console.error('[countSellersOfProduct]', err);
  }
};

export const getSellersOfProduct =
  (productCode, paging = {}, status) =>
  async (dispatch) => {
    try {
      dispatch({ type: IS_FETCH_SELLERS_CAMPAIGN, payload: true });
      const query = { ...paging };
      if (status) query.status = status;

      const search = new URLSearchParams(query);
      const { data } = await client.request({
        path: api.path.sellersOfProduct(productCode, search.toString()),
        method: 'get',
      });

      // const fake = ["opens",
      //   "deal",
      //   "proposal",
      //   "accepted",
      //   "rejected",
      //   "verified",].map(status => {
      //     const seller = Object.assign({}, data.data[0]);
      //     seller.status = status;
      //     return seller;
      //   });
      // const fake2 = ["complete",
      //   "verify",
      //   "execute",
      // ].map(status => {
      //   const seller = Object.assign({}, data.data[0]);
      //   seller.status = 'complete';
      //   seller.jobStatus = status;
      //   return seller;
      // });
      // data.data = data.data.concat(fake).concat(fake2);
      if (data) {
        dispatch({
          type: FETCH_SELLERS_CAMPAIGN_SUCCESS,
          payload: data,
        });
      }
    } catch (err) {
      console.error('[getSellersOfProduct]', err);
      dispatch({
        type: FETCH_SELLERS_CAMPAIGN_FAILED,
        payload: err.message,
      });
    } finally {
      dispatch({ type: IS_FETCH_SELLERS_CAMPAIGN, payload: false });
    }
  };
