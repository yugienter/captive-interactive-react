import { IconInfor, IconPhoto, IconMoney, IconAudience, IconVariant } from 'views/components/Icon/product';
import { connect } from 'react-redux'
import moment from 'moment';
import InputCategories from 'views/components/InputCategories';
import { utils } from 'helpers/utils'
import { useEffect, useState } from 'react';

const { getMediaUrl, handleFormatInput, handleFormatRightSide } = utils

const InfoProduct = ({ currentProduct, isLoading, product }) => {
    const [data, setData] = useState()
    useEffect(() => {
        if (product) {
            setData(product)
        }
        else {
            setData(currentProduct)
        }
    }, [product, currentProduct])
    return (
        !isLoading ? (
            <div className="product-detail">
                <div className="product-detail__general-information">
                    <h2><span ><IconInfor width={20} height={20} /></span>General Information</h2>
                    <table>
                        <tbody>
                            <tr>
                                <td>Brand Name</td>
                                <td>{data && data.brandName}</td>
                            </tr>
                            <tr>
                                <td>Product Story</td>
                                <td>{data && data.story}</td>
                            </tr>
                            <tr>
                                <td>Manufacturer</td>
                                <td>{data && data.manufacturerCountry}</td>
                            </tr>
                            <tr>
                                <td>USP of Product</td>
                                <td>{data && data.USPProduct ? data.USPProduct : 'Not Specific'}</td>
                            </tr>
                            <tr>
                                <td>Product Benefits</td>
                                <td>{data && data.productBenefits}</td>
                            </tr>
                            <tr>
                                <td>Categories</td>
                                <td className="flex">{data && data.category && <InputCategories data={data.category} detail />}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div className="product-detail__media">
                    <h2 ><span ><IconPhoto width={20} height={20} /></span>Product Photos and Videos</h2>
                    <div className="media">
                        {
                            data && data.media && data.media.length > 0 && data.media.map((item, index) => (
                                <img src={getMediaUrl(item.url)} alt="product"></img>
                            ))
                        }
                    </div>
                </div>
                <div className="product-detail__product-price">
                    <h2 ><span ><IconMoney width={20} height={20} /></span>Product Price</h2>
                    <table>
                        <tbody>
                            <tr>
                                <td>Public RRP</td>
                                <td>{data && data.originalPrice ? `$${handleFormatRightSide(handleFormatInput(String(data.originalPrice), 2), 2)}` : 'Not Specific'}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="product-detail__variant-attributes">
                    <h2 ><span ><IconVariant width={20} height={20} /></span>Variant Attributes</h2>
                    <table>
                        <tbody>
                            <tr>
                                <td>Weight</td>
                                <td>{data && data.weight ? `${handleFormatRightSide(handleFormatInput(String(data.weight), 1), 1)} ${data.weightUnit}` : 'Not Specific'}</td>
                            </tr>
                            <tr>
                                <td>Dimension (W x D x H)</td>
                                <td>{data && data.dimension ? `${data.dimension} cm` : 'Not Specific'}</td>
                            </tr>
                            <tr>
                                <td>Expiry Date</td>
                                <td>{data && data.expiryDate ? moment(data.expiryDate).format('DD/MM/YYYY') : 'Not Specific'}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="product-detail__variant-attributes">
                    <h2 ><span ><IconAudience width={20} height={20} /></span>Target Audiences</h2>
                    <table>
                        <tbody>
                            <tr>
                                <td>Age Range</td>
                                <td>{data && data.ageMax && data.ageMin ? `${data.ageMin} - ${data.ageMax}` : 'Not Specific'}</td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td>{data && data.gender && data.gender.join(', ')}</td>
                            </tr>
                            <tr>
                                <td>Career</td>
                                <td>{data && data.occupation ? data.occupation : 'Not Specific'}</td>
                            </tr>
                            <tr>
                                <td>Insights</td>
                                <td>{data && data.insights ? data.insights : 'Not Specific'}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        ) : (<div>Loading...</div>)
    )
}
const mapStateToProps = ({ detailProduct: { currentProduct, isLoading } }) => ({
    currentProduct: currentProduct,
    isLoading

});
export default connect(mapStateToProps, {})(InfoProduct)