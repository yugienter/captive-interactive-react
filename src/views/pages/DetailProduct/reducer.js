import * as types from "./actionTypes";

const initialState = {
  isLoading: false,
  error: null,
  currentProduct: {},
  recommendedSellers: [],
  getRecommendedSellersError: "",
  currentTab: "1",
};

const createDetailReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.IS_FETCH_PRODUCT:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_PRODUCT_SUCCESS:
      const ids = [];
      const names = [];
      payload.categories.forEach((item) => {
        ids.push(item.code);
        names.push(item.name);
      });
      return {
        ...state,
        currentProduct: { ...payload, category: { ids, names } },
      };
    case types.FETCH_PRODUCT_FAILED:
      return {
        ...state,
        error: payload,
      };
    case types.IS_FETCH_RECOMMENDED_SELLERS:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_RECOMMENDED_SELLERS_SUCCESS:
      return {
        ...state,
        recommendedSellers: payload,
      };
    case types.FETCH_RECOMMENDED_SELLERS_FAILED:
      return {
        ...state,
        getRecommendedSellersError: payload,
      };
    case types.SELECT_TAB:
      return {
        ...state,
        currentTab: payload,
      };
    default:
      return state;
  }
};

export default createDetailReducer;
