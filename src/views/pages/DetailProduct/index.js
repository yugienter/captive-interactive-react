import { useState, useMemo } from "react";
import Page from "views/components/Page";
import { Link } from "react-router-dom";
import { Tabs, Button, Progress, Avatar, Menu, Dropdown, message } from "antd";
import { InfoProduct } from "./part";
import {
  IconBack,
  IconEdit,
  IconMore,
  IconDrop,
  IconCreateCampaign,
  IconDeleteProduct,
  IconRecommendSeller,
} from "views/components/Icon/product";
import { IconLoading, IconCampain } from "views/components/Icon/pages";
import { useHistory, useParams } from "react-router";
import { router } from "helpers";
import { getDetailProduct, selectedTab, deleteProduct } from "./actions";
import { constants } from "helpers";
import { useEffect } from "react";
import { connect } from "react-redux";
import CreateCampaign from "./components/CreateCampaign";
import ContentCampaignTab from "./components/ContentCampaignTab";
import TableSellers from "views/components/TableSellers";
import {
  IconPricing,
  IconStream,
  IconClock,
} from "views/components/Icon/myjobs";

import { getDetailCampaignForRightProduct } from "./components/actions";
import ProductCampaignSummary from "./components/ProductCampaignSummary";
import DeleteModal from "views/pages/DetailProduct/components/DeleteModal";

const { userRoles } = constants;
const { TabPane } = Tabs;

const DetailProduct = ({
  data,
  getDetailProduct,
  selectedTab,
  currentTab,
  getDetailCampaignForRightProduct,
  currentCampaign,
}) => {
  const { code } = useParams();
  const history = useHistory();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [showDeleteProduct, setShowDeleteProduct] = useState(false);

  useEffect(() => {
    getDetailProduct({ code });
  }, [code, getDetailProduct]);

  useEffect(() => {
    if (data && data.code) {
      getDetailCampaignForRightProduct();
    }
  }, [data]);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const changeTab = (value) => {
    selectedTab(value);
  };

  const showCampaignDetailOrCreateButton = () => {
    return hasCurrentCampaign ? (
      <ProductCampaignSummary campaign={currentCampaign} />
    ) : (
      <div className="block-link-recommended">
        Select for next step:
        <Button onClick={showModal} className="link-recommended-seller">
          <IconCampain />
          Create Campaign
        </Button>
        <Link to={router.recommenderSeller} className="link-recommended-seller">
          <IconLoading />
          Recommended Sellers
        </Link>
      </div>
    );
  };

  const hasCurrentCampaign = useMemo(
    () => currentCampaign && currentCampaign.data && [currentCampaign]
  );

  const goToRecommendSeller = () => {
    history.push(router.recommenderSeller);
  };

  const deleteProductAction = () => {
    setShowDeleteProduct(true);
  };

  const handleDeleteProduct = async () => {
    const result = await deleteProduct(code);
    setShowDeleteProduct(false);
    if (result && !result.error) {
      message.success("Delete product success");
      history.push(router.products);
      return;
    }
    message.error("Delete product error");
  };

  const renderMoreActions = () => {
    const overlay = (
      <Menu>
        <div className="popup-row-actions">
          <Menu.Item key="0" onClick={showModal}>
            <IconCreateCampaign />
            &nbsp;Create Campaign
          </Menu.Item>
          <Menu.Item key="1" onClick={goToRecommendSeller}>
            <IconRecommendSeller />
            &nbsp;Recommended Sellers
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="2" className="text-red" onClick={deleteProductAction}>
            <IconDeleteProduct />
            &nbsp;Delete Product
          </Menu.Item>
        </div>
      </Menu>
    );
    return (
      <Dropdown overlay={overlay} placement="bottomCenter" trigger={["click"]}>
        <span className="clickable">
          <IconMore />
          More
          <IconDrop />
        </span>
      </Dropdown>
    );
  };

  return (
    <Page helmet="Detail Product" alignTop requiredAccess={[userRoles.COMPANY]}>
      <div className="detail-product">
        <div className="detail-product__header">
          <div>
            <span onClick={() => history.goBack()}>
              <IconBack />
            </span>
            <h2>{data.name && data.name}</h2>
          </div>
          <div className="detail-product__header__button">
            <Button>{renderMoreActions()}</Button>
            <Button
              onClick={() =>
                history.push({
                  pathname: router.editProduct.split(":")[0].concat(code),
                })
              }
            >
              <IconEdit />
              Edit
            </Button>
          </div>
        </div>
        <Tabs defaultActiveKey={currentTab} onChange={changeTab}>
          <TabPane tab="Product Information" key="1">
            <div className="product-info">
              <InfoProduct />
              {showCampaignDetailOrCreateButton()}
            </div>
          </TabPane>
          <TabPane tab="Campaigns" key="2">
            <ContentCampaignTab />
          </TabPane>
          <TabPane tab="Sellers" key="3" style={{ padding: "0 35px" }}>
            <TableSellers />
          </TabPane>
        </Tabs>
      </div>
      {isModalVisible && (
        <CreateCampaign
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        />
      )}
      <DeleteModal
        title="Delete Product"
        visible={showDeleteProduct}
        handleOk={handleDeleteProduct}
        handleClose={() => setShowDeleteProduct(false)}
      >
        <p>
          Are you sure you want to delete this product?
          <br />
          You can’t undo this action.
        </p>
      </DeleteModal>
    </Page>
  );
};
const mapStateToProps = ({
  detailProduct: { currentProduct, isLoading, currentTab },
  campaign: { currentCampaignForProduct: currentCampaign },
}) => ({
  data: currentProduct,
  isLoading,
  currentTab,
  currentCampaign,
});
const mapDispatchToProps = {
  getDetailProduct,
  selectedTab,
  getDetailCampaignForRightProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(DetailProduct);
