import { Button, Col, Modal, Row } from "antd";
import "./style.less";
import { IconModalDeleteProduct } from "views/components/Icon/product";

const DeleteModal = ({ title, children, handleOk, handleClose, visible }) => {
  return (
    <Modal
      centered
      visible={visible}
      footer={false}
      closable={false}
      className="modal-delete-product"
    >
      <IconModalDeleteProduct />
      <h4>{title}</h4>
      {children}
      <br />
      <Row>
        <Col span={12}>
          <Button onClick={handleClose} primary>
            Cancel
          </Button>
        </Col>
        <Col span={12}>
          <Button onClick={handleOk} type="primary" danger>
            Delete
          </Button>
        </Col>
      </Row>
    </Modal>
  );
};

export default DeleteModal;
