import { Avatar, Button, Progress, Tag } from "antd";
import { Link } from "react-router-dom";
import { router } from "helpers";
import moment from "moment";
import { constants, utils } from "helpers";
import { ICampaignDetailResponse, IListHost } from "types/campaign";
import {
  IconClock,
  IconPricing,
  IconStream,
} from "views/components/Icon/myjobs";
import "styles/product-campaign-summary.less";
import { IconCampain, IconLoading } from "views/components/Icon/pages";
import { useHistory } from "react-router-dom";

const { dateFormat } = constants;
interface IProps {
  campaign: ICampaignDetailResponse;
}
const ProductCampaignSummary = ({ campaign }: IProps) => {
  const history = useHistory();
  const { data, dealData, listHost, countHost } = campaign;
  const viewCampaign = () => {
    history.push({
      pathname: router.viewCampaign.split(":")[0].concat(data.code),
    });
  };
  return (
    <div>
      <div
        className="drawer-submit-proposal__result"
        style={{ marginBottom: "15px" }}
      >
        <div className="drawer-submit-proposal__result__tags">
        {campaign?.dealData?.streamHourlyRateRange?.start && <Tag color="#55acee">Fixed Rate</Tag>}
        {campaign?.dealData?.streamCommission?.description && <Tag color="#FF9A4D">Commission</Tag>}
        </div>
        <div className="drawer-submit-proposal__result__title">
          <p>{data.campaign.name}</p>
          <p>{data.campaign.description}</p>
        </div>
        <div className="drawer-submit-proposal__result__duration">
          <p className="green">
            <IconClock />{" "}
            {data.campaign &&
              moment(data.campaign.timeStart).format(dateFormat)}{" "}
            -{" "}
            {data.campaign && moment(data.campaign.timeEnd).format(dateFormat)}
          </p>
          <Progress
            className="progress-green"
            strokeColor="#fff"
            percent={0}
            showInfo={false}
          />
        </div>
        <div className="drawer-submit-proposal__result__deal">
          {campaign?.dealData?.streamHourlyRateRange?.start && (<p className="fixed-price-text">
            ${campaign.dealData.streamHourlyRateRange.start}-
            ${campaign.dealData.streamHourlyRateRange.end}/hr
          </p>)}
          {dealData &&
            dealData.streamCommission &&
            dealData.streamCommission.negiable && (
              <p>
                Commission: <strong>Negotiable</strong>
              </p>
            )}
          {dealData &&
            dealData.streamCommissionUnit === "%" && (
              <p>
                Commission: <strong>{dealData.streamCommission.description}%</strong>
              </p>
            )}
        </div>

        <div className="avatar-group">
          <Avatar.Group>
            {listHost?.map((h: IListHost, index: number) => (
              <Avatar src={utils.getMediaUrl(h.media)} key={index} />
            ))}
          </Avatar.Group>
          <div className="bids">
            {countHost} <span>Pitch</span>
          </div>
        </div>
        <div className="drawer-submit-proposal__result__table">
          <p>
            <IconPricing /> Product Price
          </p>
          <table>
            <tr>
              <td>Public RRP</td>
              <td>{data.publicRRP}</td>
            </tr>
            <tr>
              <td>Discount</td>
              <td>
                {data.discountPercentage}
                {data.discountPercentageUnit}
              </td>
            </tr>
            <tr>
              <td>Discounted Price</td>
              <td> {data.discountPrice}</td>
            </tr>
            <tr>
              <td>Discount Period</td>
              <td>
                {data.discountPeriodStart &&
                  moment(data.discountPeriodStart).format(dateFormat)}{" "}
                -{" "}
                {data.discountPeriodEnd &&
                  moment(data.discountPeriodEnd).format(dateFormat)}
              </td>
            </tr>
          </table>
        </div>
        <div className="drawer-submit-proposal__result__table">
          <p>
            <IconStream /> Stream Details
          </p>
          <table>
            <tr>
              <td>Time Suggestion</td>
              <td>
                {dealData &&
                  dealData.streamTime &&
                  dealData.streamTime.description &&
                  moment(dealData.streamTime.description).format(dateFormat)}
              </td>
            </tr>
            <tr>
              <td>Type of Stream</td>
              <td>{dealData?.streamType?.description}</td>
            </tr>
            <tr>
              <td>Elements</td>
              <td>{dealData?.streamElement?.description?.join(", ")}</td>
            </tr>
            <tr>
              <td>Platform</td>
              <td>{dealData?.streamPlatform?.description?.join(", ")}</td>
            </tr>
          </table>
        </div>
        <div className="drawer-submit-proposal__result__message">
          <p className="sub-title">Message</p>
          <p className="fontStyle13">{dealData?.note}</p>
        </div>
        <Button
          className="link-recommended-seller"
          onClick={() => viewCampaign()}
        >
          <IconCampain />
          View Campaign
        </Button>
      </div>
      <div className="block-link-recommended" style={{ height: "124px" }}>
        Can’t find any Sellers?
        <Link to={router.recommenderSeller} className="link-recommended-seller">
          <IconLoading />
          Recommended Sellers
        </Link>
      </div>
    </div>
  );
};

export default ProductCampaignSummary;
