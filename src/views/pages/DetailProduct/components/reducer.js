import * as types from "./actionTypes";

const initialState = {
  isLoading: false,
  error: null,
  campaigns: {
    data: [],
    total: 0,
    page: 0,
    perPage: 5,
    lastPage: 1,
  },
  campaignDetail: null,
  sellers: {
    data: [],
    total: 0,
    page: 0,
    perPage: 10,
    lastPage: 1,
  },
  sellerActivities: [],
  currentCampaignForProduct: null,
};

const createDetailReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.IS_FETCH_CAMPAIGNS:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_CAMPAIGNS_SUCCESS:
      return {
        ...state,
        campaigns: { ...payload },
      };
    case types.FETCH_CAMPAIGNS_FAILED:
      return {
        ...state,
        error: payload,
      };

    case types.IS_FETCH_CAMPAIGN:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_CAMPAIGN_SUCCESS:
      return {
        ...state,
        campaignDetail: { ...payload },
      };
    case types.FETCH_CAMPAIGN_FAILED:
      return {
        ...state,
        campaignDetail: null,
        error: payload,
      };

    case types.IS_FETCH_SELLERS_CAMPAIGN:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_SELLERS_CAMPAIGN_SUCCESS:
      return {
        ...state,
        sellers: { ...payload },
      };
    case types.FETCH_SELLERS_CAMPAIGN_FAILED:
      return {
        ...state,
        error: payload,
      };

    case types.IS_FETCH_SELLER_ACTIVITIES:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_SELLER_ACTIVITIES_SUCCESS:
      return {
        ...state,
        sellerActivities: payload,
      };
    case types.FETCH_SELLER_ACTIVITIES_FAILED:
      return {
        ...state,
        error: payload,
      };

    case types.FETCH_CAMPAIGN_DETAIL_FOR_PRODUCT_SUCCESS:
      return {
        ...state,
        currentCampaignForProduct: { ...payload },
      };
    case types.ADD_DEAL_SUCCESS:
      return {
        ...state,
        sellerActivities: [payload, ...state.sellerActivities],
      };

    default:
      return state;
  }
};

export default createDetailReducer;
