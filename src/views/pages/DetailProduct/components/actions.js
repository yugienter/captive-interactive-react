import { constants, client } from "helpers";
import _ from "lodash";
import * as types from "./actionTypes";
import { utils } from "helpers";
import moment from "moment";
import { getDetailProduct } from "../actions";
const { convertPeriodToArrayAndMoment } = utils;
const { CAMPAIGN_STATUS } = constants;

const { api } = constants;
export const getListCampaigns = ({ productCode, size, page }) => async (
  dispatch
) => {
  try {
    if (page > 0) {
      page = page - 1;
    }
    dispatch({ type: types.IS_FETCH_CAMPAIGNS, payload: true });

    const { data } = await client.request({
      path: api.path.fetchCampaignsByProductCode({
        productCode,
        size,
        page,
      }),
      method: "get",
    });
    // data.data = {
    //   data: [
    //     {
    //       code: "000001",
    //       campaign: {
    //         status: "notStarted",
    //         _id: "61a0d89b33670f3aa0382a24",
    //         name: "campaign name current",
    //         description: "campaign description current",
    //         timeStart: "2021-11-26T00:00:00.000Z",
    //         timeEnd: "2021-11-29T00:00:00.000Z",
    //       },
    //       bid: {
    //         count: 1,
    //       },
    //     },
    //     {
    //       code: "000001",
    //       campaign: {
    //         status: "deal",
    //         _id: "61a0d89b33670f3aa0382a24",
    //         name: "campaign name",
    //         description: "campaign description",
    //         timeStart: "2021-11-26T00:00:00.000Z",
    //         timeEnd: "2021-11-29T00:00:00.000Z",
    //       },
    //       bid: {
    //         count: 1,
    //       },
    //     },
    //     {
    //       code: "000001",
    //       campaign: {
    //         status: "deal",
    //         _id: "61a0d89b33670f3aa0382a24",
    //         name: "campaign name",
    //         description: "campaign description",
    //         timeStart: "2021-11-26T00:00:00.000Z",
    //         timeEnd: "2021-11-29T00:00:00.000Z",
    //       },
    //       bid: {
    //         count: 1,
    //       },
    //     },
    //     {
    //       code: "000001",
    //       campaign: {
    //         status: "deal",
    //         _id: "61a0d89b33670f3aa0382a24",
    //         name: "campaign name",
    //         description: "campaign description",
    //         timeStart: "2021-11-26T00:00:00.000Z",
    //         timeEnd: "2021-11-29T00:00:00.000Z",
    //       },
    //       bid: {
    //         count: 1,
    //       },
    //     },
    //   ],
    //   total: 3,
    //   page: 0,
    //   perPage: 5,
    //   lastPage: 1,
    // };
    if (data) {
      dispatch({ type: types.FETCH_CAMPAIGNS_SUCCESS, payload: data });
    }
  } catch (err) {
    console.log("[getListCampaigns]", err);
    dispatch({ type: types.FETCH_CAMPAIGNS_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_CAMPAIGNS, payload: false });
  }
};

export const getDetailCampaign = () => async (dispatch, getState) => {
  try {
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: true });
    const { code: productCode } = getState().detailProduct.currentProduct;
    const { data: campaignResponse } = await client.request({
      path: api.path.getDetailCampaign(productCode),
      method: "get",
    });

    if (campaignResponse.data) {
      const { code: jobCode } = campaignResponse.data;
      const { data: campaignAndDealResponse } = await client.request({
        path: api.path.getCampaignAndDeal(jobCode),
        method: "get",
      });
      const value = {
        ...campaignAndDealResponse.data,
      };
      // value.campaign.campaign.time = convertPeriodToArrayAndMoment(
      //   value.campaign.campaign.timeStart,
      //   value.campaign.campaign.timeEnd
      // );

      // value.campaign.discountPeriod = convertPeriodToArrayAndMoment(
      //   value.campaign.discountPeriodStart,
      //   value.campaign.discountPeriodEnd
      // );
      // if (value.deal && value.deal.streamTime) {
      //   value.deal.streamTime = {
      //     ...value.deal.streamTime,
      //     description: value.deal.streamTime.description
      //       ? moment(value.deal.streamTime.description)
      //       : null,
      //   };
      // }

      // if (value.deal && value.deal.streamCommission) {
      //   value.deal.streamCommission = {
      //     ...value.deal.streamCommission,
      //     description: value.deal.streamCommission.description
      //       ? Number(value.deal.streamCommission.description)
      //       : 0,
      //   };
      // }
      dispatch({
        type: types.FETCH_CAMPAIGN_SUCCESS,
        payload: value,
      });
    } else {
      dispatch({
        type: types.FETCH_CAMPAIGN_SUCCESS,
        payload: null,
      });
    }
  } catch (err) {
    console.log("[getDetailCampaign]", err);
    dispatch({ type: types.FETCH_CAMPAIGN_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: false });
  }
};

export const getCampaignDetailByJobCode = (jobCode) => async (
  dispatch,
  getState
) => {
  try {
    const { currentProduct } = getState().detailProduct;
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: true });
    const { data: campaignAndDealResponse } = await client.request({
      path: api.path.getCampaignAndDeal(jobCode),
      method: "get",
    });

    const value = {
      ...campaignAndDealResponse.data,
    };
    if (!currentProduct || !currentProduct.code) {
      getDetailProduct({ code: value.productCode })(dispatch);
    }

    value.campaign.time = convertPeriodToArrayAndMoment(
      value.campaign.timeStart,
      value.campaign.timeEnd
    );

    value.campaign.discountPeriod = convertPeriodToArrayAndMoment(
      value.campaign.discountPeriodStart,
      value.campaign.discountPeriodEnd
    );
    if (value.deal && value.deal.streamTime) {
      value.deal.streamTime = {
        ...value.deal.streamTime,
        description: value.deal.streamTime.description
          ? moment(value.deal.streamTime.description)
          : null,
      };
    }

    if (value.deal && value.deal.streamCommission) {
      value.deal.streamCommission = {
        ...value.deal.streamCommission,
        description: value.deal.streamCommission.description
          ? Number(value.deal.streamCommission.description)
          : 0,
      };
    }
    dispatch({
      type: types.FETCH_CAMPAIGN_SUCCESS,
      payload: value,
    });
  } catch (err) {
    console.log("[getCampaignDetailByJobCode]", err);
  } finally {
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: false });
  }
};

export const getSellersOfCampaign = (jobCode, paging = {}, status) => async (
  dispatch
) => {
  try {
    dispatch({ type: types.IS_FETCH_SELLERS_CAMPAIGN, payload: true });
    const query = { ...paging };
    if (status) query.status = status;

    const search = new URLSearchParams(query);
    const { data } = await client.request({
      path: api.path.sellersOfCampaign(jobCode, search.toString()),
      method: "get",
    });

    // const fake = ["opens",
    //   "deal",
    //   "proposal",
    //   "accepted",
    //   "rejected",
    //   "verified",].map(status => {
    //     const seller = Object.assign({}, data.data[0]);
    //     seller.status = status;
    //     return seller;
    //   });
    // const fake2 = ["complete",
    //   "verify",
    //   "execute",
    // ].map(status => {
    //   const seller = Object.assign({}, data.data[0]);
    //   seller.status = 'complete';
    //   seller.jobStatus = status;
    //   return seller;
    // });
    // data.data = data.data.concat(fake).concat(fake2);
    if (data) {
      dispatch({
        type: types.FETCH_SELLERS_CAMPAIGN_SUCCESS,
        payload: data,
      });
    }
  } catch (err) {
    console.error("[getSellersOfCampaign]", err);
    dispatch({
      type: types.FETCH_SELLERS_CAMPAIGN_FAILED,
      payload: err.message,
    });
  } finally {
    dispatch({ type: types.IS_FETCH_SELLERS_CAMPAIGN, payload: false });
  }
};

export const getSellersOfCampaignStatusCount = async (jobCode) => {
  try {
    const { data = {} } = await client.request({
      path: api.path.sellersOfCampaignStatusCount(jobCode),
      method: "get",
    });

    const value = data.data || {};
    const all = Object.keys(value).reduce((total, status) => {
      return value[status] + total;
    }, 0);
    value.all = all;
    return value;
  } catch (err) {
    console.error("[getSellersOfCampaignStatusCount]", err);
  }
};

export const getSellerActivities = (hostCode, jobCode) => async (dispatch) => {
  try {
    const path = api.path.sellerActivities(jobCode, hostCode);
    const { data = {} } = await client.request({
      path: path,
      method: "get",
    });

    // const fake = ['verify', 'complete', 'execute'].map(status => {
    //   const item = Object.assign({}, data.data[0]);
    //   item.status = 'complete';
    //   item.statusJob = status;
    //   return item;
    // });
    // const fake2 = ['proposal'].map(status => {
    //   const item = Object.assign({}, data.data[0]);
    //   item.status = status;
    //   return item;
    // });
    // const activities = fake.concat(data.data).concat(fake2);
    const activities = data.data;
    dispatch({
      type: types.FETCH_SELLER_ACTIVITIES_SUCCESS,
      payload: activities,
    });
    return activities;
  } catch (err) {
    console.error("[getSellerActivities]", err);
  }
};

export const completeCampaign = (jobCode) => async (dispatch) => {
  try {
    const path = api.path.completeCampaign(jobCode);
    const { data = {} } = await client.request({
      path: path,
      method: "patch",
    });
  } catch (err) {
    console.error("[completeCampaign]", err);
  }
};

export const acceptDeal = (dealCode) => async (dispatch) => {
  try {
    const path = api.path.acceptDeal(dealCode);
    const { data = {} } = await client.request({
      path: path,
      method: "patch",
    });
  } catch (err) {
    console.error("[acceptDeal]", err);
  }
};

export const rejectDeal = (dealCode) => async (dispatch) => {
  try {
    const path = api.path.rejectDeal(dealCode);
    const { data = {} } = await client.request({
      path: path,
      method: "patch",
    });
  } catch (err) {
    console.error("[rejectDeal]", err);
  }
};

export const verifyJob = ({ dealCode, jobCode, hostCode }) => async (
  dispatch
) => {
  try {
    const path = api.path.verifyJob({ dealCode, hostCode, jobCode });
    const { data = {} } = await client.request({
      path: path,
      method: "POST",
    });
  } catch (err) {
    console.error("[verifyJob]", err);
  }
};

export const rateHost = ({ dealCode, jobCode, hostCode, score }) => async (
  dispatch
) => {
  try {
    const path = api.path.rateHost({ dealCode, hostCode, jobCode, score });
    const { data = {} } = await client.request({
      path: path,
      method: "POST",
    });
  } catch (err) {
    console.error("[rateHost]", err);
  }
};

export const getDetailCampaignForRightProduct = () => async (
  dispatch,
  getState
) => {
  try {
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: true });
    const { code: productCode } = getState().detailProduct.currentProduct;
    const { data: campaignResponse } = await client.request({
      path: api.path.getDetailCampaign(productCode),
      method: "get",
    });

    const status = _.get(
      campaignResponse,
      "campaignResponse.data.campaign.status",
      ""
    );

    if (status !== CAMPAIGN_STATUS.COMPLETE) {
      dispatch({
        type: types.FETCH_CAMPAIGN_DETAIL_FOR_PRODUCT_SUCCESS,
        payload: { ...campaignResponse },
      });
    } else {
      dispatch({
        type: types.FETCH_CAMPAIGN_DETAIL_FOR_PRODUCT_SUCCESS,
        payload: null,
      });
    }
  } catch (err) {
    console.log("[getDetailCampaignForRightProduct]", err);
    dispatch({ type: types.FETCH_CAMPAIGN_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_CAMPAIGN, payload: false });
  }
};

export const addDealByCompany = (params) => async (dispatch) => {
  try {
    dispatch({ type: types.ADD_DEAL, payload: true });

    const { data } = await client.request({
      path: api.path.createDealByCompany,
      method: "POST",
      data: params,
    });
    if (data) {
      dispatch({ type: types.ADD_DEAL_SUCCESS, payload: data });
      return data;
    }
  } catch (err) {
    console.log("[addDealByCompany]", err);
    dispatch({ type: types.ADD_DEAL_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.ADD_DEAL, payload: false });
  }
};

export const deleteCampaign = async (code) => {
  try {
    const path = api.path.deleteCampaign(code);
    const { data } = await client.request({
      path: path,
      method: "delete",
    });
    return data;
  } catch (err) {
    console.error("[deleteCampaign]", err);
  }
};

export const submitFixedRatePayment = async ({ dealCode }) => {
  try {
    const path = api.path.fixedRatePayment({ dealCode });
    const { data } = await client.request({
      path: path,
      method: "POST",
    });
    return data;
  } catch (err) {
    return false;
  }
};

export const updateProofByCompany = async ({ paymentCode, proof }) => {
  try {
    const path = api.path.companyUpdateProof({ paymentCode });
    const { data } = await client.request({
      path: path,
      data: { proof },
      method: "POST",
    });
    return data;
  } catch (err) {
    return false;
  }
};


export const findPaymentByCode = async ({ paymentCode }) => {
  try {
    const path = api.path.companyFindPayment({ paymentCode });
    const { data } = await client.request({
      path: path,
      method: "get",
    });
    return data;
  } catch (err) {
    console.error("[getPayment]", err);
  }
};