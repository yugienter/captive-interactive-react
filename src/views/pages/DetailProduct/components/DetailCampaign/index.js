import { useState, useEffect } from "react";
import {
  Row,
  Col,
  Table,
  Space,
  Menu,
  Dropdown,
  Modal,
  Rate,
  Button,
  message,
  Tooltip,
} from "antd";
import { connect } from "react-redux";
import Page from "views/components/Page";
import CampaignDetailDrawer from "views/components/CampaignDetailDrawer";
import { fetchHostProfile } from "views/pages/HostProfile/actions";
import {
  IconBack,
  IconEdit,
  IconMore,
  IconDrop,
  IconCampaignActionComplete,
  IconDeleteProduct,
  IconInfo,
} from "views/components/Icon/product";
import {
  IconClockGrey,
  IconExpand,
  IconNoted,
  IconDots,
  IconCompleteGrey,
  IconNotedFull,
  IconPropose,
  IconTickGreen,
  IconX,
  IconStarYellow,
} from "views/components/Icon/myjobs";
import CreateCampaign from "../CreateCampaign";
import { constants, RATE_SCORES, ReactUtils, utils } from "helpers";
import { useHistory, useParams } from "react-router-dom";
import {
  getCampaignDetailByJobCode,
  getSellersOfCampaign,
  acceptDeal,
  rejectDeal,
  verifyJob,
  rateHost,
  getSellersOfCampaignStatusCount,
  completeCampaign,
  deleteCampaign,
  submitFixedRatePayment,
} from "../actions";
import { IconStar } from "views/components/Icon/basic";
import CampaignDetailPanel from "views/components/CampaignDetailPanel";
import DrawerProposeDeal from "views/components/DrawerProposeDeal";
import { IconMessage } from "views/components/Icon/drawer";
import { IconRating } from "views/components/Icon/recommender";
import DirectOfferDrawer from "views/components/DirectOfferDrawer";
import { resetSelectedSellers } from "views/pages/RecommenderSeller/actions";
import DeleteModal from "views/pages/DetailProduct/components/DeleteModal";
import PopupConfirmOrder from 'views/components/CollapseMyJobs/components/PopupConfirmOrder';
import DrawerSubmitFixedRate from 'views/components/TableSellers/DrawerSubmitFixedRate';

const {
  userRoles,
  statusDeal,
  statusJob,
  JobStatusText,
  CAMPAIGN_STATUS,
} = constants;

const TAB_IDS = {
  ALL: "all",
  RECEIVED: statusJob.submitted,
  NEGOTIATING: statusJob.negotiating,
  EXECUTE: statusJob.execute,
  COMPLETED: statusJob.complete,
  REJECT: statusJob.rejected,
};

const DetailCampaign = ({
  getCampaignDetailByJobCode,
  getSellersOfCampaign,
  sellers = {},
  fetchHostProfile,
  jobDetail,
  rejectDeal,
  acceptDeal,
  isLoading,
  verifyJob,
  rateHost,
  selectedSellers,
  resetSelectedSellers,
  completeCampaign,
}) => {
  const history = useHistory();
  const { jobCode } = useParams();

  const [selectedTab, setSelectedTab] = useState(TAB_IDS.ALL);
  const [selectedSeller, setSelectedSeller] = useState(null);
  const [rateScore, setRateScore] = useState(0);
  const [drawerShow, setDrawerShow] = useState(false);
  const [rateModalVisible, setRateModalVisible] = useState(false);
  const [sellerProfile, setSellerProfile] = useState({});
  const [statusCount, setStatusCount] = useState({});
  const [proposalDeal, setProposalDeal] = useState(null);
  const [directOffer, setDirectOffer] = useState(false);
  const [editCampaignVisible, setEditCampaignVisible] = useState(false);
  const [showDeleteCampaign, setShowDeleteCampaign] = useState(false);
  const [submitFixedRatePopup, setSubmitFixedRatePopup] = useState(false);
  const [confirmOrderPopup, setConfirmOrderPopup] = useState(false);
  const [fixedRatePayment, setFixedRatePayment] = useState(false);

  const { campaign = {} } = jobDetail || {};

  useEffect(() => {
    setDirectOffer(!!selectedSellers.length);
  }, [selectedSellers, setDirectOffer]);

  useEffect(() => {
    return () => {
      resetSelectedSellers();
    };
  }, []);

  useEffect(() => {
    async function getCampaign() {
      await getCampaignDetailByJobCode(jobCode);
      await getSellersOfCampaign(jobCode);
      const statusCount = await getSellersOfCampaignStatusCount(jobCode);
      setStatusCount(statusCount);
    }

    getCampaign();
  }, [getCampaignDetailByJobCode, getSellersOfCampaign, jobCode]);

  const onCloseDrawer = () => {
    setDrawerShow(false);
  };

  const columns = [
    {
      title: "Seller",
      dataIndex: "seller",
      key: "seller",
      render(text, record, index) {
        return (
          <div className="box-products-wrapper">
            <div className="box-products">
              <div className="box-products__left">
                <figure>
                  <img
                    alt={record.name}
                    className="dropdown-img"
                    src={utils.getMediaUrl(record.avatar)}
                  />
                </figure>
              </div>
              <div className="box-products__right">
                <p className="last">{record.name}</p>
              </div>
            </div>
            <div className="last"></div>
          </div>
        );
      },
    },
    {
      title: "Status",
      dataIndex: "jobStatus",
      key: "status",
      render(text, record, index) {
        return <div className="status direct inline-flex justify-center">
          {JobStatusText[text]}
          &nbsp;&nbsp;{record.verifyPaymentRequired && 
          <Tooltip placement="top" title="Verifying Payment"><div><IconInfo /></div></Tooltip>}
          {!record.verifyPaymentRequired && record.fixedRatePaymentRequired && 
          <Tooltip placement="top" title="Waiting pay for fixed rate"><div><IconInfo /></div></Tooltip>}
        </div>;
      },
    },
    {
      title: "Followers",
      dataIndex: "followers",
      key: "followers",
      sorter: true,
      render(text, record, index) {
        return <div className="followers">{utils.kFormatter(text)}</div>;
      },
    },
    {
      title: "Fixed Rate",
      dataIndex: "streamHourlyRate",
      key: "streamHourlyRate",
      sorter: true,
      render(hourlyRate, record, index) {
        const value = hourlyRate ? hourlyRate.description : 0;
        return <div className="hourly">${value}</div>;
      },
    },
    {
      title: "Commission",
      dataIndex: "streamCommission",
      key: "streamCommission",
      sorter: true,
      render(commission, record, index) {
        const value = commission ? commission.description : "";
        if (!value) return "--";

        return (
          <div className="commission">
            {value}
            {record.streamCommissionUnit}
          </div>
        );
      },
    },
    {
      title: "Date Modified",
      dataIndex: "createdAt",
      key: "createdAt",
      sorter: true,
      render(text, record, index) {
        return (
          <div className="modified">
            {utils.toDateString(text, { defaultValue: "--" })}
          </div>
        );
      },
    },
    {
      title: "Due Date",
      dataIndex: "streamTime",
      key: "streamTime",
      sorter: true,
      render(text, record, index) {
        return (
          <div className="stream">
            {utils.toDateString(text, { defaultValue: "--" })}
          </div>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "actions",
      render: (text, record) => (
        <Space size="middle">
          <>{renderIconNoted(record)}</>
          <>{renderIcon(record)}</>
        </Space>
      ),
    },
  ];

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const requestComplement = () => {};

  const renderRowActions = (actions) => {
    const overlay = <Menu>{actions}</Menu>;
    return (
      <Dropdown overlay={overlay} trigger={["click"]}>
        <span className="clickable">
          <IconDots />
        </span>
      </Dropdown>
    );
  };

  function renderIcon(record) {
    switch (record.status) {
      case statusDeal.proposal: {
        if (record.verifyPaymentRequired) {
          return <span className="box-tooltip">
            <IconClockGrey />
            {
              <div className="box-tooltip__content">
                A bank transfer usually takes 3-5 business days to be received. 
                If it takes longer please send a proof of payment and we will submit an inquiry on your behalf. 
              </div>
            }
          </span>
        }
        return renderRowActions(
          <div className="popup-row-actions">
            <Menu.Item key="0" onClick={() => setProposalDeal(record)}>
              <IconPropose />
              Propose a deal
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="1" onClick={() => handleAcceptDeal(record)}>
              <IconTickGreen />
              Accept
            </Menu.Item>
            <Menu.Item key="2" onClick={() => handleRejectDeal(record)}>
              <IconX />
              Reject
            </Menu.Item>
          </div>
        );
      }
      case statusDeal.deal: {
        return (
          <span className="box-tooltip">
            <IconClockGrey />
            {
              <div className="box-tooltip__content">
                Wait for Seller to response your proposed deal
              </div>
            }
          </span>
        );
      }
      case statusDeal.accepted: {
        return (
          <span className="box-tooltip">
            <IconClockGrey />
            {
              <div className="box-tooltip__content">
                Wait for Seller to stream @{" "}
                {utils.toDateString(record.streamTime, {
                  format: "DD MMM, YYYY | hh:mm",
                  defaultValue: "--",
                })}
              </div>
            }
          </span>
        );
      }
      case statusDeal.complete: {
        if (record.jobStatus === statusJob.execute) {
          return renderRowActions(
            <>
              <Menu.Item key="0" onClick={() => requestComplement(record)}>
                <IconMessage />
                Request Complement
              </Menu.Item>
            </>
          );
        }
        if (record.jobStatus === statusJob.complete) {
          return renderRowActions(
            <>
              <Menu.Item key="0" onClick={() => handleVerifyJob(record)}>
                <IconCompleteGrey />
                Verify this job
              </Menu.Item>
            </>
          );
        }
        if (record.jobStatus === statusJob.verify) {
          const rated = record.rate;
          if (rated) {
            return (
              <span className="clickable box-tooltip">
                <IconStarYellow />
                {
                  <div className="box-tooltip__content">
                    Thanks for your feedback
                  </div>
                }
              </span>
            );
          } else {
            return (
              <span className="clickable box-tooltip">
                <span
                  className="clickable"
                  onClick={() => setRateModalVisible(true)}
                >
                  <IconStar />
                </span>
                {
                  <div className="box-tooltip__content">
                    Click to give feedback
                  </div>
                }
              </span>
            );
          }
        }
        return "";
      }
      default: {
        return "";
      }
    }
  }

  const handleAcceptDeal = async (record) => {
    const dealCode = record.dealCode;
    if (dealCode) {
      if (record?.streamHourlyRate?.description) {
        setSubmitFixedRatePopup(true);
        return;
      }
      await acceptDeal(dealCode);
      const params = {
        page: sellers.page,
        per_page: sellers.perPage,
      };
      getSellersOfCampaign(
        jobCode,
        params,
        selectedTab === TAB_IDS.ALL ? "" : selectedTab
      );
    }
  };

  const handleRejectDeal = async (record) => {
    const dealCode = record.dealCode;
    if (dealCode) {
      await rejectDeal(dealCode);
      const params = {
        page: sellers.page,
        per_page: sellers.perPage,
      };
      getSellersOfCampaign(
        jobCode,
        params,
        selectedTab === TAB_IDS.ALL ? "" : selectedTab
      );
    }
  };

  const handleVerifyJob = (record) => {
    const dealCode = record.dealCode;
    const hostCode = record.hostCode;
    if (dealCode && hostCode && jobCode) {
      verifyJob({ jobCode, hostCode, dealCode });
    }
  };

  const handleRateHost = async () => {
    const dealCode = selectedSeller.dealCode;
    const hostCode = selectedSeller.hostCode;
    if (dealCode && hostCode && jobCode) {
      await rateHost({ jobCode, hostCode, dealCode, score: rateScore });
    }

    setRateModalVisible(false);
    const params = {
      page: sellers.page,
      per_page: sellers.perPage,
    };
    getSellersOfCampaign(
      jobCode,
      params,
      selectedTab === TAB_IDS.ALL ? "" : selectedTab
    );
  };

  function handleActionClick(record) {
    if (!record) return;

    fetchHostProfile(record.email).then((seller) => {
      setSellerProfile(seller);
    });
    setDrawerShow(true);
  }

  function renderIconNoted(record) {
    let icon = <IconNoted />;
    if (
      record.followers === undefined ||
      record.commission === undefined ||
      record.jobStatus === statusJob.rejected
    ) {
      icon = <IconNotedFull />;
    }
    return (
      <span className="clickable" onClick={() => handleActionClick(record)}>
        {icon}
      </span>
    );
  }

  function handleTableChange(pagination, filters, sorter) {
    let sortType = null;
    if (sorter.order === "descend") sortType = -1;
    if (sorter.order === "ascend") sortType = 1;

    const params = {
      page: pagination.current - 1,
      per_page: pagination.pageSize,
      sort_col: sorter.order ? sorter.columnKey : null,
      sort_type: sortType,
    };
    getSellersOfCampaign(
      jobCode,
      params,
      selectedTab === TAB_IDS.ALL ? "" : selectedTab
    );
  }

  const onRow = (record) => ({
    onClick: () => {
      console.log("onRow", record);
      setSelectedSeller(record);
    },
  });

  const handleTableTabClick = (e) => {
    e.preventDefault();

    const target = e.target;
    const tabId = target.dataset.tabId;
    setSelectedTab(tabId);
    getSellersOfCampaign(jobCode, {}, tabId === TAB_IDS.ALL ? "" : tabId);
  };

  const handleEditCampaign = async () => {
    setEditCampaignVisible(false);
    await getCampaignDetailByJobCode(jobCode);
  };

  const handleCompleteCampaign = async () => {
    await completeCampaign(jobCode);
    await getCampaignDetailByJobCode(jobCode);
  };

  const tableTabClassName = (tabId) => {
    return `item ${tabId === selectedTab ? "selected" : ""} ${
      tabId === TAB_IDS.REJECT && "reject"
    }`;
  };

  const campaignStatus = () => {
    switch (campaign.status) {
      case CAMPAIGN_STATUS.NOT_STARTED:
        return <span className="campaign-status not-started">Not Started</span>;
      case CAMPAIGN_STATUS.IN_PROGRESS:
        return <span className="campaign-status in-progress">In Progress</span>;
      case CAMPAIGN_STATUS.COMPLETE:
        return <span className="campaign-status complete">Completed</span>;
      default:
        return <span className="campaign-status">{campaign.status}</span>;
    }
  };

  const campaignAction = () => {
    switch (campaign.status) {
      case CAMPAIGN_STATUS.NOT_STARTED:
        return (
          <>
            <Button onClick={() => setEditCampaignVisible(true)}>
              <IconEdit />
              Edit
            </Button>
            <Button>{renderMoreActions()}</Button>
          </>
        );
      case CAMPAIGN_STATUS.IN_PROGRESS:
        return (
          <Button onClick={handleCompleteCampaign}>
            <IconCampaignActionComplete />
            Complete
          </Button>
        );
      default:
        return;
    }
  };

  const deleteCampaignAction = () => {
    setShowDeleteCampaign(true);
  };

  const handleDeleteCampaign = async () => {
    const result = await deleteCampaign(jobCode);
    setShowDeleteCampaign(false);
    if (result && !result.error) {
      message.success("Delete Campaign success");
      history.goBack();
      return;
    }
    message.error("Delete Campaign error");
  };

  const renderMoreActions = () => {
    const overlay = (
      <Menu>
        <div className="popup-row-actions">
          <Menu.Item
            key="2"
            className="text-red"
            onClick={deleteCampaignAction}
          >
            <IconDeleteProduct />
            &nbsp;Delete Campaign
          </Menu.Item>
        </div>
      </Menu>
    );
    return (
      <Dropdown overlay={overlay} placement="bottomCenter" trigger={["click"]}>
        <span className="clickable">
          <IconMore />
          More
          <IconDrop />
        </span>
      </Dropdown>
    );
  };

  const createPayment = async () => {
    const result = await submitFixedRatePayment({ dealCode: selectedSeller.dealCode });
    if (!result.code) {
      ReactUtils.messageWarn({ content: 'Create fixed rate payment fail' });
      return;
    }
    setFixedRatePayment(result);
    setConfirmOrderPopup(true);
  }

  const onSubmitOrder = async () => {
    setSubmitFixedRatePopup(false);
    setConfirmOrderPopup(false);
    ReactUtils.messageSuccess({ content: 'Submit fixed rate payment successfully' });
    refreshData();
  }

  const closePopupConfirmOrder = async () => {
    setConfirmOrderPopup(false);
    refreshData();
  }

  const refreshData = () => {
    const params = {
      page: sellers.page,
      per_page: sellers.perPage,
    };
    getSellersOfCampaign(
      jobCode,
      params,
      selectedTab === TAB_IDS.ALL ? "" : selectedTab
    );
  }

  const closeHandleProposeDeal = () => {
    setProposalDeal(false);
    refreshData();
  }

  return (
    <Page
      helmet="Campaign Detail"
      alignTop
      requiredAccess={[userRoles.COMPANY]}
    >
      <div className="detail-product">
        <div className="detail-product__header">
          <div style={{ alignItems: "baseline" }}>
            <span onClick={() => history.goBack()}>
              <IconBack />
            </span>
            <h2>{campaign.name}</h2>
            <span>{campaignStatus()}</span>
          </div>
          <div
            className="detail-product__header__button"
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            {campaignAction()}
          </div>
        </div>
        <div className="pannel-table">
          <Row style={{ width: "100%" }}>
            <Col span={7}>
              <CampaignDetailPanel />
            </Col>
            <Col span={17}>
              <div className="wrapper-filter">
                <div
                  data-tab-id={TAB_IDS.ALL}
                  className={tableTabClassName(TAB_IDS.ALL)}
                  onClick={handleTableTabClick}
                >
                  All Statuses{" "}
                  <span className="no-click">({statusCount.all})</span>
                </div>
                <div
                  data-tab-id={TAB_IDS.RECEIVED}
                  className={tableTabClassName(TAB_IDS.RECEIVED)}
                  onClick={handleTableTabClick}
                >
                  Bid Received{" "}
                  <span className="no-click">
                    ({statusCount[statusJob.submitted]})
                  </span>
                </div>
                <div
                  data-tab-id={TAB_IDS.NEGOTIATING}
                  className={tableTabClassName(TAB_IDS.NEGOTIATING)}
                  onClick={handleTableTabClick}
                >
                  Negotiating{" "}
                  <span className="no-click">
                    ({statusCount[statusJob.negotiating]})
                  </span>
                </div>
                <div
                  data-tab-id={TAB_IDS.EXECUTE}
                  className={tableTabClassName(TAB_IDS.EXECUTE)}
                  onClick={handleTableTabClick}
                >
                  Waiting to Execute{" "}
                  <span className="no-click">
                    ({statusCount[statusJob.execute]})
                  </span>
                </div>
                <div
                  data-tab-id={TAB_IDS.COMPLETED}
                  className={tableTabClassName(TAB_IDS.COMPLETED)}
                  onClick={handleTableTabClick}
                >
                  Completed{" "}
                  <span className="no-click">
                    ({statusCount[statusJob.complete]})
                  </span>
                </div>
                <div
                  data-tab-id={TAB_IDS.REJECT}
                  className={tableTabClassName(TAB_IDS.REJECT)}
                  onClick={handleTableTabClick}
                >
                  Reject{" "}
                  <span className="no-click">
                    ({statusCount[statusJob.rejected]})
                  </span>
                </div>
              </div>
              <div className="table-sellers">
                <Table
                  columns={columns}
                  dataSource={sellers.data}
                  onChange={handleTableChange}
                  onRow={onRow}
                  loading={isLoading}
                  pagination={{
                    position: ["bottomCenter"],
                    showSizeChanger: true,
                    showTotal: (total, range) =>
                      `Showing ${range[0]}-${range[1]} of ${total} results`,
                    size: "small",
                    current: sellers.page + 1,
                    pageSize: sellers.perPage,
                    total: sellers.total,
                  }}
                />
              </div>
              <CampaignDetailDrawer
                showDrawer={drawerShow}
                onCloseDrawer={onCloseDrawer}
                seller={sellerProfile}
                jobCode={jobDetail.code}
              />
              {proposalDeal && (
                <DrawerProposeDeal
                  deal={proposalDeal}
                  visible={true}
                  handleClose={closeHandleProposeDeal}
                />
              )}
              {selectedSeller && <DrawerSubmitFixedRate
                visible={submitFixedRatePopup}
                deal={selectedSeller}
                seller={selectedSeller}
                handleClose={() => setSubmitFixedRatePopup(false)}
                onContinue={() => createPayment()} /> }
              <PopupConfirmOrder 
                payment={fixedRatePayment}
                handleOK={onSubmitOrder}
                handleClose={closePopupConfirmOrder} 
                visible={confirmOrderPopup} />
              <DirectOfferDrawer
                showDrawer={directOffer}
                onCloseDrawer={() => setDirectOffer(false)}
              />
              <Modal
                title="Rate Host"
                centered
                visible={rateModalVisible}
                okText="Submit"
                onCancel={() => setRateModalVisible(false)}
                onOk={handleRateHost}
              >
                <div className="box-rating">
                  <Rate
                    character={<IconRating />}
                    onChange={setRateScore}
                    value={rateScore}
                    className="rating-actions"
                  />
                  <div className="rating-result">
                    Rating this Host:
                    <strong className="ant-rate-text">
                      {RATE_SCORES[rateScore]}
                    </strong>
                  </div>
                </div>
              </Modal>
              <CreateCampaign
                visible={editCampaignVisible}
                getCampaign={false}
                onOk={handleEditCampaign}
                onCancel={() => setEditCampaignVisible(false)}
                isEdit
              />
            </Col>
          </Row>
        </div>
      </div>
      <DeleteModal
        title="Delete Campaign"
        visible={showDeleteCampaign}
        handleOk={handleDeleteCampaign}
        handleClose={() => setShowDeleteCampaign(false)}
      >
        <p>
          Are you sure you want to delete this Campaign?
          <br />
          You can’t undo this action.
        </p>
      </DeleteModal>
    </Page>
  );
};

const mapStateToProps = ({
  campaign: { campaignDetail, sellers, isLoading },
  recommenderSeller,
}) => ({
  sellers,
  jobDetail: campaignDetail || {},
  isLoading,
  selectedSellers: recommenderSeller.selectedSellers || [],
});

const mapDispatchToProps = {
  getCampaignDetailByJobCode,
  getSellersOfCampaign,
  fetchHostProfile,
  acceptDeal,
  rejectDeal,
  verifyJob,
  rateHost,
  resetSelectedSellers,
  completeCampaign,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailCampaign);
