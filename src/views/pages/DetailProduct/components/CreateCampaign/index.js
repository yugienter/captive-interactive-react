import React from "react";
import { connect } from "react-redux";
import {
  Modal,
  Steps,
  Button,
  message,
  Form,
  Input,
  Radio,
  Checkbox,
  DatePicker,
  Row,
  Col,
  Select,
  InputNumber,
} from "antd";
import { IconArrowRightWhite } from "views/components/Icon/basic";
import { IconArrowLeft } from "views/components/Icon/pages";
import moment from "moment";
import { utils, constants } from "helpers";
import { addOrUpdateCampaign } from "views/pages/DetailProduct/actions";
import { getDetailCampaignForRightProduct, getListCampaigns } from "../actions";
import { getDetailCampaign } from "../actions";
import { ReactUtils } from "helpers";
import _ from "lodash";

const { Step } = Steps;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { STREAM_TYPES, PLATFORM_TYPES, AGREED_PRICINGS } = constants;
const { convertPeriodToArrayAndMoment } = utils;
const dealKey = "deal";
const DISCOUNT_UNITS = {
  PERCENTAGE: "percentage",
  PRICE: "amount",
};

const CreateCampaign = ({
  isEdit,
  visible,
  onOk,
  onCancel,
  getCampaign = true,
  addOrUpdateCampaign,
  getDetailCampaign,
  campaignDetail,
  currentProduct,
  getDetailCampaignForRightProduct,
  getListCampaigns,
}) => {
  const [current, setCurrent] = React.useState(0);
  const [agreedPricingSwitcher, setAgreedPricingSwitcher] = React.useState(0);
  const [discountedPrice, setDiscountedPrice] = React.useState(0);
  const [form] = Form.useForm();
  const [updatedData, setUpdatedData] = React.useState({
    [dealKey]: {},
  });
  const onFinish = async (values) => {};
  const [data, setData] = React.useState({
    publicRRPType: "USD",
    [dealKey]: {
      streamCommissionUnit: "%",
    },
  });

  const onFinishFailed = () => {
    message.error("Submit failed!");
  };

  const rangeConfig = {
    rules: [
      {
        type: "array",
        required: true,
        message: "Please select time!",
      },
    ],
  };

  React.useEffect(() => {
    if (isEdit && getCampaign) {
      getDetailCampaign();
    }
  }, [isEdit, visible, getCampaign]);

  React.useEffect(() => {
    let newVal = {
      ...data,
    };

    if (isEdit) {
      newVal = _.cloneDeep(campaignDetail || {});
      if (newVal && newVal.campaign) {
        newVal.campaign.time = convertPeriodToArrayAndMoment(
          newVal.campaign.timeStart,
          newVal.campaign.timeEnd
        );

        newVal.discountPeriod = convertPeriodToArrayAndMoment(
          newVal.discountPeriodStart,
          newVal.discountPeriodEnd
        );
      }
      if (newVal && newVal.deal) {
        if (newVal.deal.streamTime) {
          newVal.deal.streamTime = {
            ...newVal.deal.streamTime,
            description: newVal.deal.streamTime.description
              ? moment(newVal.deal.streamTime.description)
              : null,
          };
        }

        if (newVal.deal.streamCommission) {
          newVal.deal.streamCommission = {
            ...newVal.deal.streamCommission,
            description: newVal.deal.streamCommission.description
              ? parseInt(newVal.deal.streamCommission.description)
              : 0,
          };
        }
      }
    }

    form.setFieldsValue({
      ...newVal,
    });
  }, [data, campaignDetail]);

  const onChangeAgreedPricings = (e) => {
    switch (e.target.value) {
      case "Commissions":
        setAgreedPricingSwitcher(1);
        break;
      case "Hourly Rate":
        setAgreedPricingSwitcher(2);
        break;
      default:
        setAgreedPricingSwitcher(3);
        break;
    }
  };

  const onPriceChange = (e) => {
    calculateDiscountedPrice();
  };

  const onDiscountChange = (e) => {
    calculateDiscountedPrice();
  };

  const onUnitChange = (value) => {
    calculateDiscountedPrice();
  };

  const calculateDiscountedPrice = () => {
    const price = form.getFieldValue("publicRRP");
    const discount = form.getFieldValue("discountValue");
    const discountUnit =
      form.getFieldValue("discountUnit") || DISCOUNT_UNITS.PRICE;
    if (![price, discount, discountUnit].includes(undefined)) {
      const priceNumber = parseFloat(price);
      const discountNumber = parseFloat(discount);
      let discountedPrice = 0;
      if (discountUnit === DISCOUNT_UNITS.PRICE) {
        discountedPrice = priceNumber - discountNumber;
      } else if (discountUnit === DISCOUNT_UNITS.PERCENTAGE) {
        discountedPrice = priceNumber - (discountNumber * priceNumber) / 100;
      }
      form.setFieldsValue({ discountedPrice: discountedPrice || 0 });
      setDiscountedPrice(discountedPrice);
    }
  };

  const steps = [
    {
      title: "1",
      content: (
        <div className="step step1">
          <h4> {isEdit ? "Edit Campaign" : "Create Campaign"}</h4>
          <h6>Start talking to sellers with a product campaign</h6>
          <div className="start-form">
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Campaign Name *"
                name={["campaign", "name"]}
                rules={[{ required: true, message: "Campaign is missing" }]}
              >
                <Input placeholder="Campaign 1" />
              </Form.Item>
              <Form.Item
                name={["campaign", "time"]}
                label="Campaign Period *"
                {...rangeConfig}
              >
                <RangePicker />
              </Form.Item>
              <Form.Item label="Description" name={["campaign", "description"]}>
                <Input placeholder="e.g. Tell us more about the objectives of this campaign" />
              </Form.Item>
            </Form>
          </div>
        </div>
      ),
    },
    {
      title: "2",
      content: (
        <div className="step step2">
          <h4>Set Product Price</h4>
          <h6>You can skip dispensable fields</h6>
          <div className="start-form">
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Row style={{ textAlign: "left" }}>
                <Col span={12}>
                  <Form.Item label="Recommended Retail Pricing (RRP) *">
                    <Input.Group compact>
                      <Form.Item noStyle name={["publicRRPType"]}>
                        <Select defaultValue="USD">
                          <Option value="USD">SGD</Option>
                          <Option value="EUR">EUR</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name={["publicRRP"]}
                        noStyle
                        rules={[
                          {
                            message: "RRP is missing",
                            required: true,
                          },
                        ]}
                      >
                        <InputNumber
                          onChange={onPriceChange}
                          placeholder="14.99"
                        />
                      </Form.Item>
                    </Input.Group>
                  </Form.Item>

                  <Form.Item label="Wholesale Discount">
                    <Input.Group compact>
                      <Form.Item name={["discountUnit"]} noStyle>
                        <Select onChange={onUnitChange} defaultValue="amount">
                          <Option value="amount">Amount</Option>
                          <Option value="percentage">Percentage (%)</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item name={["discountValue"]} noStyle>
                        <InputNumber
                          onChange={onDiscountChange}
                          placeholder="Discount Amount"
                        />
                      </Form.Item>
                    </Input.Group>
                  </Form.Item>

                  <Form.Item
                    label="Discount Period"
                    // {...rangeConfig}
                    name={["discountPeriod"]}
                  >
                    <RangePicker />
                  </Form.Item>
                </Col>
                <Col span={12} className="col-right">
                  <Form.Item
                    label="Seller Purchase Price"
                    style={{ marginTop: "30px" }}
                    name="discountedPrice"
                  >
                    <Input.Group compact style={{ backgroundColor: "#f5f5f5" }}>
                      <Select defaultValue="USD" disabled>
                        <Option value="USD">SGD</Option>
                        <Option value="EUR">EUR</Option>
                      </Select>
                      <InputNumber
                        value={discountedPrice}
                        placeholder="14.99"
                        disabled
                      />
                    </Input.Group>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
      ),
    },
    {
      title: "3",
      content: (
        <div className="step step3">
          <h4>Additional Livestream Details (If applicable)</h4>
          <h6>
            Key details about the stream <br />
            Tips: Tick “Negotible” in which fields you accept Sellers to
            negotiate.
          </h6>
          <div className="start-form">
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Row>
                <Col span={12}>
                  <Form.Item
                    label="Stream Date"
                    {...rangeConfig}
                    className="form-position"
                  >
                    <Form.Item
                      name={[dealKey, "streamTime", "description"]}
                      noStyle
                    >
                      <DatePicker />
                    </Form.Item>
                    <Form.Item
                      name={[dealKey, "streamTime", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox className="cb-abs">Negotiable</Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
                <Col span={12}></Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Type of Stream"
                    className="list-radio form-position"
                  >
                    <Form.Item
                      name={[dealKey, "streamType", "description"]}
                      className="list-radio form-position"
                      noStyle
                    >
                      <Radio.Group className="list-radio form-position">
                        {Object.keys(STREAM_TYPES).map((value) => (
                          <Radio key={value} value={value}>
                            {STREAM_TYPES[value]}
                          </Radio>
                        ))}
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item
                      name={[dealKey, "streamType", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox className="cb-abs">Negotiable</Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Streaming Elements"
                    className="list-radio form-position"
                  >
                    <Form.Item
                      name={[dealKey, "streamElement", "description"]}
                      noStyle
                    >
                      <Checkbox.Group noStyle>
                        <Checkbox className="cb-item" value={"Studio"}>
                          <p className="black">
                            <strong>Studio</strong>
                          </p>
                          <p className="grey">Including Location</p>
                        </Checkbox>
                        <Checkbox className="cb-item" value={"Overlay"}>
                          <p className="black">
                            <strong>Overlay</strong>
                          </p>
                          <p className="grey">Including Graphics</p>
                        </Checkbox>
                      </Checkbox.Group>
                    </Form.Item>
                    <Form.Item
                      name={[dealKey, "streamElement", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox className="cb-abs">Negotiable</Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Platform"
                    className="list-radio form-position"
                  >
                    <Form.Item
                      name={[dealKey, "streamPlatform", "description"]}
                      noStyle
                    >
                      <Checkbox.Group noStyle>
                        {PLATFORM_TYPES.map((op) => (
                          <Checkbox
                            key={op.value}
                            className="cb-item-4"
                            value={op.value}
                          >
                            <p className="black">{op.text}</p>
                          </Checkbox>
                        ))}
                      </Checkbox.Group>
                    </Form.Item>
                    <Form.Item
                      name={[dealKey, "streamPlatform", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox className="cb-abs">Negotiable</Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
      ),
    },
    {
      title: "4",
      content: (
        <div className="step step4">
          <h4>Seller Fees</h4>
          <h6>
            Tips: Select “Negotiable” to attract more Sellers to join bidding;{" "}
            <br />
            Switch to “Fixed” if you have a fixed budget plan.
          </h6>
          <div className="start-form">
            <Form
              form={form}
              layout="vertical"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Fee Type"
                    className="list-radio list-radio-col2"
                    name={[dealKey, "streamPricings"]}
                    rules={[
                      {
                        required: true,
                        message: "Fee Type is missing",
                      },
                    ]}
                  >
                    <Radio.Group className="list-radio-group-2">
                      {Object.keys(AGREED_PRICINGS).map((value) => (
                        <Radio
                          key={value}
                          value={value}
                          onChange={onChangeAgreedPricings}
                        >
                          {AGREED_PRICINGS[value]}
                        </Radio>
                      ))}
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col
                  span={12}
                  style={{ paddingRight: "4px" }}
                  hidden={!agreedPricingSwitcher || agreedPricingSwitcher === 1}
                >
                  <Form.Item
                    label="Fixed rate per campaign *"
                    className="form-position"
                  >
                    <Input.Group compact>
                      <Form.Item
                        name={[dealKey, "streamHourlyRateRange", "unit"]}
                        noStyle
                      >
                        <Select
                          defaultValue={"USD"}
                          disabled={
                            !agreedPricingSwitcher ||
                            agreedPricingSwitcher === 1
                          }
                        >
                          <Option value="USD">USD</Option>
                          <Option value="EUR">EUR</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name={[dealKey, "streamHourlyRateRange", "start"]}
                        noStyle
                      >
                        <InputNumber
                          placeholder="20 -120"
                          style={{ width: "150px" }}
                          disabled={
                            !agreedPricingSwitcher ||
                            agreedPricingSwitcher === 1
                          }
                        />
                      </Form.Item>
                      <Form.Item
                        name={[dealKey, "streamHourlyRateRange", "end"]}
                        noStyle
                      >
                        <InputNumber
                          placeholder="20 -120"
                          style={{ width: "150px" }}
                          disabled={
                            !agreedPricingSwitcher ||
                            agreedPricingSwitcher === 1
                          }
                        />
                      </Form.Item>
                    </Input.Group>
                    <Form.Item
                      name={[dealKey, "streamHourlyRateRange", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox
                        className="cb-abs"
                        disabled={
                          !agreedPricingSwitcher || agreedPricingSwitcher === 1
                        }
                      >
                        Negotiable
                      </Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
                <Col
                  span={12}
                  style={{ paddingLeft: "4px" }}
                  hidden={!agreedPricingSwitcher || agreedPricingSwitcher === 2}
                >
                  <Form.Item
                    label="Campaign Sales Commissions *"
                    className="form-position"
                  >
                    <Input.Group compact>
                      <Form.Item
                        name={[dealKey, "streamCommissionUnit"]}
                        noStyle
                      >
                        <Select
                          defaultValue="%"
                          disabled={
                            !agreedPricingSwitcher ||
                            agreedPricingSwitcher === 2
                          }
                        >
                          <Option value="%">%</Option>
                          <Option value="Amount">Amount</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        rules={[
                          {
                            required: agreedPricingSwitcher !== 2,
                            message: "Campaign Sales Commissions is missing",
                          },
                          {
                            type: "number",
                            min: 0,
                            message:
                              "Campaign Sales Commissions must be number and more than zero",
                          },
                        ]}
                        name={[dealKey, "streamCommission", "description"]}
                        noStyle
                      >
                        <InputNumber
                          disabled={
                            !agreedPricingSwitcher ||
                            agreedPricingSwitcher === 2
                          }
                        />
                      </Form.Item>
                    </Input.Group>
                    <Form.Item
                      name={[dealKey, "streamCommission", "negiable"]}
                      noStyle
                      valuePropName="checked"
                    >
                      <Checkbox
                        className="cb-abs"
                        disabled={
                          !agreedPricingSwitcher || agreedPricingSwitcher === 2
                        }
                      >
                        Negotiable
                      </Checkbox>
                    </Form.Item>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
      ),
    },
  ];
  const next = async () => {
    try {
      await form.validateFields();
      const value = form.getFieldsValue();
      let newData = {};
      if (current < 2) {
        newData = {
          ...updatedData,
          ...value,
        };
      } else {
        newData = {
          ...updatedData,
          [dealKey]: {
            ...updatedData[dealKey],
            ...value[dealKey],
          },
        };
      }
      setUpdatedData(newData);
      if (current < 3) {
        setCurrent(current + 1);
      } else {
        await addOrUpdateCampaign(isEdit, newData);
        getDetailCampaignForRightProduct();
        if (currentProduct) {
          getListCampaigns({
            productCode: currentProduct.code,
            size: 5,
            page: 1,
          });
        }
        const msg = isEdit
          ? "Edited Campaign successfully"
          : "Created Campaign successfully";
        ReactUtils.messageSuccess({ content: msg });
        onOk();
      }
    } catch (e) {}
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const skip = () => {
    const value = form.getFieldsValue();
    let newData = {};
    if (current < 2) {
      newData = {
        ...updatedData,
        ...value,
      };
    } else {
      newData = {
        ...updatedData,
        [dealKey]: {
          ...updatedData[dealKey],
          ...value[dealKey],
        },
      };
    }
    setUpdatedData(newData);
    setCurrent(current + 1);
  };

  const customDot = (dot, { status, index }) => (
    <div className="customDot"></div>
  );

  console.log("st", steps[current]);
  return (
    <Modal
      title=""
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
      className="create-campaign-container"
      footer={null}
      width={900}
    >
      <div className="wrapper">
        <Steps
          size="small"
          current={current + 1}
          progressDot={customDot}
          style={{ margin: "20px 0 36px", width: "100%" }}
        >
          {Array(steps.length + 2)
            .fill()
            .map(() => (
              <Step />
            ))}
        </Steps>
        <div
          className={`${
            steps[current].title === "2"
              ? "steps-content steps-content-full"
              : ` ${
                  steps[current].title === "3"
                    ? "steps-content steps-content-full"
                    : ` ${
                        steps[current].title === "4"
                          ? "steps-content steps-content-full"
                          : "steps-content"
                      }`
                }`
          }`}
        >
          {steps[current].content}
        </div>
        <div
          className={`${
            steps[current].title === "2"
              ? "steps-action steps-action-full"
              : ` ${
                  steps[current].title === "3"
                    ? "steps-action steps-action-full2"
                    : ` ${
                        steps[current].title === "4"
                          ? "steps-action steps-action-full3"
                          : "steps-action"
                      }`
                }`
          }`}
        >
          {current < steps.length - 1 && current === 0 ? (
            <Row style={{ width: "100%" }}>
              <Col span={24}>
                <Form layout="vertical" style={{ display: "flex" }}>
                  <Form.Item>
                    <Button
                      onClick={next}
                      type="primary"
                      size="large"
                      className="cta continue"
                      block
                    >
                      Continue <IconArrowRightWhite />
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          ) : current === 3 ? (
            <Button
              onClick={next}
              size="large"
              className="cta control create"
              block
            >
              {isEdit ? "Save" : "Create"} <IconArrowRightWhite />
            </Button>
          ) : (
            <Form.Item>
              <Button
                onClick={next}
                type="primary"
                size="large"
                className="cta next"
                block
              >
                Next <IconArrowRightWhite />
              </Button>
            </Form.Item>
          )}
          {(current === 2 || current === 4) && (
            <Button size="large" className="cta skip" block onClick={skip}>
              Skip (if not applicable)
            </Button>
          )}
          {current > 0 && (
            <Button onClick={prev} size="large" className="cta prev" block>
              <IconArrowLeft /> Back
            </Button>
          )}
        </div>
      </div>
    </Modal>
  );
};

const mapStateToProps = ({
  campaign: { campaignDetail },
  detailProduct: { currentProduct },
}) => ({
  campaignDetail,
  currentProduct,
});

const mapDispatchToProps = {
  addOrUpdateCampaign,
  getDetailCampaign,
  getDetailCampaignForRightProduct,
  getListCampaigns,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateCampaign);
