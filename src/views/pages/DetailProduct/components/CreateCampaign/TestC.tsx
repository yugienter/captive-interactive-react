import { useDispatch, useSelector } from "react-redux";
import { doTest, getEmail } from "redux/testStore/testSlice";
interface IProps {
  name?: string;
}
const TestC = ({ name }: IProps) => {
  const dispatch = useDispatch();
  const getEmail1 = useSelector(getEmail);
  dispatch(doTest("thanh test"));
  return <h1>{getEmail1}</h1>;
};

export default TestC;
