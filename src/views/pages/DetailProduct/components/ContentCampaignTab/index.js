import React, { useState, useEffect, useMemo } from "react";
import { useHistory } from "react-router-dom";
import { Avatar, Pagination } from "antd";
import { connect } from "react-redux";
import moment from "moment";
import {
  IconAvatarDefault,
  IconAvatarGreyDefault,
  IconUsersNoti,
  IconUsers,
  IconLock,
  IconGreyLock,
  IconCreate,
} from "views/components/Icon/campaign";
import PannelTable from "views/components/PannelTable";
import CreateCampaign from "../CreateCampaign";
import { constants } from "helpers";

import { getListCampaigns } from "../actions";

const { dateFormat, router, CAMPAIGN_STATUS } = constants;

const ContentCampaignTab = ({ productCode, getListCampaigns, campaigns }) => {
  const history = useHistory();
  const [showBlock, setShowBlock] = useState(false);
  const [showPannelTable, setShowPannelTable] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [campaignId, setCampaignId] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  const [filter, setFilter] = useState({
    page: 1,
    size: 5,
  });

  const campaignComplete = useMemo(() => {
    return campaigns.data.filter(
      (x) => x.campaign.status === CAMPAIGN_STATUS.COMPLETE
    );
  }, [campaigns, productCode]);

  const currentCampaign = useMemo(() => {
    return campaigns.data.find(
      (x) => x.campaign.status !== CAMPAIGN_STATUS.COMPLETE
    );
  }, [campaigns, productCode]);

  useEffect(() => {
    getListCampaigns({ productCode, ...filter });
  }, [productCode, filter]);

  const onShowPannelTable = () => {
    setShowPannelTable(true);
  };

  const changePage = (page, pageSize) => {
    setFilter({ ...filter, page, size: pageSize });
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    setCampaignId(null);
    setIsEdit(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setCampaignId(null);
    setIsEdit(false);
  };

  const editCampaign = (edit) => {
    setIsEdit(!!edit);
    showModal();
  };

  const viewCampaign = (campaign = {}) => {
    history.push({
      pathname: router.viewCampaign.split(":")[0].concat(campaign.code),
    });
  };

  return (
    <div className="content-campaign-tab">
      <div className="wrapper">
        {currentCampaign ? (
          <div className="block-current" style={{ marginBottom: "26px" }}>
            <label className="title">Current</label>
            <div className="block-line">
              <div
                className="box-one"
                onClick={() => viewCampaign(currentCampaign)}
              >
                <Avatar src={<IconAvatarDefault />} />
                <div className="box-one-right">
                  <p>{currentCampaign.campaign?.name}</p>
                  <p>{currentCampaign.campaign?.description}</p>
                </div>
              </div>
              <div className="box-two">
                <div className="grey">
                  <IconUsersNoti />
                  {currentCampaign.bid && (
                    <>
                      {currentCampaign.bid?.count} <span> Pitch</span>
                    </>
                  )}
                </div>
              </div>
              <div className="box-three">
                <div className="green">
                  <IconLock />
                  Start on:{" "}
                  {currentCampaign.campaign &&
                    moment(currentCampaign.campaign.timeStart).format(
                      dateFormat
                    )}
                </div>
              </div>
              <div className="box-action">
                {currentCampaign.campaign.status ===
                  CAMPAIGN_STATUS.NOT_STARTED && (
                  <button
                    type="button"
                    className="edit"
                    onClick={() => editCampaign(true)}
                  >
                    Edit
                  </button>
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className="block-create">
            <h6>To connect more Sellers to your product:</h6>
            <button
              type="button"
              className="create"
              onClick={() => editCampaign(false)}
            >
              <IconCreate />
              {isEdit ? "Edit Campaign" : "Create Campaign"}
            </button>
          </div>
        )}

        <div className="block-completed">
          <label className="title">Completed</label>
          {campaignComplete.map((item) => {
            return (
              <div className="block-line" onClick={() => viewCampaign(item)}>
                <div className="box-one">
                  <Avatar src={<IconAvatarGreyDefault />} />
                  <div className="box-one-right">
                    <p>{item.campaign.name}</p>
                    <p>{item.campaign.description}</p>
                  </div>
                </div>
                <div className="box-two">
                  <div className="grey">
                    <IconUsers />
                    {item.bid && (
                      <>
                        {item.bid.count} <span> Pitch</span>
                      </>
                    )}
                  </div>
                </div>
                <div className="box-three">
                  <div className="grey">
                    <IconGreyLock />
                    {item.campaign &&
                      moment(item.campaign.timeStart).format(dateFormat)}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        {campaigns && (
          <div className="paging-main">
            <div className="spacing-item">
              Showing {filter.size * (filter.page - 1)} - {campaigns.total} of{" "}
              {campaigns.total} results
            </div>
            <Pagination
              defaultCurrent={filter.page}
              size="small"
              pageSize={filter.size}
              onChange={changePage}
              total={campaigns.total}
            />
          </div>
        )}
      </div>
      {showPannelTable && <PannelTable />}
      {isModalVisible && (
        <CreateCampaign
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          isEdit={isEdit}
        />
      )}
    </div>
  );
};

const mapStateToProps = ({
  detailProduct: {
    currentProduct: { code },
  },
  campaign: { campaigns },
}) => ({
  productCode: code,
  campaigns,
});

const mapDispatchToProps = {
  getListCampaigns,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContentCampaignTab);
