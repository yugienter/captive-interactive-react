import { Link } from "react-router-dom";
import { Form, Input, Button } from "antd";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import { IconEye } from 'views/components/Icon/authoring';
import { IconLock, IconRefresh } from 'views/components/Icon/basic' ;

const ResetPasswordHost = () => {
  return (
    <Page helmet="Reset Password" layout="split">
      <div className="primary-section w80">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            <h2>Reset Password</h2>
            <Form layout="vertical">
              <Form.Item
                label="New Password"
                name="password"
                rules={[{ required: true, message: 'Password is missing' },]}
              >
                <Input.Password
                  size="large"
                  placeholder="Enter password"
                  prefix={<IconLock />}
                  suffix={<IconEye />}
                />
              </Form.Item>
              <div style={{color: '#A5A3A9', fontWeight: 600, fontSize: '12px', margin: '-12px 0px 25px'}}>At least 8 characters<br /> Contains a number or symbol</div>
              <Form.Item>
                <Button type="primary" size="large" className="cta" block>
                  Reset my password <IconRefresh />
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section w20"></div>
    </Page>
  );
}

export default ResetPasswordHost;