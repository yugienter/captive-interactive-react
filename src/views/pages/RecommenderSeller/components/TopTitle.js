import React from 'react'
import { IconArrowLeft } from 'views/components/Icon/recommender'

const TopTitle = ({ history, productName }) => {
  const onBack = () => {
    history.goBack();
  }

  return (
    <div className='recommender-seller-top'>
      <h1>Recommended Sellers</h1>
      <h6>We recommend these sellers for “{productName}”</h6>
      <div className='auth-form'>
        <a onClick={onBack}><IconArrowLeft /></a>
      </div>
    </div>
  )
}

export default TopTitle
