import * as types from './actionTypes';

export const addSeller = (index, code) => (dispatch) => {
    dispatch({ type: types.ADD_SELLER, payload: { index, code } });
};

export const removeSeller = (index) => (dispatch) => {
    dispatch({ type: types.REMOVE_SELLER, payload: { index } });
};

export const resetSelectedSellers = () => (dispatch) => {
    dispatch({ type: types.RESET_SELECTED_SELLERS });
};