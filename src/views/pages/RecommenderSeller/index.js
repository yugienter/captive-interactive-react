import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Page from "views/components/Page";
import { constants } from "helpers";
import CardSeller from "../../components/CardSeller";
import TopTitle from "./components/TopTitle";
import { getRecommendedSellers } from "views/pages/DetailProduct/actions";
import { resetSelectedSellers } from "views/pages/RecommenderSeller/actions";
import { Button, Card, Checkbox } from "antd";
import {
  IconClose,
  IconSendOffer,
  IconSearch,
  ImageAvatar,
  Iconbaby,
  IconParenting,
  IconRating,
} from "views/components/Icon/recommender";
import { router } from "helpers";
import { Link } from "react-router-dom";
import { ImageDefaultSeller } from "views/components/Icon/recommender";
import DrawerSeller from "views/components/ModalRight";

const { userRoles } = constants;
const LIMIT = 6;
const { Meta } = Card;

const RecommenderSellerPage = ({
  history,
  isLoading,
  recommendedSellers,
  getRecommendedSellers,
  product,
  selectedSellers,
  resetSelectedSellers,
}) => {
  const sellers = recommendedSellers ? recommendedSellers.slice(0, LIMIT) : [];
  const [showDrawer, setShowDrawer] = useState(false);

  useEffect(() => {
    async function getSellers() {
      await getRecommendedSellers();
    }

    getSellers();
  }, []);

  useEffect(() => {
    return () => {
      resetSelectedSellers();
    };
  }, []);

  const unselect = () => {
    resetSelectedSellers();
  };

  const openSendOfferDrawler = () => {
    setShowDrawer(true);
  };

  const onCloseDrawer = () => {
    setShowDrawer(false);
  };

  const selectedSellersCount = selectedSellers.filter((item) => !!item).length;

  return isLoading ? (
    <div>Loading...</div>
  ) : (
    <Page
      helmet="Recommender Seller Page"
      alignTop
      requiredAccess={[userRoles.COMPANY]}
    >
      <div className="recommender-seller-page">
        <TopTitle history={history} productName={product.name} />
        <div className="recommender-seller-container">
          <div className="recommender-seller-container_content">
            {sellers.map((seller, i) => (
              <CardSeller
                index={i}
                key={seller.code}
                seller={seller}
                overlay={i === LIMIT - 1}
              />
            ))}
            <DrawerSeller
              showDrawer={showDrawer}
              onCloseDrawer={onCloseDrawer}
              isDirectOffer
            />

            <Card
              style={{ width: 325 }}
              className="recommender-card-border recommender-card-last"
              cover={
                <>
                  <div className="picture-cover__default picture-cover_first">
                    <ImageDefaultSeller />
                  </div>
                  <div className="rating">
                    <IconRating />
                    4.8
                  </div>
                  <div className="seller-check">
                    <Checkbox size="medium"></Checkbox>
                  </div>
                </>
              }
            >
              <Meta
                avatar={<ImageAvatar />}
                title="Bessie Cooper"
                description={
                  <>
                    <p>Age: 63</p>
                    <ul className="list-tags">
                      <li className="list-tags__item">
                        <Iconbaby />
                        Baby Goods
                      </li>
                      <li className="list-tags__item">
                        <IconParenting />
                        Baby Furniture ‘s
                      </li>
                      <li className="list-tags__item list-tags__item--num">
                        +1
                      </li>
                    </ul>
                    <hr style={{ borderColor: "#E8E8EA" }} />
                    <ul className="list-info">
                      <li className="list-info__item">
                        <div className="list-info__item--title">Exp. Years</div>
                        <div className="list-info__item--content">8+</div>
                      </li>
                      <li className="list-info__item">
                        <div className="list-info__item--title">Followers</div>
                        <div className="list-info__item--content">32.5k</div>
                      </li>
                      <li className="list-info__item">
                        <div className="list-info__item--title">
                          Hourly Rate
                        </div>
                        <div className="list-info__item--content">$150</div>
                      </li>
                    </ul>
                  </>
                }
              />
              <div className="overlay">
                <div className="overlay-box">
                  <h5>Want more results?</h5>
                  <p className="profiles">
                    999+ Seller profiles in Marketplace
                  </p>
                  <Link to={router.hostList}>
                    <Button type="primary" className="search">
                      <IconSearch />
                      Search
                    </Button>
                  </Link>
                </div>
              </div>
            </Card>
          </div>
          {!!selectedSellersCount && (
            <div className="recommender-seller-container_popup">
              <div className="lbl-selected">
                Selected Sellers: <span>{selectedSellersCount}</span>
              </div>
              <div className="group-buttons">
                <Button type="primary" className="unselect" onClick={unselect}>
                  <IconClose />
                  Unselect
                </Button>
                <Button
                  type="primary"
                  className="send-offer"
                  onClick={openSendOfferDrawler}
                >
                  <IconSendOffer />
                  Send Offer
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    </Page>
  );
};

const mapStateToProps = ({
  detailProduct: { isLoading, recommendedSellers, currentProduct },
  recommenderSeller: { selectedSellers },
}) => ({
  recommendedSellers,
  isLoading,
  product: currentProduct || {},
  selectedSellers,
});
const mapDispatchToProps = {
  getRecommendedSellers,
  resetSelectedSellers,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecommenderSellerPage);
