
import * as types from './actionTypes';

const initialState = {
    selectedSellers: []
};

const recommendedSellersReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.ADD_SELLER:
            const sellers = [...state.selectedSellers];
            sellers[payload.index] = payload.code;
            return {
                ...state,
                selectedSellers: sellers
            }
        case types.REMOVE_SELLER:
            return {
                ...state,
                selectedSellers: state.selectedSellers.map((item, index ) => {
                    return index === payload.index ? false : item;
                })
            }
        case types.RESET_SELECTED_SELLERS:
            return {
                ...state,
                selectedSellers: []
            }
        default:
            return state;
    }
};

export default recommendedSellersReducer;
