import React, { useRef, useState } from "react";
import Page from "views/components/Page";
import { Button } from "antd";
import readXlsxFile from "read-excel-file";

import { constants } from "helpers";
import { importData } from "./actions";

const { userRoles } = constants;

const ImportHostList = () => {
  const inputFile = useRef(null);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const clickInput = () => {
    inputFile.current.click();
  }

  const handleChange = async (event) => {
    const input = event.target;
    const rows = await readXlsxFile(input.files[0]);
    const data = rows.slice(2);
    setLoading(true);
    const result = await importData(data);
    if (result) {
      setLoading(false);
      setSuccess(true);
    }
  };

  let textStatus = 'Choose Host List File';
  if (loading) textStatus = 'Importing and sending email';
  if (success) textStatus = 'Success to Import and Send Email for Host'

  return (
    <Page
      helmet="Import Host list"
      selectedKeys={["import-host-list"]}
      alignTop
      requiredAccess={[userRoles.ADMIN]}
    >
      <div className="content">
        <h2>Import HostList by Excel File</h2>
        <Button
          style={{
            "display": "block",
            "margin": "auto",
            "width": "100%",
            "backgroundColor": "#282F39",
            "color": "white"
          }}
          onClick={clickInput}
          loading={loading}
          disabled={success}
        >
          {textStatus}
        </Button>
        <input
          type="file"
          name="ImportExcel"
          id="importData"
          ref={inputFile}
          onChange={handleChange}
          accept=".xls,.xlsx,.csv"
          hidden
        />
      </div>
    </Page>
  );
};

export default ImportHostList;
