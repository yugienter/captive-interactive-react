/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 06/09/2021
 */

import { constants, client } from "helpers";

import _ from "lodash";

const { api } = constants;

const _splitString = (str = "") => {
  return _(str)
    .split(",")
    .map((val) => val.trim())
    .compact()
    .value();
};

const _mapDataHostExcel = (row = []) => {
  const aboutMe = row[13]
    .split('\n')
    .map(text => `<p>${text}</p>`).join("");
  return {
    name: row[0],
    age: row[1],
    sex: row[2],
    weight: row[3],
    height: row[4],
    social: {
      facebook: row[16],
      instagram: row[17],
      tiktok: row[18],
    },
    facebookFollowers: row[5],
    instagramFollowers: row[6],
    tiktokFollowers: row[7],
    phoneNumber: row[8],
    email: row[9],
    photo: _splitString(row[10]),
    rates: row[11],
    experience: row[12],
    aboutMe,
    porfolio: row[14],
    tags: _splitString(row[15] ? row[15].toLowerCase() : row[15]),
    title: row[19],
    avatar: row[20],
  };
};

export const importData = async (rows = []) => {
  try {
    const res = await client.request({
      path: api.path.importHosts,
      method: "post",
      data: { hosts: rows.map(_mapDataHostExcel) },
    });

    const { data } = res.data;
    return { message: data.message, success: true };
  } catch (error) {
    return { message: error.message, success: false };
  }
};
