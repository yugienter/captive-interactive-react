import React, { useState } from 'react';
import Page from "views/components/Page";
import { constants } from "helpers";
import { Link, useHistory } from "react-router-dom";
import { IconBack } from "views/components/Icon/product";
import TagMarket from "views/components/TagMarket";
import ListFilter from "views/components/ListFilter";
import { connect, useDispatch } from 'react-redux';
import { getFeaturedProduct } from '../../pages/Marketplace/actions'
import { useEffect } from "react";
import _ from "lodash";
import moment from "moment"
import { utils } from 'helpers'
import * as type from "../Marketplace/actionTypes"

const { getMediaUrl } = utils

const { userRoles, router } = constants;

function FeaturedProducts({ getFeaturedProduct, isUpdating, products }) {
    const dispatch = useDispatch()
    const history = useHistory()
    const [category, setCategory] = useState()

    useEffect(async () => {
        await getFeaturedProduct()
    }, [])

    useEffect(async () => {
        if (category) {
            getFeaturedProduct({ categories: category })
        }
        else {
            getFeaturedProduct()
        }
    }, [category])

    const getDiffDate = (now, then) => {
        return `${moment(now).diff(moment(then), 'days')} days`
    }
    return (
        <Page
            helmet="Featured Products"
            alignTop
            selectedKeys={["featured-products"]}
            requiredAccess={[userRoles.HOST]}
            loadingPage={isUpdating}
        >
            <div className="featured-products market ">
                <div className="featured-products__header">
                    <div>
                        <Link to={router.marketPlace}><IconBack /></Link>
                        <h2>All Products</h2>
                    </div>
                </div>
                <div className="featured-products__content maket-product">
                    <ListFilter onChooseTag={(e) => {
                        setCategory(e)
                    }} />
                    <div className="market-product__list">
                        {
                            products && products.map((item, index) => (
                                <div onClick={() => {
                                    history.push(`/detail-campaign/${_.get(item, 'jobCode')}`)
                                    dispatch({
                                        type: type.SET_SELECTED_PRODUCT,
                                        payload: _.get(item, 'product')
                                    })
                                }} className="item" key={index}>
                                    <div className="item__bg" style={{ backgroundImage: `url(${getMediaUrl(_.get(item, 'product.media[0].url'))})` }}>
                                        <div>
                                            {_.get(item, 'campaign.status') === 'complete' && <div className="item__tag blue">Pre-Streaming</div>}
                                            {_.get(item, 'campaign.submitted') === 'complete' && <div className="item__tag gray">Bid Submitted</div>}
                                            {_.get(item, 'campaign.negotiating') === 'complete' && <div className="item__tag orange">Dealing</div>}
                                            {!_.get(item, 'campaign.status') && <div className="item__tag gray">Job invited</div>}
                                        </div>
                                        <div>
                                            <TagMarket isHideFixedRate={_.get(item, 'hourlyRateRange.negiable')}
                                                isHideComission={!_.get(item, 'commission.negiable')} />
                                            <div className="bid">
                                                <span>{_.get(item, 'big') || 0}</span>
                                                <span>Pitch</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="item__content">
                                        <div>
                                            <div className="avatar">
                                                <img src={getMediaUrl(_.get(item, 'company.logo'))}></img>
                                                <span>{_.get(item, 'company.name')}</span>
                                            </div>
                                            <h3>{_.get(item, 'product.name')}</h3>
                                        </div>
                                        <div>
                                            <span>
                                                <div>Ending in</div>
                                                <div>{getDiffDate(_.get(item, 'campaign.timeEnd'), _.get(item, 'campaign.timeStart'))}</div>
                                            </span>
                                            <span>
                                                <div>Hourly Rate</div>
                                                <div>{'$' + (_.get(item, 'hourlyRateRange.start') || 0) + ' - ' + '$' + (_.get(item, 'hourlyRateRange.end') || 0)}</div>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            ))}
                    </div>
                </div>
            </div>
        </Page>
    );
}
const mapDispatchToProps = {
    getFeaturedProduct
};

const mapStateToProps = ({ marketplace }) => ({
    products: _.get(marketplace, 'featuredProduct.data') || [],
    isUpdating: marketplace.isUpdating,
});

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedProducts);