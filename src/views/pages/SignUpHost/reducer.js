import * as types from './actionTypes';

const initialState = {
  isOnSignUp: false,
  errorMessage: ''
};

const signUpReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ON_SIGN_UP:
      return { ...state, isOnSignUp: action.payload };

    case types.UPDATE_SIGN_UP_ERR_MSG:
      return { ...state, errorMessage: action.payload };

    default:
      return state;
  }
};

export default signUpReducer;
