import { constants, client } from "helpers";

import * as types from "./actionTypes";

const { api, router } = constants;
const { statusCode } = api;

const _handleSignUpResponse = (res, history) => (dispatch) => {
  dispatch({ type: types.UPDATE_SIGN_UP_ERR_MSG, payload: "" });

  if (res instanceof Error) {
    const error = "Network Error";
    return dispatch({ type: types.UPDATE_SIGN_UP_ERR_MSG, payload: error });
  }

  const { status, data } = res;
  if (!status || status !== statusCode.SUCCESS_) {
    const { message } = data;
    const error = message || "Internal Server Error";
    return dispatch({ type: types.UPDATE_SIGN_UP_ERR_MSG, payload: error });
  }

  return history.replace(router.thanks);
};

export const doSignUp = (data, history) => async (dispatch) => {
  try {
    dispatch({ type: types.SET_ON_SIGN_UP, payload: true });
    const { role } = data;
    if (role) {
      localStorage.setItem("roleSignUp", `${role}`);
    }

    const res = await client.request({
      path: api.path.signUp,
      method: "post",
      data,
    });

    return _handleSignUpResponse(res, history)(dispatch);
  } catch (err) {
    console.log("[doSignUp]", err);
    return _handleSignUpResponse(err)(dispatch);
  } finally {
    dispatch({ type: types.SET_ON_SIGN_UP, payload: false });
  }
};
