import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from "react-router-dom";
import { Form, Input, Checkbox, Button } from "antd";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import InfluencdeSlider from "views/components/InfluencdeSlider";
import PolicyModal from "views/components/PolociModal/_policyModal";
import { IconLock, IconArrowRightWhite, IconUserName } from 'views/components/Icon/basic'
import { IconEnvelope } from 'views/components/Icon/essentials'
import { IconEye } from 'views/components/Icon/authoring'
import { ReactUtils, constants } from 'helpers';
import { doSignUp } from './actions';

const { userRoles } = constants;

const SignUpHost = ({ doSignUp, history, errorMessage }) => {

  const [policyVisible, setPolicyVisible] = useState(false);
  const [message, setMessage] = useState(errorMessage);

  useEffect(() => {
    setMessage(errorMessage);
  }, [errorMessage, setMessage]);

  const closePolicyModal = () => {
    setPolicyVisible(false)
  }

  const openPolicyModal = () => {
    setPolicyVisible(true);
  }

  const onFinish = async (values) => {
    await doSignUp({
      name: values.name,
      email: values.email,
      password: values.password,
      role: userRoles.HOST
    }, history);
  }

  return (
    <Page helmet="Sign Up" layout="split">
      <div className="primary-section w50">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            <h2>Sign Up</h2>
            <p>
              Already have an account? <Link to="sign-in/host">Log in</Link>
            </p>
            <Form
              layout="vertical"
              onFinish={onFinish}
            >
              <Form.Item
                label="Name"
                name="name"
                className='cb-term-condition'
                rules={[{ required: true, message: 'Name is missing' },]}
              >
                <Input
                  size="large"
                  prefix={<IconUserName />}
                  placeholder="e.g. Brian Chen"
                />
              </Form.Item>
              <Form.Item
                label="Email"
                name="email"
                className='cb-term-condition'
                rules={[
                  { required: true, message: 'Email is missing' },
                  { validator: ReactUtils.emailValidator }
                ]}
              >
                <Input
                  onChange={() => setMessage(null)}
                  size="large"
                  prefix={<IconEnvelope />}
                  placeholder="Enter your email address"
                />
              </Form.Item>
              <Form.Item
                label="Password"
                name="password"
                className='cb-term-condition'
                help="At least 8 characters. Contains a number or symbol."
                rules={[
                  { required: true, message: 'Password is missing' },
                  { validator: ReactUtils.passwordVaidator }
                ]}
              >
                <Input.Password
                  size="large"
                  placeholder="Enter your password"
                  prefix={<IconLock />}
                  suffix={<IconEye />}
                />
              </Form.Item>
              <Form.Item
                name="isAgree"
                valuePropName="checked"
                className='cb-term-condition'
                rules={[
                  { validator: (rule, value, callback) => {
                    if (!value) {
                      callback('Please read and accept the Terms and Conditions');
                    } else {
                      callback();
                    }
                  }}
                ]}
              >
                <Checkbox style={{marginTop: '20px'}}>
                  I agree to the <Link onClick={openPolicyModal}>Terms &amp; Conditions</Link>
                </Checkbox>
              </Form.Item>
              <Form.Item>
                <Button type="primary" size="large" className="cta" block htmlType="submit">
                  Start Platform <IconArrowRightWhite />
                </Button>
              </Form.Item>
              {message &&
                <p style={{ color: "red" }}>{message}</p>
              }
            </Form>
            {/* <div className="third-party">
              <p>Or Continue with:</p>
              <Button icon={<IconGoogle />} size="large" />
              <Button icon={<IconFacbook />} size="large" />
            </div> */}
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section-host w50">
        <InfluencdeSlider />
      </div>
      <PolicyModal visible={policyVisible} handleClose={closePolicyModal} />
    </Page>
  );
}

const mapStateToProps = ({ signUp }) => ({
  errorMessage: signUp.errorMessage,
});

const mapDispatchToProps = {
  doSignUp,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SignUpHost)
);