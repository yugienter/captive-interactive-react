import React, { useEffect, useState } from 'react';
import Page from "views/components/Page";
import { constants } from "helpers";
import { Link } from "react-router-dom";
import { IconBack, IconArrowRight } from "views/components/Icon/product";
import InfoProduct from "views/pages/DetailProduct/part/InfoProduct"
import { Row, Col, Button } from "antd"
import CampaignBidCard from "views/components/CampaignBidCard";
import { connect, useDispatch } from "react-redux";
import _ from "lodash"
import { getListDeal, getCampaignCardByProductAndSeller } from "../Marketplace/actions"
import { useParams } from "react-router-dom";
import ContentDeal from "views/components/CollapseSellerActivity/ContentDeal";
import MyJobsDrawer from 'views/components/MyJobsDrawer';
import * as types from './../MyJobs/actionTypes';
import DrawerBidJob from "views/components/DrawerBidJob"

const { userRoles, router } = constants;


function CampaignDetail({ isUpdating, getListDeal, trending, listDeal, product, getCampaignCardByProductAndSeller, campaign }) {
    let { code } = useParams();
    const [open, setOpen] = useState(false)
    const [activitiesDrawer, setActivitesDrawer] = useState(false)
    const dispatch = useDispatch()

    useEffect(async () => {
        if (code) {
            await getListDeal(code)
            await getCampaignCardByProductAndSeller({ jobCode: code, productCode: _.get(product, 'code') })
        }
    }, [code])

    return (
        <Page
            loadingPage={isUpdating}
            helmet="Campaign Detail"
            alignTop
            selectedKeys={["detail-campaign"]}
            requiredAccess={[userRoles.HOST]}
        >
            <div className="campaigns detail-product">
                <div className="campaigns__header">
                    <div>
                        <Link to={router.marketPlace}><IconBack /></Link>
                        <h2>{_.get(product, 'name')}</h2>
                    </div>
                </div>
                <Row>
                    <Col span={12}>
                        <div className="product-info">
                            <InfoProduct product={product} />
                        </div>
                    </Col>
                    <Col span={12}>
                        <div style={{
                            display: 'flex',
                            justifyContent: 'flex-end',
                            marginTop: 35,
                            paddingRight: 30,
                        }}>
                            {_.get(listDeal, 'data') && _.get(listDeal, 'data.length') > 0 ?
                                <div className="collapse-main">
                                    <ContentDeal deal={_.get(listDeal, 'data[0]')} />
                                    <Button className="view-activity-button" onClick={() => {
                                        setActivitesDrawer(true)
                                        dispatch({
                                            type: types.SET_DEALS,
                                            payload: _.get(listDeal, 'data'),
                                        });
                                    }}><IconArrowRight /> View all activities</Button>
                                </div> :
                                <CampaignBidCard onBid={() => {
                                    setOpen(true)
                                }} isShowBidButton={true} />}
                        </div>
                    </Col>
                </Row>
            </div>
            <MyJobsDrawer showDrawer={activitiesDrawer} onCloseDrawer={() => { setActivitesDrawer(false) }} />
            <DrawerBidJob
                jobCode={code}
                visible={open}
                handleClose={async () => {
                    await getListDeal(code)
                    setOpen(false)
                }} />
        </Page>
    );
}

const mapStateToProps = ({ marketplace }) => ({
    isUpdating: marketplace.isUpdating,
    trending: marketplace.productTrending || {},
    listDeal: marketplace.listDeal,
    product: marketplace.selectedProduct,
    campaign: marketplace.campaign
});

const mapDispatchToProps = {
    getListDeal,
    getCampaignCardByProductAndSeller,
};

export default connect(mapStateToProps, mapDispatchToProps)(CampaignDetail);
