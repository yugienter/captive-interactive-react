/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import { client, constants } from 'helpers';
import * as types from './actionTypes';

const { api } = constants;

export const verifyToken = token => async dispatch => {
  dispatch({ type: types.VERYFY_TOKEN });
  const apiPath = api.path.verify(token);

  try {
    const { data } = await client.api({
      path: apiPath,
      method: 'GET',
    });
    dispatch({ type: types.VERYFY_TOKEN_SUCCESS });
    const { accessToken: token } = data.data;
    localStorage.setItem('token', `Bearer ${token}`);
    return true;
  } catch (err) {
    if (err.response) {
      if (err.response.status >= 400) {
        dispatch({
          type: types.VERYFY_TOKEN_FAIL,
          message: err.response.data.message
        });
      }
    }
  }
};

export const createPassword = data => async dispatch => {
  try {
    dispatch({ type: types.CREATE_PASSWORD });
    const res = await client.request({
      path: api.path.createPassword,
      method: 'POST',
      data
    });
    dispatch({ type: types.CREATE_PASSWORD_SUCCESS });
    const { data: success } = res.data;
    return success;
  } catch (err) {
    if (err.response) {
      if (err.response.status >= 400) {
        dispatch({
          type: types.CREATE_PASSWORD_FAIL,
          message: err.response.data.message
        });
      }
    }
  }
};