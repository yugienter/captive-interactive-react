/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import React from 'react';
import { connect } from 'react-redux';
import qs from 'query-string';
import { Link } from 'react-router-dom';
import { verifyToken, createPassword } from './actions';
import { Form, Input, Checkbox, Button } from "antd";
import {
  LockOutlined,
  EyeOutlined,
  ArrowRightOutlined,
} from "@ant-design/icons";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import WelcomeSlider from "views/components/WelcomeSlider";
import PolicyModal from 'views/components/PolociModal/_policyModal';
import { constants, router, utils } from 'helpers';

const { userRoles } = constants
const { BRAND, HOST, ADMIN } = userRoles;

class CreatePassword extends React.Component {
  state = {
    isInvalidPassword: false,
    agreeTerms: false,
    password: '',
    step: 'changePassword',

    policyVisible: false,
  };

  componentDidMount() {
    const { location, history } = this.props;
    const params = qs.parse(location.search);
    const { token, role, email } = params;
    if (!token || !role || ![BRAND, HOST, ADMIN].includes(role) || !email) {
      return history.push(router.home);
    }
    sessionStorage.setItem('create-password-token', token);
    sessionStorage.setItem('create-password-role', role);
    sessionStorage.setItem('create-password-email', email);
    history.push({
      pathname: router.createPassword,
      search: null,
    });
  }

  onFinish = async (value) => {
    const { history } = this.props;

    const token = sessionStorage.getItem('create-password-token');
    const role = sessionStorage.getItem('create-password-role');
    const email = sessionStorage.getItem('create-password-email');
    const { password } = value;

    // verify token
    const { verifyToken, createPassword } = this.props;
    const verify = await verifyToken(token);
    if (verify) {
      sessionStorage.removeItem('create-password-token');
      // create password
      const result = await createPassword({
        email,
        password,
        role,
      })
      if (result) {
        sessionStorage.removeItem('create-password-role');
        sessionStorage.removeItem('create-password-email');
        history.push(router.selectRole)
      }
    }
  }

  onFinishFailed = (errorInfo) => {
    console.log('errorInfo', errorInfo);
  }

  passwordVaidation = (rule, value, callback) => {
    const isInvalidPassword = !utils.validatePassword(value);
    if (value.length < 8 || isInvalidPassword) {
      callback('8 or more characters, at least one number and one letter');
    } else {
      callback();
    }
  };

  onCheckboxChange = (e) => {
    this.setState({ agreeTerms: e.target.checked });
  }

  termsValidation = (rule, value, callback) => {
    const { agreeTerms } = this.state;
    if (agreeTerms) {
      return callback();
    }
    return callback("Please read and accept the terms and conditions");
  };

  openPolicyModal = () => {
    this.setState({ policyVisible: true });
  }

  closePolicyModal = () => {
    this.setState({ policyVisible: false });
  }

  render() {
    const { isProcessing, errorMessage } = this.props;
    const { agreeTerms, policyVisible } = this.state;
    return (
      <Page helmet="Create Password" layout="split">
        <div className="primary-section">
          <div className="header">
            <LogoFull />
          </div>
          <div className="content">
            <div className="auth-form">
              <h2>Create Password</h2>
              <p>
                Already have an account? <Link to="sign-in">Sign in</Link>
              </p>
              <Form
                layout="vertical"
                requiredMark={false}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
              >
                <Form.Item
                  label="Password"
                  name="password"
                  rules={[
                    { required: true, message: 'Password must be filed out' },
                    { validator: this.passwordVaidation }
                  ]}
                  help="8 or more characters, at least one number and one letter"
                >
                  <Input.Password
                    size="large"
                    prefix={<LockOutlined />}
                    suffix={<EyeOutlined />}
                  />
                </Form.Item>
                <Form.Item
                  name="agree"
                  rules={[{ validator: this.termsValidation }]}
                >
                  <Checkbox
                    checked={agreeTerms}
                    onChange={this.onCheckboxChange}
                  >
                    I agree to the
                    <Link onClick={this.openPolicyModal}>Terms &amp; Conditions</Link>
                  </Checkbox>
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    size="large"
                    htmlType="submit"
                    className="cta"
                    loading={isProcessing}
                    block
                  >
                    Start Platform
                    <ArrowRightOutlined />
                  </Button>
                </Form.Item>
                {
                  errorMessage &&
                  <p style={{ fontWeight: '100', color: 'red' }}>
                    {errorMessage}
                  </p>
                }
              </Form>
            </div>
          </div>
          <div className="footer">
            © 2021 SCX. All Rights Reserved.
          </div>
        </div>
        <div className="secondary-section">
          <WelcomeSlider />
        </div>
        <PolicyModal visible={policyVisible} handleClose={this.closePolicyModal} />
      </Page >
    )
  }
}

const mapStateToProps = state => ({
  isProcessing: state.createPassword.isProcessing,
  errorMessage: state.createPassword.message
});

const mapDispatchToProps = {
  verifyToken,
  createPassword,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatePassword);
