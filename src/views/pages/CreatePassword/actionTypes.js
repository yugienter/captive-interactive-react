/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

const REDUCER = 'create_password';

export const VERYFY_TOKEN = `@@${REDUCER}/VERYFY_TOKEN`;
export const VERYFY_TOKEN_SUCCESS = `@@${REDUCER}/VERYFY_TOKEN_SUCCESS`;
export const VERYFY_TOKEN_FAIL = `@@${REDUCER}/VERYFY_TOKEN_FAIL`;

export const CREATE_PASSWORD = `@@${REDUCER}/CREATE_PASSWORD`;
export const CREATE_PASSWORD_SUCCESS = `@@${REDUCER}/CREATE_PASSWORD_SUCCESS`;
export const CREATE_PASSWORD_FAIL = `@@${REDUCER}/CREATE_PASSWORD_FAIL`;

