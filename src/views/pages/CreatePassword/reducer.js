/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import * as types from './actionTypes';

const initialState = {
  message: null,
  isProceeesing: false,
};

const createPasswordReducer = (state = initialState, action) => {
  switch (action.type) {

    case types.VERYFY_TOKEN:
      return { ...state, message: null, isProceeesing: true };
    case types.VERYFY_TOKEN_SUCCESS:
      return { ...state, message: null, isProceeesing: false };
    case types.VERYFY_TOKEN_FAIL:
      return { ...state, message: action.message, isProceeesing: false };

    case types.CREATE_PASSWORD:
      return { ...state, message: null, isProceeesing: true };
    case types.CREATE_PASSWORD_SUCCESS:
      return { ...state, message: null, isProceeesing: false };
    case types.CREATE_PASSWORD_FAIL:
      return { ...state, message: action.message, isProceeesing: false };

    default:
      return state;
  }
};

export default createPasswordReducer;
