import React, { useState } from "react";
import Page from "views/components/Page";
import { constants } from "helpers";
import { Link, useHistory } from "react-router-dom";
import { IconBack } from "views/components/Icon/product";
import TagMarket from "views/components/TagMarket";
import ListFilter from "views/components/ListFilter";
import { connect, useDispatch } from "react-redux";
import { getOpenBidList } from "../../pages/Marketplace/actions";
import { useEffect } from "react";
import _ from "lodash";
import moment from "moment";
import { utils } from "helpers";
import * as type from "../Marketplace/actionTypes";

const { getMediaUrl } = utils;

const { userRoles, router } = constants;

function CampaignList({ getOpenBidList, campaigns, isUpdating }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const [category, setCategory] = useState();

  useEffect(async () => {
    await getOpenBidList();
  }, []);

  useEffect(async () => {
    if (category) {
      getOpenBidList({ categories: category });
    } else {
      getOpenBidList();
    }
  }, [category]);

  const getDiffDate = (now, then) => {
    return `${moment(now).diff(moment(then), "days")} days`;
  };
  return (
    <Page
      helmet="Campaign List"
      alignTop
      selectedKeys={["campaigns"]}
      requiredAccess={[userRoles.HOST]}
      loadingPage={isUpdating}
    >
      <div className="campaigns market">
        <div className="campaigns__header">
          <div>
            <Link to={router.marketPlace}>
              <IconBack />
            </Link>
            <h2>All Campaigns</h2>
          </div>
        </div>
        <div className="campaigns__content market-bid">
          <ListFilter
            onChooseTag={(e) => {
              setCategory(e);
            }}
          />
          <div className="market-product__list">
            <div className="market-bid__product">
              {campaigns &&
                campaigns.map((item, index) => (
                  <div
                    onClick={() => {
                      history.push(
                        `/detail-campaign/${_.get(item, "jobCode")}`
                      );
                      dispatch({
                        type: type.SET_SELECTED_PRODUCT,
                        payload: _.get(item, "product"),
                      });
                    }}
                    className="market-bid__product-item"
                    style={{
                      backgroundImage: `url(${getMediaUrl(
                        _.get(item, "product.media[0].url")
                      )})`,
                    }}
                    key={index}
                  >
                    <div className="market-bid__product-item__header">
                      <div className="header__avatar">
                        <span>
                          <img
                            src={getMediaUrl(_.get(item, "company.logo"))}
                            alt="avatar"
                          />
                        </span>
                        <span>{_.get(item, "company.name")}</span>
                      </div>
                      {_.get(item, "submit") && (
                        <div className="header__tag">Pitch Submission</div>
                      )}
                    </div>
                    <div className="market-bid__product-item__content">
                      <TagMarket
                        isHideFixedRate={true}
                        isHideComission={!_.get(item, "commission.negiable")}
                      />
                      <div className="title">{_.get(item, "product.name")}</div>
                      <div className="infor">
                        <div>
                          <div className="infor-title">Ending in</div>
                          <div className="infor-content">
                            {getDiffDate(
                              _.get(item, "campaign.timeEnd"),
                              _.get(item, "campaign.timeStart")
                            )}
                          </div>
                        </div>
                        <div>
                          <div className="infor-title">Fixed Rate</div>
                          <div className="infor-content">
                            {"$" +
                              _.get(item, "hourlyRateRange.start") +
                              " - " +
                              "$" +
                              _.get(item, "hourlyRateRange.end")}
                          </div>
                        </div>
                        <div>
                          <div className="infor-title">Pitch</div>
                          <div className="infor-content">
                            {_.get(item, "big")}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </Page>
  );
}

const mapDispatchToProps = {
  getOpenBidList,
};

const mapStateToProps = ({ marketplace }) => ({
  campaigns: _.get(marketplace, "openBid.data") || [],
  isUpdating: marketplace.isUpdating,
});

export default connect(mapStateToProps, mapDispatchToProps)(CampaignList);
