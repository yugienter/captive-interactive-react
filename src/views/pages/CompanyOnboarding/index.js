import React, { useState, useEffect, useRef } from 'react';
import { Row, Col, Steps, Button, Form } from 'antd';
import LogoFull from "views/components/Logo/LogoFull";
import Page from "views/components/Page";
import Overview from "../../components/CompanyOnboarding/Overview"
import CompanyCategories from "../../components/CompanyOnboarding/CompanyCategories"
import CreateBrand from "../../components/CompanyOnboarding/CreateBrand"
import BrandDetail from "../../components/CompanyOnboarding/BrandDetail"
import { IconArrowRightWhite } from 'views/components/Icon/basic'
import { IconArrowLeft } from 'views/components/Icon/pages'
import { getTags } from 'views/pages/Host/actions';
import { connect } from 'react-redux';
import { uploadMedia, updateCompanyLogo, createCompany, fetchCompany, updateCompany, createBrand, getBrand, updateBrand, getCompany, resetError } from '../Company/actions'
import { ReactUtils, router } from 'helpers';

const { Step } = Steps;
const fallbackDelay = 500;

function CompanyOnboarding(props) {
    const {
        history,
        getTags,
        tags,
        user,
        updateCompanyLogo,
        company,
        createCompany,
        fetchCompany,
        updateCompany,
        createBrand,
        getBrand,
        updateBrand,
        brand,
        isUpdating,
        errorMsg,
        resetError
    } = props;

    const next = async () => {
        resetError()
        if (current === 3) {
            ReactUtils.messageSuccess({ content: 'Update company sucessfully' });
            history.replace(router.products);
            return;
        }
        setCurrent(current + 1);
    };

    const skip = async () => {
        resetError()
        if (current > 1) {
            ReactUtils.messageSuccess({ content: 'Update company sucessfully' });
            history.replace(router.products);
            return;
        }
        setCurrent(current + 1);
    };

    const stepProps = {
        uploadMedia,
        tags,
        user,
        updateCompanyLogo,
        company,
        createCompany,
        fetchCompany,
        updateCompany,
        createBrand,
        getBrand,
        updateBrand,
        brand,
        errorMsg,
        resetError,
        next,
        skip
    };

    const [current, setCurrent] = useState(0)

    useEffect(() => {
        getTags();
    }, [getTags])

    useEffect(() => {
        getCompany()
        resetError()
    }, [])

    const stepRefs = [
        useRef(),
        useRef(),
        useRef(),
        useRef(),
    ]

    const steps = [
        {
            title: '0',
            content: <Overview ref={stepRefs[0]} {...stepProps} />,
        },
        {
            title: '1',
            content: <CompanyCategories ref={stepRefs[1]} {...stepProps} />,
        },
        {
            title: '2',
            content: <CreateBrand ref={stepRefs[2]} {...stepProps} />,
        },
        {
            title: '3',
            content: <BrandDetail ref={stepRefs[3]} {...stepProps} />,
        },
    ];

    const customDot = (dot, { status, index }) => (
        <div className='customDot'></div>
    );

    const prev = () => {
        setCurrent(current - 1);
        resetError()
    };

    const onContinue = () => {
        const ref = stepRefs[current].current;
        if (ref && ref.onContinue) {
            ref.onContinue();
        }
    }


    return (
        <Page helmet="Company onboarding" layout="split" loadingPage={isUpdating}>
            <div className="primary-section w80 flow-signup">
                <div className="header w700">
                    <LogoFull />
                </div>
                <div className="content w700 jc-fs">
                    <div className="auth-form">
                        <Steps size="small" current={current + 1} progressDot={customDot} style={{ margin: '55px 0 36px' }}>
                            {Array(steps.length + 2).fill().map(() => (
                                <Step />
                            ))}
                        </Steps>
                        <div className="steps-content">{steps[current].content}</div>
                        <div className="steps-action">
                            {current < steps.length - 1 && current === 0 ? (
                                <Row style={{ width: '100%' }}>
                                    <Col span={8}></Col>
                                    <Col span={14}>
                                        <Form layout="vertical" style={{ display: 'flex' }}>
                                            <Form.Item>
                                                <Button onClick={onContinue} type="primary" size="large" className="cta continue" block>
                                                    Continue <IconArrowRightWhite />
                                                </Button>
                                            </Form.Item>
                                        </Form>
                                    </Col>
                                </Row>
                            ) : (
                                    current === 3 ? (
                                        <Button onClick={onContinue} size="large" className="cta control next" block>
                                            Finish <IconArrowRightWhite />
                                        </Button>
                                    ) : (
                                            <Form.Item>
                                                <Button onClick={onContinue} type="primary" size="large" className="cta control next" block>
                                                    Next <IconArrowRightWhite />
                                                </Button>
                                            </Form.Item>
                                        )
                                )}
                            {current > 0 && (
                                <Button size="large" className="cta control skip" block onClick={skip} >
                                    Skip
                                </Button>
                            )}
                            {current > 0 && (
                                <Button onClick={prev} type="primary" size="large" className="cta control prev" block>
                                    <IconArrowLeft /> Back
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
                <div className="footer">
                    © 2021 Socom Exchange. All Rights Reserved.
        </div>
            </div>
            <div className="secondary-section w20"></div>
        </Page>
    );
}

const mapStateToProps = ({ common, hostList, company }) => ({
    user: common.user,
    tags: hostList.tags,
    tagsById: hostList.tagsById,
    isUpdating: company.isUpdating,
    company: company.data,
    brand: company.selectedBrand,
    errorMsg: company.errorMsg
});

const mapDispatchToProps = {
    getTags,
    updateCompanyLogo,
    createCompany,
    fetchCompany,
    updateCompany,
    createBrand,
    getBrand,
    updateBrand,
    resetError
};


export default connect(mapStateToProps, mapDispatchToProps)(CompanyOnboarding);