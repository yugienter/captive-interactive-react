import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import { IconArrRightCircle, ImageCube } from "views/components/Icon/pages";
import { constants } from "helpers";

const { router } = constants;

const Thanks = ({ history }) => {
  const [count, setCount] = useState(15);
  const roleSignUp = localStorage.getItem("roleSignUp") || "host";
  const ROUTER = router.signIn.replace(":role", roleSignUp);

  useEffect(() => {
    const id = setInterval(() => {
      setCount((c) => c - 1);
    }, 1000);
    return () => clearInterval(id);
  }, []);

  useEffect(() => {
    if (count === 0) {
      history.replace(ROUTER);
    }
  }, [count, history]);

  return (
    <Page helmet="Thank You" layout="split">
      <div className="primary-section w80">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            <ImageCube />
            <h2>Thanks for signing up!</h2>
            <div className="text-bold mt4">
              Please check the entered email and click{" "}
              <Link className="link-bold">Verify Link</Link> to start using the
              plarform.
            </div>
            <div className="text-normal mt48">
              <IconArrRightCircle /> Redirect to <Link to={ROUTER}>Log in</Link>{" "}
              page in <strong>{count}</strong> seconds
            </div>
          </div>
        </div>
        <div className="footer">© 2021 SCX. All Rights Reserved.</div>
      </div>
      <div className="secondary-section w20"></div>
    </Page>
  );
};

export default Thanks;
