import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Row, Col, Input, Form, Upload, message, Image, Tag, DatePicker, Checkbox } from "antd";
import { CountryDropdown } from 'react-country-region-selector';
import {
  IconNumber, IconFlagAlt, IconCalendar, IconPorfolio, IconOverviews,
  IconUpload, IconHome
} from 'views/components/Icon/pages'
import HostTag from 'views/pages/Host/parts/HostTag';
import { updateHostProfile } from 'views/pages/HostProfile/actions';
import MenuScroll from '../MenuScroll';
import ReactPhoneInput from "react-phone-input-2";
import UploadMedia from 'views/components/UploadMedia';
import { uploadMedia } from 'views/pages/HostProfile/actions';
import { LoadingOutlined } from '@ant-design/icons';
import { constants, ReactUtils, COUNTRY_CODE } from 'helpers';
import moment from 'moment'
import { utils } from 'helpers'
import { updateCompany } from '../../Company/actions'

const { getMediaUrl } = utils
const { mainStyle: { gutter }, MEDIA_TYPE } = constants

const { TextArea } = Input;
const { CheckableTag } = Tag;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const EditProfile = ({ tags, company, form, handleChangeMode, updateCompany, errorMsg }) => {

  const [tagsData, setTagsData] = useState([]);
  const [selectedTags, setSelectedTags] = useState(company ? company.tags : []);
  const [country, setCountry] = useState(company ? company.country : 'Singapore');
  const [countryCode, setCountryCode] = useState('sg');
  const [logoUrl, setLogoUrl] = useState(company && company.logo);

  useEffect(() => {
    const data = tags.map(t => {
      return {
        id: t.id,
        categories: t.name,
      };
    });
    setTagsData(data);
    selectedTags && selectedTags.forEach((item) => {
      form.setFieldsValue({ [`${item}`]: true });
    })
  }, [tags, setTagsData, setSelectedTags]);

  useEffect(() => {
    form.setFieldsValue({
      name: company ? company.name : '',
      registrationNo: company ? company.registrationNo : '',
      countryOrigin: company ? company.countryOrigin : 'Singapore',
      phoneNumber: company ? company.phoneNumber : '',
      yearEstablisment: (company && company.yearEstablisment) ? moment(`1/1/${company.yearEstablisment}`) : '',
      website: company ? company.website : '',
    });
    setCountry(company ? company.countryOrigin : 'Singapore');
    setImageUrl(company ? getMediaUrl(company.logo) : null);
    setLogoUrl(company ? company.logo : null);

  }, [company, form]);

  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      setLogoUrl(info.file.response.data.url)
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        setImageUrl(imageUrl),
        setLoading(false)
      );
      updateHostProfile({ avatar: info.file.response.data.url });
    }
  };

  const handleChangeCountry = (value) => {
    setCountryCode(COUNTRY_CODE[value]);
    setCountry(value);
    form.setFieldsValue({ countryOrigin: value });
  }

  const uploadButton = (
    <div className='upload' style={{
      position: 'absolute',
      left: '50%',
      transform: 'translate(-50%, 0)',
      cursor: 'pointer',
    }}>
      {loading ? <LoadingOutlined /> : <IconUpload />}
      <div className='text'>Change</div>
    </div>
  );

  const onFinish = async values => {
    let res = ''
    const update = {};
    if (logoUrl !== company.logo) {
      update.logo = logoUrl;
    }
    if (values.name !== company.name) {
      update.name = values.name;
    }
    if (values.registrationNo !== company.registrationNo) {
      update.registrationNo = values.registrationNo;
    }
    if (values.countryOrigin !== company.countryOrigin) {
      update.countryOrigin = values.countryOrigin;
    }
    if (values.phoneNumber !== company.phoneNumber) {
      update.phoneNumber = values.phoneNumber;
    }
    if (values.yearEstablisment.year() !== company.yearEstablisment) {
      update.yearEstablisment = values.yearEstablisment.year();
    }
    if (values.website !== company.website) {
      update.website = values.website;
    }

    const categories = []

    Object.keys(values).length > 0 && Object.keys(values).forEach(item => {
      if (values[item] && Number(item)) {
        categories.push(item)
      }
    })

    update.tags = categories;

    res = await updateCompany({ ...company, ...update });
    if (!res) {
      handleChangeMode();
      ReactUtils.messageSuccess({ content: 'Update company sucessfully' });
    }
  }

  const upload = () => {
    return uploadMedia({ ownerType: MEDIA_TYPE.HOST, ownerCode: company.code, noCreate: true });
  }

  const disabledDate = (current) => {
    return current && current.valueOf() >= Date.now();
  }

  return (
    <>
      <section className='main-content host-profile_content company-profile'>
        <Row gutter={gutter} style={{ display: 'flex', width: '100%' }} className='row-host-profile'>
          <Col className="gutter-row" md={8} lg={8} xl={6} style={{ marginTop: '20px' }}>
            <MenuScroll tabMode='1' />
          </Col>
          <Col className="gutter-row" md={12} lg={16} xl={18} style={{ marginTop: '20px' }}>
            <div id="overview" className='section-scroll section-scroll_overview'>
              <h1>
                <IconHome />Company Profile<br />
                <label>Company profile will be displayed with product information</label>
              </h1>
              {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
              <Row gutter={gutter} style={{ padding: '0 26px' }}>
                <Col className="gutter-row input-with-lbl-hori" span={8}>
                  <Form className="auth-form" layout="vertical">
                    <Form.Item
                      label="Avatar"
                      name="avatar"
                      className='form-textarea'
                    >
                      <Image
                        width={194}
                        src={imageUrl || company.avatar}
                      />
                      <Upload
                        accept={constants.FILE_TYPES}
                        name="file"
                        listType="picture-card"
                        className="avatar-uploader avatar-image"
                        showUploadList={false}
                        action={upload}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                      >
                        {uploadButton}
                      </Upload>
                    </Form.Item>
                  </Form>
                </Col>
                <Col className="gutter-row" span={16}>
                  <Form form={form} layout="vertical" onFinish={onFinish} >
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{ width: '100%' }}>
                        <Form.Item name="name" label="Company Name" rules={[{ required: true, message: 'Company Name is missing' }]}>
                          <Input
                            size="large"
                            className="input-basic"
                            prefix={<IconNumber />}
                            placeholder="Captive"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{ width: '100%' }}>
                        <Form.Item name="registrationNo" label="Company Registration No." rules={[{ required: true, message: 'Registration No is missing' }]}>
                          <Input
                            size="large"
                            className="input-basic"
                            prefix={<IconNumber />}
                            placeholder="e.g. CO12345"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{ width: '100%' }}>
                        <Form.Item
                          label="Country"
                          className='form-select'
                          name="countryOrigin"
                          rules={[{ required: true, message: 'Country is missing' }]}
                        >
                          <CountryDropdown
                            name="my-country-field"
                            classes="select-style"
                            showDefaultOption={true}
                            value={country}
                            onChange={handleChangeCountry}
                          />
                          <IconFlagAlt />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" style={{ width: '100%' }}>
                        <Form.Item
                          label="Phone Number"
                          name="phoneNumber"
                          className="input-basic"
                          rules={[{ required: true, message: 'Phone Number is missing' }]}
                        >
                          <ReactPhoneInput
                            country={countryCode}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" span={24}>
                        <Form.Item
                          label="Year of Establishment"
                          name="yearEstablisment"
                          rules={[{ required: true, message: 'Year of Establishment is missing' }]}
                        >
                          <DatePicker
                            placeholder='YYYY'
                            style={{ fontWeight: 'normal' }}
                            format="YYYY"
                            disabledDate={disabledDate}
                            picker="year"
                            suffixIcon={<IconCalendar />}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" span={24}>
                        <Form.Item name="website" label="Website" rules={[
                          { validator: ReactUtils.urlValidator }
                        ]}>
                          <Input
                            size="large"
                            className="input-basic"
                            prefix={<IconPorfolio />}
                            placeholder="(Optional)"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={gutter} style={{ display: 'flex', alignItems: 'baseline' }}>
                      <Col className="gutter-row input-with-lbl-hori" span={24}>
                        <Form.Item name="categories" label="Categories">
                          <Row justify="space-between">
                            {
                              tagsData && tagsData.map((item) => (
                                <Col span={11}>
                                  <Form.Item
                                    name={item.id}
                                    valuePropName="checked"
                                  >
                                    <Checkbox key={item.id} checked={selectedTags && selectedTags.includes(item.id)} value={item.id} className="checkbox-basic">{item.categories}</Checkbox>
                                  </Form.Item>
                                </Col>
                              ))
                            }
                          </Row>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </section>
    </>
  )
}

const mapStateToProps = ({ company, hostList }) => ({
  company: company.data,
  tagsById: hostList.tagsById,
  tags: hostList.tags,
  errorMsg: company.errorMsg
});

const mapDispatchToProps = {
  updateCompany
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EditProfile)
);