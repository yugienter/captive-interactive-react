import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Form, Input, Row, Col, Select, DatePicker } from "antd";
import { IconFlagAlt, IconSelectDown, IconCalendar } from 'views/components/Icon/pages';
import ReactPhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import { CountryDropdown } from 'react-country-region-selector';
import moment from 'moment';
import { COUNTRY_CODE } from 'helpers';
import { constants } from 'helpers';

const { Option } = Select;
const dateFormat = 'DD/MM/YYYY';

const PersonalInfoForm = ({ form, host }) => {

  form.setFieldsValue({
    country: host ? host.country : null,
    dob: host ? moment(host.dob) : null,
    experience: host ? host.yearExperience : null,
    height: host ? host.height : null,
    phoneNumber: host ? host.phoneNumber : null,
    rates: host ? host.rates : null,
    gender: host ? host.sex : null,
    weight: host ? host.weight : null,
  });

  const [country, setCountry] = useState(host ? host.country : null);
  const [countryCode, setCountryCode] = useState('us');

  const handleChangeCountry = (value) => {
    setCountryCode(COUNTRY_CODE[value]);
    setCountry(value);
  }

  useEffect(() => {
    form.setFieldsValue({ country });
  }, [form, country]);

  const disabledDate = (current) => {
    return current && current.valueOf() >= Date.now();
  }
  const suffixWeight = <div className='suffix-custom'>kg</div>
  const suffixHeight = <div className='suffix-custom'>cm</div>
  const suffixYears = <div className='suffix-custom'>years</div>
  const perfixPrice = <div className='perfix-custom'>SGD</div>

  return (
    <>
      <Form form={form} layout="vertical">
        <Row gutter={constants.mainStyle.gutter}>
          <Col className="gutter-row" span={12}>
            <Form.Item
              label="Country"
              className='form-select'
              name="country"
              rules={[{ required: true, message: 'Country is missing' }]}
            >
              <CountryDropdown
                name="my-country-field"
                classes="select-style"
                showDefaultOption={true}
                value={country}
                onChange={handleChangeCountry}
              />
              <IconFlagAlt />
            </Form.Item>
            <Form.Item name="gender" label="Gender" rules={[{ required: true, message: 'Gender is missing' }]}>
              <Select
                placeholder="Select gender"
                className='select-noicon'
                style={{ width: '100%' }}
                suffixIcon={<IconSelectDown />}
              >
                <Option value="male">Male</Option>
                <Option value="female">Female</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Weight"
              name="weight"
              rules={[{ required: false, message: 'Weight is missing' }]}
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixWeight}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <Form.Item
              label="Experience Year"
              name="experience"
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixYears}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <div className='experience'>It will be added 1 after one year.</div>
          </Col>
          <Col className="gutter-row" span={12}>
            <Form.Item
              label="Phone Number"
              name="phoneNumber"
              rules={[{ required: true, message: 'Phone Number is missing' }]}
            >
              <ReactPhoneInput
                country={countryCode}
              />
            </Form.Item>
            <Form.Item
              label="Date of Birth"
              name="dob"
              rules={[{ required: true, message: 'Date of Birth is missing' }]}
            >
              <DatePicker
                placeholder='DD/MM/YYYY'
                style={{ fontWeight: 'normal' }}
                defaultValue={moment()}
                format={dateFormat}
                suffixIcon={<IconCalendar />}
                disabledDate={disabledDate}
              />
            </Form.Item>
            <Form.Item
              label="Height"
              name="height"
              rules={[{ required: false, message: 'Height is missing' }]}
            >
              <Input
                size="large"
                className="input-basic"
                suffix={suffixHeight}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
            <Form.Item
              label="Hourly Rate"
              name="rates"
            >
              <Input
                size="large"
                className="input-basic"
                prefix={perfixPrice}
                placeholder="Number"
                type="number"
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  )
}

const mapStateToProps = ({ hostProfile, hostList }) => ({
  host: hostProfile.data,
});

const mapDispatchToProps = {

};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PersonalInfoForm)
);