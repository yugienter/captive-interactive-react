import * as types from './actionTypes';

const initialState = {
  isFetching: false,
  isUpdating: false,
  isUploadingMedia: false,
  data: null,
  brands: [],
  brandList: [],
  selectedBrand: {},
  errorMsg: '',
  defaultBrand: {},
  tab: '1'
};

const companyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_COMPANY_LOGO:
      return { ...state, data: { ...state.data, logo: action.payload } };
    case types.UPDATE_COMPANY:
      return { ...state, data: action.payload };
    case types.ADD_BRAND:
      const newBrands = state.brands ? [...state.brands] : []
      newBrands.push(action.payload)
      return { ...state, brands: newBrands };
    case types.SELECT_BRAND:
      return { ...state, selectedBrand: action.payload };
    case types.SET_LOADING:
      return { ...state, isUpdating: action.payload };
    case types.REPLACE_BRAND:
      return { ...state, brands: action.payload };
    case types.SET_BRAND_LIST:
      return { ...state, brandList: action.payload };
    case types.SET_ERROR:
      return { ...state, errorMsg: action.payload };
      case types.SET_DEFAULT_BRAND:
      return { ...state, defaultBrand: action.payload };
    case types.SET_TAB:
      return { ...state, tab: action.payload };
    default:
      return state;
  }
};

export default companyReducer;
