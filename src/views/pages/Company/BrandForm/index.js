import React, { useState, useEffect } from 'react'
import { Form, Input, Row, Col, Upload, message, Image, Tag, Dropdown, Slider, Select, Button } from "antd";
import { constants, COUNTRY_CODE } from 'helpers';
import { IconSaveAs, IconPorfolio, IconUpload, IconFlagAlt, IconTag, IconTargetAudiences, IconInfo, IconSelectDown, IconBestSeller, IconAddPlus, IconRemove } from 'views/components/Icon/pages';
import { LoadingOutlined } from '@ant-design/icons';
import { CountryDropdown } from 'react-country-region-selector';
import { utils, ReactUtils } from 'helpers'
import { connect } from 'react-redux';
import HostTag from 'views/pages/Host/parts/HostTag';
import { uploadMedia, updateBrand, createBrand, getBrandList } from '../../Company/actions'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import * as type from '../../Company/actionTypes'

const { Option } = Select;
const { career } = constants
const { CheckableTag } = Tag;
const { TextArea } = Input;
const { getMediaUrl } = utils

const { MEDIA_TYPE } = constants;

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function BrandForm({ tags, brand, company, updateBrand, createBrand, onCancel, errorMsg, brands, getBrandList }, ref) {
    const [form1] = Form.useForm();
    const [form2] = Form.useForm();
    const [imageUrl, setImageUrl] = useState();
    const [loading, setLoading] = useState(false);
    const [country, setCountry] = useState(company ? company.country : 'Singapore');
    const [countryCode, setCountryCode] = useState('sg');
    const [tagsData, setTagsData] = useState([]);
    const [selectedTags, setSelectedTags] = useState([]);
    const [logoUrl, setLogoUrl] = useState();
    const [age, setAge] = useState([0, 100]);
    const [sellers, setSellers] = useState([]);
    const { search } = useLocation()
    const query = queryString.parse(search)

    const handleChangeCountry = (value) => {
        setCountryCode(COUNTRY_CODE[value]);
        setCountry(value);
        form2.setFieldsValue({ countryOrigin: value });
    }

    useEffect(() => {
        const currentTag = brand ? brand.tags : []
        const selectedTags = [];
        const data = tags.map(parent => {
            return {
                id: parent.id,
                categories: parent.name,
                data: parent.item.map(child => {
                    const tag = {
                        id: child.id,
                        icon: HostTag({ tag: child.name }).props.icon,
                        name: child.name
                    };
                    if (currentTag && currentTag.includes(child.id)) {
                        selectedTags.push(tag)
                    }
                    return tag;
                })
            };
        });
        setTagsData(data);
        setSelectedTags(selectedTags);
    }, [tags, setTagsData, setSelectedTags, brand]);

    const handleSelectedTags = (tag, checked) => {
        let nextSelectedTags = selectedTags;
        if (checked && selectedTags.length < 5) {
            nextSelectedTags = [...selectedTags, tag];
        } else if (!checked) {
            nextSelectedTags = selectedTags.filter(t => t !== tag);
        } else {
            ReactUtils.messageWarn({ content: 'Limit of 5 categories reached' });
        }
        setSelectedTags(nextSelectedTags)
    }

    useEffect(() => {
        form2.setFieldsValue({
            logo: logoUrl,
            name: brand ? brand.name : '',
            concept: brand ? brand.concept : '',
            countryOrigin: brand ? brand.countryOrigin : 'Singapore',
            facebook: brand ? brand.facebook : '',
            instagram: brand ? brand.instagram : '',
            career: brand ? brand?.targetAudiences?.carrer : '',
            insights: brand ? brand?.targetAudiences?.insights : '',
            genders: brand ? (brand?.targetAudiences?.genders ? brand?.targetAudiences?.genders[0] : '') : ''
        });
        setImageUrl((brand && brand.logo) ? getMediaUrl(brand.logo) : '');
        setLogoUrl(brand ? brand.logo : null);
        setSellers(brand ? brand.bestSellers : [])
        setAge([brand?.targetAudiences?.ageMin || 0, brand?.targetAudiences?.ageMax || 100])
        setCountry(brand ? brand.countryOrigin : 'Singapore');

    }, [brand, setImageUrl, form2]);

    const upload = () => {
        const url = uploadMedia({ ownerType: MEDIA_TYPE.COMPANY, noCreate: true });
        return url;
    }

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            setLogoUrl(info.file.response.data.url)
            getBase64(info.file.originFileObj, imageUrl =>
                setImageUrl(imageUrl),
                setLoading(false)
            );
            form1.resetFields();
        }
    };

    const onFinish = async (values) => {
        const update = {}
        let res = ''
        if (logoUrl) {
            update.logo = logoUrl
        }
        if (values.name) {
            update.name = values.name
        }
        if (values.concept) {
            update.concept = values.concept
        }
        if (values.countryOrigin) {
            update.countryOrigin = values.countryOrigin
        }
        if (values.facebook) {
            update.facebook = values.facebook
        }
        if (values.instagram) {
            update.instagram = values.instagram
        }
        if (selectedTags && selectedTags.length > 0) {
            update.tags = selectedTags.map(item => item = item.id)
        }
        if (sellers && sellers.length > 0) {
            update.bestSellers = sellers
        }
        update.targetAudiences = {
            genders: [values.genders],
            ageMin: age[0],
            ageMax: age[1],
            carrer: values.career,
            insights: values.insights
        }
        if (Object.keys(update).length) {
            update.companyCode = company.code
            if (brand && brand.code) {
                res = await updateBrand({ ...brand, ...update });
            }
            else {
                res = await createBrand(update, query && query["open-brand-popup"] == "true");
            }
        }
        if (!res) {
            onCancel()
            ReactUtils.messageSuccess({ content: 'Update brand Sucessfully' });
            await getBrandList(brands)
            form1.resetFields()
            form2.resetFields()
        }
    };

    const uploadButton = (
        <div className='upload'>
            {loading ? <LoadingOutlined /> : <IconUpload />}
            <div className='text'>Upload</div>
        </div>
    );

    const handleChangeAge = (e) => {
        setAge(e)
    }

    const addBestSeller = () => {
        setSellers(seller => {
            const temp = [...seller]
            temp.unshift('')
            return temp;
        })
    }

    const removeBestSeller = index => {
        setSellers(seller => {
            const temp = [...seller]
            temp.splice(index, 1)
            return temp;
        })
    }

    const handleBestSellerChange = (e, index) => {
        setSellers(sel => {
            sel = [...sellers]
            sel[index] = e.target.value
            return sel
        })
    }

    const onSave = () => {
        form2.submit()
    }

    return (
        <div className="brand-form flow-signup captive">
            <div className="brand-form__header">
                <p>Add new brand</p>
                <div className="brand-form__action">
                    <a onClick={() => {
                        form1.resetFields()
                        form2.resetFields()
                        onCancel()
                    }}>Cancel</a>
                    <a onClick={onSave}><IconSaveAs />Save</a>
                </div>
            </div>
            <div className="brand-form__section ">
                <p className="brand-form__section__title"><IconInfo /> Brand</p>
                <div className="brand-form__section__content auth-form">
                    <p className="brand-form__section__subtitle">Provide basic information of your brand</p>
                    {errorMsg && <p style={{ color: "red" }}>{errorMsg}</p>}
                    <Row>
                        <Col span={8}>
                            <Form form={form1} onFinish={onFinish} layout="vertical">
                                <Form.Item
                                    label="Brand Logo"
                                    name="logo"
                                    className='form-textarea section-scroll_overview'
                                >
                                    <Upload
                                        accept={constants.FILE_TYPES}
                                        name="file"
                                        listType="picture-card"
                                        className="avatar-uploader avatar-image"
                                        showUploadList={false}
                                        action={upload}
                                        beforeUpload={beforeUpload}
                                        onChange={handleChange}
                                    >
                                        {imageUrl ? <Image width={194} preview={false} src={imageUrl} className='avatar-image_view' alt="Company Logo" /> : uploadButton}
                                    </Upload>

                                </Form.Item>
                            </Form>
                        </Col>
                        <Col span={16}>
                            <Form onFinish={onFinish} form={form2} layout="vertical">
                                <Form.Item name="name" label="Brand Name" rules={[{ required: true, message: 'Brand Name is missing' }]}>
                                    <Input
                                        size="large"
                                        className="input-basic"
                                        placeholder="e.g. Captive LIVE+"
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="concept"
                                    label="Brand Concept"
                                >
                                    <TextArea style={{
                                        height: 82
                                    }} rows={3} />
                                </Form.Item>
                                <Form.Item
                                    label="Country"
                                    className='form-select'
                                    name="countryOrigin"
                                >
                                    <CountryDropdown
                                        name="my-country-field"
                                        classes="select-style"
                                        showDefaultOption={true}
                                        value={country}
                                        onChange={handleChangeCountry}
                                    />
                                    <IconFlagAlt />
                                </Form.Item>
                                <Form.Item name="facebook" label="Facebook" rules={[
                                    { validator: ReactUtils.urlValidator }
                                ]}>
                                    <Input
                                        size="large"
                                        className="input-basic"
                                        prefix={<IconPorfolio />}
                                        placeholder="(Optional)"
                                    />
                                </Form.Item>
                                <Form.Item name="instagram" label="Instagram" >
                                    <Input
                                        size="large"
                                        className="input-basic"
                                        prefix={<IconPorfolio />}
                                        placeholder="(Optional)"
                                    />
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="brand-form__section ">
                <p className="brand-form__section__title"><IconTag /> Brand Tags</p>
                <div className="brand-form__section__content auth-form">
                    <p className="brand-form__section__subtitle">Select up to 5 that best describe the areas that your brand focuses on</p>
                    {tagsData.map(tag => (
                        company && company.tags && company.tags.includes(tag.id) &&
                        <>
                            <div className='topics-categories'>{tag.categories}</div>

                            <div className='topics-tags'>
                                {tag.data.map(item => (
                                    <CheckableTag
                                        key={item.id}
                                        className={selectedTags.length > 5 ? 'none-cheked' : ''}
                                        checked={selectedTags.indexOf(item) > -1}
                                        onChange={checked => handleSelectedTags(item, checked)}
                                    >
                                        {item.icon} {item.name}
                                    </CheckableTag>
                                ))}
                            </div>
                        </>
                    ))}
                </div>
            </div>
            <div className="brand-form__section ">
                <p className="brand-form__section__title"><IconTargetAudiences /> Target Audiences</p>
                <div className="brand-form__section__content auth-form">
                    <p className="brand-form__section__subtitle">Identify target audiences of the brand to find matching Sellers easier</p>
                    <Form onFinish={onFinish} form={form2} layout="vertical">
                        <Row justify="space-between">
                            <Col span={7}>
                                <Dropdown overlay={<div style={{
                                    background: '#FFF',
                                    height: 60,
                                    padding: 10
                                }}><Slider marks={{
                                    0: '0',
                                    100: '100'
                                }} onChange={handleChangeAge} range value={age} /></div>} placement="bottomLeft">
                                    <Form.Item label="Age Range">
                                        <Input
                                            readOnly
                                            value={`${age[0]} - ${age[1]}`}
                                            size="large"
                                            className="input-basic"
                                        />

                                    </Form.Item>
                                </Dropdown>
                            </Col>
                            <Col span={7}>
                                <Form.Item name="genders" label="Gender">
                                    <Select
                                        placeholder="Select gender"
                                        className='select-noicon'
                                        style={{ width: '100%' }}
                                        suffixIcon={<IconSelectDown />}
                                    >
                                        <Option value="all">All</Option>
                                        <Option value="female">Female</Option>
                                        <Option value="male">Male</Option>
                                        <Option value="ohthers">Ohthers</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={7}>
                                {/* <Form.Item name="career" label="Career">
                                    <Select
                                        placeholder="Select career"
                                        className='select-noicon'
                                        style={{ width: '100%' }}
                                        suffixIcon={<IconSelectDown />}
                                    >
                                        {
                                            career && career.map((item, index) => (
                                                <Option key={index} value={item}>{item}</Option>
                                            ))
                                        }

                                    </Select>
                                </Form.Item> */}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item name="insights" label="Insights">
                                    <Input
                                        size="large"
                                        className="input-basic"
                                        placeholder="Locals from all walks of life"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
            <div className="brand-form__section">
                <p className="brand-form__section__title"><IconBestSeller /> Best Sellers</p>
                <div className="brand-form__section__content auth-form">
                    <p className="brand-form__section__subtitle">Give Sellers an idea of the brand’s popularity</p>
                    <Button onClick={addBestSeller} type="primary" className="add-more"><IconAddPlus />Add more</Button>
                    <Form form={form1} layout="vertical">
                        {
                            sellers && sellers.map((item, index) => (
                                <Form.Item >
                                    <Input
                                        onChange={(e) => handleBestSellerChange(e, index)}
                                        value={item}
                                        size="large"
                                        className="input-basic"
                                        placeholder="Enter Seller Name"
                                        suffix={
                                            <a onClick={() => removeBestSeller(index)}><IconRemove /></a>
                                        }
                                    />
                                </Form.Item>
                            ))
                        }
                    </Form>
                </div>
            </div>
            <div className="brand-form__action">
                <div>
                    <a onClick={() => {
                        form1.resetFields()
                        form2.resetFields()
                        onCancel()
                    }}>Cancel</a>
                    <a onClick={onSave}><IconSaveAs />Save</a>
                </div>
            </div>
        </div>
    );
}

const mapDispatchToProps = {
    createBrand,
    updateBrand,
    getBrandList
};

const mapStateToProps = ({ common, hostProfile, hostList, company }) => ({
    user: common.user,
    host: hostProfile.data,
    tags: hostList.tags,
    tagsById: hostList.tagsById,
    isUpdating: hostProfile.isUpdating,
    company: company.data,
    brand: company.selectedBrand,
    brands: company.brands,
    errorMsg: company.errorMsg
});

export default connect(mapStateToProps, mapDispatchToProps)(BrandForm);