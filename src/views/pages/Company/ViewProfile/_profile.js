import React, { useEffect, useState } from 'react';
import {
  Tag,
  Row,
  Col,
  Collapse,
  Image,
  Button,
  Modal,
  Drawer
} from "antd";
import {
  IconRemove,
  IconAddPlus,
  IconHome,
  IconSave,
  IconEdit
} from 'views/components/Icon/pages'
import HostTag from 'views/pages/Host/parts/HostTag';
import MenuScroll from '../MenuScroll';
import { utils, constants, ReactUtils } from 'helpers';
import BrandForm from '../BrandForm';
import { selectBrand, deleteBrand, getBrandList } from '../actions'
import { connect } from 'react-redux';
import { IconCloseDrawer } from 'views/components/Icon/drawer'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'

const { getMediaUrl } = utils
const { Panel } = Collapse;

const { mainStyle } = constants;
const { CheckableTag } = Tag;

const Profile = (props) => {
  let { company, tagsById, tabMode, brandList, selectBrand, deleteBrand, brands, getBrandList } = props
  const [brandPopup, setBrandPopup] = useState(false);
  const { search } = useLocation()
  const query = queryString.parse(search)
  company = company || {}

  const tagsData = company && company.tags && company.tags.map(tagId => {
    const tag = tagsById && tagsById[tagId];
    return {
      icon: tag ? HostTag({ tag: tag.name }).props.icon : '',
      name: tag?.name
    }
  });

  useEffect(() => {
    if (query && query["open-brand-popup"] == "true") {
      setBrandPopup(true)
    }
  }, [])

  const collapseHeader = (item) => (
    item && <div className="collapse-brand__header">
      <div className="collapse-brand__header__logo">
        {item.logo && <img src={getMediaUrl(item.logo)} alt="Brand Logo" />}
      </div>
      <div className="collapse-brand__header__info">
        <p>{item.name}</p>
        <p>{item.concept}</p>
      </div>
      <div className="collapse-brand__header__action">
        <Button onClick={(e) => openBrandPopup(e, item)} type="primary" className="add-more"><IconEdit />Edit</Button>
        <Button onClick={e => {
          e.stopPropagation()
          showConfirm(item)
        }} type="primary" className="add-more"><IconRemove />Remove</Button>
      </div>
    </div>
  )

  const renderBrandTag = item => {
    const tag = tagsById ? tagsById[item] : {};
    return (
      <CheckableTag
        key={tag?.name}
      >
        {tag ? HostTag({ tag: tag.name }).props.icon : ''} {tag?.name}
      </CheckableTag>
    )
  }

  const openBrandPopup = (e, brand) => {
    e.stopPropagation()
    selectBrand({ ...brand })
    setBrandPopup(true)
  }

  const showConfirm = (brand) => {
    let res = ''
    Modal.confirm({
      title: `Do you Want to delete these '${brand?.name}'`,
      onOk: async () => {
        res = await deleteBrand(brand)
        if (res) {
          ReactUtils.messageWarn({ content: 'Something went wrong' });
        }
        else {
          ReactUtils.messageSuccess({ content: 'Delete brand Sucessfully' });
          getBrandList(brands)
        }
      },
    });
  }

  return (
    <>
      <Row gutter={mainStyle.gutter} className='row-host-profile'>
        <Col className="gutter-row" md={8} lg={8} xl={6}>
          <MenuScroll tabMode={tabMode} />
        </Col>
        <Col className="gutter-row" md={12} lg={16} xl={18}>
          <div id="overview" className='section-scroll section-scroll_overview'>
            <h1><IconHome />Overview</h1>
            <Row
              gutter={mainStyle.gutter}
              style={{ padding: '0 26px' }}
            >
              <Col className="gutter-row" span={8} className='avatar-image'>
                <div className='lbl-hozi'>Avatar</div>
                <Image
                  width={194}
                  src={getMediaUrl(company.logo)}
                  className='avatar-image_view'
                />
              </Col>
              <Col className="gutter-row" span={16}>
                <div className='lbl-hozi'>Company Name</div>
                <div className='txt-hozi mb26'>{company.name}</div>

                <div className='lbl-hozi'>Company Registration No.</div>
                <div className='txt-hozi mb26'>{company.registrationNo}</div>

                <div className='lbl-hozi'>Country of Origin</div>
                <div className='txt-hozi mb26'>{company.registrationNo}</div>

                <div className='lbl-hozi'>Contact Number</div>
                <div className='txt-hozi mb26'>{company.countryOrigin}</div>

                <div className='lbl-hozi'>Year of Establishment</div>
                <div className='txt-hozi mb26'>{company.yearEstablisment}</div>

                <div className='lbl-hozi'>Website</div>
                <div className='txt-hozi mb26'><a href={company.website} target="_blank">{company.website}</a></div>

                <div className='lbl-hozi'>Categories</div>
                <div className='txt-hozi mb26'>
                  <div className='topics-tags-view'>
                    {tagsData && tagsData.map(tag => (
                      <CheckableTag
                        key={tag.name}
                      >
                        {tag.icon} {tag.name}
                      </CheckableTag>
                    ))}
                  </div>
                </div>
              </Col>
            </Row>
          </div>
          <div id="brand" className='section-scroll'>
            <h1><IconSave />Brand</h1>
            <div className="pl30">
              <p >Create and manage your brands</p>
              <Button onClick={(e) => openBrandPopup(e)} type="primary" className="add-more"><IconAddPlus />New brand</Button>
              <ul className="collapse-brand-list">
                {
                  brandList && brandList.map(item => (
                    item && <li className="collapse-brand">
                      <Collapse>
                        <Panel header={collapseHeader(item)} key="1">
                          <div className="collapse-brand__content">
                            <div className="topics-tags-view collapse-brand__content__tags">
                              {
                                item && item.tags && item.tags.map(tag => (
                                  renderBrandTag(tag)
                                ))
                              }
                            </div>
                            <table className="collapse-brand__table">
                              <tr>
                                <td>Country of Origin</td>
                                <td>{item.countryOrigin}</td>
                              </tr>
                              <tr>
                                <td>Target Audiences</td>
                                <td>
                                  <p>{`${item?.targetAudiences?.ageMin && item?.targetAudiences?.ageMax ? `${item?.targetAudiences?.ageMin} - ${item?.targetAudiences?.ageMax} age, ` : ''}
                                  ${item?.targetAudiences?.genders && item?.targetAudiences?.genders.length > 0 ? `${item?.targetAudiences?.genders[0]} gender, ` : ''}
                                  ${item?.targetAudiences?.carrer ? `${item?.targetAudiences?.carrer} career, ` : ''}`}</p>
                                  <p>{item?.targetAudiences?.insights}</p>
                                </td>
                              </tr>
                              <tr>
                                <td>Best Sellers</td>
                                <td><ul>{item.bestSellers && item.bestSellers.map(seller => (
                                  <li>{seller}</li>
                                ))}</ul></td>
                              </tr>
                              <tr>
                                <td>Facebook</td>
                                <td><a href={item.facebook} target="_blank">{item.facebook}</a></td>
                              </tr>
                              <tr>
                                <td>Instagram</td>
                                <td><a href={item.instagram} target="_blank">{item.instagram}</a></td>
                              </tr>
                            </table>
                          </div>
                        </Panel>
                      </Collapse>
                    </li>
                  ))
                }
              </ul>
            </div>
          </div>
        </Col>
      </Row>
      <Drawer
        className={'drawer-main'}
        placement="right"
        width={860}
        onClose={() => setBrandPopup(false)}
        visible={brandPopup}
        closeIcon={<IconCloseDrawer />}
      >
        <BrandForm onCancel={() => setBrandPopup(false)} />
      </Drawer>
    </>
  )
}

const mapDispatchToProps = {
  selectBrand,
  deleteBrand,
  getBrandList
};

const mapStateToProps = ({ common, company }) => ({
  user: common.user,
  brands: company.brands,
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
