import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { Tabs } from "antd";
import { setTab } from 'views/pages/Company/actions';
import Profile from './_profile';
import Account from './_account';

const { TabPane } = Tabs;

const ViewProfile = ({ company, tagsById, handleChangeMode, tab, setTab, history, brandList }) => {
  const [tabMode, setTabMode] = useState(tab);

  useEffect(() => {
    setTabMode(tab);
  }, [tab]);

  const callback = (key) => { setTab(key) }

  return (
    <>
      <section className='main-content host-profile_content'>
        <Tabs defaultActiveKey="1" onChange={callback} activeKey={tabMode}>
          <TabPane tab="Profile" key="1">
            <Profile
              history={history}
              company={company}
              tagsById={tagsById}
              handleChangeMode={handleChangeMode}
              brandList={brandList}
              tabMode={tabMode}
            />
          </TabPane>
          <TabPane tab="Account" key="2">
            <Account email={''} tabMode={tabMode} />
          </TabPane>
        </Tabs>
      </section>
    </>
  )
}

const mapStateToProps = ({  company, hostList }) => ({
  company: company.data,
  tab: company.tab,
  tagsById: hostList.tagsById,
  brandList: company.brandList
});

const mapDispatchToProps = {
  setTab
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewProfile)
);