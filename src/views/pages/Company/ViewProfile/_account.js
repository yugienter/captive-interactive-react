import React from 'react';
import {
  Button,
  Row,
  Col,
  Form,
  Input
} from "antd";
import {
  IconAccSett,
  IconVerify,
  IconResetPass
} from 'views/components/Icon/pages'
import MenuScroll from '../MenuScroll';
import { constants } from 'helpers';

const { mainStyle } = constants;

const Account = (props) => {
  const { email, tabMode } = props
  const [form] = Form.useForm();
  form.setFieldsValue({ email });

  return (
    <>
      <Row gutter={mainStyle.gutter} className='row-host-profile'>
        <Col className="gutter-row" md={8} lg={8} xl={6}>
          <MenuScroll tabMode={tabMode} />
        </Col>
        <Col className="gutter-row" md={12} lg={16} xl={18}>
          <div id="account-settings" className='section-scroll'>
            <h1>
              <IconAccSett />Account Settings<br />
              <label>You can change your verified email or password</label>
            </h1>
            <Form form={form} layout="vertical" style={{ marginLeft: '26px' }}>
              <Row gutter={mainStyle.gutter}>
                <Col className="gutter-row input-with-lbl-hori" span={24}>
                  <Form.Item
                    label="Email"
                    name="email"
                  >
                    <Input
                      size="large"
                      className="input-h48"
                      placeholder="jessixchen_0219@outlook.sg"
                      disabled={true}
                      suffix={<IconVerify />}
                    />

                  </Form.Item>
                  <Form.Item label="Password" name="password">
                    <Button className='control reset-pass' block>
                      <IconResetPass />Forgot Password
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
    </>
  )
}

export default Account;