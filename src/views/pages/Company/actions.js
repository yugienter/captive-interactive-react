import { constants, client, utils } from 'helpers';
import config from 'config';

import * as types from './actionTypes';

const { api } = constants;
const { API_END_POINT } = config;

export const uploadMedia = (params) => {
    const url = new URL(`${API_END_POINT}${api.path.uploadMedia}`);
    url.search = new URLSearchParams(params);
    return url.toString();
};

export const updateCompanyLogo = (values) => dispatch => {
    dispatch({ type: types.UPDATE_COMPANY_LOGO, payload: values });
};
export const createCompany = (payload) => async dispatch => {
    let error = ''
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.createCompany,
            method: 'post',
            data: payload
        });
        dispatch({ type: types.UPDATE_COMPANY, payload: data.data });
        dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
        error = data?.error || ''
    } catch (err) {
        dispatch({ type: types.SET_ERROR, payload: err?.message });
        error = err?.message || ''
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
        return error
    }
}

export const updateCompany = (payload) => async dispatch => {
    let error = ''
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.updateCompany(payload.code),
            method: 'put',
            data: payload
        });
        dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
        dispatch({ type: types.UPDATE_COMPANY, payload: payload });
        error = data?.error || ''
    } catch (err) {
        dispatch({ type: types.SET_ERROR, payload: err?.message });
        error = err?.message || ''
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
        return error
    }
}

export const resetError = () => async dispatch => {
    dispatch({ type: types.SET_ERROR, payload: '' });
}

export const fetchCompany = (id) => async dispatch => {
    dispatch({ type: types.SET_LOADING, payload: true });
    const { data } = await client.request({
        path: api.path.fetchCompany(id),
        method: 'get',
    });
    dispatch({ type: types.UPDATE_COMPANY, payload: data.data });
    dispatch({ type: types.SET_LOADING, payload: false });
}

export const getBrand = (id) => async dispatch => {
    const { data } = await client.request({
        path: api.path.getBrand(id),
        method: 'get',
    });
    dispatch({ type: types.SELECT_BRAND, payload: data.data });
}

export const createBrand = (payload, isSetDefault) => async dispatch => {
    let error = ''
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.createBrand,
            method: 'post',
            data: payload
        });
        dispatch({ type: types.ADD_BRAND, payload: data.data.code });
        dispatch({ type: types.SELECT_BRAND, payload: data.data });
        dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
        if (isSetDefault) {
            dispatch({ type: types.SET_DEFAULT_BRAND, payload: data.data });
        }
        error = data?.error || ''
    } catch (err) {
        dispatch({ type: types.SET_ERROR, payload: err?.message });
        error = err?.message || ''
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
        return error
    }

}

export const selectBrand = (payload) => dispatch => {
    dispatch({ type: types.SELECT_BRAND, payload: payload });
}

export const updateBrand = (payload) => async dispatch => {
    let error = ''
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.updateBrand(payload.code),
            method: 'put',
            data: payload
        });
        dispatch({ type: types.SELECT_BRAND, payload: payload });
        dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
        error = data?.error || ''
    } catch (err) {
        dispatch({ type: types.SET_ERROR, payload: err?.message });
        error = err?.message || ''
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
        return error
    }
}

export const deleteBrand = (payload) => async dispatch => {
    let error = ''
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.deleteBrand(payload.code),
            method: 'delete',
            data: {
                companyCode: payload?.companyCode
            }
        });
        dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
        error = data?.error || ''
    } catch (err) {
        dispatch({ type: types.SET_ERROR, payload: err?.message });
        error = err?.message || ''
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
        return error
    }
}

export const getCompany = () => async dispatch => {
    dispatch({ type: types.SET_LOADING, payload: true });
    const { data } = await client.request({
        path: api.path.getCompanyCode,
        method: 'get',
    });
    dispatch({ type: types.REPLACE_BRAND, payload: data?.data?.brandCodes })
    dispatch({ type: types.SELECT_BRAND, payload: null });
    const companyCode = data?.data?.companyCode
    if (companyCode) {
        const { data } = await client.request({
            path: api.path.fetchCompany(companyCode),
            method: 'get',
        });
        dispatch({ type: types.UPDATE_COMPANY, payload: data.data });
        dispatch({ type: types.SET_LOADING, payload: false });
        return data.data
    }
    else {
        dispatch({ type: types.UPDATE_COMPANY, payload: null });
        dispatch({ type: types.SET_LOADING, payload: false });
        return null
    }

}

export const getBrandList = (payload) => async dispatch => {
    const data = []
    dispatch({ type: types.SET_LOADING, payload: true });
    if (payload && payload.length > 0) {
        for (const p of payload) {
            const d = await client.request({
                path: api.path.getBrand(p),
                method: 'get',
            })
            data.push(d?.data?.data)
        }
    }
    dispatch({ type: types.SET_LOADING, payload: false });
    dispatch({ type: types.SET_BRAND_LIST, payload: data });
}

export const setTab = (tab) => dispatch => {
    dispatch({ type: types.SET_TAB, payload: tab });
};