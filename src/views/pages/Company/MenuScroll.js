import React from 'react';
import { connect } from 'react-redux';
import { Affix } from "antd";
import { IconSave, IconHome } from 'views/components/Icon/pages'
import { IconWarning } from "views/components/Icon";
import { Link } from 'react-scroll';

const MenuScroll = ({ tabMode, hostProfile }) => {
  // const [top, setTop] = useState(10);
  const top = 10;

  const menu = [
    { label: 'Company Profile', location: 'overview', icon: <IconHome /> }
  ]
  return (
    <Affix offsetTop={top}>
      <ul className='menu-scroll'>
        {
          tabMode !== '1' ?
            <>
              <li>
                <Link
                  to="overview"
                  spy={true}
                  smooth={true}
                >
                  <IconHome />
                Company Profile
              </Link>
              </li>
              <li>
                <Link
                  to="brand"
                  spy={true}
                  smooth={true}
                >
                  <IconSave />
                Brand
              </Link>
              </li>
            </>
            :
            menu.map((item, key) => {
              const step = hostProfile && hostProfile.completePercentage < 100 && hostProfile.step;
              return (
                <li key={key}>
                  <Link
                    activeClass={key === 0 ? 'active' : ''}
                    to={item.location}
                    spy={true}
                    smooth={true}
                  >
                    {item.icon}{item.label}
                  </Link>
                  {step && step - 1 === key && (
                    <span style={{
                      float: 'right',
                      marginTop: 8
                    }}>
                      <IconWarning />
                    </span>
                  )}
                </li>
              )
            })
        }
      </ul>
    </Affix>
  )
}

const mapStateToProps = ({ hostProfile }) => ({
  hostProfile: hostProfile.data,
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuScroll);