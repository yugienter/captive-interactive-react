import React, { useState, useEffect } from 'react';
import { Button, Form } from "antd";
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import Page from "views/components/Page";
import { constants } from "helpers";
import { IconEdit, IconCancelBtn, IconSaveBtn } from 'views/components/Icon/pages'
import PromptModal from 'views/components/PromptModal';
import ViewProfile from './ViewProfile';
import EditProfile from './EditProfile';
import { getCompany, getBrandList, resetError } from './actions'
const { userRoles } = constants;

function Company({ getCompany, email, brands, getBrandList, isUpdating, brandList, company }) {
    const [form] = Form.useForm();
    const [visible, setVisible] = useState(false);
    const [changeMode, setChangeMode] = useState(false);

    const handleChangeMode = () => {
        setChangeMode(!changeMode);
        setVisible(false);
    }

    const onSave = () => {
        form.submit();
    }

    useEffect(async () => {
        await getCompany(email);
    }, [email, getCompany]);

    useEffect(async () => {
        await getBrandList(brands)
    }, [getBrandList, brands]);

    useEffect(() => {
        resetError()
    }, [changeMode])

    const getUpdates = () => {
        const values = form.getFieldsValue();
        const update = {};
        return update;
    };

    const handleCancel = () => {
        const update = getUpdates();
        if (Object.keys(update).length > 0) {
            setVisible(true);
        } else {
            handleChangeMode();
        }
    }

    const handleClose = () => {
        setVisible(false);
    }

    return (
        <Page
            helmet="Setting Profile Page"
            alignTop
            selectedKeys={["company-profile"]}
            requiredAccess={[userRoles.COMPANY]}
            loadingPage={isUpdating}
        >
            <div className='host-profile_page'>
                <div className='main-header_title host-profile_title'>
                    <div className='main-header_name'>
                        {changeMode ? 'Edit Company Info' : 'Settings'}
                    </div>
                    <div className='main-header_actions host-profile_actions'>
                        {changeMode ? (
                            <>
                                <Button
                                    type="primary"
                                    className="control cancel"
                                    block
                                    onClick={handleCancel}
                                >
                                    <IconCancelBtn />Cancel
                                </Button>
                                <Button
                                    type="primary"
                                    className="control save"
                                    block
                                    onClick={onSave}
                                >
                                    <IconSaveBtn />Save
                             </Button>
                            </>
                        ) :
                            <Button
                                type="primary"
                                className="control cancel"
                                block
                                onClick={handleChangeMode}
                            >
                                <IconEdit />Edit Company Info
                            </Button>}
                    </div>
                </div>
                {changeMode ?
                    <EditProfile form={form} handleChangeMode={handleChangeMode} />
                    :
                    <ViewProfile handleChangeMode={handleChangeMode} />
                }
                <PromptModal
                    title="Confirm all discard changes"
                    text="Are you sure you want to discard the changes?"
                    visible={visible}
                    okText="Discard"
                    handleOk={handleChangeMode}
                    handleClose={handleClose}
                />
            </div>
        </Page>
    );
}

const mapStateToProps = ({ company, common }) => ({
    company: company.data,
    email: common.user.email,
    brands: company.brands,
    brandList: company.brandList,
    isUpdating: company.isUpdating,
});

const mapDispatchToProps = {
    getCompany,
    getBrandList,
    resetError
};

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Company)
);
