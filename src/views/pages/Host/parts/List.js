/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 25/08/2021
 */

import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  Table,
  // Tag,
  Button,
} from 'antd';
import { ExpandAltOutlined, StarFilled } from '@ant-design/icons';
import {
  IconFacbook,
  IconInstagram,
  IconTiktok,
  IconUser,
} from 'views/components/Icon';
import HostTag from './HostTag';
import { search } from '../actions';
import { utils, constants } from 'helpers';
import { addSeller, removeSeller } from 'views/pages/RecommenderSeller/actions';

const { PAGINATION } = constants;

const HostList = ({
  handleOpen,
  data,
  pagination,
  search,
  isLoading,
  addSeller,
  removeSeller,
  selectedSellers,
}) => {
  const hosts = data;

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    search({ page: PAGINATION.PAGE, perPage: PAGINATION.PER_PAGE });
  }, [search]);

  useEffect(() => {
    if (selectedSellers.length === 0) {
      setSelectedRowKeys([]);
    }
  }, [selectedSellers]);

  const handleTableChange = (pagination, filter, sorter) => {
    let sortType = null;
    if (sorter.order === 'descend') sortType = -1;
    if (sorter.order === 'ascend') sortType = 1;

    const params = {
      page: pagination.current - 1,
      perPage: pagination.pageSize,
      sortCol: sorter.order ? sorter.columnKey : null,
      sortType,
    };
    search(params);
  };

  const renderFollower = (type, number) => {
    let icon;
    switch (type) {
      case 'facebook':
        icon = <IconFacbook />;
        break;
      case 'instagram':
        icon = <IconInstagram />;
        break;
      case 'tiktok':
        icon = <IconTiktok />;
        break;
      case 'other':
      default:
        icon = <small>Other</small>;
        break;
    }
    return number ? (
      <div className="host-follower">
        {icon}
        <br />
        {number}
      </div>
    ) : (
      <div className="host-follower none">
        {icon}
        <br />
        --
      </div>
    );
  };

  const columns = [
    {
      title: 'Name',
      key: 'name',
      render: (record) => (
        <div className="host">
          <div className="host-avatar">
            <img
              src={
                record.avatar
                  ? record.avatar
                  : 'images//user/default_avatar.jpg'
              }
              alt={record.name}
            />
          </div>
          <div className="host-info">
            <div className="host-name">{record.name}</div>
            {record.tags.map((tag, index) => {
              if (index === 3) {
                return (
                  <HostTag
                    tag={`+${record.tags.length - 3}`}
                    key={`${index}_`}
                  />
                );
              }
              if (index > 3) return null;
              return <HostTag tag={tag.name} key={`${index}_${tag.id}`} />;
            })}
          </div>
        </div>
      ),
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
      align: 'center',
    },

    {
      title: 'Exp. Years',
      dataIndex: 'experienceGroup',
      key: 'experience',
      align: 'center',
      sorter: (a, b) => a.experience - b.experience,
    },
    {
      title: 'Followers',
      key: 'social',
      render: (record) => {
        return (
          <div className="host-followers">
            {renderFollower('facebook', record.facebookFollowers)}
            {renderFollower('instagram', record.instagramFollowers)}
            {renderFollower('tiktok', record.tiktokFollowers)}
            {renderFollower('other', record.otherFollowers)}
          </div>
        );
      },
    },
    {
      title: 'Rates',
      dataIndex: 'rates',
      key: 'rates',
      sorter: true,
      render: (rates) => utils.numberWithCommas(rates),
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
      sorter: true,
      render: (value) => {
        return (
          !!typeof value === 'number' && (
            <>
              <StarFilled style={{ color: '#FFDE54' }} />
              {value}
            </>
          )
        );
      },
    },
    {
      title: 'Action',
      width: 80,
      key: 'action',
      align: 'right',
      render: (record) => <IconUser />,
    },
  ];

  const rowSelection = {
    onSelect(record, selected) {
      if (selected) {
        addSeller(record.rowIndex, record.code);
      } else {
        removeSeller(record.rowIndex);
      }
    },
    selectedRowKeys,
    onChange(selectedRowKeys) {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  return (
    <div className="host-list-table">
      <Table
        dataSource={hosts}
        columns={columns}
        rowKey={(record) => record.key}
        rowSelection={rowSelection}
        onChange={handleTableChange}
        onRow={(record, rowIndex) => {
          return {
            onClick: () => {
              if (selectedSellers.length) return;
              handleOpen(record);
            },
          };
        }}
        pagination={{
          position: ['bottomCenter'],
          showSizeChanger: true,
          showTotal: (total, range) =>
            `Showing ${range[0]}-${range[1]} of ${total} results`,
          size: 'small',
          current: pagination.page + 1,
          pageSize: pagination.perPage,
          total: pagination.total,
        }}
        loading={isLoading}
      />
    </div>
  );
};

const mapStateToProps = ({
  hostList,
  recommenderSeller: { selectedSellers },
}) => ({
  pagination: hostList.pagination,
  isLoading: hostList.isLoading,
  selectedSellers,
});

const mapDispatchToProps = {
  search,
  addSeller,
  removeSeller,
};

export default connect(mapStateToProps, mapDispatchToProps)(HostList);
