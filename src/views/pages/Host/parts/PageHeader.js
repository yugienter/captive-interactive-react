import { Button } from 'antd';
import { CloseOutlined, SearchOutlined } from '@ant-design/icons';
import HostListFilter from './filter';
import { useState } from 'react';

const PageHeader = () => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <div className="page-header">
        <h2>Seller by Category</h2>
        <Button
          icon={visible ? <CloseOutlined /> : <SearchOutlined />}
          onClick={() => setVisible(!visible)}
        />
      </div>
      {visible && <HostListFilter />}
    </>
  );
};
export default PageHeader;
