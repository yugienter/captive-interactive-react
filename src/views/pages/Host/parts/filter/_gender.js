
import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Form, Checkbox, } from "antd";
import { search, handlePopupFilter } from "../../actions";
import { constants } from "helpers";

const { FILTER_DEFAULT: { gender } } = constants;

const GenderFilter = (props) => {
  const { genderFilterStore, visible, search, handlePopupFilter } = props;

  const [form] = Form.useForm();
  const [genderCheck, setGender] = useState({ male: false, female: false })

  useEffect(() => {
    if (genderFilterStore === null)
      setGender({ male: false, female: false });
  }, [genderFilterStore])

  useEffect(() => {
    form.setFieldsValue({ male: genderCheck.male, female: genderCheck.female, })
  }, [form, genderCheck])

  const getGenderQuery = () => {
    if (genderCheck.male === genderCheck.female) return null;
    return genderCheck.male ? gender.male : gender.female;
  }

  useEffect(() => {
    if (!visible) search({ gender: getGenderQuery(), })

    let title = gender.title;
    if (!genderCheck.male === genderCheck.female) {
      title = genderCheck.male ? 'Male' : 'Female';
    }

    handlePopupFilter({ key: 'gender', title });

  }, [visible, genderCheck, handlePopupFilter, search])

  const onChecked = (e) => {
    const { id, checked } = e.target;
    setGender({ ...genderCheck, [id]: checked })
  }

  return (
    <div className="filter-content no-padding">
      <div>
        <Form form={form} layout="vertical">
          <Form.Item
            name="male"
            valuePropName="checked"
          >
            <Checkbox onChange={onChecked}>Male</Checkbox>
          </Form.Item>
          <Form.Item
            name="female"
            valuePropName="checked"
          >
            <Checkbox onChange={onChecked}>Female</Checkbox>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

const mapStateToProps = ({ hostList: { stateFilter, filter } }) => ({
  visible: stateFilter.gender.isVisible,
  genderFilterStore: filter.gender,
});

const mapDispatchToProps = {
  search,
  handlePopupFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(GenderFilter);