import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Checkbox, Collapse } from "antd";

const { Panel } = Collapse;

const CheckboxGroup = Checkbox.Group;

const CategoryList = ({ tag, getTagFromChild, tagFilterStore }) => {
  const newTag = { ...tag };

  const onloadOptions = newTag.item.map(item => {
    return {
      label: item.name,
      value: item.id
    }
  })

  const [checkedList, setCheckedList] = useState([]);
  const [indeterminate, setIndeterminate] = useState(false);
  const [checkAll, setCheckAll] = useState(false);

  useEffect(() => {
    if (!tagFilterStore.length) {
      setIndeterminate(false);
      setCheckedList([]);
    }
  }, [tagFilterStore])


  useEffect(() => {
    getTagFromChild({ parentId: newTag.id, checkedList });
  }, [checkedList])

  const onGroupChange = list => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < onloadOptions.length);
    setCheckAll(list.length === onloadOptions.length);
  };

  const onCheckAll = e => {
    const values = onloadOptions.map(item => item.value);
    setCheckedList(e.target.checked ? values : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  return (
    <Collapse expandIconPosition="right" >
      <Panel
        header={
          <>
            <div onClick={(e) => { e.stopPropagation() }} >
              <Checkbox
                indeterminate={indeterminate}
                onChange={onCheckAll}
                checked={checkAll}
              />
            </div>
            {newTag.name}
          </>
        }
      >
        <CheckboxGroup value={checkedList} onChange={onGroupChange}>
          <div className="sub-category">
            {
              newTag.item.map((tagItem, tagIndex) =>
                <div className="sub-category-item" key={tagIndex}>
                  <Checkbox value={tagItem.id}>
                    {tagItem.name}
                  </Checkbox>
                </div>
              )
            }
          </div>
        </CheckboxGroup>
      </Panel>
    </Collapse>
  )
}


const mapStateToProps = ({ hostList: { filter } }) => ({
  tagFilterStore: filter.tag,
});

export default connect(mapStateToProps, {})(CategoryList);
