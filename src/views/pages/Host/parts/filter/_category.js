import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { search, handlePopupFilter } from "../../actions";
import CategoryList from "./_categoryList";
import { constants } from "helpers";

const { FILTER_DEFAULT: { tags } } = constants;

const CategoryFilter = (props) => {

  const { data, visible, search, handlePopupFilter, tagFilterStore } = props;

  const [tagList, setTagList] = useState([]);
  const [tagParentName, setTagParentName] = useState([]);

  const getTagFromChild = ({ parentId, checkedList }) => {
    const tagCheck = data.filter(tag => tag.id === parentId)[0];

    const newTagName = [...tagParentName];
    const indexTagName = newTagName.indexOf(tagCheck.name)
    if (indexTagName !== -1) newTagName.splice(indexTagName, 1);
    if (checkedList.length) {
      newTagName.push(tagCheck.name);
    }
    setTagParentName(newTagName);

    tagCheck.item.forEach(item => {
      const index = tagList.indexOf(item.id);
      if (index !== -1) tagList.splice(index, 1);
      if (checkedList.includes(item.id)) tagList.push(item.id);
    })

    setTagList(tagList);
  }

  useEffect(() => {
    if (!tagFilterStore.length) {
      setTagList([]);
      setTagParentName([]);
    }
  }, [tagFilterStore])

  useEffect(() => {
    if (!visible) {
      search({ tag: tagList });
    }

    let title = tags.title;
    if (tagParentName.length) {
      tagParentName.sort();
      tagParentName.forEach((tagName, index) => {
        if (index === 0) {
          title = tagName;
          return;
        }
        title += `, ${tagName}`;
      })
    }
    if (title.length > 20) {
      title = title.substring(0, 20) + "...";
      const countTagName = (title.match(/,/g) || []).length;
      const restTagName = tagParentName.length - countTagName - 1;
      if (restTagName > 0) title += `+${restTagName}`
    }
    handlePopupFilter({ key: 'tags', title });

  }, [visible, tagList, tagParentName, handlePopupFilter, search])


  return (
    <div className="filter-content  no-padding">
      {
        data.length && data.map((tag, index) =>
          <CategoryList tag={tag} key={index} getTagFromChild={getTagFromChild} />
        )
      }
    </div >
  );
}

const mapStateToProps = ({ hostList: { stateFilter, filter } }) => ({
  visible: stateFilter.tags.isVisible,
  tagFilterStore: filter.tag,
});

const mapDispatchToProps = {
  search,
  handlePopupFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryFilter);
