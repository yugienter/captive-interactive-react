import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Slider } from "antd";
import { search, handlePopupFilter } from "../../actions";
import { constants } from "helpers";

const { FILTER_DEFAULT: { age } } = constants;


const AgeFilter = (props) => {

  const { visible, search, handlePopupFilter } = props;

  const [min, setMin] = useState(age.min);
  const [max, setMax] = useState(age.max);

  useEffect(() => {
    const isMin = min === age.min;
    const isMax = max === age.max;
    if (!visible) {
      const newAgeStart = isMin ? null : min;
      const newAgeEnd = isMax ? null : max;
      search({ ageStart: newAgeStart, ageEnd: newAgeEnd });
    }

    let title = age.title;
    if (!isMin || !isMax) {
      title = `${min} - ${max}`
    }

    handlePopupFilter({ key: 'age', title });

  }, [visible, min, max, handlePopupFilter, search])

  const handleRange = (min, max) => {
    setMin(min);
    setMax(max);
  }

  const onSlideChange = (value) => {
    const min = Number(value[0]);
    const max = Number(value[1]);
    handleRange(min, max);
  }

  return (
    <div className="filter-content">
      <div className="filter-slider">
        <Slider
          range
          value={[min, max]}
          min={age.min}
          max={age.max}
          onChange={onSlideChange}
        />
      </div>
    </div>
  );
}

const mapStateToProps = ({ hostList: { stateFilter } }) => ({
  visible: stateFilter.age.isVisible,
});

const mapDispatchToProps = {
  search,
  handlePopupFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(AgeFilter);