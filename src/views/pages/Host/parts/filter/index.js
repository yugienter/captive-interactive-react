
/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 26/08/2021
 */

import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Popover, } from "antd";
import { DownOutlined } from "@ant-design/icons";
import TitleSubtitle from "views/components/TitleSubtitle";
import CategoryFilter from './_category';
import ExperienceFilter from './_experience';
import GenderFilter from './_gender';
import AgeFilter from './_age';
import RatesFilter from './_rates';
import FilterSearch from './_search';
import ButtonClearFilter from './_clearFilter';
import { constants } from 'helpers';

import { getTags, handlePopupFilter, getRateRange } from '../../actions';

const { FILTER_DEFAULT } = constants

const HostListFilter = (props) => {
  const {
    getTags,
    handlePopupFilter,
    tags,
    stateFilter,
    getRateRange,
    ratesRange,
  } = props;

  useEffect(() => {
    getTags();
    getRateRange();
  }, [getTags, getRateRange])

  const onFilterChange = (isVisible, key) => {
    handlePopupFilter({ key, isVisible });
  }

  const filter = [
    {
      key: "tags",
      component: <CategoryFilter data={tags} />,
      title: stateFilter.tags.title
        ? stateFilter.tags.title
        : FILTER_DEFAULT.tags.title,
      subtitle: "Category",
      style: { minWidth: '180px' },
    },
    {
      key: "experience",
      component: <ExperienceFilter />,
      title: stateFilter.experience.title
        ? stateFilter.experience.title
        : FILTER_DEFAULT.experience.title,
      subtitle: "Experience",
      style: { minWidth: '140px' },
    },
    {
      key: "gender",
      component: <GenderFilter />,
      title: stateFilter.gender.title
        ? stateFilter.gender.title
        : FILTER_DEFAULT.gender.title,
      subtitle: "Gender",
      style: { minWidth: '100px' },
    },
    {
      key: "age",
      component: <AgeFilter />,
      title: stateFilter.age.title
        ? stateFilter.age.title
        : FILTER_DEFAULT.age.title,
      subtitle: "Age Range",
      style: { minWidth: '100px' },
    },
    {
      key: "rates",
      component: <RatesFilter rates={ratesRange} />,
      title: stateFilter.rates.title
        ? stateFilter.rates.title
        : FILTER_DEFAULT.rates.title,
      subtitle: "Rate",
      style: { minWidth: '100px' },
    },
  ]

  return (
    <div className="host-list-filter">
      <FilterSearch />
      {
        filter.map(item => {
          return (
            <FilterItem
              onChange={(visible) => onFilterChange(visible, item.key)}
              dropdown={item.component}
              key={item.key}
            >
              <TitleSubtitle
                title={item.title}
                subtitle={item.subtitle}
                style={item.style}
                reverse
              />
            </FilterItem>
          )
        })
      }
      <ButtonClearFilter />
    </div>
  );
}

const FilterItem = ({ children, dropdown, onChange, value }) => (
  <div className="filter-item">
    <Popover
      trigger="click"
      placement="bottom"
      content={dropdown}
      overlayClassName="filter-popover"
      onVisibleChange={onChange}
    >
      <div className="filter-body">
        {children}
        <div className="filter-icon">
          <DownOutlined />
        </div>
      </div>
    </Popover>
  </div>
);

const mapStateToProps = ({ hostList }) => ({
  tags: hostList.tags || [],
  stateFilter: hostList.stateFilter,
  ratesRange: hostList.filterRange.rates,
});

const mapDispatchToProps = {
  getTags,
  handlePopupFilter,
  getRateRange,
};

export default connect(mapStateToProps, mapDispatchToProps)(HostListFilter);