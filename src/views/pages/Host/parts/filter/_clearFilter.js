/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 29/08/2021
 */

import React from "react";
import { connect } from "react-redux";
import { Button } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import { clearFilter } from "../../actions";
import * as types from "../../actionTypes";

const ButtonClearFilter = ({ clearFilter, makeLoading }) => {
  const fallbackDelay = 500;

  const clearAllFilter = () => {
    makeLoading(true);
    clearFilter();
    setTimeout(function () {
      makeLoading(false);
    }, fallbackDelay);
  };

  return (
    <Button
      type="link"
      onClick={clearAllFilter}
      icon={<CloseOutlined size="small" />}
    >
      Clear Filters
    </Button>
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  clearFilter: () => dispatch(clearFilter()),
  makeLoading: (delayed) =>
    dispatch({ type: types.UPDATE_LOADING, payload: delayed }),
});

export default connect(mapStateToProps, mapDispatchToProps)(ButtonClearFilter);
