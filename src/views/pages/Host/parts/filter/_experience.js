import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Radio, Space } from "antd";
import { search, handlePopupFilter } from "../../actions";
import { constants } from "helpers";

const { FILTER_DEFAULT: { experience } } = constants;

const radioOptions = Object.entries(experience.groups).map(item => {
  return { value: item[0], label: item[1] };
});

const ExperienceFilter = (props) => {

  const { visible, search, handlePopupFilter } = props;

  const [value, setValue] = useState(experience.all);

  useEffect(() => {
    const isAll = value === experience.all;
    if (!visible) {
      search({ exp: isAll ? null : value });
    }

    let title = experience.title;
    if (!isAll) {
      title = experience.groups[value];
    }

    handlePopupFilter({ key: 'experience', title });
  }, [visible, value, handlePopupFilter, search])

  const onChange = (e) => {
    const { value } = e.target;
    setValue(value);
  }

  return (
    <div className="filter-content">
      <Radio.Group
        onChange={onChange}
        value={value}
      >
        <Space direction="vertical">
          {
            radioOptions.map(option => {
              return <Radio key={option.value} value={option.value}>{option.label}</Radio>
            })
          }
        </Space>
      </Radio.Group>
    </div>
  );
}

const mapStateToProps = ({ hostList: { stateFilter } }) => ({
  visible: stateFilter.experience.isVisible,
});

const mapDispatchToProps = {
  search,
  handlePopupFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperienceFilter);