import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Input } from "antd";
import { search } from "../../actions";

const FilterSearch = ({ search, textSearchStore }) => {

  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    if (textSearchStore === null) {
      setInputValue('');
    }
  }, [textSearchStore])

  const onSearch = (value) => {
    search({ search: value });
  }

  return (
    <div className="filter-item filter-search">
      <Input.Search
        placeholder="Search by keyword..."
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
        onSearch={onSearch} />
    </div>
  );
}

const mapStateToProps = ({ hostList: { filter } }) => ({
  textSearchStore: filter.search,
});

const mapDispatchToProps = {
  search,
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterSearch);