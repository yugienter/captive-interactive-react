import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Slider } from "antd";
import { search, handlePopupFilter } from "../../actions";
import { constants } from "helpers";

const { FILTER_DEFAULT: { rates: ratesConstants } } = constants;

const RatesFilter = (props) => {
  const { rates, visible, search, handlePopupFilter } = props;

  const [min, setMin] = useState(rates.min);
  const [max, setMax] = useState(rates.max);

  useEffect(() => {
    if (rates.min && rates.max) {
      setMin(rates.min);
      setMax(rates.max);
    }
  }, [rates])

  useEffect(() => {
    const isMax = max === rates.max;
    const isMin = min === rates.min;
    if (!visible) {
      const newRateStart = isMin ? null : min;
      const newRateEnd = isMax ? null : max;
      search({ rateStart: newRateStart, rateEnd: newRateEnd });
    }

    let title = ratesConstants.title;
    if (min !== null && max !== null && (!isMin || !isMax)) {
      let titleMax = isMax ? 'Max' : `$${max}`;
      let titleMin = isMin ? 'Min' : `$${min}`;
      title = `${titleMin} - ${titleMax}`
    }
    handlePopupFilter({ key: 'rates', title });
  }, [visible, min, max, handlePopupFilter, search, rates])

  const handleRange = (min, max) => {
    setMin(min);
    setMax(max);
  }

  const onSlideChange = (value) => {
    const min = Number(value[0]);
    const max = Number(value[1]);
    handleRange(min, max);
  }

  return (
    <div className="filter-content">
      <div className="filter-slider">
        <Slider
          range
          value={[min, max]}
          min={rates.min}
          max={rates.max}
          onChange={onSlideChange}
        />
      </div>
    </div>
  );
}

const mapStateToProps = ({ hostList: { stateFilter } }) => ({
  visible: stateFilter.rates.isVisible,
});

const mapDispatchToProps = {
  search,
  handlePopupFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(RatesFilter);