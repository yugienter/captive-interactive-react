import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { Modal, Row, Col, Carousel, Button } from "antd";
import {
  CheckCircleOutlined,
  LeftOutlined,
  RightOutlined,
} from "@ant-design/icons";
import TitleSubtitle from "views/components/TitleSubtitle";
import HostTag from "./HostTag";
import {
  IconFacbook,
  IconInstagram,
  IconTiktok,
  IconPlay,
} from "views/components/Icon";
import { utils } from "helpers";
import { selectHost } from "../actions";

const HostModal = (props) => {

  const {
    visible,
    handleClose,
    host,
    showNextPrev,
    rowPosition,
    selectHostAction,
  } = props;

  const {
    name,
    avatar,
    gallery,
    title,
    age,
    physique,
    experienceGroup,
    rates,
    tags,
    about,
    gender,
    social,
  } = host;

  const messagesEndRef = useRef(null);

  const [selectHostLoading, setSelectHostLoading] = useState(false);
  const [showMessage, setMessage] = useState('');

  const scrollToBottom = () => {
    if (showMessage)
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(() => {
    setMessage('');
  }, [host.key]);

  useEffect(scrollToBottom, [showMessage]);

  const renderImage = gallery.map(
    (url, key) => {
      if (utils.isImage(url)) return (
        <img key={key} alt="Host" src={url} />
      )

      if (utils.isVideo(url)) return (
        <video
          key={key}
          controls
          src={url}
        />
      )
      return <img key={key} alt="Host" src={url} />
    });

  const styleOpacity = {
    backgroundColor: "#f0f0f0",
    opacity: 0.3,
  };
  const disablePrev = ["first", "one"].includes(rowPosition);
  const disableNext = ["last", "one"].includes(rowPosition);

  const selectHost = async () => {
    setSelectHostLoading(true);
    await selectHostAction({ hostId: host.key });
    setSelectHostLoading(false);
    setMessage('Captive will get back to you soon with futher update');
  }

  const renderSocialNetwork = (key, data) => {
    let icon;
    switch (key) {
      case 'facebook':
        icon = <IconFacbook />
        break;
      case 'instagram':
        icon = <IconInstagram />
        break;
      case 'tiktok':
        icon = <IconTiktok />
        break;
      default:
        icon = <IconFacbook />
        break;
    };
    return <TitleSubtitle
      reverse
      subtitle={utils.ucwords(key)}
      title={utils.numberWithCommas(host[`${key}Followers`])}
      icon={icon}
      link={utils.getSocialLink(key, data[key])}
    />
  }

  return (
    <Modal
      centered
      footer={null}
      visible={visible}
      className="host-modal"
      onOk={handleClose}
      onCancel={handleClose}
    >
      <div className="prev">
        <Button
          icon={<LeftOutlined />}
          shape="circle"
          size="large"
          disabled={disablePrev}
          style={disablePrev ? styleOpacity : {}}
          onClick={() => showNextPrev({ order: -1, currentId: host.key })}
        />
      </div>
      <div className="next">
        <Button
          icon={<RightOutlined />}
          shape="circle"
          size="large"
          disabled={disableNext}
          style={disableNext ? styleOpacity : {}}
          onClick={() => showNextPrev({ order: 1, currentId: host.key })}
        />
      </div>
      <Row gutter={20}>
        <Col lg={12}>
          <Carousel autoplay>{renderImage}</Carousel>
        </Col>
        <Col lg={12}>
          <div className="host-info scrollbar">
            <div className="host-avatar">
              <div className="image">
                <img src={avatar} alt={name} />
              </div>
              <div>
                <h2>
                  {name}
                  <IconPlay />
                </h2>
                <h4>{title}</h4>
              </div>
            </div>

            <div className="main-info">
              <TitleSubtitle reverse subtitle="Age" title={age} />
              <TitleSubtitle
                reverse
                subtitle="Height"
                title={physique.height}
              />
              <TitleSubtitle
                reverse
                subtitle="Weight"
                title={physique.weight}
              />
              <TitleSubtitle reverse subtitle="Gender" title={gender} />
            </div>
            <h3 className='followrsTitle'>Followers</h3>
            <div className="followers">
              {renderSocialNetwork('facebook', social)}
              {renderSocialNetwork('instagram', social)}
              {renderSocialNetwork('tiktok', social)}
            </div>
            <h3 className='categoriesTitle'>Categories</h3>
            <div className="category">
              {tags.map((tag) => (
                <HostTag tag={tag.name} key={tag.name} />
              ))}
            </div>
            <h3 className='aboutTitle'>About</h3>
            <div
              className="about"
              dangerouslySetInnerHTML={{ __html: about }}
            />
            <div className="other-info">
              <TitleSubtitle reverse subtitle="Experience" title={experienceGroup} />
              <TitleSubtitle reverse subtitle="Rates" title={utils.numberWithCommas(rates)} />
            </div>
            <div className="action">
              <Button
                icon={<CheckCircleOutlined />}
                onClick={selectHost}
                loading={selectHostLoading}
              >
                Contact seller
              </Button>
            </div>
            {showMessage && <p style={{ color: 'red', textAlign: 'center' }}>{showMessage}</p>}
            <div ref={messagesEndRef} />
          </div>
        </Col>
      </Row>
    </Modal>
  );
};

const mapStateToProps = ({ hostList }) => ({
});

const mapDispatchToProps = {
  selectHostAction: selectHost
};

export default connect(mapStateToProps, mapDispatchToProps)(HostModal);
