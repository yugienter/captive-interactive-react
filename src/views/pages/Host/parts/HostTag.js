import { Tag } from "antd";
import {
  // IconHealth,
  // IconTravel,
  IconFood
} from "views/components/Icon";
import {
  IconOrganicFood,
  IconRawFood,
  IconHealthProduct,
  IconReadyToEat,
  IconSnack,
  IconSupplements,
  IconBabyFood,
  IconBabyFurniture,
  IconBabyToy,
  IconBabyWear,
  IconAudio,
  IconGaming,
  IconHomeAppliance,
  IconUtilityGadget,
  IconAccessories,
  IconBags,
  IconEyewear,
  IconFemale,
  IconMale,
  IconSportsFashion,
  IconWatch,
  IconBabyGoods,
  IconEducationalProduct,
  IconInfantCare,
  IconToddlerProduct,
  IconToy,
} from "views/components/Icon/category";
import { constants } from "helpers";

const { category } = constants;

const HostTag = ({ tag }) => {
  switch (tag) {
    case `${category.Food}`:
      return <Tag icon={<IconFood />}>{tag}</Tag>;

    case `${category.OrganicFood}`:
      return <Tag icon={<IconOrganicFood />}>{tag}</Tag>;

    case `${category.RawFood}`:
      return <Tag icon={<IconRawFood />}>{tag}</Tag>;

    case `${category.ReadytoEat}`:
      return <Tag icon={<IconReadyToEat />}>{tag}</Tag>;

    case `${category.Snacks}`:
      return <Tag icon={<IconSnack />}>{tag}</Tag>;

    case `${category.HealthProducts}`:
      return <Tag icon={<IconHealthProduct />}>{tag}</Tag>;

    case `${category.UtilityGadgets}`:
      return <Tag icon={<IconUtilityGadget />}>{tag}</Tag>;

    case `${category.HomeAppliances}`:
      return <Tag icon={<IconHomeAppliance />}>{tag}</Tag>;

    case `${category.Gaming}`:
      return <Tag icon={<IconGaming />}>{tag}</Tag>;

    case `${category.Audio}`:
      return <Tag icon={<IconAudio />}>{tag}</Tag>;

    case `${category.BabyFurniture}`:
      return <Tag icon={<IconBabyFurniture />}>{tag}</Tag>;

    case `${category.BabyToys}`:
      return <Tag icon={<IconBabyToy />}>{tag}</Tag>;

    case `${category.BabyFood}`:
      return <Tag icon={<IconBabyFood />}>{tag}</Tag>;

    case `${category.BabyWear}`:
      return <Tag icon={<IconBabyWear />}>{tag}</Tag>;

    case `${category.BabyGoods}`:
      return <Tag icon={<IconBabyGoods />}>{tag}</Tag>;

    case `${category.ToddlerProducts}`:
      return <Tag icon={<IconToddlerProduct />}>{tag}</Tag>;

    case `${category.InfantCare}`:
      return <Tag icon={<IconInfantCare />}>{tag}</Tag>;

    case `${category.Toys}`:
      return <Tag icon={<IconToy />}>{tag}</Tag>;

    case `${category.EducationalProducts}`:
      return <Tag icon={<IconEducationalProduct />}>{tag}</Tag>;

    case `${category.Bags}`:
      return <Tag icon={<IconBags />}>{tag}</Tag>;

    case `${category.SportsFashion}`:
      return <Tag icon={<IconSportsFashion />}>{tag}</Tag>;

    case `${category.Eyewear}`:
      return <Tag icon={<IconEyewear />}>{tag}</Tag>;

    case `${category.Watches}`:
      return <Tag icon={<IconWatch />}>{tag}</Tag>;

    case `${category.Accessories}`:
      return <Tag icon={<IconAccessories />}>{tag}</Tag>;

    case `${category.Male}`:
      return <Tag icon={<IconMale />}>{tag}</Tag>;

    case `${category.Female}`:
      return <Tag icon={<IconFemale />}>{tag}</Tag>;

    case `${category.Supplements}`:
      return <Tag icon={<IconSupplements />}>{tag}</Tag>;

    default:
      return <Tag >{tag}</Tag>;
  }
};
export default HostTag;
