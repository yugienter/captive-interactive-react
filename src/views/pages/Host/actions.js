/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 26/08/2021
 */

import { constants, client, utils } from "helpers";
import moment from "moment";

import * as types from "./actionTypes";
import _ from "lodash";

const { api, PAGINATION } = constants;
// const { statusCode } = api;

// ----------------------- Make Host Data Result ----------------------- //

const _makeSocialResult = (social) => {
  for (const key in social) {
    social[key] = social[key] || "";
  }
  return social;
};

const _makePhotosResult = (media) => {
  if (media && media.length > 0) {
    return media.map(({ url }) => utils.getMediaUrl(url));
  }
  return ["images//user/no_image_available.jpg"];
};

const _makeAvatarResult = (avatar) => {
  if (avatar) return utils.getMediaUrl(avatar);

  return "images//user/default_avatar.jpg";
};

export const _mapDataHostResponse = (host, index) => {
  let gender = "#";
  if (host.sex) gender = host.sex === "male" ? "Male" : "Female";

  const yearExperience = utils.getYearOfExperience(host.startDate);

  return {
    rowIndex: index,
    key: host.code,
    code: host.code,
    rating: host.rate,
    avatar: _makeAvatarResult(host.avatar),
    gallery: _makePhotosResult(host.media),
    name: host.name || "",
    title: host.title || "", // handel new variable in server
    age: host.dob ? moment().diff(moment(host.dob), "years") : "#",
    gender,
    country: host.country,
    phone: host.phoneNumber,
    physique: {
      weight: `${host.weight || '#'}kg`,
      height: `${host.height || '#'}cm`,
    },
    experience: host.experience,
    yearExperience,
    experienceGroup: utils.getExperienceGroup(yearExperience),
    tags: host.tags, // id and name "tags": [{name, id}]
    social: _makeSocialResult(host.social),
    facebookFollowers: utils.kFormatter(host.facebookFollowers),
    instagramFollowers: utils.kFormatter(host.instagramFollowers),
    tiktokFollowers: utils.kFormatter(host.tiktokFollowers),
    otherFollowers: utils.kFormatter(host.otherFollowers),
    followers: host.followers || 0,
    rates: `$${host.rates || 0}`,
    about: host.aboutMe,
    porfolio: host.porfolio,
  };
};

// ----------------------- Query and Search Data Function ----------------------- //

const _updateStore = (key, value) => (dispatch) => {
  const payload = key === "tag" ? { [key]: [...value] } : { [key]: value };
  dispatch({
    type: types.UPDATE_FILTER,
    payload,
  });
};

const _makeQuery = (options, stateHostList) => (dispatch) => {
  const { filter, pagination } = stateHostList;
  const query = {};

  Object.entries(filter).forEach(([key, value]) => {
    const condition = key !== "tag" ? value : value.length;
    const queryKey = utils.camelCaseToUnderScore(key);
    if (condition || !isNaN(value)) {
      query[queryKey] = value;
    }
  });

  const { search = "" } = filter;
  if (!search) delete query.search;

  Object.entries(options).forEach(([key, value]) => {
    const condition = key !== "tag" ? value : value.length;
    const queryKey = utils.camelCaseToUnderScore(key);
    if (condition || !isNaN(value)) {
      query[queryKey] = value;
      if (!["page", "perPage"].includes(key)) {
        _updateStore(key, value)(dispatch);
      }
    }
  });
  /**
   * if option have no page and perPage mean it is the filter.
   * then reset page to default
   * if option have page and perPage mean action in table,
   * then get page and perPage and sort query with param of table.
   */
  const { page = PAGINATION.PAGE, perPage = pagination.perPage } = options;
  query.page = page;
  query.per_page = perPage;

  return query;
};

const _doSearch = async (query = {}) => {
  try {
    const res = await client.request({
      path: api.path.getHosts,
      method: "get",
      params: query,
    });

    const { data, page, perPage, total, lastPage } = res.data.data;

    const hosts = data.map((host, index) => _mapDataHostResponse(host, index));

    return {
      data: hosts || [],
      pagination: { page, perPage, total, lastPage },
    };
  } catch (error) {
    console.log("[_doSearch] Error: ", error);
    return { data: [], pagination: {} };
  }
};

export const search =
  (options = {}) =>
  async (dispatch, getState) => {
    dispatch({ type: types.UPDATE_LOADING, payload: true });
    const stateHostList = getState().hostList;
    let data = stateHostList.data;
    const newFilterDiffState = _.reduce(
      options,
      function (result, value, key) {
        return _.isEqual(value, stateHostList.filter[key])
          ? result
          : result.concat(key);
      },
      []
    );
    if (newFilterDiffState.length) {
      dispatch({ type: types.CLEAR_FILTER_RESET });
      const query = _makeQuery(options, stateHostList)(dispatch);
      const result = await _doSearch(query);
      data = result.data;
      dispatch({ type: types.UPDATE_DATA, payload: result });
    }
    dispatch({ type: types.UPDATE_LOADING, payload: false });
    return data;
  };

export const getTags = () => async (dispatch) => {
  dispatch({ type: types.FETCH_TAGS });

  try {
    const res = await client.request({
      path: api.path.getTags,
      method: "get",
    });
    const aTags = res.data.data;
    const parentTags = aTags.filter((tag) => !tag.parentCode);
    const result = parentTags.map((parentTag) => {
      parentTag.item = [];
      aTags.forEach((tag) => {
        if (tag.parentCode === parentTag.code) {
          parentTag.item.push({
            id: tag.code,
            name: tag.name,
          });
        }
      });
      return {
        id: parentTag.code,
        name: parentTag.name,
        item: parentTag.item,
      };
    });

    const tagsById = aTags.reduce((result, tag) => {
      result[tag.code] = tag;
      return result;
    }, {});

    dispatch({
      type: types.FETCH_TAGS_SUCCESS,
      payload: [result, tagsById],
    });

    return res.data.data;
  } catch (err) {
    if (err.response) {
      if (err.response.status >= 400) {
        dispatch({
          type: types.FETCH_TAGS_FAIL,
          message: err.response.data.message,
        });
      }
    }
    return null;
  }
};

export const handlePopupFilter =
  ({ key, isVisible, title }) =>
  (dispatch) => {
    dispatch({
      type: types.UPDATE_STATE_FILTER,
      payload: { key, isVisible, title },
    });
  };

export const getRateRange = () => async (dispatch) => {
  dispatch({ type: types.GET_RATE_RANGE });

  try {
    const res = await client.request({
      path: api.path.getRateRange,
      method: "post",
    });

    let { min, max } = res.data.data;

    min =
      min < 10
        ? 0
        : utils.getFirsDigit(min) * 10 ** Math.floor(utils.log10(min));
    max = max < 10 ? 10 : Math.ceil(max / 100) * 100;

    dispatch({
      type: types.GET_RATE_RANGE_SUCCESS,
      payload: { min, max },
    });
  } catch (err) {
    if (err.response) {
      if (err.response.status >= 400) {
        dispatch({
          type: types.GET_RATE_RANGE_FAIL,
          message: err.response.data.message,
        });
      }
    }
  }
};

export const clearFilter = () => async (dispatch, getState) => {
  const { checkClearFilter } = getState().hostList;
  if (checkClearFilter) return;
  dispatch({ type: types.CLEAR_FILTER });
  const stateHostList = getState().hostList;
  const query = _makeQuery({}, stateHostList)(dispatch);
  const result = await _doSearch(query);
  dispatch({ type: types.UPDATE_DATA, payload: result });
};

// ------------------ Host Modal --------------------------- //

export const selectHost = (data) => async (dispatch) => {
  try {
    const res = await client.request({
      path: api.path.selectHost,
      method: "post",
      data,
    });
    const { message } = res.data.data;
    return { message };
  } catch (err) {
    if (err.response) {
      if (err.response.status >= 400) {
        return { message: "failed" };
      }
    }
  }
};
