/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 26/08/2021
 */

import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Page from 'views/components/Page';
import PageHeader from './parts/PageHeader';
import List from './parts/List';
import { constants } from 'helpers';
import { search } from './actions';
import { Button } from 'antd';
import DirectOfferDrawer from 'views/components/DirectOfferDrawer';
import DrawerSeller from 'views/components/ModalRight';
import { IconClose, IconSendOffer } from 'views/components/Icon/recommender';
import { resetSelectedSellers } from '../RecommenderSeller/actions';

const { userRoles } = constants;

const HostPage = ({
  hosts,
  pagination,
  search,
  tags,
  selectedSellers,
  resetSelectedSellers,
  filteredTag
}) => {
  const [showDrawer, setToggleModal] = useState(false);
  const [hostInfo, setHostInfo] = useState({});
  const [rowPosition, setRowPosition] = useState(''); // fist/last/one/empty
  const [directOffer, setDirectOffer] = useState(false);
  const [selectedTag, setSelectedTag] = useState(false);
  const [clicked, setClick] = useState(false);

  const { page, perPage, lastPage, total } = pagination;

  useEffect(() => {
    if (showDrawer)
      document.body.style.setProperty('overflow', 'hidden', 'important');
  }, [showDrawer]);

  useEffect(() => {
    setDirectOffer(!!selectedSellers.length);
  }, [selectedSellers, setDirectOffer]);

  useEffect(() => {
    if (!filteredTag || !filteredTag.length) {
      setSelectedTag(null);
    }
  }, [filteredTag]);

  const getRowPosition = (hostKey) => {
    if (hosts.length === 1) {
      setRowPosition('one');
      return;
    }
    const currentHostInfoIndex = hosts.findIndex(
      (host) => host.key === hostKey,
    );

    let position = '';
    if (page === 0 && currentHostInfoIndex === 0) position = 'first';

    if (page + 1 === lastPage && currentHostInfoIndex + 1 === total % perPage) {
      position = 'last';
    }
    setRowPosition(position);
  };

  const handleHostInfo = (hostInfo) => {
    getRowPosition(hostInfo.key);
    setHostInfo(hostInfo);
  };

  const handleOpen = (hostInfo) => {
    console.log('click', hostInfo);
    handleHostInfo(hostInfo);
    setToggleModal(true);
  };

  const handleClose = () => {
    setHostInfo({});
    setToggleModal(false);
  };

  const handleSelectingTag = (tag) => {
    if (selectedTag === tag.id) {
      setSelectedTag(null);
      search({ tag: [] });
      return;
    }

    setSelectedTag(tag.id);
    search({ tag: tag.item.map((item) => item.id) });
  };

  const selectedSellersCount = selectedSellers.filter((item) => !!item).length;
  return (
    <Page
      helmet="Host List Page"
      selectedKeys={['host-list']}
      alignTop
      requiredAccess={[userRoles.COMPANY]}
    >
      <div className="host-list-page">
        <PageHeader />
        <div className="tags-bar">
          {tags.map((tag) => {
            return (
              <Button
                className={`tag${tag.id === selectedTag ? ' active' : ''}`}
                type="text"
                key={tag.id}
                onClick={() => handleSelectingTag(tag)}
              >
                {tag.name}
              </Button>
            );
          })}
        </div>
        <List handleOpen={handleOpen} data={hosts} />
      </div>
      <DrawerSeller
        showDrawer={showDrawer}
        isDirectOffer={false}
        onCloseDrawer={handleClose}
        seller={hostInfo}
      />
      <DirectOfferDrawer
        showDrawer={(showDrawer || clicked) && directOffer}
        onCloseDrawer={() => {
          setDirectOffer(false);
          setClick(false);
        }}
      />
      {!!selectedSellersCount && (
        <div className="recommender-seller-container_popup">
          <div className="lbl-selected">
            Selected Sellers: <span>{selectedSellersCount}</span>
          </div>
          <div className="group-buttons">
            <Button
              type="primary"
              className="unselect"
              onClick={() => resetSelectedSellers()}
            >
              <IconClose />
              Unselect
            </Button>
            <Button
              type="primary"
              className="send-offer"
              onClick={() => {
                setDirectOffer(true);
                setClick(true);
              }}
            >
              <IconSendOffer />
              Send Offer
            </Button>
          </div>
        </div>
      )}
    </Page>
  );
};

const mapStateToProps = ({
  hostList,
  recommenderSeller: { selectedSellers },
}) => ({
  hosts: hostList.data,
  isLoading: hostList.isLoading,
  pagination: hostList.pagination,
  tags: hostList.tags,
  filteredTag: hostList.filter.tag,
  selectedSellers,
});

const mapDispatchToProps = {
  search,
  resetSelectedSellers,
};

export default connect(mapStateToProps, mapDispatchToProps)(HostPage);
