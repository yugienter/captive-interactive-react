/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 26/08/2021
 */

import * as types from "./actionTypes";
import { constants } from "helpers";
const { PAGINATION } = constants;

const initialState = {
  isLoading: false,
  data: [],
  tags: [],
  tagsById: {},
  filter: {
    search: null,
    tag: [],
    exp: null,
    gender: null,
    ageStart: null,
    ageEnd: null,
    rateStart: null,
    rateEnd: null,
    sortCol: null,
    sortType: null,
  },
  pagination: {
    page: PAGINATION.PAGE,
    perPage: PAGINATION.PER_PAGE,
    total: null,
    lastPage: null,
  },
  filterRange: {
    rates: {
      min: null,
      max: null,
    },
  },
  stateFilter: {
    tags: {
      isVisible: false,
      title: "",
    },
    experience: {
      isVisible: false,
      title: "",
    },
    gender: {
      isVisible: false,
      title: "",
    },
    age: {
      isVisible: false,
      title: "",
    },
    rates: {
      isVisible: false,
      title: "",
    },
  },
  checkClearFilter: true,
};

const hostListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.UPDATE_LOADING:
      return {
        ...state,
        isLoading: !!payload,
      };

    case types.UPDATE_DATA: {
      return {
        ...state,
        data: payload.data,
        pagination: payload.pagination,
      };
    }

    case types.UPDATE_PAGINATION: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          payload,
        },
      };
    }

    case types.UPDATE_FILTER: {
      return {
        ...state,
        filter: {
          ...state.filter,
          ...payload,
        },
      };
    }

    case types.UPDATE_SEARCH_TEXT: {
      const tmpFilter = state.filter;
      tmpFilter.filter.search = payload;
      return {
        ...state,
        filter: tmpFilter,
      };
    }

    case types.UPDATE_TABLE_SORT: {
      return {
        ...state,
        filter: {
          ...state.filter,
          ...payload,
        },
      };
    }

    case types.FETCH_TAGS: {
      return { ...state, isLoadingTags: true };
    }
    case types.FETCH_TAGS_SUCCESS: {
      return {
        ...state,
        tags: payload[0],
        tagsById: payload[1],
        isLoadingTags: false,
      };
    }
    case types.FETCH_TAGS_FAIL: {
      return { ...state, isLoadingTags: false };
    }

    case types.UPDATE_STATE_FILTER: {
      const { key, isVisible, title } = payload;
      const tmpFilter = { ...state.stateFilter[key] };
      if (typeof isVisible === "boolean") tmpFilter.isVisible = isVisible;
      if (title) tmpFilter.title = title;
      return {
        ...state,
        stateFilter: {
          ...state.stateFilter,
          [key]: tmpFilter,
        },
      };
    }

    case types.GET_RATE_RANGE_SUCCESS: {
      return {
        ...state,
        filterRange: {
          rates: payload,
        },
      };
    }

    case types.CLEAR_FILTER: {
      const tmpStateFilter = { ...state.stateFilter };
      Object.entries(tmpStateFilter).forEach(([key, value]) => {
        tmpStateFilter[key] = { ...value, title: "" };
      });
      const clearFilter = {
        search: null,
        tag: [],
        exp: null,
        gender: null,
        ageStart: null,
        ageEnd: null,
        rateStart: null,
        rateEnd: null,
        // sortCol: null,
        // sortType: null,
      };
      return {
        ...state,
        filter: { ...state.filter, ...clearFilter },
        stateFilter: tmpStateFilter,
        checkClearFilter: true,
      };
    }
    case types.CLEAR_FILTER_RESET: {
      return {
        ...state,
        checkClearFilter: false,
      };
    }

    default:
      return state;
  }
};

export default hostListReducer;
