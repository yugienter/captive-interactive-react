import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { Form, Input, Checkbox, Button } from "antd";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import { IconFacbook, IconGoogle } from "views/components/Icon";
import WelcomeSlider from "views/components/WelcomeSlider";
import PolicyModal from "views/components/PolociModal/_policyModal";
import { IconLock, IconArrowRightWhite, IconStar } from 'views/components/Icon/basic' 
import { IconEnvelope } from 'views/components/Icon/essentials'
import { IconEye } from 'views/components/Icon/authoring'

const SignUp = () => {

  const [policyVisible, setPolicyVisible] = useState(false);

  const closePolicyModal = () => {
    setPolicyVisible(false)
  }
  const openPolicyModal = () => {
    setPolicyVisible(true);
  }

  return (
    <Page helmet="Sign Up" layout="split">
      <div className="primary-section">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            <h2>Sign Up</h2>
            <p>
              Already have an account? <Link to="sign-in">Sign in</Link>
            </p>
            <Form layout="vertical">
              <Form.Item label="Company Name">
                <Input
                  size="large"
                  prefix={<IconStar />}
                  placeholder="e.g. Captive Interactive"
                />
              </Form.Item>
              <Form.Item label="Email">
                <Input
                  size="large"
                  prefix={<IconEnvelope />}
                  placeholder="e.g. captive@interactive.sg"
                />
              </Form.Item>
              <Form.Item
                label="Password"
                help="At least 8 characters. Contains a number or symbol."
              >
                <Input.Password
                  size="large"
                  prefix={<IconLock />}
                  suffix={<IconEye />}
                />
              </Form.Item>
              <Form.Item>
                <Checkbox>
                  I agree to the <Link onClick={openPolicyModal}>Terms &amp; Conditions</Link>
                </Checkbox>
              </Form.Item>
              <Form.Item>
                <Button type="primary" size="large" className="cta" block>
                  Start Platform <IconArrowRightWhite />
                </Button>
              </Form.Item>
            </Form>
            <div className="third-party">
              <p>Or Continue with:</p>
              <Button icon={<IconGoogle />} size="large" />
              <Button icon={<IconFacbook />} size="large" />
            </div>
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section">
        <WelcomeSlider />
      </div>
      <PolicyModal visible={policyVisible} handleClose={closePolicyModal} />
    </Page>
  );
}


export default SignUp;
