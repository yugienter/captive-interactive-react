import React, { useState } from 'react';
import Page from "views/components/Page";
import TopBar from "./components/TopBar"
import MainTabs from "./components/MainTabs"
import OrderSubmission from "./components/OrderSubmission"
import OrderManagement from "./components/OrderManagement"
import { connect } from 'react-redux';

import { constants } from "helpers";

const { userRoles } = constants;

function JobOrder({ loading }) {
  const [tab, setTab] = useState('submission')
  return (
    <Page
      helmet="Job Order"
      alignTop
      requiredAccess={[userRoles.HOST]}
      loadingPage={loading}
    >
      <div className="job-order">
        <TopBar />
        <MainTabs changeTab={(tab) => setTab(tab)} />
        {tab === 'submission' && <OrderSubmission />}
        {tab === 'management' && <OrderManagement />}
      </div>
    </Page>
  );
}

const mapStateToProps = ({ jobOrder }) => ({
  loading: jobOrder.loading,
});

const mapDispatchToProps = {
};

export default
  connect(mapStateToProps, mapDispatchToProps)(JobOrder)
  ;
