import { constants, client, utils, router } from 'helpers';
import config from 'config';

import * as types from './actionTypes';

const { api } = constants;
const { API_END_POINT } = config;
const { getMediaUrl } = utils

export const uploadMedia = (formData, params) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });

        const { data } = await client.request({
            path: api.path.uploadMedia,
            method: 'post',
            data: formData,
            headers: { 'Content-Type': 'multipart/form-data' },
            params
        });
        if (data.data && data.data.url) {
            dispatch({ type: types.ADD_PROOF, payload: [data.data.url] });
            return data.data.url;
        }
        return '';
    } catch (err) {
        console.log('[uploadMedia]', err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
};

export const selectJob = (job) => async (dispatch) => {
    dispatch({ type: types.JOB, payload: job });
};

export const getOrder = (code) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getJobOrders(code),
            method: 'get',
        });
        dispatch({ type: types.SET_ORDER_SUBMISSION, payload: data.data });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const getOrderManagement = (code, status) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getJobOrderManagement(code) + `${status ? '?status=' + status : ''}`,
            method: 'get',
        });
        dispatch({ type: types.SET_ORDER_MANAGEMENT, payload: data });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const getJobStatistic = (code) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getJobOrderStatistic(code),
            method: 'get',
        });
        dispatch({ type: types.SET_JOB_STATISTIC, payload: data });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const getJobDetail = (code) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getCampaignAndDeal(code),
            method: 'get',
        });
        dispatch({ type: types.JOB, payload: data.data });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const getOrderPricing = (code, payload) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.getOrderPricing(code),
            method: 'post',
            data: payload
        });
        dispatch({ type: types.SET_ORDER_PRICING, payload: data });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const handleSubmitOrder = (code, payload) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.submitOrder(code),
            method: 'post',
            data: payload
        });
        dispatch({ type: types.SET_ORDER_MANAGEMENT, payload: '' });
        dispatch({ type: types.SET_LOADING, payload: false });
    }
    catch (err) {
        dispatch({ type: types.SET_ORDER_MANAGEMENT, payload: 'Something went wrong' });
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const importOrder = (code, payload) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.importOrder(code),
            method: 'post',
            data: payload
        });
        dispatch({ type: types.SET_LOADING, payload: false });
        if (data.message) {
            return data.message
        }
        return false
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}

export const removeOrder = (job, code) => async dispatch => {
    try {
        dispatch({ type: types.SET_LOADING, payload: true });
        const { data } = await client.request({
            path: api.path.removeOrder(job, code),
            method: 'delete',
        });
        dispatch({ type: types.SET_LOADING, payload: false });
        if (data.message) {
            return data.message
        }
        getOrder()
        return false
    }
    catch (err) {
        dispatch({ type: types.SET_LOADING, payload: false });
    } finally {
        dispatch({ type: types.SET_LOADING, payload: false });
    }
}
export const setProof = (data) => async dispatch => {
    dispatch({ type: types.SET_PROOF, payload: data });
}
export const resetProof = () => async dispatch => {
    dispatch({ type: types.RESET_PROOF });
}
export const removeProof = (index) => async dispatch => {
    dispatch({ type: types.REMOVE_PROOF, payload: index });
}

export const editOrder = (code, orderCode, payload) => async dispatch => {
    try {
        const { data } = await client.request({
            path: api.path.editOrder(code, orderCode),
            method: 'patch',
            data: payload
        });
        if (data.message) {
            return data.message
        }
        
        return false
    }
    catch (err) {

    }
}

export const addOrder = (code, payload) => async dispatch => {
    try {
        const { data } = await client.request({
            path: api.path.addOrder(code),
            method: 'post',
            data: payload
        });
        if (data.message) {
            return data.message
        }
        return false
    }
    catch (err) {

    }
}


// export const getOrder = (payload) => async dispatch => {
//     let error = ''
//     try {
//         dispatch({ type: types.SET_LOADING, payload: true });
//         const { data } = await client.request({
//             path: api.path.createCompany,
//             method: 'post',
//             data: payload
//         });
//         dispatch({ type: types.UPDATE_COMPANY, payload: data.data });
//         dispatch({ type: types.SET_ERROR, payload: data?.error || '' });
//         error = data?.error || ''
//     } catch (err) {
//         dispatch({ type: types.SET_ERROR, payload: err?.message });
//         error = err?.message || ''
//     } finally {
//         dispatch({ type: types.SET_LOADING, payload: false });
//         return error
//     }
// }