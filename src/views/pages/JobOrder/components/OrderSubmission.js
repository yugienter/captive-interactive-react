import React, { useEffect, useState } from 'react';
import _ from "lodash";
import {IconArrowUp } from "views/components/Icon/pages"
import { IconFeeInfo } from "views/components/Icon/pages"

import DrawerSubmitOrder from "views/components/DrawerSubmitOrder"
import PopupConfirmOrder from "views/components/PopupConfirmOrder"
import { connect } from 'react-redux';
import { getOrder, getOrderPricing, handleSubmitOrder, getOrderManagement, importOrder, removeOrder, getJobStatistic } from "../actions"
import { useParams } from "react-router-dom"

import { utils, ReactUtils, constants } from 'helpers';

import EditableTable from "./EditableTable"

const { MEDIA_TYPE } = constants


const { getMediaUrl } = utils

function OrderSubmission({ getOrder, order, job, getOrderPricing, orderPricing, handleSubmitOrder, getOrderManagement, proof, getJobStatistic }) {
    const { code } = useParams();
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const [expandTotal, setExpandTotal] = useState(false)
    const [submitOrderPopup, setSubmitOrderPopup] = useState(false)
    const [confirmOrderPopup, setConfirmOrderPopup] = useState(false)

    const onSelectChange = selectedRowKeys => {
        getOrderPricing(code, { ordersCode: selectedRowKeys })
        setSelectedRowKeys(selectedRowKeys);
    };


    useEffect(() => {
        getOrder(code)
        getOrderPricing(code, { ordersCode: selectedRowKeys })
    }, [])

    const onSubmitOrder = async () => {
        const submitOrder = []
        selectedRowKeys.forEach(item => {
            submitOrder.push(order.find(i => i.code === item))
        })
        await handleSubmitOrder(code, {
            orders: submitOrder,
            proof: proof
        })
        setSubmitOrderPopup(false)
        setConfirmOrderPopup(false)
        getOrder(code)
        ReactUtils.messageSuccess({ content: 'Submit order sucessfully' });
        getOrderManagement(code)
        getJobStatistic(code)
    }

    return (
        <>
            <div className="job-order__table-container">
                <div className="job-order__table">
                    <EditableTable selectRow={onSelectChange}/>
                </div>
                <div className="job-order__total">
                    <div className="job-order__total__container">
                        <div className="job-order__total__job">
                            {/* {expandTotal || <div><Checkbox>Select all ({selectedRowKeys.length})</Checkbox></div>} */}
                            <p className="job-order__total__job__result">Selected Orders: {selectedRowKeys.length}</p>
                            {expandTotal && <>
                                <div className="job-order__total__job__container">
                                    <div className="job-order__total__job__image">
                                        <img src={getMediaUrl(_.get(job, 'product.media[0].url'))} alt="proof" />
                                    </div>
                                    <div className="job-order__total__job__info">
                                        <p>{_.get(job, 'product.name')}</p>
                                        <p>Public RRP: <span>${_.get(job, 'publicRRP')}</span></p>
                                    </div>
                                </div> </>}
                        </div>
                        <div className="job-order__total__job__summary">
                            <div>
                                {expandTotal && <table>
                                    <tr>
                                        <td>Order Total</td>
                                        <td>${_.get(orderPricing, 'productAmount') ? _.get(orderPricing, 'productAmount').toFixed(2) : 0}</td>
                                    </tr>
                                    <tr>
                                        <td>Commision</td>
                                        <td>- ${_.get(orderPricing, 'commission') ? _.get(orderPricing, 'commission').toFixed(2) : 0}</td>
                                    </tr>
                                    <tr>
                                        <td>Platform Fee <a><IconFeeInfo /></a></td>
                                        <td>+ ${_.get(orderPricing, 'platformFee') ? _.get(orderPricing, 'platformFee').toFixed(2) : 0}</td>
                                    </tr>
                                </table>}
                                <div>
                                    <span>Total Payment</span>
                                    <span >${_.get(orderPricing, 'total') ? _.get(orderPricing, 'total').toFixed(2) : 0}</span>
                                </div>
                            </div>
                            <div><a onClick={() => setExpandTotal(!expandTotal)}><IconArrowUp /></a>
                                <a className={selectedRowKeys && selectedRowKeys.length > 0 ? '' : 'disabled'} onClick={() => setSubmitOrderPopup(true)}>Submit Order</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <DrawerSubmitOrder
                visible={submitOrderPopup}
                handleClose={() => { setSubmitOrderPopup(false) }}
                onContinue={() => setConfirmOrderPopup(true)} />
            <PopupConfirmOrder handleOK={onSubmitOrder} handleClose={() => setConfirmOrderPopup(false)} visible={confirmOrderPopup} />
        </>
    );
}

const mapStateToProps = ({ jobOrder }) => ({
    loading: jobOrder.loading,
    order: jobOrder.orderSubmission,
    job: jobOrder.job,
    orderPricing: jobOrder.orderPricing,
    error: jobOrder.error,
    proof: jobOrder.proof
});

const mapDispatchToProps = {
    getOrder,
    getOrderPricing,
    handleSubmitOrder,
    getOrderManagement,
    importOrder,
    removeOrder,
    getJobStatistic
};

export default
    connect(mapStateToProps, mapDispatchToProps)(OrderSubmission)
    ;
