import { useEffect, useState } from "react";
import { Tabs } from "antd";
import { constants } from "helpers";

const { TabPane } = Tabs;

const MainTabs = ({changeTab}) => {
    const [status, SetStatus] = useState('')

    const changeTabByStatus = (status) => {
        changeTab(status)
    };

    return (
        <>
            <Tabs
                defaultActiveKey="1"
                onChange={changeTabByStatus}
                className="payment__container--tabs"
            >
                <TabPane
                    tab={
                        <div>
                            Order Submission
                    </div>
                    }
                    key="submission"
                >
                </TabPane>
                <TabPane
                    tab={
                        <div>
                            Order Management
            </div>
                    }
                    key="management"
                >
                </TabPane>
            </Tabs>
        </>
    );
};

export default MainTabs;
