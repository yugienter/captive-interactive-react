import React, { useEffect, useState } from 'react';
import {
    IconExpand,
    IconMoney,
    IconBack,
} from "views/components/Icon/myjobs";
import { connect } from 'react-redux';
import _ from "lodash"
import moment from "moment"
import { Link } from "react-router-dom"
import { constants } from "helpers";
import { useParams } from "react-router-dom"
import { getJobStatistic, getJobDetail } from "../actions"

const { router } = constants;

function TopBar({ job, getJobStatistic, jobStatistic, getJobDetail }) {
    const { code } = useParams();
    const [isShowDetail, setShowDetail] = useState(false)

    useEffect(() => {
        getJobStatistic(code)
        getJobDetail(code)
    }, [])
    return (
        <div className="job-order__top">
            <div className="job-order__top__container">
                <Link to={router.myJobs} className="job-order__top__back-button"><IconBack /></Link>
                <div className="job-order__top__info">
                    <div>
                        <span className="job-order__top__info__title">{_.get(job, 'product.name')}</span>
                        {/* <span><IconExpand /></span> */}
                        {/* <span className="chip">Ordering</span> */}
                    </div>
                    <div>
                        <span>Public RRP: <span>${_.get(job, 'publicRRP')}</span></span>
                        <span>Due Date: <span>{moment(_.get(job, 'updatedAt')).format('YYYY/MM/DD')}</span></span>
                    </div>
                </div>
                <div className="job-order__top__order-statistics">
                    <div onClick={() => setShowDetail(detail => !detail)}>
                        <div className="job-order__top__order-statistics__product">
                            <p>Product Requested</p>
                            <p className="job-order__top__order-statistics__product-count">{_.get(jobStatistic, 'productCount') || 0}</p>
                            {isShowDetail && <div>
                                <p>Total Order Paid</p>
                                <p className="job-order__top__order-statistics__product-total">${_.get(jobStatistic, 'totalPaid').toFixed(2)}</p>
                            </div>}
                        </div>
                        <div>
                            <div className="job-order__top__order-statistics__income">
                                <div>
                                    <div>
                                        <p>Your Income</p>
                                        <p>${_.get(jobStatistic, 'income') ? _.get(jobStatistic, 'income').toFixed(2) : 0}</p>
                                    </div>
                                </div>
                                <div><span><IconMoney /></span></div>
                            </div>
                            {isShowDetail && <div className="job-order__top__order-statistics__income-detail">
                                <div>
                                    <table>
                                        <tr>
                                            <td>Commission:</td>
                                            <td>${_.get(jobStatistic, 'commission').toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td>Fixed Rate:</td>
                                            <td>${_.get(jobStatistic, 'fixedRate').toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td>Platform Fee:</td>
                                            <td>- ${_.get(jobStatistic, 'platformFee').toFixed(2)}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = ({ jobOrder }) => ({
    job: jobOrder.job,
    jobStatistic: jobOrder.jobStatistic
});

const mapDispatchToProps = {
    getJobStatistic,
    getJobDetail
};

export default
    connect(mapStateToProps, mapDispatchToProps)(TopBar)
    ;