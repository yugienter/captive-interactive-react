import React, { useEffect, useState } from 'react';
import { Table, Input, InputNumber, Popconfirm, Form, Typography, Menu, Dropdown, Modal } from 'antd';
import {
    IconDots,
} from "views/components/Icon/myjobs";
import moment from "moment"
import { IconEditRow, IconExcel } from "views/components/Icon/pages"
import { utils, ReactUtils, constants } from 'helpers';
import { removeOrder, editOrder, getOrder, importOrder, addOrder, getJobStatistic } from "../actions"
import { connect } from 'react-redux';
import { useParams } from "react-router-dom"
import * as XLSX from "xlsx";
import _ from "lodash"

const EditableCell = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
}) => {
    const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{ margin: 0 }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                    children
                )}
        </td>
    );
};

const EditableTable = ({ order, removeOrder, selectRow, editOrder, getOrder, importOrder, addOrder, getJobStatistic }) => {
    const { code } = useParams();
    const [form] = Form.useForm();
    const [dataSource, setDataSource] = useState(order)
    const [editingKey, setEditingKey] = useState('');
    const [selectedRowKeys, setSelectedRowKeys] = useState([])

    const isEditing = (record) => record.code === editingKey;

    const edit = (record) => {
        form.setFieldsValue({ name: '', age: '', address: '', ...record });
        setEditingKey(record.code);
    };

    useEffect(() => {
        setDataSource(order)
    }, [order])

    const cancel = () => {
        setEditingKey('');
    };

    const renderRowActions = (actions) => {
        const overlay = <Menu>{actions}</Menu>;
        return (
            <Dropdown overlay={overlay} trigger={["click"]}>
                <span className="clickable">
                    <IconDots />
                </span>
            </Dropdown>
        );
    };

    const onSelectChange = selectedRowKeys => {
        setSelectedRowKeys(selectedRowKeys);
        selectRow(selectedRowKeys)
    };

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };


    const showConfirm = (order) => {
        let res = ''
        Modal.confirm({
            title: `Do you Want to delete this record?`,
            onOk: async () => {
                if (order) {
                    res = await removeOrder(code, order.code)
                    if (res) {
                        ReactUtils.messageWarn({ content: res });
                    }
                    else {
                        ReactUtils.messageSuccess({ content: 'Delete order Sucessfully' });
                        getOrder(code)
                        getJobStatistic(code)
                    }
                }
            },
        });
    }

    const save = async (ordercode) => {
        try {
            const row = (await form.validateFields());
            const originalrow = order && order.find(item => item.code === ordercode)
            let payload = {}
            if (originalrow) {
                payload.sku = row.sku
                payload.quantity = Number(row.quantity)
                payload.deliveryAddress = row.deliveryAddress
                payload.consumerName = row.consumerName
                payload.contactNumber = row.contactNumber
                if (Object.keys(payload).length > 0) {
                    const error = await editOrder(code, ordercode, payload)
                    if (error) {
                        ReactUtils.messageWarn({ content: error });
                    }
                    else {
                        getOrder(code)
                        getJobStatistic(code)
                        ReactUtils.messageSuccess({ content: 'Edit order sucessfully' });
                    }
                    setEditingKey('');
                }
            }
            else {
                
                payload = row
                payload.quantity = Number(payload.quantity)
                const error = await addOrder(code, payload)
                if (error) {
                    ReactUtils.messageWarn({ content: error });
                    // setDataSource(d => d.shift())
                }
                else {
                    getOrder(code)
                    getJobStatistic(code)
                    ReactUtils.messageSuccess({ content: 'Add order sucessfully' });
                }
                setEditingKey('');
            }

        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };
    const columns = [
        {
            title: 'Order ID',
            render: (record) => (
                <>{record.isHideCode || record.code}</>
            )
        },
        {
            title: 'SKU',
            dataIndex: 'sku',
            editable: true,
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            editable: true,
        },
        {
            title: 'Consumer Name',
            dataIndex: 'consumerName',
            editable: true,
        },
        {
            title: 'Contact Number',
            dataIndex: 'contactNumber',
            editable: true,
        },
        {
            title: 'Delivery Address',
            dataIndex: 'deliveryAddress',
            editable: true,
        },
        {
            title: 'Last Modified',
            render: (payment) => {
                return moment(payment.updatedAt).format('YYYY/MM/DD')
            },
        },
        {
            title: 'Action',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
                        <Typography.Link onClick={() => save(record.code)} style={{ marginRight: 8 }}>
                            Save
                </Typography.Link>
                        <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
                            <a>Cancel</a>
                        </Popconfirm>
                    </span>
                ) : (
                        <div style={{ display: 'flex' }}>
                            <div className="job-order__table__action" style={{ marginRight: 10 }}>
                                <a onClick={() => edit(record)}><IconEditRow /></a>
                            </div>
                            {renderRowActions(
                                <div className="popup-row-actions">
                                    <Menu.Item key="0" onClick={() => showConfirm(record)}>
                                        <span style={{ color: 'red' }}>Remove</span>
                                    </Menu.Item>
                                </div>
                            )}
                        </div>
                    );
            },
        },
    ];

    const mergedColumns = columns.map(col => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: col.dataIndex === 'age' ? 'number' : 'text',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });

    const onChangeSheet = (e) => {
        const file = _.get(e, 'target.files[0]')
        const reader = new FileReader();
        reader.onload = async (evt) => {
            const bstr = evt.target.result;
            const wb = XLSX.read(bstr, { type: "binary" });
            /* Get first worksheet */
            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];
            let data = XLSX.utils.sheet_to_csv(ws, { header: 1 });
            data = convertToJson(data)
            data.pop()
            const error = (await importOrder(code, data))
            if (error) {
                await getOrder(code)
                ReactUtils.messageWarn({ content: error });
            }
            else {
                await getOrder(code)
                getJobStatistic(code)
                ReactUtils.messageSuccess({ content: 'Import order sucessfully' });
            }

        };
        if (file) {
            reader.readAsBinaryString(file);
        }
    }


    const convertToJson = (csv) => {
        const lines = csv.split("\n");
        const result = [];
        const headers = lines[0].split(",");
        for (let i = 1; i < lines.length; i++) {
            const obj = {};
            const currentline = lines[i].split(",");
            for (let j = 0; j < headers.length; j++) {
                obj[headers[j]] = currentline[j];
            }
            result.push(obj);
        }
        return result;
    }

    const addRow = () => {
        const newData = {
            sku: '',
            quantity: 0,
            consumerName: '',
            contactNumber: '',
            deliveryAddress: '',
            code: new Date().valueOf().toString(),
            isHideCode: true,
        };
        edit(newData)
        setDataSource(d => {
            if (d && d.length > 0) {
                return [newData, ...d]
            }
            return [newData]
        })
    }

    return (
        <>
            <div className="job-order__table-filter">
                <div className="job-order__list-filter">
                    <div className="job-order__list-filter__tags">
                        <ul>
                            <li className="job-order__list-filter__tag">
                                <a onClick={addRow}> + Add an order</a></li>
                            <li className="job-order__list-filter__tag">
                                <input onChange={onChangeSheet} style={{
                                    display: 'none'
                                }} id="fileSelect" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                <label for="fileSelect"><IconExcel /></label></li>
                        </ul>
                    </div>
                </div>
            </div>
            <Form form={form} component={false}>
                <Table
                    rowKey={(record) => record.code}
                    components={{
                        body: {
                            cell: EditableCell,
                        },
                    }}
                    rowSelection={rowSelection}
                    bordered
                    dataSource={dataSource}
                    columns={mergedColumns}
                    rowClassName="editable-row"
                    pagination={{
                        onChange: cancel,
                    }}
                />
            </Form>
        </>
    );
};
const mapStateToProps = ({ jobOrder }) => ({
    loading: jobOrder.loading,
    order: jobOrder.orderSubmission,
    job: jobOrder.job,
    orderPricing: jobOrder.orderPricing,
    error: jobOrder.error,
    proof: jobOrder.proof
});

const mapDispatchToProps = {
    removeOrder,
    importOrder,
    editOrder,
    getOrder,
    addOrder,
    getJobStatistic
};

export default
    connect(mapStateToProps, mapDispatchToProps)(EditableTable)
    ;
