import React, { useEffect, useState } from 'react';
import _ from "lodash";
import { Table, Menu, Dropdown, Checkbox } from 'antd';
import { IconQRCode, IconExpandTable } from "views/components/Icon/pages"
import {
    IconDots,
} from "views/components/Icon/myjobs";
import { getOrderManagement } from "../actions"
import { useParams, useHistory } from "react-router-dom"
import moment from "moment"
import { connect } from 'react-redux';
import DrawerPaymentDetail from "../../../components/DrawerPaymentDetail/DrawerPaymentDetail"

const ORDER_STATUS = {
    submitted: {
        id: 'submitted',
        label: 'Submitted',
    },
    rejected: {
        id: 'rejected',
        label: 'Rejected',
    },
    approved: {
        id: 'approved',
        label: 'Approved',
    },
    ondelivery: {
        id: 'ondelivery',
        label: 'On Delivery',
    },
    completed: {
        id: 'completed',
        label: 'Completed',
    }
}

function OrderManagement({ isLoading, getOrderManagement, orderManagement }) {
    const { code } = useParams();
    const [selectedCategory, setSelectedCategory] = useState('')
    const history = useHistory()
    const [payment, setPayment] = useState({})
    const [paymentPopup, setPaymentPopup] = useState(false)
    const onChooseCategory = (category) => {
        setSelectedCategory(category)
    }

    const onRow = (record) => ({
        onClick: () => {
        },
    });

    useEffect(() => {
        getOrderManagement(code, selectedCategory)
    }, [])

    useEffect(() => {
        getOrderManagement(code, selectedCategory)
    }, [selectedCategory])


    const renderRowActions = (actions) => {
        const overlay = <Menu>{actions}</Menu>;
        return (
            <Dropdown placement="bottomRight" overlay={overlay} trigger={["click"]}>
                <span className="clickable">
                    <IconDots />
                </span>
            </Dropdown>
        );
    };

    const columns = [
        {
            title: 'Order ID',
            dataIndex: 'code',
        },
        {
            title: 'SKU',
            dataIndex: 'sku',
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
        },
        {
            title: 'Order Status',
            dataIndex: 'status',
        },
        {
            title: 'Consumer Name',
            dataIndex: 'consumerName',
        },
        {
            title: 'Contact Number',
            dataIndex: 'contactNumber',
        },
        {
            title: 'Delivery Address',
            dataIndex: 'deliveryAddress',
        },
        {
            title: 'Last Modified',
            render: (payment) => {
                return moment(payment.updatedAt).format('YYYY/MM/DD')
            },
        },
        {
            title: '',
            dataIndex: 'action',
        },
    ];


    return (
        <>
            <div className="job-order__table-container">
                <div className="job-order__table-filter">
                    <div className="job-order__list-filter">
                        <div className="job-order__list-filter__tags">
                            <ul>
                                <li className={`payment__list-filter__tag${selectedCategory === '' ? ' payment__list-filter__tag--selected' : ''}`}>
                                    <a onClick={() => {
                                        setSelectedCategory('')
                                    }}>All Status{orderManagement.total ? ' ('+ orderManagement.total +')' : ''}</a></li>
                                {
                                    Object.keys(ORDER_STATUS).map(item => (
                                        <li key={_.get(ORDER_STATUS[item], 'id')} className={`payment__list-filter__tag${ORDER_STATUS[item].id === selectedCategory ? ' payment__list-filter__tag--selected' : ''}`}>
                                            <a onClick={() => {
                                                setSelectedCategory(ORDER_STATUS[item].id)
                                            }}>{_.get(ORDER_STATUS[item], 'label')}{orderManagement[ORDER_STATUS[item].id] ? ' ('+ orderManagement[ORDER_STATUS[item].id] +')' : ''}</a></li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="job-order__table">
                    <Table
                        columns={columns}
                        loading={isLoading}
                        pagination={false}
                    />
                    {
                        orderManagement && orderManagement.data && orderManagement.data.map(item => {
                            return (
                                <div className="job-order__table__list">
                                    <div className="job-order__table__list__header">
                                        <div>
                                            <p className="text-bold">#{item.code} <IconQRCode /></p>
                                            <div>
                                                <span className="job-order__table__list__total">{item.orders.length} items <IconExpandTable /></span>
                                                <span>
                                                    <span>Date:</span><span className="text-bold"> {moment(item.updatedAt).format('YYYY/MM/DD')}</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="job-order__table__list__action">
                                            <div>
                                                <p className="text-title">${item.total.toFixed(2)}</p>
                                                <p>{item.status === 'submitted' ? 'Verifying Payment' : item.status === 'approved' ? 'Paid' : ''}</p>
                                            </div>
                                            <div>
                                                {renderRowActions((
                                                    <div className="job-row-actions">
                                                        <Menu.Item key="0">
                                                            <span onClick={() => {
                                                                setPayment(item)
                                                                setPaymentPopup(true)
                                                            }}>View Payment</span>
                                                        </Menu.Item>
                                                        <Menu.Item key="1" onClick={() => history.push("/payment")}>
                                                            <span>Show in Payment Processing</span>
                                                        </Menu.Item>
                                                    </div>
                                                ))}

                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <Table
                                            rowKey={record => record.key}
                                            columns={columns}
                                            dataSource={item.orders}
                                            onRow={onRow}
                                            loading={isLoading}
                                            pagination={false}
                                            showHeader={false}
                                        />
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <DrawerPaymentDetail
            handleClose={() => setPaymentPopup(false)}
            visible={paymentPopup}
            dataSource={payment}
            ></DrawerPaymentDetail>
        </>
    );
}
const mapStateToProps = ({ jobOrder }) => ({
    loading: jobOrder.loading,
    orderManagement: jobOrder.orderManagement
});

const mapDispatchToProps = {
    getOrderManagement
};

export default
    connect(mapStateToProps, mapDispatchToProps)(OrderManagement)
    ;