import * as types from './actionTypes';

const initialState = {
  job: false,
  loading: false,
  orderSubmission: [],
  orderManagement: [],
  jobStatistic: {},
  orderPricing: {},
  proof: [],
  error: ''
};

const companyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.JOB:
      return { ...state, job: action.payload };
    case types.SET_LOADING:
      return { ...state, loading: action.payload };
    case types.SET_ORDER_SUBMISSION:
      return { ...state, orderSubmission: action.payload };
    case types.SET_ERROR:
      return { ...state, error: action.payload };
    case types.SET_ORDER_MANAGEMENT:
      return { ...state, orderManagement: action.payload };
    case types.SET_JOB_STATISTIC:
      return { ...state, jobStatistic: action.payload };
    case types.SET_ORDER_PRICING:
      return { ...state, orderPricing: action.payload };
    case types.SET_PROOF:
      return { ...state, proof: action.payload };
    case types.ADD_PROOF:
      return { ...state, proof: [...state.proof, ...action.payload] };
    case types.RESET_PROOF:
      return { ...state, proof: [] };
    case types.REMOVE_PROOF:
      return { ...state, proof: state.proof.filter(item => item !== action.payload) };
    default:
      return state;
  }
};

export default companyReducer;
