import { Link } from "react-router-dom";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import sphere from "./media/sphere.png";
import { constants } from "helpers";

const { router } = constants;

const ResetPasswordSucces = () => (
  <Page helmet="Reset Password Success" layout="split">
    <div className="primary-section w80">
      <div className="header">
        <LogoFull />
      </div>
      <div className="content">
        <div className="auth-form">
          <img src={sphere} alt="Captive" style={{ marginBottom: "20px" }} />
          <h2>Your password was sent.</h2>
          <p>
            Check the email address connected to your account for a password
            reset email. From the email, click Reset password and enter your new
            password
          </p>
          <p>
            <Link to={router.selectRole}>
              <strong>Sign in again</strong>
            </Link>
          </p>
        </div>
      </div>
      <div className="footer">
        © 2021 SCX. All Rights Reserved.
      </div>
    </div>
    <div className="secondary-section w20"></div>
  </Page>
);

export default ResetPasswordSucces;