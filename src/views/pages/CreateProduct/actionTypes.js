const REDUCER = 'CREATE_PRODUCT';

export const UPDATE_AUDIENCES = `@@${REDUCER}/UPDATE_AUDIENCES`;
export const UPDATE_CATEGORIES = `@@${REDUCER}/UPDATE_CATEGORIES`;
export const UPDATE_COUNTRY = `@@${REDUCER}/UPDATE_COUNTRY`;
export const UPDATE_WEIGHT_UNIT = `@@${REDUCER}/UPDATE_WEIGHT_UNIT`;

export const IS_CREATE_PRODUCT = `@@${REDUCER}/IS_CREATE_PRODUCT`;
export const CREATE_PRODUCT_SUCCESS = `@@${REDUCER}/CREATE_PRODUCT_SUCCESS`;
export const CREATE_PRODUCT_FAILED = `@@${REDUCER}/CREATE_PRODUCT_FAILED`;

export const IS_UPLOAD_MEDIA = `@@${REDUCER}/IS_UPLOAD_MEDIA`;

export const IS_FETCH_TAGS = `@@${REDUCER}/FETCH_TAGS`;
export const FETCH_TAGS_SUCCESS = `@@${REDUCER}/FETCH_TAGS_SUCCESS`;
export const FETCH_TAGS_FAILED = `@@${REDUCER}/FETCH_TAGS_FAILED`;

export const IS_UPDATE_PRODUCT = `@@${REDUCER}/IS_UPDATE_PRODUCT`;
export const UPDATE_PRODUCT_SUCCESS = `@@${REDUCER}/UPDATE_PRODUCT_SUCCESS`;
export const UPDATE_PRODUCT_FAILED = `@@${REDUCER}/UPDATE_PRODUCT_FAILED`;

export const SET_ERROR = `@@${REDUCER}/SET_ERROR`;
export const RESET_REDUCER = `@@${REDUCER}/RESET_REDUCER`;
