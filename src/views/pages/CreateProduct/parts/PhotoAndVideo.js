import React, { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { Form, Upload, Modal } from "antd";
import { IconAdd, IconPhoto } from "views/components/Icon/product";
import { constants } from 'helpers';
import { uploadMedia } from '../actions'
import { connect } from 'react-redux'
import { utils } from 'helpers'
import { useParams } from "react-router-dom";


const { MEDIA_TYPE } = constants;
const { getMediaUrl } = utils

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
const formatLinkMedia = (arrayMedia) => {
  const result = []
  arrayMedia && arrayMedia.forEach((item, index) => {
    result.push({ url: getMediaUrl(item.url), uid: index })
  })
  return result
}
const PhotoAndVideo = forwardRef(({ uploadMedia, currentProduct }, ref) => {

  useImperativeHandle(ref, () => ({
    onUpload: async (file, code) => {
      if (file && file.fileList.length > 0) {
        file.fileList.forEach(async media => {
          if (media.originFileObj) {
            const formData = new FormData();
            formData.append("file", media.originFileObj);
            // formData.append('ownerType', MEDIA_TYPE.PRODUCT);
            // formData.append('ownerCode', code);
            const params = {
              ownerType: MEDIA_TYPE.PRODUCT,
              ownerCode: code,
            }
            await uploadMedia(formData, params);
          }
        })
      }
    }
  }));
  const [fileList, setFileList] = useState([])
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');
  const { code } = useParams();

  useEffect(() => {
    code && setFileList(formatLinkMedia(currentProduct.media))
  }, [currentProduct])

  const uploadButton = (
    <div>
      <IconAdd />
      <div className="title-add">Add photo or video</div>
    </div>
  );
  const handleChange = ({ fileList }) => {
    setFileList(fileList)
  };
  const handleCancel = () => setPreviewVisible(false)
  const handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreviewImage(file.url || file.preview)
    setPreviewVisible(true)
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1))
  };
  return (
    <div>
      <h2 id="PhotosAndVideos"><span className="create-product__content-icon"><IconPhoto width={20} height={20} /></span>Product Photos and Videos</h2>
      <p className="upload-warning">Upload 4 photos of product is recommended (Ideal size is 1024x1024px and Max 1MB each)</p>
      <Form.Item name="upload">
        <Upload
          listType="picture-card"
          fileList={fileList}
          onChange={handleChange}
          onPreview={handlePreview}
          beforeUpload={() => false}
          multiple
        >
          {fileList.length >= 4 ? null : uploadButton}
        </Upload>
      </Form.Item>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    </div>
  )
});
const mapStateToProps = ({ detailProduct: { currentProduct } }) => ({
  currentProduct
});
const mapDispatchToProps = {
  uploadMedia,
};
export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(PhotoAndVideo);
