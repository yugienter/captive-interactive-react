import React, { useEffect, useState } from 'react'
import { Form, Input, Popover, Select } from "antd";
import { IconInfor } from 'views/components/Icon/product';
import InputCategories from 'views/components/InputCategories';
import TreeStructure from './categories/_categories'
import ReactFlagsSelect from 'react-flags-select'
import { handleUpdateCountry } from '../actions'
import { connect } from 'react-redux'
import { getBrandList, selectBrand } from '../../Company/actions'
import { IconSelectDown, IconAddBrand } from 'views/components/Icon/pages';
import { Link } from "react-router-dom";
import { constants } from 'helpers';
const { router } = constants;

const { TextArea } = Input;
const { Option } = Select;


const GeneralInformation = ({ form, tags, category, country, handleUpdateCountry, getBrandList, brands, brandList, selectedBrand }) => {

  useEffect(() => {
    form.setFieldsValue({
      manufacturerCountry: country,
    });
  }, [country])

  useEffect(() => {
    if (selectedBrand && selectedBrand.code) {
      form.setFieldsValue({
        brandName: selectedBrand.code,
      });
    }
  }, [selectedBrand])

  useEffect(async () => {
    await getBrandList(brands)
  }, [getBrandList, brands]);

  return (
    <div>
      <h2 id="GeneralInformation"><span className="create-product__content-icon"><IconInfor width={20} height={20} /></span>General Information</h2>
      <Form.Item label="Brand Name" name="brandName">
        <Select
          placeholder="Select brand"
          className="input-brand"
          notFoundContent={
            <div className="no-content">
              <p>No brand data.</p>
              <Link to={`${router.companyProfile}?open-brand-popup=true`}><IconAddBrand />  Add new Brand</Link>
            </div>
          }
          suffixIcon={<IconSelectDown />}
        >
          {
            brandList && brandList.length > 0 && brandList.map(item => (
              <Option value={item.code}>{item.name}</Option>
            ))
          }
          <Option><Link to={`${router.companyProfile}?open-brand-popup=true`}><IconAddBrand />  Add new Brand</Link></Option>

        </Select>
      </Form.Item>
      <Form.Item label="Product Story" name="story" rules={[{ required: true, message: 'Product Story is missing' }]}>
        <TextArea
          placeholder={"Tell us about the story behind this product:\nWhat problems does this solve?\nWho will benefit most from this product\nWhy is this product the right fit for your users?"}
        />
      </Form.Item>
      <div className="grid-input-2">
        <Form.Item label="Country of Origin" name="manufacturerCountry"
          rules={[
            () => ({
              validator(_, value) {
                if (country !== '') {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Country is missing'));
              },
            }),
          ]}
        >
          <ReactFlagsSelect
            selected={country}
            onSelect={(code) => handleUpdateCountry({ value: code })}
            className="select-country"
          />
        </Form.Item>
        <Form.Item label="Manufacturer" name="manufacturerName" rules={[{ required: true, message: 'Manufacturer is missing' }]}>
          <Input
            size="large"
            placeholder="eg: ACME Company"
          />
        </Form.Item>
      </div>
      <Form.Item label="Product Unique Selling Points (USPs)" name="USPProduct">
        <Input
          size="large"
          placeholder="Unique Selling Points of this product"
        />
      </Form.Item>
      <Form.Item label="Product Benefits" name="productBenefits" rules={[{ required: true, message: 'Product Benefits is missing' }]}>
        <Input
          size="large"
          placeholder="What are some commonly asked questions about this product?"
        />
      </Form.Item>
      <Form.Item label={
        <div className="label-categories">
          <div>Categories</div>
          <div className="filter-item">
            <Popover
              trigger="click"
              placement="bottom"
              content={<TreeStructure data={tags} />}
              overlayClassName="filter-popover"
            >
              <div>Select on categories list</div>
            </Popover>
          </div>
        </div>
      }>
        <span className="categoties">
          <InputCategories data={category} />
        </span>
      </Form.Item>
    </div>
  )
};
const mapStateToProps = ({ createProduct: { country, category, tags }, company }) => ({
  category,
  country,
  tags,
  brands: company.brands,
  brandList: company.brandList,
  selectedBrand: company.defaultBrand
});
const mapDispatchToProps = {
  handleUpdateCountry,
  getBrandList
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralInformation);