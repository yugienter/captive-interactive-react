import React from 'react'
import { Form, Input, Popover, Select } from "antd";
import { connect } from 'react-redux';
import { IconAudience, IconDropdown } from 'views/components/Icon/product';
import TitleSubtitle from "views/components/TitleSubtitle";
import { GenderSelect, AgeSelect } from '../parts/variant';
import { OCCUPATION_LIST } from 'helpers'

const { Option, OptGroup } = Select;

const TargetAudiences = (props) => {

    const { age, gender } = props

    const selectField = [
        {
            key: "gender",
            component: <GenderSelect />,
            title: gender.title,
            label: "Gender",
        },
        {
            key: "age",
            component: <AgeSelect />,
            title: age.title ? age.title : "0 - 100",
            label: "Age Range",
        },
    ]
    return (
        <div>
            <h2 id="TargetAudiences"><span className="create-product__content-icon"><IconAudience width={20} height={20} /></span>Tell us about your users... </h2>
            <div className="grid-input-3">
                {
                    selectField.map(item => {
                        return (
                            <Form.Item label={item.label} key={item.key}>
                                <SelectItem
                                    dropdown={item.component}
                                >
                                    <TitleSubtitle
                                        title={item.title}
                                        reverse
                                    />
                                </SelectItem>
                            </Form.Item>
                        )
                    })
                }
                {/* <Form.Item label="Occupation" name="occupation" >
                    <Select suffixIcon={<IconDropdown />}>
                        {
                            OCCUPATION_LIST.map((sItem) => (
                                <OptGroup label={sItem.name} key={sItem.id}>
                                    {
                                        sItem.item.map((item) => (
                                            <Option value={item.name} key={item.id}>{item.name}</Option>
                                        ))
                                    }

                                </OptGroup>
                            ))
                        }
                    </Select>
                </Form.Item> */}
            </div>
            <Form.Item label="Insights" name="insights" >
                <Input
                    size="large"
                    placeholder="Describe more about your audiences"
                />
            </Form.Item >
        </ div>
    )
};
const SelectItem = ({ children, dropdown, onChange }) => (
    <div className="filter-item">
        <Popover
            trigger="click"
            placement="bottom"
            content={dropdown}
            overlayClassName="filter-popover"
        >
            <div className="filter-body">
                {children}
                <div className="filter-icon">
                    <IconDropdown />
                </div>
            </div>
        </Popover>
    </div>
);

const mapStateToProps = ({ createProduct: { audiences } }) => ({
    age: audiences.age,
    gender: audiences.gender
});


export default connect(mapStateToProps, {})(TargetAudiences);