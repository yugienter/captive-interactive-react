import { Anchor } from "antd";
import {
  IconInfor,
  IconPhoto,
  IconMoney,
  IconVariant,
  IconAudience
} from "views/components/Icon/product";
const { Link } = Anchor;

const Menu = ({ top }) => {

  return (
    <div >
      <Anchor offsetTop={top}> {/* height of header container product name*/}
        <Link href="#GeneralInformation" title={<><IconInfor /><span>General Information</span></>} />
        <Link href="#PhotosAndVideos" title={<><IconPhoto /><span>Photos & Videos</span></>} />
        <Link href="#ProductPrice" title={<><IconMoney /><span>Product Price</span></>} />
        <Link href="#Variants" title={<><IconVariant /><span>Key Product Details</span></>} />
        <Link href="#TargetAudiences" title={<><IconAudience /><span>Tell us about your users... </span></>} />
      </Anchor>
    </div>
  )
};

export default Menu;