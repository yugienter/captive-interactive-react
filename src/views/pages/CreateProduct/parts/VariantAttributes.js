import React from "react";
import { Form, Input, Select, DatePicker } from "antd";
import { IconDropdown, IconVariant } from "views/components/Icon/product";
import { connect } from "react-redux";
import { handleUpdateWeightUnit } from '../actions'
import { utils } from 'helpers'

const { Option } = Select;
const { handleFormatInput } = utils

const VariantAttributes = ({ weightUnit, handleUpdateWeightUnit }) => {

  const suffixWeight = (
    <Select defaultValue={weightUnit} suffixIcon={<IconDropdown />} onChange={(value) => handleUpdateWeightUnit({ value })}>
      <Option value="ml">ml</Option>
      <Option value="g">g</Option>
    </Select>
  )
  const suffixDimesion = (
    <div className="suffix-input">cm</div>
  )
  const handleBlur = (input) => {
    if (input === "") { return ''; }
    if (input.indexOf(".") >= 0) {
      return handleFormatInput(input + '0', 1)
    }
    return input + '.0'
  }
  return (
    <div>
      <h2 id="Variants"><span className="create-product__content-icon"><IconVariant width={20} height={20} /></span>Key Product Details</h2>
      <div className="grid-input-2">
        <Form.Item label="Weight" name="weight" >
          <Input
            size="large"
            className="input-select"
            placeholder="Number"
            suffix={suffixWeight}
            onInput={(e) => e.target.value = handleFormatInput(e.target.value, 1)}
            onBlur={(e) => e.target.value = handleBlur(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Dimension (W x D x H)" name="dimension">
          <Input size="large" placeholder="Width x Depth x Height" suffix={suffixDimesion} />
        </Form.Item>
      </div>
      <Form.Item label="Expiry Date" name="expiryDate">
        <DatePicker placeholder="mm/dd/yyyy" format="MM/DD/YYYY" />
      </Form.Item>
    </div>
  )
};
const mapStateToProps = ({ createProduct: { weightUnit } }) => ({
  weightUnit
});
const mapDispatchToProps = {
  handleUpdateWeightUnit
};
export default connect(mapStateToProps, mapDispatchToProps)(VariantAttributes);