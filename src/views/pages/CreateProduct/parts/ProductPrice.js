import React from "react";
import { Form, Input } from "antd";
import { IconMoney } from "views/components/Icon/product";
import { utils } from 'helpers'

const { handleFormatInput } = utils


const ProductPrice = () => {

  const prefixOriginalPrice = (
    <div className="suffix-input-left">SGD</div>
  )
  const handleBlur = (input) => {
    if (input === "") { return ''; }
    if (input.indexOf(".") >= 0) {
      return handleFormatInput(input + '00', 2)
    }
    return input + '.00'
  }

  return (
    <div>
      <h2 id="ProductPrice"><span className="create-product__content-icon"><IconMoney width={20} height={20} /></span>Product Price</h2>
      <Form.Item
        label="Recommended Retail Pricing (RRP)"
        name="originalPrice"
      >
        <Input
          size="large"
          className="input-brand select-input"
          placeholder="Price"
          prefix={prefixOriginalPrice}
          onInput={(e) => e.target.value = handleFormatInput(e.target.value, 2)}
          onBlur={(e) => e.target.value = handleBlur(e.target.value)}
        />
      </Form.Item>
    </div>
  )

};

export default ProductPrice;