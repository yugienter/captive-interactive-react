import React, { useState } from "react";
import { connect } from 'react-redux';
import { Slider } from "antd";
import { constants } from "helpers";
import { handleUpdateAudiences } from "../../actions";

const { FILTER_DEFAULT: { age } } = constants;


const AgeSelect = (props) => {

    const { ageValue, handleUpdateAudiences } = props;


    const [min, setMin] = useState(ageValue[0] ? ageValue[0] : age.min);
    const [max, setMax] = useState(ageValue[1] ? ageValue[1] : age.max);

    const handleRange = (min, max) => {
        setMin(min);
        setMax(max);
    }

    const onSlideChange = (value) => {
        const min = Number(value[0]);
        const max = Number(value[1]);
        handleRange(min, max);
        handleUpdateAudiences({ key: 'age', title: `${min}-${max}`, value: [min, max] })
    }

    return (
        <div className="filter-content">
            <div className="filter-slider">
                <Slider
                    range
                    value={[min, max]}
                    min={age.min}
                    max={age.max}
                    onChange={onSlideChange}
                />
            </div>
        </div>
    );
}
const mapStateToProps = ({ createProduct: { audiences } }) => ({
    ageValue: audiences.age.value
});

const mapDispatchToProps = {
    handleUpdateAudiences
};
export default connect(mapStateToProps, mapDispatchToProps)(AgeSelect);