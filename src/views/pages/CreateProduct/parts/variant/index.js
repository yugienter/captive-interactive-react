import GenderSelect from './_gender';
import AgeSelect from './_age';

export {
    GenderSelect,
    AgeSelect
}