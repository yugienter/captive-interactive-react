
import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Form, Checkbox, } from "antd";
import { handleUpdateAudiences } from "../../actions";

const GenderSelect = (props) => {

  const { gender, handleUpdateAudiences } = props;

  const [form] = Form.useForm();
  const [genderCheck, setGender] = useState({
    All: gender.includes('All'), Male: gender.includes('Male'), Female: gender.includes('Female'), Other: gender.includes('Other')
  })
  useEffect(() => {
    form.setFieldsValue({ All: genderCheck.All, Male: genderCheck.Male, Female: genderCheck.Female, Other: genderCheck.Other })
  }, [form, genderCheck])

  const onChecked = (e) => {
    const { id, checked } = e.target;
    const tempGender = { ...genderCheck, [id]: checked }
    const arrayActiveValue = Object.keys(tempGender).filter(item => tempGender[item] === true)
    if (arrayActiveValue.length > 1 || arrayActiveValue.length < 1) {
      handleUpdateAudiences({ key: 'gender', title: 'Not Specific', value: arrayActiveValue })
    }
    else {
      handleUpdateAudiences({ key: 'gender', title: arrayActiveValue[0], value: arrayActiveValue })
    }
    setGender({ ...genderCheck, [id]: checked })

  }

  return (
    <div className="filter-content no-padding">
      <Form form={form} layout="vertical">
        <Form.Item
          name="All"
          valuePropName="checked"
        >
          <Checkbox onChange={onChecked}>All</Checkbox>
        </Form.Item>
        <Form.Item
          name="Male"
          valuePropName="checked"
        >
          <Checkbox onChange={onChecked}>Male</Checkbox>
        </Form.Item>
        <Form.Item
          name="Female"
          valuePropName="checked"
        >
          <Checkbox onChange={onChecked}>Female</Checkbox>
        </Form.Item>
        <Form.Item
          name="Other"
          valuePropName="checked"
        >
          <Checkbox onChange={onChecked}>Other</Checkbox>
        </Form.Item>
      </Form>
    </div>
  );
}
const mapStateToProps = ({ createProduct: { audiences } }) => ({
  gender: audiences.gender.value
});

const mapDispatchToProps = {
  handleUpdateAudiences
};
export default connect(mapStateToProps, mapDispatchToProps)(GenderSelect);