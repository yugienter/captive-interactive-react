import Menu from './Menu'
import GeneralInformation from './GeneralInformation'
import PhotoAndVideo from './PhotoAndVideo'
import ProductPrice from './ProductPrice'
import VariantAttributes from './VariantAttributes'
import TargetAudiences from './TargetAudiences'

export {
  Menu,
  GeneralInformation,
  PhotoAndVideo,
  ProductPrice,
  VariantAttributes,
  TargetAudiences
}