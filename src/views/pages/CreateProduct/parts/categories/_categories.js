import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { handleUpdateCategories } from "../../actions";
import TreeList from "./_category";

const TreeStructure = (props) => {
    const { data, handleUpdateCategories, currentProduct } = props;

    const [tagList, setTagList] = useState([]);
    const [tagListName, setTagListName] = useState([])

    useEffect(() => {
        if (currentProduct.category) {
            setTagList(currentProduct.category.ids)
            setTagListName(currentProduct.category.names)
        }
    }, [currentProduct])

    const getTagFromChild = ({ parentId, checkedList }) => {
        const tagCheck = data.filter(tag => tag.id === parentId)[0];
        const tempTagList = [...tagList]
        const tempListName = [...tagListName]
        tagCheck.item.forEach(item => {
            const index = tempTagList.indexOf(item.id);
            if (index !== -1) {
                tempTagList.splice(index, 1);
                tempListName.splice(index, 1)
            }
            if (checkedList.includes(item.id)) {
                tempTagList.push(item.id);
                tempListName.push(item.name)
            }
        })
        handleUpdateCategories({ ids: tempTagList, names: tempListName })
        setTagList(tempTagList);
        setTagListName(tempListName)
    }

    return (
        <div className="filter-content  no-padding">
            {
                data.length && data.map((tag, index) =>
                    <TreeList tag={tag} key={index} getTagFromChild={getTagFromChild} />
                )
            }
        </div >

    );
}

const mapStateToProps = ({ createProduct: { category }, detailProduct: { currentProduct } }) => ({
    category,
    currentProduct
});
const mapDispatchToProps = {
    handleUpdateCategories
};

export default connect(mapStateToProps, mapDispatchToProps)(TreeStructure);


