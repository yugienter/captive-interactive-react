import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { Checkbox, Collapse } from "antd";

const { Panel } = Collapse;

const CheckboxGroup = Checkbox.Group;

const TreeList = ({ tag, getTagFromChild, category }) => {
    const newTag = { ...tag };

    const onloadOptions = newTag.item.map(item => {
        return {
            label: item.name,
            value: item.id
        }
    })

    const [checkedList, setCheckedList] = useState([]);
    const [indeterminate, setIndeterminate] = useState(false);
    const [checkAll, setCheckAll] = useState(false);

    useEffect(() => {
        setCheckedList(category.ids)
        setIndeterminate(false);
        setCheckAll(handleCheckAll(category.ids))
    }, [category])
    const handleCheckAll = (list) => {
        for (let i = 0; i < onloadOptions.length; i++) {
            const index = list.indexOf(onloadOptions[i].value)
            if (index === -1) { return false }
        }
        return true
    }
    const onGroupChange = list => {
        setCheckedList(list);
        setIndeterminate(!!list.length && list.length < onloadOptions.length);
        setCheckAll(list.length === onloadOptions.length);
        getTagFromChild({ parentId: newTag.id, checkedList: list });
    };

    const onCheckAll = e => {
        const values = onloadOptions.map(item => item.value);
        setCheckedList(e.target.checked ? values : []);
        setIndeterminate(false);
        setCheckAll(e.target.checked);
        getTagFromChild({ parentId: newTag.id, checkedList: e.target.checked ? values : [] });
    };

    return (
        <Collapse expandIconPosition="right" >
            <Panel
                header={
                    <>
                        <div onClick={(e) => { e.stopPropagation() }} >
                            <Checkbox
                                indeterminate={indeterminate}
                                onChange={onCheckAll}
                                checked={checkAll}
                            />
                        </div>
                        {newTag.name}
                    </>
                }
            >
                <CheckboxGroup value={checkedList} onChange={onGroupChange}>
                    <div className="sub-category">
                        {
                            newTag.item.map((tagItem, tagIndex) =>
                                <div className="sub-category-item" key={tagIndex}>
                                    <Checkbox value={tagItem.id}>
                                        {tagItem.name}
                                    </Checkbox>
                                </div>
                            )
                        }
                    </div>
                </CheckboxGroup>
            </Panel>
        </Collapse>
    )
}


const mapStateToProps = ({ createProduct: { category } }) => ({
    category
});

export default connect(mapStateToProps, {})(TreeList);
