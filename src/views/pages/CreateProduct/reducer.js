
import * as types from './actionTypes';

const initialState = {
    isLoading: false,
    error: null,
    audiences: {
        gender: {
            value: ['All'],
            title: 'All'
        },
        age: {
            value: [],
            title: ''
        },
    },
    category: {
        ids: [],
        names: []
    },
    country: '',
    tags: [],
    weightUnit: 'ml',

};

const createProductReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.UPDATE_AUDIENCES:
            const { key, title, value } = payload;
            let tempUpdate = { ...state.audiences[key], value, title };
            return {
                ...state,
                audiences: {
                    ...state.audiences,
                    [key]: tempUpdate
                }
            }
        case types.UPDATE_CATEGORIES:
            const { ids, names } = payload
            return {
                ...state,
                category: { ...state.category, ids, names }
            }
        case types.UPDATE_COUNTRY:
            return {
                ...state,
                country: payload
            }
        case types.UPDATE_WEIGHT_UNIT:
            return {
                ...state,
                weightUnit: payload
            }
        case types.IS_FETCH_TAGS:
            return {
                ...state,
                isLoading: payload
            }
        case types.FETCH_TAGS_SUCCESS:
            return {
                ...state,
                tags: payload
            }
        case types.FETCH_TAGS_FAILED:
            return {
                ...state,
                error: payload
            }
        case types.IS_CREATE_PRODUCT:
            return {
                ...state,
                isLoading: payload
            }
        case types.CREATE_PRODUCT_FAILED:
            return {
                ...state,
                error: payload
            }
        case types.RESET_REDUCER:
            return initialState
        default:
            return state;
    }
};

export default createProductReducer;
