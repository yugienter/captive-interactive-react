const data = [
    {
        "name": "Food",
        "createdAt": "2021-08-15T18:33:04.691Z",
        "updatedAt": "2021-08-15T18:33:04.691Z",
        "tags": null,
        "id": "61195de014ddf4a6d96221d5"
    },
    {
        "parentId": "61195de014ddf4a6d96221d5",
        "name": "Organic Food",
        "createdAt": "2021-08-15T18:33:56.250Z",
        "updatedAt": "2021-08-15T18:33:56.250Z",
        "tags": {
            "name": "Food",
            "id": "61195de014ddf4a6d96221d5"
        },
        "id": "61195e1414ddf4a6d96221d8"
    },
    {
        "parentId": "61195de014ddf4a6d96221d5",
        "name": "Raw Food",
        "createdAt": "2021-08-15T18:35:32.624Z",
        "updatedAt": "2021-08-15T18:35:32.624Z",
        "tags": {
            "name": "Food",
            "id": "61195de014ddf4a6d96221d5"
        },
        "id": "61195e7415d7fea7a0233ed3"
    },
    {
        "parentId": "61195de014ddf4a6d96221d5",
        "name": "Ready to Eat",
        "createdAt": "2021-08-15T18:44:32.560Z",
        "updatedAt": "2021-08-15T18:44:32.560Z",
        "tags": {
            "name": "Food",
            "id": "61195de014ddf4a6d96221d5"
        },
        "id": "61196090fc42fda9b180acf3"
    },
    {
        "parentId": "61195de014ddf4a6d96221d5",
        "name": "Snacks",
        "createdAt": "2021-08-15T18:44:43.154Z",
        "updatedAt": "2021-08-15T18:44:43.154Z",
        "tags": {
            "name": "Food",
            "id": "61195de014ddf4a6d96221d5"
        },
        "id": "6119609bfc42fda9b180acf6"
    },
    {
        "parentId": "61195de014ddf4a6d96221d5",
        "name": "Health Products",
        "createdAt": "2021-08-15T18:44:50.542Z",
        "updatedAt": "2021-08-15T18:44:50.542Z",
        "tags": {
            "name": "Food",
            "id": "61195de014ddf4a6d96221d5"
        },
        "id": "611960a2fc42fda9b180acf9"
    },
    {
        "name": "Electronics",
        "createdAt": "2021-08-15T18:45:00.970Z",
        "updatedAt": "2021-08-15T18:45:00.970Z",
        "tags": null,
        "id": "611960acfc42fda9b180acfb"
    },
    {
        "parentId": "611960acfc42fda9b180acfb",
        "name": "Utility Gadgets",
        "createdAt": "2021-08-15T18:45:11.670Z",
        "updatedAt": "2021-08-15T18:45:11.670Z",
        "tags": {
            "name": "Electronics",
            "id": "611960acfc42fda9b180acfb"
        },
        "id": "611960b7fc42fda9b180acfe"
    },
    {
        "parentId": "611960acfc42fda9b180acfb",
        "name": "Home Appliances",
        "createdAt": "2021-08-15T18:46:02.868Z",
        "updatedAt": "2021-08-15T18:46:02.868Z",
        "tags": {
            "name": "Electronics",
            "id": "611960acfc42fda9b180acfb"
        },
        "id": "611960eafc42fda9b180ad01"
    },
    {
        "parentId": "611960acfc42fda9b180acfb",
        "name": "Gaming",
        "createdAt": "2021-08-15T18:46:16.974Z",
        "updatedAt": "2021-08-15T18:46:16.974Z",
        "tags": {
            "name": "Electronics",
            "id": "611960acfc42fda9b180acfb"
        },
        "id": "611960f8fc42fda9b180ad04"
    },
    {
        "parentId": "611960acfc42fda9b180acfb",
        "name": "Audio",
        "createdAt": "2021-08-15T18:46:30.293Z",
        "updatedAt": "2021-08-15T18:46:30.293Z",
        "tags": {
            "name": "Electronics",
            "id": "611960acfc42fda9b180acfb"
        },
        "id": "61196106fc42fda9b180ad07"
    },
    {
        "name": "Parenting",
        "createdAt": "2021-08-15T18:46:40.135Z",
        "updatedAt": "2021-08-15T18:46:40.135Z",
        "tags": null,
        "id": "61196110fc42fda9b180ad09"
    },
    {
        "parentId": "61196110fc42fda9b180ad09",
        "name": "Baby Furniture's",
        "createdAt": "2021-08-15T18:46:57.580Z",
        "updatedAt": "2021-08-15T18:46:57.580Z",
        "tags": {
            "name": "Parenting",
            "id": "61196110fc42fda9b180ad09"
        },
        "id": "61196121fc42fda9b180ad0c"
    },
    {
        "parentId": "61196110fc42fda9b180ad09",
        "name": "Baby Toys",
        "createdAt": "2021-08-15T18:47:09.153Z",
        "updatedAt": "2021-08-15T18:47:09.153Z",
        "tags": {
            "name": "Parenting",
            "id": "61196110fc42fda9b180ad09"
        },
        "id": "6119612dfc42fda9b180ad0f"
    },
    {
        "parentId": "61196110fc42fda9b180ad09",
        "name": "Baby Food",
        "createdAt": "2021-08-15T18:47:20.356Z",
        "updatedAt": "2021-08-15T18:47:20.356Z",
        "tags": {
            "name": "Parenting",
            "id": "61196110fc42fda9b180ad09"
        },
        "id": "61196138fc42fda9b180ad12"
    },
    {
        "parentId": "61196110fc42fda9b180ad09",
        "name": "Baby Wear",
        "createdAt": "2021-08-15T18:47:28.781Z",
        "updatedAt": "2021-08-15T18:47:28.781Z",
        "tags": {
            "name": "Parenting",
            "id": "61196110fc42fda9b180ad09"
        },
        "id": "61196140fc42fda9b180ad15"
    },
    {
        "name": "Children Education and Toys",
        "createdAt": "2021-08-15T18:47:41.110Z",
        "updatedAt": "2021-08-15T18:47:41.110Z",
        "tags": null,
        "id": "6119614dfc42fda9b180ad17"
    },
    {
        "parentId": "6119614dfc42fda9b180ad17",
        "name": "Baby Goods",
        "createdAt": "2021-08-15T18:47:59.237Z",
        "updatedAt": "2021-08-15T18:47:59.237Z",
        "tags": {
            "name": "Children Education and Toys",
            "id": "6119614dfc42fda9b180ad17"
        },
        "id": "6119615ffc42fda9b180ad1a"
    },
    {
        "parentId": "6119614dfc42fda9b180ad17",
        "name": "Toddler Products",
        "createdAt": "2021-08-15T18:48:07.228Z",
        "updatedAt": "2021-08-15T18:48:07.228Z",
        "tags": {
            "name": "Children Education and Toys",
            "id": "6119614dfc42fda9b180ad17"
        },
        "id": "61196167fc42fda9b180ad1d"
    },
    {
        "parentId": "6119614dfc42fda9b180ad17",
        "name": "Infant Care",
        "createdAt": "2021-08-15T18:48:12.803Z",
        "updatedAt": "2021-08-15T18:48:12.803Z",
        "tags": {
            "name": "Children Education and Toys",
            "id": "6119614dfc42fda9b180ad17"
        },
        "id": "6119616cfc42fda9b180ad20"
    },
    {
        "parentId": "6119614dfc42fda9b180ad17",
        "name": "Toys",
        "createdAt": "2021-08-15T18:48:18.130Z",
        "updatedAt": "2021-08-15T18:48:18.130Z",
        "tags": {
            "name": "Children Education and Toys",
            "id": "6119614dfc42fda9b180ad17"
        },
        "id": "61196172fc42fda9b180ad23"
    },
    {
        "parentId": "6119614dfc42fda9b180ad17",
        "name": "Educational Products",
        "createdAt": "2021-08-15T18:48:33.641Z",
        "updatedAt": "2021-08-15T18:48:33.641Z",
        "tags": {
            "name": "Children Education and Toys",
            "id": "6119614dfc42fda9b180ad17"
        },
        "id": "61196181fc42fda9b180ad26"
    },
    {
        "name": "Fashion",
        "createdAt": "2021-08-15T18:49:00.102Z",
        "updatedAt": "2021-08-15T18:49:00.102Z",
        "tags": null,
        "id": "6119619cfc42fda9b180ad28"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Bags",
        "createdAt": "2021-08-15T18:49:16.888Z",
        "updatedAt": "2021-08-15T18:49:16.888Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961acfc42fda9b180ad2b"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Sports Fashion",
        "createdAt": "2021-08-15T18:49:23.146Z",
        "updatedAt": "2021-08-15T18:49:23.146Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961b3fc42fda9b180ad2e"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Eyewear",
        "createdAt": "2021-08-15T18:49:28.024Z",
        "updatedAt": "2021-08-15T18:49:28.024Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961b8fc42fda9b180ad31"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Watches",
        "createdAt": "2021-08-15T18:49:32.357Z",
        "updatedAt": "2021-08-15T18:49:32.357Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961bcfc42fda9b180ad34"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Accessories",
        "createdAt": "2021-08-15T18:49:36.970Z",
        "updatedAt": "2021-08-15T18:49:36.970Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961c0fc42fda9b180ad37"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Male",
        "createdAt": "2021-08-15T18:49:43.269Z",
        "updatedAt": "2021-08-15T18:49:43.269Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961c7fc42fda9b180ad3a"
    },
    {
        "parentId": "6119619cfc42fda9b180ad28",
        "name": "Female",
        "createdAt": "2021-08-15T18:49:48.344Z",
        "updatedAt": "2021-08-15T18:49:48.344Z",
        "tags": {
            "name": "Fashion",
            "id": "6119619cfc42fda9b180ad28"
        },
        "id": "611961ccfc42fda9b180ad3d"
    },
    {
        "name": "Health and Wellness",
        "createdAt": "2021-08-15T18:49:59.711Z",
        "updatedAt": "2021-08-15T18:49:59.711Z",
        "tags": null,
        "id": "611961d7fc42fda9b180ad3f"
    },
    {
        "parentId": "611961d7fc42fda9b180ad3f",
        "name": "Supplements",
        "createdAt": "2021-08-15T18:50:14.325Z",
        "updatedAt": "2021-08-15T18:50:14.325Z",
        "tags": {
            "name": "Health and Wellness",
            "id": "611961d7fc42fda9b180ad3f"
        },
        "id": "611961e6fc42fda9b180ad42"
    }
]
const parentTags = data.filter(tag => !tag.parentId);
const result = parentTags.map(parentTag => {
    parentTag.item = [];
    data.forEach(tag => {
        if (tag.parentId === parentTag.id) {
            parentTag.item.push({
                id: tag.id,
                name: tag.name
            });
        }
    })
    return ({
        id: parentTag.id,
        name: parentTag.name,
        item: parentTag.item
    })
});
export default result