import { constants, client } from 'helpers';
import * as types from './actionTypes';

const { api } = constants;

export const handleUpdateAudiences = ({ key, title, value }) => dispatch => {
    dispatch({
        type: types.UPDATE_AUDIENCES,
        payload: { key, title, value },
    });
};
export const handleUpdateCategories = ({ ids, names }) => dispatch => {
    dispatch({
        type: types.UPDATE_CATEGORIES,
        payload: { ids, names },
    });
};
export const handleUpdateCountry = ({ value }) => dispatch => {
    dispatch({
        type: types.UPDATE_COUNTRY,
        payload: value,
    });
};
export const handleUpdateWeightUnit = ({ value }) => dispatch => {
    dispatch({
        type: types.UPDATE_WEIGHT_UNIT,
        payload: value,
    });
};
export const resetFields = () => dispatch => {
    dispatch({
        type: types.RESET_REDUCER
    })
}

export const getTags = () => async dispatch => {
    dispatch({ type: types.IS_FETCH_TAGS, payload: true });

    try {
        const res = await client.request({
            path: api.path.getTags,
            method: 'get',
        });
        const aTags = res.data.data;
        const parentTags = aTags.filter(tag => !tag.parentCode);
        const result = parentTags.map(parentTag => {
            parentTag.item = [];
            aTags.forEach(tag => {
                if (tag.parentCode === parentTag.code) {
                    parentTag.item.push({
                        id: tag.code,
                        name: tag.name
                    });
                }
            })
            return ({
                id: parentTag.code,
                name: parentTag.name,
                item: parentTag.item
            })
        });


        dispatch({
            type: types.FETCH_TAGS_SUCCESS,
            payload: result
        });

        return res.data.data;
    } catch (err) {
        if (err.response) {
            if (err.response.status >= 400) {
                dispatch({
                    type: types.FETCH_TAGS_FAILED,
                    payload: err.response.data.message
                });
            }
        }
        return null;
    } finally {
        dispatch({ type: types.IS_FETCH_TAGS, payload: false });
    }
};
export const handleCreateProduct = (payload) => async dispatch => {
    try {
        dispatch({ type: types.IS_CREATE_PRODUCT, payload: true });

        const res = await client.request({
            path: api.path.createProduct,
            method: 'post',
            data: payload
        });
        if (res.data) {
            dispatch({ type: types.CREATE_PRODUCT_SUCCESS });
            return res.data.code
        }

    } catch (err) {
        console.log('[createProduct]', err);
        dispatch({ type: types.CREATE_PRODUCT_FAILED, payload: err.message });
    } finally {
        dispatch({ type: types.IS_CREATE_PRODUCT, payload: false });
    }
};
export const uploadMedia = (formData, params) => async dispatch => {
    try {
        dispatch({ type: types.IS_UPLOAD_MEDIA, payload: true });

        const { data } = await client.request({
            path: api.path.uploadMedia,
            method: 'post',
            data: formData,
            headers: { 'Content-Type': 'multipart/form-data' },
            params
        });
        if (data.data) {
            return data.data.url
        }
        return '';
    } catch (err) {
        console.log('[uploadMedia]', err);
        dispatch({ type: types.SET_ERROR, payload: err.message });
    } finally {
        dispatch({ type: types.IS_UPLOAD_MEDIA, payload: false });
    }
};
export const handleUpdateProduct = ({ code, newData }) => async dispatch => {
    try {
        dispatch({ type: types.IS_UPDATE_PRODUCT, payload: true });

        const res = await client.request({
            path: api.path.editProduct(code),
            method: 'patch',
            data: newData
        });
        if (res.data) {
            dispatch({ type: types.UPDATE_PRODUCT_SUCCESS })
            return res.data.code
        }
    } catch (err) {
        console.log('[updateProduct]', err);
        dispatch({ type: types.UPDATE_PRODUCT_FAILED });
    } finally {
        dispatch({ type: types.IS_UPDATE_PRODUCT, payload: false });
    }
}

