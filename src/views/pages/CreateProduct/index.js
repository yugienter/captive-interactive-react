import Page from "views/components/Page";
import {
  SaveOutlined,
  CloseOutlined,
  CloseCircleOutlined
} from "@ant-design/icons";
import { Button, Form } from "antd";
import { useHistory, useParams } from "react-router-dom";
import {
  Menu,
  GeneralInformation,
  PhotoAndVideo,
  ProductPrice,
  VariantAttributes,
  TargetAudiences
}
  from './parts'
import {
  handleUpdateCategories,
  handleUpdateAudiences,
  handleUpdateCountry,
  handleUpdateWeightUnit,
  handleCreateProduct,
  handleUpdateProduct,
  getTags,
  resetFields
} from "./actions";
import { getDetailProduct } from '../DetailProduct/actions'
import { useEffect, useRef, useState } from "react";
import { connect } from 'react-redux';
import { COUNTRY_CODE, router } from 'helpers';
import moment from "moment";
import { constants, utils } from "helpers";

const { userRoles } = constants;
const { handleFormatInput, handleFormatRightSide } = utils

const CreateProduct = (props) => {
  const {
    categories,
    country,
    minAge,
    maxAge,
    gender,
    weightUnit,
    handleUpdateAudiences,
    handleUpdateCategories,
    handleUpdateWeightUnit,
    handleUpdateCountry,
    handleCreateProduct,
    handleUpdateProduct,
    resetFields,
    getTags,
    getDetailProduct,
    currentProduct
  } = props

  const history = useHistory()
  const { code } = useParams();
  const [form] = Form.useForm();

  // const productName = useRef()
  const photoAndVideo = useRef()
  const [refName, setRefName] = useState(false)
  const [productName, setProductName] = useState('')

  useEffect(() => {
    getTags();
  }, [getTags])

  useEffect(() => {
    if (!currentProduct.name) {
      getDetailProduct({ code })
    }
  }, [getDetailProduct, code, currentProduct.name])

  useEffect(() => {
    if (code) {
      form.setFieldsValue({
        brandName: currentProduct.brandName ? currentProduct.brandName : '',
        story: currentProduct.story ? currentProduct.story : '',
        manufacturerName: currentProduct.manufacturerName ? currentProduct.manufacturerName : '',
        USPProduct: currentProduct.USPProduct ? currentProduct.USPProduct : '',
        productBenefits: currentProduct.productBenefits ? currentProduct.productBenefits : '',
        originalPrice: currentProduct.originalPrice ? handleFormatRightSide(handleFormatInput(String(currentProduct.originalPrice), 2), 2) : '',
        weight: currentProduct.weight ? handleFormatRightSide(handleFormatInput(String(currentProduct.weight), 1), 1) : '',
        dimension: currentProduct.dimension ? currentProduct.dimension : '',
        occupation: currentProduct.occupation ? currentProduct.occupation : '',
        expiryDate: currentProduct.expiryDate ? moment(currentProduct.expiryDate) : '',
        insights: currentProduct.insights ? currentProduct.insights : ''
      });
      currentProduct.name && setProductName(currentProduct.name)
      currentProduct.gender && handleUpdateAudiences({ key: 'gender', title: currentProduct.gender.length > 1 ? 'Not Specific' : currentProduct.gender[0], value: currentProduct.gender })
      currentProduct.ageMin && currentProduct.ageMax && handleUpdateAudiences({ key: 'age', title: `${currentProduct.ageMin}-${currentProduct.ageMax}`, value: [currentProduct.ageMin, currentProduct.ageMax] })
      currentProduct.category && handleUpdateCategories(
        { ids: currentProduct.category.ids, names: currentProduct.category.names }
      )
      currentProduct.weightUnit && handleUpdateWeightUnit({ value: currentProduct.weightUnit })
      currentProduct.manufacturerCountry && handleUpdateCountry({ value: COUNTRY_CODE[currentProduct.manufacturerCountry].toUpperCase() })
    }
    else {
      form.resetFields();
      resetFields()
    }
  }, [currentProduct, code, form, handleUpdateAudiences, handleUpdateCategories, handleUpdateCountry, handleUpdateWeightUnit])

  const onFinish = async (values) => {
    if (productName !== '') {
      if (code) {
        const name = productName
        const manufacturerCountry = Object.keys(COUNTRY_CODE).find(key => COUNTRY_CODE[key] === country.toLowerCase());
        const codeProduct = await handleUpdateProduct({
          code: code,
          newData: {
            ...values,
            name,
            manufacturerCountry,
            currency: 'USD',
            ageMax: maxAge,
            ageMin: minAge,
            categories,
            gender,
            originalPrice: values.originalPrice && Number(values.originalPrice.replaceAll(',', '')),
            weight: values.weight && Number(values.weight.replaceAll(',', '')),
            weightUnit
          }
        })
        if (codeProduct) {
          photoAndVideo.current.onUpload(values.upload, code)
          history.push(router.products)
        }
      }
      else {
        const name = productName
        const manufacturerCountry = Object.keys(COUNTRY_CODE).find(key => COUNTRY_CODE[key] === country.toLowerCase());
        const code = await handleCreateProduct({
          ...values,
          name,
          manufacturerCountry,
          currency: 'USD',
          ageMax: maxAge,
          ageMin: minAge,
          categories,
          gender,
          originalPrice: values.originalPrice && Number(values.originalPrice && values.originalPrice.replaceAll(',', '')),
          weight: values.weight && Number(values.weight.replaceAll(',', '')),
          weightUnit
        })
        if (code) {
          photoAndVideo.current.onUpload(values.upload, code)
          history.push(router.products)
        }
      }
    }
  }

  const handleChangeInput = (input) => {
    setRefName(!input)
    setProductName(input)
  }
  const handleSubmit = () => {
    setRefName(true)
    form.submit()
  }
  return (
    <Page helmet="Create Product" alignTop requiredAccess={[userRoles.COMPANY]}>
      <div className="create-product">
        <div className="create-product__header">
          <div>
            <CloseOutlined onClick={() => setProductName('')} />
            <input className={refName && productName === '' ? "input-error" : undefined} placeholder="Add Product Name" required value={productName} onChange={(e) => handleChangeInput(e.target.value)}></input>
            {refName && productName === '' && <div className="error-name">Product name is missing</div>}
          </div>
          <div className="create-product__header__button">
            <Button onClick={() => history.goBack()}><CloseCircleOutlined />Cancel</Button>
            <Button type="submit" onClick={() => handleSubmit()} > <SaveOutlined />Save</Button>
          </div>
        </div>
        <div className="create-product__main">
          <Menu top={refName && productName === '' ? 104 : 78} />
          <div className="create-product__content">
            <Form form={form} onFinish={onFinish} layout="vertical" >
              <GeneralInformation form={form} />
              <PhotoAndVideo ref={photoAndVideo} />
              <ProductPrice />
              <VariantAttributes />
              <TargetAudiences />
            </Form>
          </div>
        </div>
      </div>
    </Page >
  )
};
const mapStateToProps = ({ createProduct: { country, category, audiences, weightUnit }, detailProduct: { currentProduct } }) => ({
  country,
  weightUnit,
  categories: category.ids,
  minAge: audiences.age.value[0],
  maxAge: audiences.age.value[1],
  gender: audiences.gender.value,
  currentProduct
});
const mapDispatchToProps = {
  handleUpdateCategories,
  handleUpdateAudiences,
  handleUpdateCountry,
  handleUpdateWeightUnit,
  handleCreateProduct,
  handleUpdateProduct,
  getTags,
  getDetailProduct,
  resetFields
};
export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);