import React, { useState } from 'react';
import { Link, withRouter } from "react-router-dom";
import { Form, Input, Checkbox, Button } from "antd";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";
import { IconFacbook, IconGoogle } from "views/components/Icon";
import WelcomeSlider from "views/components/WelcomeSlider";
import PolicyModal from "views/components/PolociModal/_policyModal";
import { IconLock, IconArrowRightWhite, IconStar } from 'views/components/Icon/basic'
import { IconEnvelope } from 'views/components/Icon/essentials'
import { IconEye } from 'views/components/Icon/authoring'
import { doSignUp } from '../SignUpHost/actions';
import { constants, ReactUtils } from 'helpers';
import { connect } from 'react-redux';

const { userRoles } = constants;

const SignUpCompany = ({ doSignUp, history, errorMessage }) => {

    const [policyVisible, setPolicyVisible] = useState(false);

    const closePolicyModal = () => {
        setPolicyVisible(false)
    }

    const openPolicyModal = () => {
        setPolicyVisible(true);
    }

    const onFinish = async (values) => {
        await doSignUp({
            name: values.name,
            email: values.email,
            password: values.password,
            role: userRoles.COMPANY
        }, history);
    }

    return (
        <Page helmet="Sign Up Company" layout="split">
            <div className="primary-section">
                <div className="header">
                    <LogoFull />
                </div>
                <div className="content">
                    <div className="auth-form">
                        <h2>Sign Up</h2>
                        <p>
                            Already have an account? <Link to="sign-in/company">Sign in</Link>
                        </p>
                        <Form onFinish={onFinish} layout="vertical">
                            <Form.Item name="name" rules={[{ required: true, message: 'Name is missing' },]} label="Company Name" className='cb-term-condition'>
                                <Input
                                    size="large"
                                    prefix={<IconStar />}
                                    placeholder="e.g. Captive Interactive"
                                />
                            </Form.Item>
                            <Form.Item name="email" label="Email" className='cb-term-condition'
                                rules={[
                                    { required: true, message: 'Email is missing' },
                                    { validator: ReactUtils.emailValidator }
                                ]}>
                                <Input
                                    size="large"
                                    prefix={<IconEnvelope />}
                                    placeholder="e.g. captive@interactive.sg"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Password"
                                name="password"
                                className='cb-term-condition'
                                label="Password"
                                help="At least 8 characters. Contains a number or symbol."
                                rules={[
                                    { required: true, message: 'Password is missing' },
                                    { validator: ReactUtils.passwordVaidator }
                                ]}
                            >
                                <Input.Password
                                    size="large"
                                    prefix={<IconLock />}
                                    suffix={<IconEye />}
                                />
                            </Form.Item>
                            <Form.Item name="isAgree"
                                valuePropName="checked"
                                className='cb-term-condition'
                                rules={[
                                    {
                                        validator: (rule, value, callback) => {
                                            if (!value) {
                                                callback('Please read and accept the Terms and Conditions');
                                            } else {
                                                callback();
                                            }
                                        }
                                    }
                                ]}>
                                <Checkbox>
                                    I agree to the <Link onClick={openPolicyModal}>Terms &amp; Conditions</Link>
                                </Checkbox>
                            </Form.Item>
                            <Form.Item>
                                <Button htmlType="submit" type="primary" size="large" className="cta" block>
                                    Start Platform <IconArrowRightWhite />
                                </Button>
                            </Form.Item>
                            {errorMessage &&
                                <p style={{ color: "red" }}>{errorMessage}</p>
                            }
                        </Form>
                        <div className="third-party">
                            <p>Or Continue with:</p>
                            <Button icon={<IconGoogle />} size="large" />
                            <Button icon={<IconFacbook />} size="large" />
                        </div>
                    </div>
                </div>
                <div className="footer">
                    © 2021 SCX. All Rights Reserved.
        </div>
            </div>
            <div className="secondary-section">
                <WelcomeSlider />
            </div>
            <PolicyModal visible={policyVisible} handleClose={closePolicyModal} />
        </Page>
    );
}

const mapStateToProps = ({ signUp }) => ({
    errorMessage: signUp.errorMessage,
});

const mapDispatchToProps = {
    doSignUp,
};

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(SignUpCompany)
);

