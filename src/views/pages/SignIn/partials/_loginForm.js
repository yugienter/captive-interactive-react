/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { doLogin } from '../actions';
import { Form, Input, Button } from "antd";
// import { ArrowRightOutlined } from "@ant-design/icons";
import { utils } from 'helpers';
import { IconLock, IconArrowRightWhite } from 'views/components/Icon/basic' 
import { IconEnvelope } from 'views/components/Icon/essentials'
import { IconEye } from 'views/components/Icon/authoring'

import { constants } from 'helpers';

const { userRoles } = constants;

/* LoginMethods in helpers/constants */
/* TODO - Process method login by google and facebook */
// const LOGIN_METHODS = Object.keys(LoginMethods).reduce((result, key) => {
//   result[key] = key;
//   return result;
// }, {});

class LoginForm extends React.PureComponent {

  onFinish = async (values) => {
    const { doLoginAction, history, match } = this.props;
    await doLoginAction({ ...values, role: match.params.role || userRoles.HOST }, history);
  }

  emailValidator = (rule, value, callback) => {
    if (!value) callback('Email is missing');
    const isInvalidEmail = !utils.validateEmail(value);
    if (isInvalidEmail) {
      callback('Email is invalid');
    } else {
      callback();
    }
  };

  render() {
    const { loginErrorMessage } = this.props;
    return (
      <Form
        layout="vertical"
        requiredMark={false}
        onFinish={this.onFinish}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            { validator: this.emailValidator }
          ]}
          validateTrigger='onBlur'
        >
          <Input
            size="large"
            prefix={<IconEnvelope />}
            placeholder="Enter your registered email"
          />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Password is missing' },]}
        >
          <Input.Password
            size="large"
            placeholder="Enter password"
            prefix={<IconLock />}
            suffix={<IconEye />}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            className="cta"
            htmlType="submit"
            block
          >
            Log in <IconArrowRightWhite />
          </Button>
        </Form.Item>
        {loginErrorMessage &&
          <p style={{ color: "red" }}>{loginErrorMessage}</p>
        }
      </Form>
    )
  }
}

const mapStateToProps = ({ login, common }) => ({
  loginErrorMessage: login.loginErrorMessage,
  role: login.role
});

const mapDispatchToProps = {
  doLoginAction: doLogin,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)
    (LoginForm)
);

