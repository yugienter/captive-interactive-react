/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

const REDUCER = 'login';

export const SET_ON_LOGIN = `@@${REDUCER}/SET_ON_LOGIN`;
export const UPDATE_LOGIN_ERR_MSG = `@@${REDUCER}/UPDATE_LOGIN_ERR_MSG`;
export const SET_ROLE = `@@${REDUCER}/SET_ROLE`;
