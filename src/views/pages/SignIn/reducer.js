/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import * as types from './actionTypes';

const initialState = {
  isOnLogin: false,
  loginErrorMessage: '',
  role: 'host'
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ON_LOGIN:
      return { ...state, isOnLogin: action.payload };

    case types.UPDATE_LOGIN_ERR_MSG:
      return { ...state, loginErrorMessage: action.payload };

    case types.SET_ROLE:
      return { ...state, role: action.payload };

    default:
      return state;
  }
};

export default loginReducer;
