/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import React from "react";
import { connect } from "react-redux";
import { LoginForm } from "./partials";
import { Link } from "react-router-dom";
import Page from "views/components/Page";
import { Button } from "antd";
import LogoFull from "views/components/Logo/LogoFull";
import { IconFacbook, IconGoogle } from "views/components/Icon";
import { constants } from "helpers";
import { withRouter } from "react-router";

const { router } = constants;

class SignIn extends React.PureComponent {
  componentDidMount() {
    localStorage.removeItem("roleSignUp");
  }

  render() {
    const { isOnInitAuth } = this.props;

    return (
      <Page helmet="Sign In" layout="split">
        <div className="primary-section w80">
          <div className="header">
            <LogoFull />
          </div>
          <div className="content">
            <div className="auth-form">
              <h2>Log In</h2>
              <p style={{ marginBottom: "30px" }}>
                New to Social Commerce Exchange?{" "}
                <Link
                  to={
                    this.props.match.params.role === "company"
                      ? router.signUpCompany
                      : router.signUpHost
                  }
                >
                  Sign up
                </Link>{" "}
                today
              </p>
              {!isOnInitAuth && <LoginForm />}
              <div className="third-party">
                <p>Or Continue with:</p>
                <Button icon={<IconFacbook />} size="large" />
                <Button icon={<IconGoogle />} size="large" />
              </div>
            </div>
          </div>
          <div className="footer">© 2021 SCX. All Rights Reserved.</div>
        </div>
        <div className="secondary-section w20"></div>
      </Page>
    );
  }
}

const mapStateToProps = ({ common }) => ({
  isOnInitAuth: common.isOnInitAuth,
});

export default connect(mapStateToProps, {})(withRouter(SignIn));
