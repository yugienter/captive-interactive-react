/**
 * Author : phu.nguyenluu@gmail.com
 * Update : 22/08/2021
 */

import { constants, client } from "helpers";

import * as types from "./actionTypes";

const { api, router } = constants;
const { statusCode } = api;

const _handleLoginResponse = (res, history) => (dispatch) => {
  // clean loginErrorMessage
  dispatch({ type: types.UPDATE_LOGIN_ERR_MSG, payload: "" });

  if (res instanceof Error) {
    const error = "Network Error";
    return dispatch({ type: types.UPDATE_LOGIN_ERR_MSG, payload: error });
  }

  const { status, data } = res;
  if (!status || status !== statusCode.SUCCESS) {
    let error = data.message || "Internal Server Error";
    if (data.error === "ValidationError") {
      Object.keys(data.validationMessages).forEach((key) => {
        error = data.validationMessages[key];
      });
    }
    return dispatch({ type: types.UPDATE_LOGIN_ERR_MSG, payload: error });
  }

  // Store data to localstorage
  const { accessToken: token } = data.data;
  localStorage.setItem("token", `Bearer ${token}`);

  return history.replace(router.home);
};

export const doLogin = (data, history) => async (dispatch) => {
  try {
    dispatch({ type: types.SET_ON_LOGIN, payload: true });

    const res = await client.request({
      path: api.path.login,
      method: "post",
      data,
    });

    return _handleLoginResponse(res, history)(dispatch);
  } catch (error) {
    return _handleLoginResponse(error)(dispatch);
  } finally {
    dispatch({ type: types.SET_ON_LOGIN, payload: false });
  }
};

export const setRole = (role) => (dispatch) => {
  dispatch({ type: types.SET_ROLE, payload: role });
};
