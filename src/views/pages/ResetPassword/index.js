import { Link } from "react-router-dom";
import { Form, Input, Button } from "antd";
import { ArrowRightOutlined, MailOutlined } from "@ant-design/icons";
import Page from "views/components/Page";
import LogoFull from "views/components/Logo/LogoFull";

const ResetPassword = () => {
  return (
    <Page helmet="Reset Password" layout="split">
      <div className="primary-section w80">
        <div className="header">
          <LogoFull />
        </div>
        <div className="content">
          <div className="auth-form">
            <h2>Reset Password</h2>
            <p>We will send a reset link to your registered email.</p>
            <Form layout="vertical">
              <Form.Item label="Email">
                <Input
                  size="large"
                  prefix={<MailOutlined />}
                  placeholder="Enter your registered email"
                />
              </Form.Item>

              <Form.Item>
                <Button type="primary" size="large" className="cta" block>
                  Send <ArrowRightOutlined />
                </Button>
              </Form.Item>
            </Form>
            <p>
              Don’t have account? <Link to="sign-up">Sign up</Link>
            </p>
          </div>
        </div>
        <div className="footer">
          © 2021 SCX. All Rights Reserved.
        </div>
      </div>
      <div className="secondary-section w20"></div>
    </Page>
  );
}

export default ResetPassword;