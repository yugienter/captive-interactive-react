import { constants, client, utils } from "helpers";
import * as types from "./actionTypes";
import _ from "lodash";

const { api, PAGINATION } = constants;

export const addJob = (params) => async (dispatch) => {
  try {
    dispatch({ type: types.ADD_JOB, payload: true });

    const { data } = await client.request({
      path: api.path.createJob,
      method: "POST",
      data: params,
    });
    if (data) {
      dispatch({ type: types.ADD_JOB_SUCCESS, payload: data });
      return data;
    }
  } catch (err) {
    console.log("[addJob]", err);
    dispatch({ type: types.ADD_JOB_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.ADD_JOB, payload: false });
  }
};

export const addDeal = (params) => async (dispatch) => {
  try {
    dispatch({ type: types.ADD_DEAL, payload: true });

    const { data } = await client.request({
      path: api.path.createDeal,
      method: "POST",
      data: params,
    });
    if (data) {
      dispatch({ type: types.ADD_DEAL_SUCCESS, payload: data });
      return data;
    }
  } catch (err) {
    console.log("[addJob]", err);
    dispatch({ type: types.ADD_DEAL_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.ADD_DEAL, payload: false });
  }
};

export const createDealByHost = (params) => async (dispatch) => {
  try {
    dispatch({ type: types.ADD_DEAL, payload: true });

    const { data } = await client.request({
      path: api.path.createDealByHost,
      method: "POST",
      data: params,
    });
    if (data) {
      dispatch({ type: types.ADD_DEAL_SUCCESS, payload: data });
      return data;
    }
  } catch (err) {
    console.log("[createDealByHost]", err);
    dispatch({ type: types.ADD_DEAL_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.ADD_DEAL, payload: false });
  }
};

const _updateStore = (key, value) => (dispatch) => {
  const payload = { [key]: value };
  dispatch({
    type: types.UPDATE_FILTER,
    payload,
  });
};

const _makeQuery = (options, stateHostList) => (dispatch) => {
  const { filter, pagination } = stateHostList;
  const query = {};

  Object.entries(filter).forEach(([key, value]) => {
    const condition = value;
    const queryKey = utils.camelCaseToUnderScore(key);
    if (condition || !isNaN(value)) {
      query[queryKey] = value;
    }
  });

  Object.entries(options).forEach(([key, value]) => {
    const condition = value;
    const queryKey = utils.camelCaseToUnderScore(key);
    if (condition || !isNaN(value)) {
      query[queryKey] = value;
      if (!["page", "perPage"].includes(key)) {
        _updateStore(key, value)(dispatch);
      }
    }
  });

  const { page = PAGINATION.PAGE, perPage = pagination.perPage } = options;
  query.page = page;
  query.per_page = perPage;

  return query;
};

const _doSearch = async (query = {}) => {
  try {
    const res = await client.request({
      path: api.path.getMyJobs,
      method: "get",
      params: query,
    });

    const { data, page, perPage, total, lastPage } = res.data;
    const jobs =
      [] ||
      [
        "opens",
        "deal",
        "proposal",
        "accepted",
        "rejected",
        "complete",
        "verified",
      ].map((status) => {
        const job = JSON.parse(JSON.stringify(data[0]));
        job.deal[0].status = status;
        return job;
      });

    return {
      data: data.concat(jobs) || [],
      pagination: { page, perPage, total, lastPage },
    };
  } catch (error) {
    console.log("[_doSearch] Error: ", error);
    return { data: [], pagination: {} };
  }
};

export const search = (options = {}) => async (dispatch, getState) => {
  dispatch({ type: types.UPDATE_LOADING, payload: true });
  const stateJobReduce = getState().jobReducer;
  let data = stateJobReduce.data;

  const query = _makeQuery(options, stateJobReduce)(dispatch);
  const result = await _doSearch(query);
  result.data.forEach((job, index) => {
    job.rowIndex = index;
  });
  data = result.data;
  dispatch({ type: types.UPDATE_JOBS, payload: result });

  dispatch({ type: types.UPDATE_LOADING, payload: false });
  return data;
};

export const countJobsOfSeller = async () => {
  try {
    const { data } = await client.request({
      path: api.path.jobsOfSellerCount(),
      method: "get",
    });
    return data.data;
  } catch (err) {
    console.log("[countJobsOfSeller]", err);
  }
};

export const getSellerTotalIncome = async () => {
  try {
    const { data } = await client.request({
      path: api.path.getSellerTotalIncome,
      method: "get",
    });
    return data;
  } catch (err) {
    console.log("[countJobsOfSeller]", err);
  }
};

export const getCampaignAndDealByJobCode = async (jobCode) => {
  try {
    const { data } = await client.request({
      path: api.path.getCampaignAndDeal(jobCode),
      method: "get",
    });

    return data || {};
  } catch (err) {
    console.log("[getCampaignAndDealByJobCode]", err);
  } finally {
  }
};

export const completeJob = ({ dealCode, jobCode, params }) => async (
  dispatch
) => {
  try {
    const { data } = await client.request({
      path: api.path.completeJob(dealCode, jobCode),
      method: "post",
      data: params,
    });
    dispatch({ type: types.ADD_DEAL_SUCCESS, payload: data });
  } catch (err) {
    console.log("[completeJob]", err);
  } finally {
  }
};

export const acceptDeal = (dealCode) => async (dispatch) => {
  try {
    const path = api.path.acceptDeal(dealCode);
    const { data = {} } = await client.request({
      path: path,
      method: "patch",
    });
    data.data &&
      dispatch({
        type: types.ADD_DEAL_SUCCESS,
        payload: data.data,
      });
  } catch (err) {
    console.error("[acceptDeal]", err);
  }
};

export const rejectDeal = (dealCode) => async (dispatch) => {
  try {
    const path = api.path.rejectDeal(dealCode);
    const { data = {} } = await client.request({
      path: path,
      method: "patch",
    });
    data.data &&
      dispatch({
        type: types.ADD_DEAL_SUCCESS,
        payload: data.data,
      });
  } catch (err) {
    console.error("[rejectDeal]", err);
  }
};
