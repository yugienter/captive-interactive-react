import React from "react";
import Page from "views/components/Page";
import { constants } from "helpers";
import TopTitle from "./components/TopTitle";
import MainTabs from "./components/MainTabs";
import MainTable from "./components/MainTable";

const { userRoles } = constants;

const index = () => {
  return (
    <Page
      helmet="Recommender Seller Page"
      alignTop
      requiredAccess={[userRoles.HOST]}
    >
      <div className="myjobs-page">
        <TopTitle />
        <div className="myjobs-page__container">
          <MainTabs />
          <div className="myjobs-page__container--tabs" style={{ padding: '0 35px' }}>
            <MainTable />
          </div>
        </div>
      </div>
    </Page>
  );
};

export default index;
