const REDUCER = "JOB";

export const ADD_JOB = `@@${REDUCER}/ADD_JOB`;
export const ADD_JOB_SUCCESS = `@@${REDUCER}/ADD_JOB_SUCCESS`;
export const ADD_JOB_FAILED = `@@${REDUCER}/ADD_JOB_FAILED`;

export const ADD_DEAL = `@@${REDUCER}/ADD_DEAL`;
export const ADD_DEAL_SUCCESS = `@@${REDUCER}/ADD_DEAL_SUCCESS`;
export const ADD_DEAL_FAILED = `@@${REDUCER}/ADD_DEAL_FAILED`;

// type for reduce of load page my-jobs list
export const UPDATE_LOADING = `@@${REDUCER}/UPDATE_LOADING`;
export const UPDATE_JOBS = `@@${REDUCER}/UPDATE_JOBS`;
export const UPDATE_PAGINATION = `@@${REDUCER}/UPDATE_PAGINATION`;
export const UPDATE_FILTER = `@@${REDUCER}/UPDATE_FILTER`;

export const SET_DEALS = `@@${REDUCER}/SET_DEALS`;
export const SET_SELECTED_JOB = `@@${REDUCER}/SET_SELECTED_JOB`;
