import * as types from './actionTypes';
import { constants } from 'helpers';
const { PAGINATION } = constants;

const initialState = {
  isAddJob: false,
  isAddDeal: false,
  jobs: [],
  isLoading: false,
  filter: {
    sortCol: null,
    sortType: null,
    status: null,
  },
  pagination: {
    page: PAGINATION.PAGE,
    perPage: PAGINATION.PER_PAGE,
    total: null,
    lastPage: null,
  },

  //
  deals: [],
  selectedJob: null,
};

const jobReducer = (state = initialState, { type, payload }) => {
  const { rowIndex } = state.selectedJob || {};
  switch (type) {
    case types.ADD_JOB:
      return { ...state, isAddJob: payload };
    case types.ADD_JOB_SUCCESS:
      return { ...state };
    case types.ADD_JOB_FAILED:
      return { ...state };
    case types.ADD_DEAL:
      return { ...state, isAddDeal: payload };
    case types.ADD_DEAL_SUCCESS:
      return {
        ...state,
        deals: [payload, ...state.deals],
        jobs: state.jobs.map((job, index) => {
          const newJob = JSON.parse(JSON.stringify(job));
          if (index === rowIndex) {
            newJob.deal.unshift(payload);
          }
          return newJob;
        }),
      };
    case types.ADD_DEAL_FAILED:
      return { ...state };

    // for job list pages
    case types.UPDATE_LOADING:
      return {
        ...state,
        isLoading: !!payload,
      };

    case types.UPDATE_JOBS: {
      return {
        ...state,
        jobs: payload.data,
        pagination: payload.pagination,
      };
    }

    case types.UPDATE_PAGINATION: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          payload,
        },
      };
    }

    case types.UPDATE_FILTER: {
      return {
        ...state,
        filter: {
          ...state.filter,
          ...payload,
        },
      };
    }

    case types.SET_DEALS: {
      return { ...state, deals: payload };
    }

    case types.SET_SELECTED_JOB: {
      return { ...state, selectedJob: payload };
    }

    default:
      return state;
  }
};

export default jobReducer;
