import React from "react";
import { ImageTotal, IconUp } from "views/components/Icon/myjobs";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getSellerTotalIncome } from "../actions";

const TopTitle = () => {
  const [income, setIncome] = useState({ total: 0, currentMonth: 0 });

  useEffect(() => {
    async function getHostTotalIncome() {
      const data = await getSellerTotalIncome();
      setIncome(data);
    }

    getHostTotalIncome();
  }, [setIncome]);
  return (
    <div className="myjobs-page__top">
      <h1>My Jobs</h1>
      <div className="myjobs-page__top--right">
        <div className="myjobs-page__top--right__left">
          <h6>Total Income</h6>
          <div className="info">
            ${income.total | 0}
            <IconUp />
            <label>
              ${income.currentMonth}
              <small>this month</small>
            </label>
          </div>
        </div>
        <div className="myjobs-page__top--right__right">
          <ImageTotal />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TopTitle);
