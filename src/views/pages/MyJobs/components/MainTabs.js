import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Tabs } from "antd";
import { constants } from "helpers";
import * as types from "./../actionTypes";
import { countJobsOfSeller } from "../actions";

const { TabPane } = Tabs;
const { statusJob } = constants;

const MainTabs = ({ updateStatus }) => {
  const [count, setCount] = useState({});

  useEffect(() => {
    async function countSellers() {
      const data = await countJobsOfSeller();
      setCount(data);
    }

    countSellers();
  }, [setCount]);

  const changeTabByStatus = (status) => {
    const statusUpdate = status === 'all' ? null : status;
    updateStatus(statusUpdate);
  };

  return (
    <>
      <Tabs
        defaultActiveKey="1"
        onChange={changeTabByStatus}
        className="myjobs-page__container--tabs"
      >
        <TabPane tab="All Jobs" key="all">
        </TabPane>
        <TabPane
          tab={
            <div>
              Direct Offer<span>{count[statusJob.offer] || 0}</span>
            </div>
          }
          key={statusJob.offer}
        >
        </TabPane>
        <TabPane
          tab={
            <div>
              Pitch Submission<span>{count[statusJob.submitted] || 0}</span>
            </div>
          }
          key={statusJob.submitted}
        >
        </TabPane>
        <TabPane
          tab={
            <div>
              In negotiation<span>{count[statusJob.negotiating] || 0}</span>
            </div>
          }
          key={statusJob.negotiating}
        >
        </TabPane>
        <TabPane
          tab={
            <div>
              Executing campaign<span>{count[statusJob.execute] || 0}</span>
            </div>
          } key={statusJob.execute}
        >
        </TabPane>
        <TabPane
          tab={
            <div>
              Completed<span>{count[statusJob.complete] || 0}</span>
            </div>
          }
          key={statusJob.complete}
        >
        </TabPane>
        <TabPane
          tab={
            <div>
              Rejected<span>{count[statusJob.rejected] || 0}</span>
            </div>
          }
          key={statusJob.rejected}
        >
        </TabPane>
      </Tabs>
    </>
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  updateStatus: (status) =>
    dispatch({
      type: types.UPDATE_FILTER,
      payload: { status: status || null },
    }),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainTabs);
