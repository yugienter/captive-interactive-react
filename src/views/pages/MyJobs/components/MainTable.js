import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Table, Space, Menu, Dropdown } from "antd";
import {
  IconExpand,
  IconNoted,
  IconDots,
  IconSubmitted,
  IconCompleteGrey,
  IconCompleteYellow,
  IconNotedFull,
  IconPropose,
  IconTickGreen,
  IconX,
} from "views/components/Icon/myjobs";
import DrawerCompleteJob from "views/components/DrawerCompleteJob";
import { acceptDeal, rejectDeal, search } from "../actions";
import { utils, constants, router } from "helpers";
import moment from "moment";
import * as types from "./../actionTypes";
import { bindActionCreators } from "redux";
import { Icon1 } from "views/components/Icon/status";
import MyJobsDrawer from "views/components/MyJobsDrawer";
import DrawerSubmitProposalMyJobs from "views/components/DrawerSubmitProposalMyJobs";
import DrawerConversation from "views/components/Conversation/DrawerConversation";
import { IconConversation } from "views/components/Icon/conversation";
import _ from "lodash"
import { useHistory } from "react-router-dom"
import { selectJob } from "../../JobOrder/actions"

const { PAGINATION, statusJob, statusDeal } = constants;

const MainTable = ({
  jobs,
  pagination,
  search,
  isLoading,
  status,
  setDeals,
  setSelectedJob,
  acceptDeal,
  rejectDeal,
  selectJob
}) => {
  const [proposalDeal, setProposalDeal] = useState(null);
  const [showProposeDeal, closeProposeDealModal] = useState(false);
  const [completeDeal, setCompleteDeal] = useState(false);
  const history = useHistory()

  useEffect(() => {
    const params = { page: PAGINATION.PAGE, perPage: PAGINATION.PER_PAGE };
    if (status) params.status = status;
    search(params);
  }, [search, status]);

  const handleTableChange = (pagination, filter, sorter) => {
    let sortType = null;
    if (sorter.order === "descend") sortType = -1;
    if (sorter.order === "ascend") sortType = 1;

    const params = {
      page: pagination.current - 1,
      perPage: pagination.pageSize,
      sortCol: sorter.order ? sorter.columnKey : null,
      sortType,
    };
    search(params);
  };

  const commissionText = (value) => {
    if (value === "percent") return "%";
    return value;
  }

  const columns = [
    {
      title: "Product",
      key: "product",
      render: (record) => {
        const urlImage = record.product?.media?.length
          ? utils.getMediaUrl(record.product.media[0].url)
          : null;
        return (
          <div className="box-products-wrapper">
            <div className="box-products">
              <div className="box-products__left">
                <figure>
                  <img className="dropdown-img" src={urlImage} alt="" />
                </figure>
              </div>
              <div className="box-products__right">
                <p className="first">{record.companyCode}</p>
                <p className="last">{record.product?.name}</p>
              </div>
            </div>
            <div className="last">
              {/* <span className="bid green">Bid</span>
              <span className="bid grey">Bid</span> */}
            </div>
          </div>
        );
      },
    },
    {
      title: "Status",
      dataIndex: "deal",
      key: "status",
      render: (deals) => {
        const { statusJob: status } = deals[0] || {};
        switch (status) {
          case statusJob.offer:
            return <div className="status direct">Direct Offer</div>;
          case statusJob.negotiating:
            return <div className="status negotiating">In negotiation</div>;
          case statusJob.execute:
            return <div className="status waiting">Executing campaign</div>;
          case statusJob.submitted:
            return <div className="status bidSubmitted">Pitch Submission</div>;
          case statusJob.complete:
          case statusJob.verify:
            return <div className="status complete">Completed</div>;
          case statusJob.rejected:
            return <div className="status reject">Rejected</div>;
          default:
            return <div className="status direct">{statusJob}</div>;
        }
      },
    },
    {
      title: "Fixed Rate",
      dataIndex: "deal",
      key: "hourly",
      sorter: {
        compare: (a, b) => a.math - b.math,
        multiple: 2,
      },
      render: (deal) => (
        <div className="hourly">
          {deal && deal.length
            ? `$${deal[0]?.streamHourlyRate?.description || 0}`
            : ""}
        </div>
      ),
    },
    {
      title: "Commission",
      dataIndex: "deal",
      key: "commission",
      sorter: {
        compare: (a, b) => a.english - b.english,
        multiple: 1,
      },
      render: (deal) => (
        <div className="hourly">
          {deal && deal.length
            ? `${deal[0].streamCommission.description || 0} ${commissionText(deal[0].streamCommissionUnit)
            }`
            : ""}
        </div>
      ),
    },
    {
      title: "Date Modified",
      dataIndex: "deal",
      key: "modified",
      render: (deal) => (
        <div className="date">
          {deal && deal.length
            ? moment(deal[0].createdAt).format("DD MMM YYYY")
            : ""}
        </div>
      ),
    },
    {
      title: "Stream Date",
      dataIndex: "deal",
      key: "stream",
      render: (deal) => (
        <div className="date">
          {deal && deal.length
            ? deal[0].streamTime.description
              ? moment(deal[0].streamTime.description).format("DD MMM YYYY")
              : "--"
            : ""}
        </div>
      ),
    },
    {
      title: "Action",
      // dataIndex: "deal",
      render: (record) => {
        const { statusJob: status } = record.deal[0] || {};
        return (
          <Space size="middle" onClick={e => e.stopPropagation()}>
            <span className="clickable" onClick={() => { onShowDrawer(record) }}>
              <IconNoted />
              {/* <IconNotedFull /> */}
            </span>
            {status !== statusJob.execute && <a href="##" onClick={() => { onShowDrawerConversation(record) }}>
              <IconConversation />
            </a>
            }
            <>{renderIcon(record.deal)}</>
          </Space>
        )
      },
    },
    {
      title: "Payment status",
      // dataIndex: "deal",
      render: (record) => (
        <span></span>
      ),
    },
  ];

  const [showDrawer, setShowDrawer] = useState(false);
  const [record, setRecord] = useState(false);
  const [showDrawerConversation, setShowDrawerConversation] = useState(false);

  const renderRowActions = (actions) => {
    const overlay = <Menu>{actions}</Menu>;
    return (
      <Dropdown overlay={overlay} trigger={["click"]}>
        <span className="clickable">
          <IconDots />
        </span>
      </Dropdown>
    );
  };

  const handleAcceptDeal = async (deal) => {
    const dealCode = deal.code;
    if (dealCode) {
      await acceptDeal(dealCode);
      refreshData();
    }
  };

  const handleRejectDeal = async (deal) => {
    const dealCode = deal.code;
    if (dealCode) {
      await rejectDeal(dealCode);
      refreshData();
    }
  };

  const refreshData = async () => {
    const params = { page: PAGINATION.PAGE, perPage: PAGINATION.PER_PAGE };
    if (status) params.status = status;
    search(params);
  }

  const handleCompleteJob = (deal) => {
    setCompleteDeal(deal);
  };

  const renderIcon = (deals) => {
    const deal = deals[0] || {};
    switch (deal.status) {
      case statusDeal.opens:
      case statusDeal.deal: {
        return renderRowActions(
          <div className="popup-row-actions">
            <Menu.Item
              key="0"
              onClick={() => {
                setProposalDeal(deal);
                closeProposeDealModal(true);
              }}
            >
              <IconPropose />
              Propose a deal
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="1" onClick={() => handleAcceptDeal(deal)}>
              <IconTickGreen />
              Accept
            </Menu.Item>
            <Menu.Item key="2" onClick={() => handleRejectDeal(deal)}>
              <IconX />
              Reject
            </Menu.Item>
          </div>
        );
      }
      case statusDeal.proposal:
        return (
          <span className="clickable box-tooltip">
            <IconSubmitted />
            {
              <div className="box-tooltip__content">
                Wait for Company to response your proposed deal
              </div>
            }
          </span>
        );
      case statusDeal.accepted: {
        return (
          <div>
          </div>
        );
      }
      case statusDeal.complete:
        return (
          <span className="clickable box-tooltip">
            <IconCompleteGrey />
            {
              <div className="box-tooltip__content box-tooltip__content--verified">
                Wait for Verifying
              </div>
            }
          </span>
        );
      case statusDeal.verified:
        return (
          <span className="clickable box-tooltip">
            <IconCompleteYellow />
            {
              <div className="box-tooltip__content box-tooltip__content--verified">
                Verified by Company
              </div>
            }
          </span>
        );
      case statusDeal.rejected:
      default:
        return;
    }
  };

  const onShowDrawer = (record) => {
    setShowDrawer(true);
    setDeals(record.deal);
  };

  const onCloseDrawer = () => {
    setDeals([]);
    setSelectedJob(null);
    setShowDrawer(false);
  };

  const onRow = (record) => ({
    onClick: () => {
      console.log("onRow", record);
      setSelectedJob(record);
      // const status = _.get(record, 'deal[0].statusJob');
      // if (status === statusJob.execute) {
      //   history.push('/job-order/' + _.get(record, 'code'))
      //   selectJob(record)
      // }
    },
  });

  const onShowDrawerConversation = (record) => {
    setShowDrawerConversation(true);
    setRecord(record);
  };

  const onCloseDrawerConversation = () => {
    setRecord(false);
    setShowDrawerConversation(false);
  };

  return (
    <>
      <Table
        columns={columns}
        dataSource={jobs}
        onChange={handleTableChange}
        rowKey={(record) => record.code}
        onRow={onRow}
        pagination={{
          position: ["bottomCenter"],
          showSizeChanger: true,
          showTotal: (total, range) =>
            `Showing ${range[0]}-${range[1]} of ${total} results`,
          size: "small",
          current: pagination.page + 1,
          pageSize: pagination.perPage,
          total: pagination.total,
        }}
        loading={isLoading}
      />
      <MyJobsDrawer showDrawer={showDrawer} onCloseDrawer={onCloseDrawer} />
      <DrawerSubmitProposalMyJobs
        deal={proposalDeal}
        visible={showProposeDeal}
        handleClose={() => closeProposeDealModal(false)}
      />
      <DrawerCompleteJob
        showDrawer={completeDeal}
        onCloseDrawer={() => setCompleteDeal(false)}
        deal={completeDeal}
      />
      <DrawerConversation
        showDrawer={showDrawerConversation}
        onCloseDrawer={onCloseDrawerConversation}
        record={record}
      />
    </>
  );
};

const mapStateToProps = ({
  jobReducer: { jobs, pagination, isLoading, filter },
}) => ({
  jobs,
  pagination,
  isLoading,
  status: filter.status,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(
    {
      search,
      acceptDeal,
      rejectDeal,
      selectJob
    },
    dispatch
  ),
  setDeals: (deals) => {
    dispatch({
      type: types.SET_DEALS,
      payload: deals,
    });
  },
  setSelectedJob: (job) => {
    dispatch({
      type: types.SET_SELECTED_JOB,
      payload: job,
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MainTable);
