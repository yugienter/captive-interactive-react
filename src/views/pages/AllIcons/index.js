import { Tooltip } from 'antd';
import { useState } from 'react';
import * as arrows from 'views/components/Icon/arrows';
import * as authoring from 'views/components/Icon/authoring';
import * as basic from 'views/components/Icon/basic';
import * as campaign from 'views/components/Icon/campaign';
import * as category from 'views/components/Icon/category';
import * as collapse from 'views/components/Icon/collapse';
import * as country from 'views/components/Icon/country';
import * as drawer from 'views/components/Icon/drawer';
import * as essentials from 'views/components/Icon/essentials';
import * as index from 'views/components/Icon/index';
import * as market from 'views/components/Icon/market';
import * as myjobs from 'views/components/Icon/myjobs';
import * as pages from 'views/components/Icon/pages';
import * as product from 'views/components/Icon/product';
import * as recommender from 'views/components/Icon/recommender';
import * as status from 'views/components/Icon/status';
import * as topics from 'views/components/Icon/topics';

const AllIcons = () => {
  const icons = [
    ...Object.entries(arrows),
    ...Object.entries(authoring),
    ...Object.entries(basic),
    ...Object.entries(campaign),
    ...Object.entries(category),
    ...Object.entries(collapse),
    ...Object.entries(country),
    ...Object.entries(drawer),
    ...Object.entries(essentials),
    ...Object.entries(index),
    ...Object.entries(market),
    ...Object.entries(myjobs),
    ...Object.entries(pages),
    ...Object.entries(product),
    ...Object.entries(recommender),
    ...Object.entries(status),
    ...Object.entries(topics),
  ];
  const [clicks, setClicks] = useState(Array(icons.length).fill(false));

  const handleDbClick = (index) => {
    setClicks(
      clicks.map((click, i) => {
        if (index === i) return !click;
        return click;
      }),
    );
  };

  return (
    <div id="all-icons" style={{ background: 'white' }}>
      {icons.map(([key, fn], index) => {
        try {
          return (
            <Tooltip title={key}>
              <span
                onDoubleClick={() => handleDbClick(index)}
                style={{ padding: 4 }}
                className={clicks[index] ? 'clicked' : ''}
              >
                {fn()}
              </span>
            </Tooltip>
          );
        } catch (err) {
          return (
            <Tooltip title={err.message}>
              <span style={{ padding: 2 }}>{key}</span>
            </Tooltip>
          );
        }
      })}
    </div>
  );
};

export default AllIcons;
