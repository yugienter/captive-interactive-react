import { Button, Input, Tabs } from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { constants, router } from "helpers";
import { countJobsOfCompany, getListProduct } from "./actions";
import { useEffect, useState } from "react";

import InfiniteScroll from "react-infinite-scroll-component";
import Item from "./parts/AllProduct/Item";
import Page from "views/components/Page";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

const { userRoles, statusJob } = constants;
const { TabPane } = Tabs;

const ProductsPage = ({
  listProduct,
  getListProduct,
  per_page,
  page,
  hasMore,
}) => {
  const history = useHistory();
  const [search, setSearch] = useState("");
  const [showSearch, setShowSearch] = useState(false);
  const [statusTab, setStatusTab] = useState("all");
  const [jobsCount, setJobsCount] = useState({});

  async function countJobs() {
    const jobs = await countJobsOfCompany(search);
    setJobsCount(jobs);
  }

  useEffect(() => { 
    countJobs();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setJobsCount]);

  useEffect(() => {
    getListProduct({ per_page, page: 0, search });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getListProduct]);

  const onChangeTab = (key) => {
    const params = { per_page, page: 0, search };
    if (key !== "all") params.statusJob = key;
    setStatusTab(key);
    getListProduct(params);
  };

  const onSearch = () => {
    const params = { per_page, page: 0, search };
    if (statusTab !== "all") params.statusJob = statusTab;
    getListProduct(params);
    countJobs();
  };

  const showSearchAction = () => {
    setShowSearch(true);
  };

  return (
    <Page
      helmet="Products"
      alignTop
      selectedKeys={["products"]}
      requiredAccess={[userRoles.COMPANY]}
    >
      <div className="products-page">
        <div className="products-page__header">
          <h2>Products </h2>
          <div>
            {showSearch ? (
              <Input.Search
                className="search-input"
                placeholder="Search by name..."
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                onSearch={onSearch}
              />
            ) : (
              <SearchOutlined onClick={showSearchAction} />
            )}

            <Button
              type="primary"
              htmlType="submit"
              onClick={() => history.push(router.createProduct)}
            >
              <PlusOutlined />
              New Product
            </Button>
          </div>
        </div>
        <Tabs defaultActiveKey={statusTab} onChange={onChangeTab}>
          <TabPane tab={`All (${jobsCount.all || 0})`} key="all">
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() => getListProduct({ per_page, page })}
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane tab={`New (${jobsCount.new || 0})`} key="new">
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() => getListProduct({ per_page, page })}
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane
            tab={`Direct Offer (${jobsCount[statusJob.offer] || 0})`}
            key={statusJob.offer}
          >
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() =>
                getListProduct({ per_page, page, status: statusJob.offer })
              }
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane
            tab={`Bid Received (${jobsCount[statusJob.submitted] || 0})`}
            key={statusJob.submitted}
          >
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() =>
                getListProduct({ per_page, page, status: statusJob.submitted })
              }
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane
            tab={`Negotiating (${jobsCount[statusJob.negotiating] || 0})`}
            key={statusJob.negotiating}
          >
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() =>
                getListProduct({
                  per_page,
                  page,
                  status: statusJob.negotiating,
                })
              }
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane
            tab={`Waiting to Execute (${jobsCount[statusJob.execute] || 0})`}
            key={statusJob.execute}
          >
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() =>
                getListProduct({ per_page, page, status: statusJob.execute })
              }
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
          <TabPane
            tab={`Completed (${jobsCount[statusJob.complete] || 0})`}
            key={statusJob.complete}
          >
            <InfiniteScroll
              dataLength={listProduct.length}
              next={() =>
                getListProduct({ per_page, page, status: statusJob.complete })
              }
              hasMore={hasMore}
              loader={
                <p style={{ textAlign: "center", color: "white" }}>
                  <b>Loading...</b>
                </p>
              }
            >
              <div className="products-page__all-product">
                {listProduct &&
                  listProduct.length > 0 &&
                  listProduct.map((item, index) => (
                    <Item data={item} key={index} index={index + 1} />
                  ))}
              </div>
            </InfiniteScroll>
          </TabPane>
        </Tabs>
      </div>
    </Page>
  );
};
const mapStateToProps = ({
  products: { listProduct, per_page, page, hasMore },
}) => ({
  listProduct,
  per_page,
  page,
  hasMore,
});
const mapDispatchToProps = {
  getListProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductsPage);
