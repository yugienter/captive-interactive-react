import * as types from "./actionTypes";

const initialState = {
  isLoading: false,
  error: null,
  listProduct: [],
  listProductOffers: [],
  hasMore: true,
  page: 0,
  per_page: 16,
};

const createDetailReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.IS_FETCH_PRODUCTS:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_LIST_PRODUCT_SUCCESS:
      return {
        ...state,
        listProduct: payload,
        page: state.page + 1,
      };
    case types.FETCH_LIST_PRODUCT_FAILED:
      return {
        ...state,
        error: payload,
      };
    case types.IS_FETCH_PRODUCT_OFFERS:
      return {
        ...state,
        isLoading: payload,
      };
    case types.FETCH_LIST_PRODUCT_OFFERS_SUCCESS:
      return {
        ...state,
        listProductOffers: payload,
      };
    case types.FETCH_LIST_PRODUCT_OFFERS_FAILED:
      return {
        ...state,
        error: payload,
      };
    case types.HAS_MORE:
      return {
        ...state,
        hasMore: payload,
      };
    default:
      return state;
  }
};

export default createDetailReducer;
