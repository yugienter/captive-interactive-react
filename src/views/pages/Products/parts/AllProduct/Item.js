import { IconBox, IconPencil } from "views/components/Icon/product";
import { IconUsers } from "views/components/Icon/essentials";
import { utils } from "helpers/utils";
import { router } from "helpers";
import moment from "moment";
import { useHistory } from "react-router-dom";

const { getMediaUrl } = utils;

const getCampaignLabel = (campaignClass, txt) => {
  if (!campaignClass)
    return (
      <div>
        <span />
      </div>
    );

  return (
    <div>
      <span className={campaignClass}>Campaign</span>
      {txt === "" ? <span></span> : <span>{txt}</span>}
    </div>
  );
};

const Item = ({ data, index }) => {
  const color = [
    "#FFB780",
    "#55ADFF",
    "#FFE165",
    "#7879F1",
    "#F65354",
    "#F178B6",
    "#35EBBD",
    "#FF8B34",
    "#12B7E2",
  ];

  const history = useHistory();
  const isMedia = data.media && data.media.length > 0 && data.media[0].url;

  const styles = isMedia
    ? {
        backgroundImage: `url(${getMediaUrl(data.media[0].url)})`,
        backgroundSize: "cover",
      }
    : { background: color[index % 9] };

  const bid = true;
  const campaign = data.campaign;
  let daysTxt = "";
  let campaignClass = "";
  if (campaign) {
    switch (campaign.status) {
      case "complete":
      case "Complete":
        campaignClass = "campaign-complete";
        break;
      case "inprogress":
      case "inProgress":
        campaignClass = "campaign-active";
        daysTxt = moment(campaign.timeEnd).fromNow();
        console.log("timeEnd", campaign.timeEnd, daysTxt);

        break;
      default:
        campaignClass = "campaign-unactive";
        daysTxt =
          "Start on " + moment(campaign.timeStart).format("MMM Do YYYY");

        break;
    }
  }

  return (
    <div
      className="products-page__all-product__item"
      onClick={() =>
        history.push({
          pathname: router.detailProduct.split(":")[0].concat(data.code),
        })
      }
    >
      <div className="products-page__all-product__item__header">
        <IconPencil />
        {moment(data.createdAt).fromNow()}
      </div>
      <div className="products-page__all-product__item__content">
        <div
          className="products-page__all-product__item__content__icon"
          style={styles}
        >
          {isMedia ? undefined : <IconBox />}
        </div>
        <div>
          <h4>{data.name}</h4>
          <p>{data.story} </p>
        </div>
      </div>
      <div className="products-page__all-product__item__footer">
        {getCampaignLabel(campaignClass, daysTxt)}
        {/* <span className={bid ? "bid-unactive" : "bid-active"}>Bid</span>
          {bid ? <span></span> : <span>30 days left</span>} */}

        <div>
          <span>
            <IconUsers /> {0 ? <div className="icon-user-active" /> : undefined}
          </span>
          <span>{data.totalSellers | 0}</span>
        </div>
      </div>
    </div>
  );
};

export default Item;
