import { constants, client } from "helpers";

import * as types from "./actionTypes";

const { api } = constants;
export const getListProduct = (params) => async (dispatch, getState) => {
  try {
    dispatch({ type: types.IS_FETCH_PRODUCTS, payload: true });

    const { page, statusJob } = params;
    const apiPath =
      statusJob === "new"
        ? api.path.listNewProduct
        : statusJob
        ? api.path.listProductWithStatus
        : api.path.listProduct;

    const { data } = await client.request({
      path: apiPath,
      method: "get",
      params: params,
    });
    if (data.data) {
      const { listProduct } = getState().products;
      const payload = page !== 0 ? listProduct.concat(data.data) : data.data;
      dispatch({
        type: types.FETCH_LIST_PRODUCT_SUCCESS,
        payload: payload,
      });
    }
  } catch (err) {
    console.log("[getListProduct]", err);
    dispatch({ type: types.FETCH_LIST_PRODUCT_FAILED, payload: err.message });
  } finally {
    dispatch({ type: types.IS_FETCH_PRODUCTS, payload: false });
  }
};

export const getListProductOffers = () => async (dispatch) => {
  try {
    dispatch({ type: types.IS_FETCH_PRODUCT_OFFERS, payload: true });

    const { data } = await client.request({
      path: api.path.listProductOffers,
      method: "get",
    });
    if (data) {
      dispatch({
        type: types.FETCH_LIST_PRODUCT_OFFERS_SUCCESS,
        payload: data,
      });
      return data;
    }
  } catch (err) {
    console.log("[getListProductOffers]", err);
    dispatch({
      type: types.FETCH_LIST_PRODUCT_OFFERS_FAILED,
      payload: err.message,
    });
  } finally {
    dispatch({ type: types.IS_FETCH_PRODUCT_OFFERS, payload: false });
  }
};

export const countJobsOfCompany = async (search = "") => {
  try {
    const { data } = await client.request({
      path: api.path.jobsOfCompanyCount(search),
      method: "get",
    });

    return data;
  } catch (err) {
    console.log("[countJobsOfCompany]", err);
  }
};
