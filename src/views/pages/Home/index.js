import React from "react";
import { connect } from "react-redux";
import Page from "views/components/Page";
import { constants } from "helpers";

const { userRoles, router } = constants;

const HomePage = ({ user, history }) => {
  if (user && user.role === userRoles.HOST) {
    history.push(router.myJobs);
  }
  if (user && user.role === userRoles.COMPANY) {
    history.push(router.products);
  }
  return (
    <Page helmet="Home Page" alignTop selectedKeys={["dashboard"]}>
      <div className="home-page"></div>
    </Page>
  );
};

const mapStateToProps = ({ common }) => ({
  user: common.user,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
