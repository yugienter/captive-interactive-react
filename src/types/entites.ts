export interface Entity {
  id: string;
}

export interface PageWrapper<T> {
  [x: string]: any;
  items: T[];
  total: number;
  pageSize: number;
}

export interface RequestWrapper {
  [x: string]: any;
  sortField: string;
  asc: boolean;
  filter: string;
  skip?: number;
  pageIndex: number;
  pageSize: number;
}
export interface Modified extends Entity {
  createdBy: string;
  updatedBy: string;
  createdDate: Date;
  updatedDate: Date;
}

export interface BaseType {}
