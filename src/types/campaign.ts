import { Entity } from "./entites";
import { IDealDto } from "./deal";

export interface ICampaignDto extends Entity {
  status: string;
  name: string;
  description?: string;
  timeStart: Date;
  timeEnd: Date;
}
export interface ICampaignDetailResponse extends Entity {
  data: {
    code: string;
    discountPrice: number;
    publicRRP: number;
    discountPercentageUnit: string;
    discountPercentage: number;
    discountPeriodStart: Date;
    discountPeriodEnd: Date;
    campaign: ICampaignDto;
  };
  dealData: IDealDto;
  listHost: IListHost[];
  countHost: number;
}

export interface IListHost {
  hostName: string;
  media: string;
}

export interface IStreamHourlyRateRange {
  negiable: boolean;
  start: number;
  end: number;
  unit: number;
}

export interface IStreamElement {
  description: string[];
}

export interface IStreamType {
  description: string;
}

export interface IStreamTime {
  negiable: boolean;
  description: Date;
}

export interface IStreamPlatform {
  description: string[];
}

export interface IStreamCommission {
  negiable: boolean;
  description: number;
}
