import {
  IStreamTime,
  IStreamType,
  IStreamElement,
  IStreamPlatform,
  IStreamHourlyRateRange,
  IStreamCommission,
} from "./campaign";

export interface IDealDto {
  code: string;
  streamTime: IStreamTime;
  streamType: IStreamType;
  streamElement: IStreamElement;
  streamPlatform: IStreamPlatform;
  streamHourlyRateRange: IStreamHourlyRateRange;
  note: string;
  streamCommission: IStreamCommission;
  streamCommissionUnit: string;
}
