import React from 'react';
import { Switch, Route } from 'react-router';
import routes from 'routes';
import store from 'redux/store';
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import config from 'config';

const { ENV, SENTRY_DSN } = config;


Sentry.init({
  dsn: SENTRY_DSN,
  environment: ENV,
  integrations: [new Integrations.BrowserTracing()],
  beforeSend(event, hint) {
    const { common } = store.getState();
    event.user_email = common.user.email;
    event.url_path = window.location.href;
    return event;
  },
});

function App() {
  return (
    <Switch>
      {routes.map(route => (
        <Route
          exact
          key={window.btoa(route.path)}
          path={route.path}
          render={props => (
            <React.Fragment>
              <route.component {...props} />
            </React.Fragment>
          )}
        />
      ))}
    </Switch>
  );
}

export default App;