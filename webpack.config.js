const PrefixWrap = require("postcss-prefixwrap");

module.exports = {
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    { loader: "css-loader", options: { importLoaders: 1 } },
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: [PrefixWrap(".template")],
                        },
                    },
                ],
            },
        ],
    },
    
};